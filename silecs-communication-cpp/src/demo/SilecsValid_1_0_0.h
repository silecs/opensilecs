/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/

#ifndef SILECSVALID_1_0_0_H_
#define SILECSVALID_1_0_0_H_

#include <silecs-communication/wrapper/Block.h>
#include <silecs-communication/wrapper/DeployUnit.h>
#include <silecs-communication/wrapper/Design.h>
#include <silecs-communication/wrapper/Device.h>

namespace SilecsValid_1_0_0
{

class Design;

class ReadBlock : public SilecsWrapper::Block
{
public:
    /*!
     * \brief readBlock constructor. It creates an empty block.
     */
    ReadBlock() :
                    SilecsWrapper::Block("readBlock"),
                    b1_ro(0),
                    b2_ro(0),
                    w1_ro(0),
                    b3_ro(0),
                    dw1_ro(0),
                    c1_ro(0),
                    c2_ro(0),
                    c3_ro(0),
                    di1_ro(0),
                    b6_ro(0)
    {
    }

    ~ReadBlock()
    {
    }

    /*!
     * \brief Get b1_ro register.
     * \return value.
     */
    uint8_t getB1_ro() const
    {
        return b1_ro;
    }

    /*!
     * \brief Set b1_ro register.
     * \param value to be set.
     */
    void setB1_ro(uint8_t value)
    {
        b1_ro = value;
    }

    /*!
     * \brief Get b2_ro register.
     * \return value.
     */
    uint8_t getB2_ro() const
    {
        return b2_ro;
    }

    /*!
     * \brief Set b2_ro register.
     * \param value to be set.
     */
    void setB2_ro(uint8_t value)
    {
        b2_ro = value;
    }

    /*!
     * \brief Get w1_ro register.
     * \return value.
     */
    uint16_t getW1_ro() const
    {
        return w1_ro;
    }

    /*!
     * \brief Set w1_ro register.
     * \param value to be set.
     */
    void setW1_ro(uint16_t value)
    {
        w1_ro = value;
    }

    /*!
     * \brief Get b3_ro register.
     * \return value.
     */
    uint8_t getB3_ro() const
    {
        return b3_ro;
    }

    /*!
     * \brief Set b3_ro register.
     * \param value to be set.
     */
    void setB3_ro(uint8_t value)
    {
        b3_ro = value;
    }

    /*!
     * \brief Set array b4_ro register.
     * \param value to be set.
     */
    void getB4_ro(uint8_t* value) const
    {
        memcpy(value, (void *) b4_ro, b4_roDim1);
    }

    /*!
     * \brief Set 2D array b4_ro register.
     * \param value to be set.
     */
    void setB4_ro(uint8_t* value) const
    {
        memcpy((void *) b4_ro, value, b4_roDim1);
    }

    /*!
     * \brief Get dw1_ro register.
     * \return value.
     */
    uint32_t getDw1_ro() const
    {
        return dw1_ro;
    }

    /*!
     * \brief Set dw1_ro register.
     * \param value to be set.
     */
    void setDw1_ro(uint32_t value)
    {
        dw1_ro = value;
    }

    /*!
     * \brief Set array i1_ro register.
     * \param value to be set.
     */
    void getI1_ro(int16_t* value) const
    {
        memcpy(value, (void *) i1_ro, i1_roDim1);
    }

    /*!
     * \brief Set 2D array i1_ro register.
     * \param value to be set.
     */
    void setI1_ro(int16_t* value) const
    {
        memcpy((void *) i1_ro, value, i1_roDim1);
    }

    /*!
     * \brief Get c1_ro register.
     * \return value.
     */
    int8_t getC1_ro() const
    {
        return c1_ro;
    }

    /*!
     * \brief Set c1_ro register.
     * \param value to be set.
     */
    void setC1_ro(int8_t value)
    {
        c1_ro = value;
    }

    /*!
     * \brief Get c2_ro register.
     * \return value.
     */
    int8_t getC2_ro() const
    {
        return c2_ro;
    }

    /*!
     * \brief Set c2_ro register.
     * \param value to be set.
     */
    void setC2_ro(int8_t value)
    {
        c2_ro = value;
    }

    /*!
     * \brief Get c3_ro register.
     * \return value.
     */
    int8_t getC3_ro() const
    {
        return c3_ro;
    }

    /*!
     * \brief Set c3_ro register.
     * \param value to be set.
     */
    void setC3_ro(int8_t value)
    {
        c3_ro = value;
    }

    /*!
     * \brief Get di1_ro register.
     * \return value.
     */
    int32_t getDi1_ro() const
    {
        return di1_ro;
    }

    /*!
     * \brief Set di1_ro register.
     * \param value to be set.
     */
    void setDi1_ro(int32_t value)
    {
        di1_ro = value;
    }

    /*!
     * \brief Set array r1_ro register.
     * \param value to be set.
     */
    void getR1_ro(float* value) const
    {
        memcpy(value, (void *) r1_ro, r1_roDim1);
    }

    /*!
     * \brief Set 2D array r1_ro register.
     * \param value to be set.
     */
    void setR1_ro(float* value) const
    {
        memcpy((void *) r1_ro, value, r1_roDim1);
    }

    /*!
     * \brief Set array b5_ro register.
     * \param value to be set.
     */
    void getB5_ro(uint8_t* value) const
    {
        memcpy(value, (void *) b5_ro, b5_roDim1);
    }

    /*!
     * \brief Set 2D array b5_ro register.
     * \param value to be set.
     */
    void setB5_ro(uint8_t* value) const
    {
        memcpy((void *) b5_ro, value, b5_roDim1);
    }

    /*!
     * \brief Get b6_ro register.
     * \return value.
     */
    uint8_t getB6_ro() const
    {
        return b6_ro;
    }

    /*!
     * \brief Set b6_ro register.
     * \param value to be set.
     */
    void setB6_ro(uint8_t value)
    {
        b6_ro = value;
    }

    /*!
     * \brief Get 2D array i2_ro register.
     * \param value buffer where the value will be stored.
     */
    void getI2_ro(int16_t* value) const
    {
        memcpy(value, static_cast<const void *>(i2_ro), i2_roDim1 * i2_roDim2);
    }

    /*!
     * \brief Get array i2_ro register.
     * \param value buffer where the value will be stored.
     */
    void setI2_ro(int16_t* value) const
    {
        memcpy((void *) i2_ro, value, i2_roDim1 * i2_roDim2);
    }

    /*!
     * \brief Get 2D array dw2_ro register.
     * \param value buffer where the value will be stored.
     */
    void getDw2_ro(uint32_t* value) const
    {
        memcpy(value, static_cast<const void *>(dw2_ro), dw2_roDim1 * dw2_roDim2);
    }

    /*!
     * \brief Get array dw2_ro register.
     * \param value buffer where the value will be stored.
     */
    void setDw2_ro(uint32_t* value) const
    {
        memcpy((void *) dw2_ro, value, dw2_roDim1 * dw2_roDim2);
    }

    /*!
     * \brief Get s1_ro register.
     * \return value.
     */
    const std::string& getS1_ro() const
    {
        return s1_ro;
    }

    /*!
     * \brief Set s1_ro register.
     * \param value to be set.
     */
    void setS1_ro(const std::string& value)
    {
        s1_ro = value;
    }

    /*!
     * \brief Get std::string s2_ro register.
     * \param value buffer where the value will be stored.
     */
    void getS2_ro(std::string* value) const
    {
        for (std::size_t i = 0; i <  s2_roDim1; i++)
        {
            value[i] =  s2_ro[i];
        }
    }

    /*!
     * \brief Set std::string s2_ro register.
     * \param value to be set.
     */
    void setS2_ro(const std::string* value)
    {
        for (std::size_t i = 0; i < s2_roDim1; i++)
        {
            s2_ro[i] = value[i];
        }
    }

    /*!
     * \brief Get s3_ro register.
     * \return value.
     */
    const std::string& getS3_ro() const
    {
        return s3_ro;
    }

    /*!
     * \brief Set s3_ro register.
     * \param value to be set.
     */
    void setS3_ro(const std::string& value)
    {
        s3_ro = value;
    }

    /*!
     * \brief Get s4_ro register.
     * \return value.
     */
    const std::string& getS4_ro() const
    {
        return s4_ro;
    }

    /*!
     * \brief Set s4_ro register.
     * \param value to be set.
     */
    void setS4_ro(const std::string& value)
    {
        s4_ro = value;
    }

    /*!
     * \brief Get 2D array r2_ro register.
     * \param value buffer where the value will be stored.
     */
    void getR2_ro(float* value) const
    {
        memcpy(value, static_cast<const void *>(r2_ro), r2_roDim1 * r2_roDim2);
    }

    /*!
     * \brief Get array r2_ro register.
     * \param value buffer where the value will be stored.
     */
    void setR2_ro(float* value) const
    {
        memcpy((void *) r2_ro, value, r2_roDim1 * r2_roDim2);
    }
    static const std::size_t b4_roDim1 = 3;
    static const std::size_t i1_roDim1 = 2;
    static const std::size_t r1_roDim1 = 3;
    static const std::size_t b5_roDim1 = 3;
    static const std::size_t i2_roDim2 = 5;
    static const std::size_t i2_roDim1 = 2;
    static const std::size_t dw2_roDim2 = 3;
    static const std::size_t dw2_roDim1 = 1;
    static const std::size_t s2_roDim1 = 3;
    static const std::size_t r2_roDim2 = 2;
    static const std::size_t r2_roDim1 = 1;
private:
    uint8_t b1_ro;
    uint8_t b2_ro;
    uint16_t w1_ro;
    uint8_t b3_ro;
    uint8_t b4_ro[b4_roDim1];
    uint32_t dw1_ro;
    int16_t i1_ro[i1_roDim1];
    int8_t c1_ro;
    int8_t c2_ro;
    int8_t c3_ro;
    int32_t di1_ro;
    float r1_ro[r1_roDim1];
    uint8_t b5_ro[b5_roDim1];
    uint8_t b6_ro;
    int16_t i2_ro[i2_roDim1][i2_roDim2];
    uint32_t dw2_ro[dw2_roDim1][dw2_roDim2];
    std::string s1_ro;
    std::string s2_ro[s2_roDim1];
    std::string s3_ro;
    std::string s4_ro;
    float r2_ro[r2_roDim1][r2_roDim2];

};

class WriteBlock : public SilecsWrapper::Block
{
public:
    /*!
     * \brief writeBlock constructor. It creates an empty block.
     */
    WriteBlock() :
                    SilecsWrapper::Block("writeBlock"),
                    b1_wo(0),
                    b2_wo(0),
                    w1_wo(0),
                    b3_wo(0),
                    dw1_wo(0),
                    c1_wo(0),
                    c2_wo(0),
                    c3_wo(0),
                    di1_wo(0),
                    b6_wo(0)
    {
    }

    ~WriteBlock()
    {
    }

    /*!
     * \brief Get b1_wo register.
     * \return value.
     */
    uint8_t getB1_wo() const
    {
        return b1_wo;
    }

    /*!
     * \brief Set b1_wo register.
     * \param value to be set.
     */
    void setB1_wo(uint8_t value)
    {
        b1_wo = value;
    }

    /*!
     * \brief Get b2_wo register.
     * \return value.
     */
    uint8_t getB2_wo() const
    {
        return b2_wo;
    }

    /*!
     * \brief Set b2_wo register.
     * \param value to be set.
     */
    void setB2_wo(uint8_t value)
    {
        b2_wo = value;
    }

    /*!
     * \brief Get w1_wo register.
     * \return value.
     */
    uint16_t getW1_wo() const
    {
        return w1_wo;
    }

    /*!
     * \brief Set w1_wo register.
     * \param value to be set.
     */
    void setW1_wo(uint16_t value)
    {
        w1_wo = value;
    }

    /*!
     * \brief Get b3_wo register.
     * \return value.
     */
    uint8_t getB3_wo() const
    {
        return b3_wo;
    }

    /*!
     * \brief Set b3_wo register.
     * \param value to be set.
     */
    void setB3_wo(uint8_t value)
    {
        b3_wo = value;
    }

    /*!
     * \brief Set array b4_wo register.
     * \param value to be set.
     */
    void getB4_wo(uint8_t* value) const
    {
        memcpy(value, (void *) b4_wo, b4_woDim1);
    }

    /*!
     * \brief Set 2D array b4_wo register.
     * \param value to be set.
     */
    void setB4_wo(uint8_t* value) const
    {
        memcpy((void *) b4_wo, value, b4_woDim1);
    }

    /*!
     * \brief Get dw1_wo register.
     * \return value.
     */
    uint32_t getDw1_wo() const
    {
        return dw1_wo;
    }

    /*!
     * \brief Set dw1_wo register.
     * \param value to be set.
     */
    void setDw1_wo(uint32_t value)
    {
        dw1_wo = value;
    }

    /*!
     * \brief Set array i1_wo register.
     * \param value to be set.
     */
    void getI1_wo(int16_t* value) const
    {
        memcpy(value, (void *) i1_wo, i1_woDim1);
    }

    /*!
     * \brief Set 2D array i1_wo register.
     * \param value to be set.
     */
    void setI1_wo(int16_t* value) const
    {
        memcpy((void *) i1_wo, value, i1_woDim1);
    }

    /*!
     * \brief Get c1_wo register.
     * \return value.
     */
    int8_t getC1_wo() const
    {
        return c1_wo;
    }

    /*!
     * \brief Set c1_wo register.
     * \param value to be set.
     */
    void setC1_wo(int8_t value)
    {
        c1_wo = value;
    }

    /*!
     * \brief Get c2_wo register.
     * \return value.
     */
    int8_t getC2_wo() const
    {
        return c2_wo;
    }

    /*!
     * \brief Set c2_wo register.
     * \param value to be set.
     */
    void setC2_wo(int8_t value)
    {
        c2_wo = value;
    }

    /*!
     * \brief Get c3_wo register.
     * \return value.
     */
    int8_t getC3_wo() const
    {
        return c3_wo;
    }

    /*!
     * \brief Set c3_wo register.
     * \param value to be set.
     */
    void setC3_wo(int8_t value)
    {
        c3_wo = value;
    }

    /*!
     * \brief Get di1_wo register.
     * \return value.
     */
    int32_t getDi1_wo() const
    {
        return di1_wo;
    }

    /*!
     * \brief Set di1_wo register.
     * \param value to be set.
     */
    void setDi1_wo(int32_t value)
    {
        di1_wo = value;
    }

    /*!
     * \brief Set array r1_wo register.
     * \param value to be set.
     */
    void getR1_wo(float* value) const
    {
        memcpy(value, (void *) r1_wo, r1_woDim1);
    }

    /*!
     * \brief Set 2D array r1_wo register.
     * \param value to be set.
     */
    void setR1_wo(float* value) const
    {
        memcpy((void *) r1_wo, value, r1_woDim1);
    }

    /*!
     * \brief Set array b5_wo register.
     * \param value to be set.
     */
    void getB5_wo(uint8_t* value) const
    {
        memcpy(value, (void *) b5_wo, b5_woDim1);
    }

    /*!
     * \brief Set 2D array b5_wo register.
     * \param value to be set.
     */
    void setB5_wo(uint8_t* value) const
    {
        memcpy((void *) b5_wo, value, b5_woDim1);
    }

    /*!
     * \brief Get b6_wo register.
     * \return value.
     */
    uint8_t getB6_wo() const
    {
        return b6_wo;
    }

    /*!
     * \brief Set b6_wo register.
     * \param value to be set.
     */
    void setB6_wo(uint8_t value)
    {
        b6_wo = value;
    }

    /*!
     * \brief Get 2D array i2_wo register.
     * \param value buffer where the value will be stored.
     */
    void getI2_wo(int16_t* value) const
    {
        memcpy(value, static_cast<const void *>(i2_wo), i2_woDim1 * i2_woDim2);
    }

    /*!
     * \brief Get array i2_wo register.
     * \param value buffer where the value will be stored.
     */
    void setI2_wo(int16_t* value) const
    {
        memcpy((void *) i2_wo, value, i2_woDim1 * i2_woDim2);
    }

    /*!
     * \brief Get 2D array dw2_wo register.
     * \param value buffer where the value will be stored.
     */
    void getDw2_wo(uint32_t* value) const
    {
        memcpy(value, static_cast<const void *>(dw2_wo), dw2_woDim1 * dw2_woDim2);
    }

    /*!
     * \brief Get array dw2_wo register.
     * \param value buffer where the value will be stored.
     */
    void setDw2_wo(uint32_t* value) const
    {
        memcpy((void *) dw2_wo, value, dw2_woDim1 * dw2_woDim2);
    }

    /*!
     * \brief Get s1_wo register.
     * \return value.
     */
    const std::string& getS1_wo() const
    {
        return s1_wo;
    }

    /*!
     * \brief Set s1_wo register.
     * \param value to be set.
     */
    void setS1_wo(const std::string& value)
    {
        s1_wo = value;
    }

    /*!
     * \brief Get std::string s2_wo register.
     * \param value buffer where the value will be stored.
     */
    void getS2_wo(std::string* value) const
    {
        for (std::size_t i = 0; i <  s2_woDim1; i++)
        {
            value[i] =  s2_wo[i];
        }
    }

    /*!
     * \brief Set std::string s2_wo register.
     * \param value to be set.
     */
    void setS2_wo(const std::string* value)
    {
        for (std::size_t i = 0; i < s2_woDim1; i++)
        {
            s2_wo[i] = value[i];
        }
    }

    /*!
     * \brief Get s3_wo register.
     * \return value.
     */
    const std::string& getS3_wo() const
    {
        return s3_wo;
    }

    /*!
     * \brief Set s3_wo register.
     * \param value to be set.
     */
    void setS3_wo(const std::string& value)
    {
        s3_wo = value;
    }

    /*!
     * \brief Get s4_wo register.
     * \return value.
     */
    const std::string& getS4_wo() const
    {
        return s4_wo;
    }

    /*!
     * \brief Set s4_wo register.
     * \param value to be set.
     */
    void setS4_wo(const std::string& value)
    {
        s4_wo = value;
    }

    /*!
     * \brief Get 2D array r2_wo register.
     * \param value buffer where the value will be stored.
     */
    void getR2_wo(float* value) const
    {
        memcpy(value, static_cast<const void *>(r2_wo), r2_woDim1 * r2_woDim2);
    }

    /*!
     * \brief Get array r2_wo register.
     * \param value buffer where the value will be stored.
     */
    void setR2_wo(float* value) const
    {
        memcpy((void *) r2_wo, value, r2_woDim1 * r2_woDim2);
    }
    static const std::size_t b4_woDim1 = 3;
    static const std::size_t i1_woDim1 = 2;
    static const std::size_t r1_woDim1 = 3;
    static const std::size_t b5_woDim1 = 3;
    static const std::size_t i2_woDim2 = 5;
    static const std::size_t i2_woDim1 = 2;
    static const std::size_t dw2_woDim2 = 3;
    static const std::size_t dw2_woDim1 = 1;
    static const std::size_t s2_woDim1 = 3;
    static const std::size_t r2_woDim2 = 2;
    static const std::size_t r2_woDim1 = 1;
private:
    uint8_t b1_wo;
    uint8_t b2_wo;
    uint16_t w1_wo;
    uint8_t b3_wo;
    uint8_t b4_wo[b4_woDim1];
    uint32_t dw1_wo;
    int16_t i1_wo[i1_woDim1];
    int8_t c1_wo;
    int8_t c2_wo;
    int8_t c3_wo;
    int32_t di1_wo;
    float r1_wo[r1_woDim1];
    uint8_t b5_wo[b5_woDim1];
    uint8_t b6_wo;
    int16_t i2_wo[i2_woDim1][i2_woDim2];
    uint32_t dw2_wo[dw2_woDim1][dw2_woDim2];
    std::string s1_wo;
    std::string s2_wo[s2_woDim1];
    std::string s3_wo;
    std::string s4_wo;
    float r2_wo[r2_woDim1][r2_woDim2];

};

class RwBlock : public SilecsWrapper::Block
{
public:
    /*!
     * \brief rwBlock constructor. It creates an empty block.
     */
    RwBlock() :
                    SilecsWrapper::Block("rwBlock"),
                    b1_rw(0),
                    b2_rw(0),
                    w1_rw(0),
                    b3_rw(0),
                    dw1_rw(0),
                    c1_rw(0),
                    c2_rw(0),
                    c3_rw(0),
                    di1_rw(0),
                    b6_rw(0)
    {
    }

    ~RwBlock()
    {
    }

    /*!
     * \brief Get b1_rw register.
     * \return value.
     */
    uint8_t getB1_rw() const
    {
        return b1_rw;
    }

    /*!
     * \brief Set b1_rw register.
     * \param value to be set.
     */
    void setB1_rw(uint8_t value)
    {
        b1_rw = value;
    }

    /*!
     * \brief Get b2_rw register.
     * \return value.
     */
    uint8_t getB2_rw() const
    {
        return b2_rw;
    }

    /*!
     * \brief Set b2_rw register.
     * \param value to be set.
     */
    void setB2_rw(uint8_t value)
    {
        b2_rw = value;
    }

    /*!
     * \brief Get w1_rw register.
     * \return value.
     */
    uint16_t getW1_rw() const
    {
        return w1_rw;
    }

    /*!
     * \brief Set w1_rw register.
     * \param value to be set.
     */
    void setW1_rw(uint16_t value)
    {
        w1_rw = value;
    }

    /*!
     * \brief Get b3_rw register.
     * \return value.
     */
    uint8_t getB3_rw() const
    {
        return b3_rw;
    }

    /*!
     * \brief Set b3_rw register.
     * \param value to be set.
     */
    void setB3_rw(uint8_t value)
    {
        b3_rw = value;
    }

    /*!
     * \brief Set array b4_rw register.
     * \param value to be set.
     */
    void getB4_rw(uint8_t* value) const
    {
        memcpy(value, (void *) b4_rw, b4_rwDim1);
    }

    /*!
     * \brief Set 2D array b4_rw register.
     * \param value to be set.
     */
    void setB4_rw(uint8_t* value) const
    {
        memcpy((void *) b4_rw, value, b4_rwDim1);
    }

    /*!
     * \brief Get dw1_rw register.
     * \return value.
     */
    uint32_t getDw1_rw() const
    {
        return dw1_rw;
    }

    /*!
     * \brief Set dw1_rw register.
     * \param value to be set.
     */
    void setDw1_rw(uint32_t value)
    {
        dw1_rw = value;
    }

    /*!
     * \brief Set array i1_rw register.
     * \param value to be set.
     */
    void getI1_rw(int16_t* value) const
    {
        memcpy(value, (void *) i1_rw, i1_rwDim1);
    }

    /*!
     * \brief Set 2D array i1_rw register.
     * \param value to be set.
     */
    void setI1_rw(int16_t* value) const
    {
        memcpy((void *) i1_rw, value, i1_rwDim1);
    }

    /*!
     * \brief Get c1_rw register.
     * \return value.
     */
    int8_t getC1_rw() const
    {
        return c1_rw;
    }

    /*!
     * \brief Set c1_rw register.
     * \param value to be set.
     */
    void setC1_rw(int8_t value)
    {
        c1_rw = value;
    }

    /*!
     * \brief Get c2_rw register.
     * \return value.
     */
    int8_t getC2_rw() const
    {
        return c2_rw;
    }

    /*!
     * \brief Set c2_rw register.
     * \param value to be set.
     */
    void setC2_rw(int8_t value)
    {
        c2_rw = value;
    }

    /*!
     * \brief Get c3_rw register.
     * \return value.
     */
    int8_t getC3_rw() const
    {
        return c3_rw;
    }

    /*!
     * \brief Set c3_rw register.
     * \param value to be set.
     */
    void setC3_rw(int8_t value)
    {
        c3_rw = value;
    }

    /*!
     * \brief Get di1_rw register.
     * \return value.
     */
    int32_t getDi1_rw() const
    {
        return di1_rw;
    }

    /*!
     * \brief Set di1_rw register.
     * \param value to be set.
     */
    void setDi1_rw(int32_t value)
    {
        di1_rw = value;
    }

    /*!
     * \brief Set array r1_rw register.
     * \param value to be set.
     */
    void getR1_rw(float* value) const
    {
        memcpy(value, (void *) r1_rw, r1_rwDim1);
    }

    /*!
     * \brief Set 2D array r1_rw register.
     * \param value to be set.
     */
    void setR1_rw(float* value) const
    {
        memcpy((void *) r1_rw, value, r1_rwDim1);
    }

    /*!
     * \brief Set array b5_rw register.
     * \param value to be set.
     */
    void getB5_rw(uint8_t* value) const
    {
        memcpy(value, (void *) b5_rw, b5_rwDim1);
    }

    /*!
     * \brief Set 2D array b5_rw register.
     * \param value to be set.
     */
    void setB5_rw(uint8_t* value) const
    {
        memcpy((void *) b5_rw, value, b5_rwDim1);
    }

    /*!
     * \brief Get b6_rw register.
     * \return value.
     */
    uint8_t getB6_rw() const
    {
        return b6_rw;
    }

    /*!
     * \brief Set b6_rw register.
     * \param value to be set.
     */
    void setB6_rw(uint8_t value)
    {
        b6_rw = value;
    }

    /*!
     * \brief Get 2D array i2_rw register.
     * \param value buffer where the value will be stored.
     */
    void getI2_rw(int16_t* value) const
    {
        memcpy(value, static_cast<const void *>(i2_rw), i2_rwDim1 * i2_rwDim2);
    }

    /*!
     * \brief Get array i2_rw register.
     * \param value buffer where the value will be stored.
     */
    void setI2_rw(int16_t* value) const
    {
        memcpy((void *) i2_rw, value, i2_rwDim1 * i2_rwDim2);
    }

    /*!
     * \brief Get 2D array dw2_rw register.
     * \param value buffer where the value will be stored.
     */
    void getDw2_rw(uint32_t* value) const
    {
        memcpy(value, static_cast<const void *>(dw2_rw), dw2_rwDim1 * dw2_rwDim2);
    }

    /*!
     * \brief Get array dw2_rw register.
     * \param value buffer where the value will be stored.
     */
    void setDw2_rw(uint32_t* value) const
    {
        memcpy((void *) dw2_rw, value, dw2_rwDim1 * dw2_rwDim2);
    }

    /*!
     * \brief Get s1_rw register.
     * \return value.
     */
    const std::string& getS1_rw() const
    {
        return s1_rw;
    }

    /*!
     * \brief Set s1_rw register.
     * \param value to be set.
     */
    void setS1_rw(const std::string& value)
    {
        s1_rw = value;
    }

    /*!
     * \brief Get std::string s2_rw register.
     * \param value buffer where the value will be stored.
     */
    void getS2_rw(std::string* value) const
    {
        for (std::size_t i = 0; i <  s2_rwDim1; i++)
        {
            value[i] =  s2_rw[i];
        }
    }

    /*!
     * \brief Set std::string s2_rw register.
     * \param value to be set.
     */
    void setS2_rw(const std::string* value)
    {
        for (std::size_t i = 0; i < s2_rwDim1; i++)
        {
            s2_rw[i] = value[i];
        }
    }

    /*!
     * \brief Get s3_rw register.
     * \return value.
     */
    const std::string& getS3_rw() const
    {
        return s3_rw;
    }

    /*!
     * \brief Set s3_rw register.
     * \param value to be set.
     */
    void setS3_rw(const std::string& value)
    {
        s3_rw = value;
    }

    /*!
     * \brief Get s4_rw register.
     * \return value.
     */
    const std::string& getS4_rw() const
    {
        return s4_rw;
    }

    /*!
     * \brief Set s4_rw register.
     * \param value to be set.
     */
    void setS4_rw(const std::string& value)
    {
        s4_rw = value;
    }

    /*!
     * \brief Get 2D array r2_rw register.
     * \param value buffer where the value will be stored.
     */
    void getR2_rw(float* value) const
    {
        memcpy(value, static_cast<const void *>(r2_rw), r2_rwDim1 * r2_rwDim2);
    }

    /*!
     * \brief Get array r2_rw register.
     * \param value buffer where the value will be stored.
     */
    void setR2_rw(float* value) const
    {
        memcpy((void *) r2_rw, value, r2_rwDim1 * r2_rwDim2);
    }
    static const std::size_t b4_rwDim1 = 3;
    static const std::size_t i1_rwDim1 = 2;
    static const std::size_t r1_rwDim1 = 3;
    static const std::size_t b5_rwDim1 = 3;
    static const std::size_t i2_rwDim2 = 5;
    static const std::size_t i2_rwDim1 = 2;
    static const std::size_t dw2_rwDim2 = 3;
    static const std::size_t dw2_rwDim1 = 1;
    static const std::size_t s2_rwDim1 = 3;
    static const std::size_t r2_rwDim2 = 2;
    static const std::size_t r2_rwDim1 = 1;
private:
    uint8_t b1_rw;
    uint8_t b2_rw;
    uint16_t w1_rw;
    uint8_t b3_rw;
    uint8_t b4_rw[b4_rwDim1];
    uint32_t dw1_rw;
    int16_t i1_rw[i1_rwDim1];
    int8_t c1_rw;
    int8_t c2_rw;
    int8_t c3_rw;
    int32_t di1_rw;
    float r1_rw[r1_rwDim1];
    uint8_t b5_rw[b5_rwDim1];
    uint8_t b6_rw;
    int16_t i2_rw[i2_rwDim1][i2_rwDim2];
    uint32_t dw2_rw[dw2_rwDim1][dw2_rwDim2];
    std::string s1_rw;
    std::string s2_rw[s2_rwDim1];
    std::string s3_rw;
    std::string s4_rw;
    float r2_rw[r2_rwDim1][r2_rwDim2];

};
class Device : public SilecsWrapper::Device
{
public:
    Device(const std::string& label, SilecsWrapper::Controller *controller) :
                    SilecsWrapper::Device(label, controller)
    {
    }

    ~Device()
    {
    }
    
    /*!
     * \brief Get readBlock block. 
     * \param block ReadBlock reference where to store returned value.
     */
    void getReadBlock(ReadBlock &block)
    {
        // Copy register b1_ro
        block.setB1_ro(getSilecsDevice()->getRegister("b1_ro")->getVal<uint8_t>());
        // Copy register b2_ro
        block.setB2_ro(getSilecsDevice()->getRegister("b2_ro")->getVal<uint8_t>());
        // Copy register w1_ro
        block.setW1_ro(getSilecsDevice()->getRegister("w1_ro")->getVal<uint16_t>());
        // Copy register b3_ro
        block.setB3_ro(getSilecsDevice()->getRegister("b3_ro")->getVal<uint8_t>());
        // Copy register b4_ro
        uint8_t *__b4_ro = new uint8_t[block.b4_roDim1];
        getSilecsDevice()->getRegister("b4_ro")->getValArray<uint8_t>(__b4_ro, block.b4_roDim1);
        block.setB4_ro(__b4_ro);
        delete[] __b4_ro;
        // Copy register dw1_ro
        block.setDw1_ro(getSilecsDevice()->getRegister("dw1_ro")->getVal<uint32_t>());
        // Copy register i1_ro
        int16_t *__i1_ro = new int16_t[block.i1_roDim1];
        getSilecsDevice()->getRegister("i1_ro")->getValArray<int16_t>(__i1_ro, block.i1_roDim1);
        block.setI1_ro(__i1_ro);
        delete[] __i1_ro;
        // Copy register c1_ro
        block.setC1_ro(getSilecsDevice()->getRegister("c1_ro")->getVal<int8_t>());
        // Copy register c2_ro
        block.setC2_ro(getSilecsDevice()->getRegister("c2_ro")->getVal<int8_t>());
        // Copy register c3_ro
        block.setC3_ro(getSilecsDevice()->getRegister("c3_ro")->getVal<int8_t>());
        // Copy register di1_ro
        block.setDi1_ro(getSilecsDevice()->getRegister("di1_ro")->getVal<int32_t>());
        // Copy register r1_ro
        float *__r1_ro = new float[block.r1_roDim1];
        getSilecsDevice()->getRegister("r1_ro")->getValArray<float>(__r1_ro, block.r1_roDim1);
        block.setR1_ro(__r1_ro);
        delete[] __r1_ro;
        // Copy register b5_ro
        uint8_t *__b5_ro = new uint8_t[block.b5_roDim1];
        getSilecsDevice()->getRegister("b5_ro")->getValArray<uint8_t>(__b5_ro, block.b5_roDim1);
        block.setB5_ro(__b5_ro);
        delete[] __b5_ro;
        // Copy register b6_ro
        block.setB6_ro(getSilecsDevice()->getRegister("b6_ro")->getVal<uint8_t>());
        // Copy register i2_ro
        int16_t *__i2_ro = new int16_t[block.i2_roDim1 * block.i2_roDim2];
        getSilecsDevice()->getRegister("i2_ro")->getValArray2D<int16_t>(__i2_ro, block.i2_roDim1, block.i2_roDim2);
        block.setI2_ro(__i2_ro);
        delete[] __i2_ro;
        // Copy register dw2_ro
        uint32_t *__dw2_ro = new uint32_t[block.dw2_roDim1 * block.dw2_roDim2];
        getSilecsDevice()->getRegister("dw2_ro")->getValArray2D<uint32_t>(__dw2_ro, block.dw2_roDim1, block.dw2_roDim2);
        block.setDw2_ro(__dw2_ro);
        delete[] __dw2_ro;
        // Copy register s1_ro
        block.setS1_ro(getSilecsDevice()->getRegister("s1_ro")->getVal<std::string>());
        // Copy register s2_ro
        std::string *__s2_ro = new std::string[block.s2_roDim1];
        getSilecsDevice()->getRegister("s2_ro")->getValArray<std::string>(__s2_ro, block.s2_roDim1);
        block.setS2_ro(__s2_ro);
        delete[] __s2_ro;
        // Copy register s3_ro
        block.setS3_ro(getSilecsDevice()->getRegister("s3_ro")->getVal<std::string>());
        // Copy register s4_ro
        block.setS4_ro(getSilecsDevice()->getRegister("s4_ro")->getVal<std::string>());
        // Copy register r2_ro
        float *__r2_ro = new float[block.r2_roDim1 * block.r2_roDim2];
        getSilecsDevice()->getRegister("r2_ro")->getValArray2D<float>(__r2_ro, block.r2_roDim1, block.r2_roDim2);
        block.setR2_ro(__r2_ro);
        delete[] __r2_ro;
    }
    
    /*!
     * \brief Set writeBlock block. 
     * \param block WriteBlock reference from where value are copied.
     */
    void setWriteBlock(WriteBlock &block)
    {
        // Copy register b1_wo
        getSilecsDevice()->getRegister("b1_wo")->setVal<uint8_t>(block.getB1_wo());
        // Copy register b2_wo
        getSilecsDevice()->getRegister("b2_wo")->setVal<uint8_t>(block.getB2_wo());
        // Copy register w1_wo
        getSilecsDevice()->getRegister("w1_wo")->setVal<uint16_t>(block.getW1_wo());
        // Copy register b3_wo
        getSilecsDevice()->getRegister("b3_wo")->setVal<uint8_t>(block.getB3_wo());
        // Copy register b4_wo
        uint8_t *__b4_wo = new uint8_t[block.b4_woDim1];
        block.getB4_wo(__b4_wo);
        getSilecsDevice()->getRegister("b4_wo")->setValArray<uint8_t>(__b4_wo, block.b4_woDim1);
        delete[] __b4_wo;
        // Copy register dw1_wo
        getSilecsDevice()->getRegister("dw1_wo")->setVal<uint32_t>(block.getDw1_wo());
        // Copy register i1_wo
        int16_t *__i1_wo = new int16_t[block.i1_woDim1];
        block.getI1_wo(__i1_wo);
        getSilecsDevice()->getRegister("i1_wo")->setValArray<int16_t>(__i1_wo, block.i1_woDim1);
        delete[] __i1_wo;
        // Copy register c1_wo
        getSilecsDevice()->getRegister("c1_wo")->setVal<int8_t>(block.getC1_wo());
        // Copy register c2_wo
        getSilecsDevice()->getRegister("c2_wo")->setVal<int8_t>(block.getC2_wo());
        // Copy register c3_wo
        getSilecsDevice()->getRegister("c3_wo")->setVal<int8_t>(block.getC3_wo());
        // Copy register di1_wo
        getSilecsDevice()->getRegister("di1_wo")->setVal<int32_t>(block.getDi1_wo());
        // Copy register r1_wo
        float *__r1_wo = new float[block.r1_woDim1];
        block.getR1_wo(__r1_wo);
        getSilecsDevice()->getRegister("r1_wo")->setValArray<float>(__r1_wo, block.r1_woDim1);
        delete[] __r1_wo;
        // Copy register b5_wo
        uint8_t *__b5_wo = new uint8_t[block.b5_woDim1];
        block.getB5_wo(__b5_wo);
        getSilecsDevice()->getRegister("b5_wo")->setValArray<uint8_t>(__b5_wo, block.b5_woDim1);
        delete[] __b5_wo;
        // Copy register b6_wo
        getSilecsDevice()->getRegister("b6_wo")->setVal<uint8_t>(block.getB6_wo());
        // Copy register i2_wo
        int16_t *__i2_wo = new int16_t[block.i2_woDim1 * block.i2_woDim2];
        block.getI2_wo(__i2_wo);
        getSilecsDevice()->getRegister("i2_wo")->setValArray2D<int16_t>(__i2_wo, block.i2_woDim1, block.i2_woDim2);
        delete[] __i2_wo;
        // Copy register dw2_wo
        uint32_t *__dw2_wo = new uint32_t[block.dw2_woDim1 * block.dw2_woDim2];
        block.getDw2_wo(__dw2_wo);
        getSilecsDevice()->getRegister("dw2_wo")->setValArray2D<uint32_t>(__dw2_wo, block.dw2_woDim1, block.dw2_woDim2);
        delete[] __dw2_wo;
        // Copy register s1_wo
        getSilecsDevice()->getRegister("s1_wo")->setVal<std::string>(block.getS1_wo());
        // Copy register s2_wo
        std::string *__s2_wo = new std::string[block.s2_woDim1];
        block.getS2_wo(__s2_wo);
        getSilecsDevice()->getRegister("s2_wo")->setValArray<std::string>(__s2_wo, block.s2_woDim1);
        delete[] __s2_wo;
        // Copy register s3_wo
        getSilecsDevice()->getRegister("s3_wo")->setVal<std::string>(block.getS3_wo());
        // Copy register s4_wo
        getSilecsDevice()->getRegister("s4_wo")->setVal<std::string>(block.getS4_wo());
        // Copy register r2_wo
        float *__r2_wo = new float[block.r2_woDim1 * block.r2_woDim2];
        block.getR2_wo(__r2_wo);
        getSilecsDevice()->getRegister("r2_wo")->setValArray2D<float>(__r2_wo, block.r2_woDim1, block.r2_woDim2);
        delete[] __r2_wo;
    }
    
    /*!
     * \brief Get rwBlock block. 
     * \param block RwBlock reference where to store returned value.
     */
    void getRwBlock(RwBlock &block)
    {
        // Copy register b1_rw
        block.setB1_rw(getSilecsDevice()->getRegister("b1_rw")->getVal<uint8_t>());
        // Copy register b2_rw
        block.setB2_rw(getSilecsDevice()->getRegister("b2_rw")->getVal<uint8_t>());
        // Copy register w1_rw
        block.setW1_rw(getSilecsDevice()->getRegister("w1_rw")->getVal<uint16_t>());
        // Copy register b3_rw
        block.setB3_rw(getSilecsDevice()->getRegister("b3_rw")->getVal<uint8_t>());
        // Copy register b4_rw
        uint8_t *__b4_rw = new uint8_t[block.b4_rwDim1];
        getSilecsDevice()->getRegister("b4_rw")->getValArray<uint8_t>(__b4_rw, block.b4_rwDim1);
        block.setB4_rw(__b4_rw);
        delete[] __b4_rw;
        // Copy register dw1_rw
        block.setDw1_rw(getSilecsDevice()->getRegister("dw1_rw")->getVal<uint32_t>());
        // Copy register i1_rw
        int16_t *__i1_rw = new int16_t[block.i1_rwDim1];
        getSilecsDevice()->getRegister("i1_rw")->getValArray<int16_t>(__i1_rw, block.i1_rwDim1);
        block.setI1_rw(__i1_rw);
        delete[] __i1_rw;
        // Copy register c1_rw
        block.setC1_rw(getSilecsDevice()->getRegister("c1_rw")->getVal<int8_t>());
        // Copy register c2_rw
        block.setC2_rw(getSilecsDevice()->getRegister("c2_rw")->getVal<int8_t>());
        // Copy register c3_rw
        block.setC3_rw(getSilecsDevice()->getRegister("c3_rw")->getVal<int8_t>());
        // Copy register di1_rw
        block.setDi1_rw(getSilecsDevice()->getRegister("di1_rw")->getVal<int32_t>());
        // Copy register r1_rw
        float *__r1_rw = new float[block.r1_rwDim1];
        getSilecsDevice()->getRegister("r1_rw")->getValArray<float>(__r1_rw, block.r1_rwDim1);
        block.setR1_rw(__r1_rw);
        delete[] __r1_rw;
        // Copy register b5_rw
        uint8_t *__b5_rw = new uint8_t[block.b5_rwDim1];
        getSilecsDevice()->getRegister("b5_rw")->getValArray<uint8_t>(__b5_rw, block.b5_rwDim1);
        block.setB5_rw(__b5_rw);
        delete[] __b5_rw;
        // Copy register b6_rw
        block.setB6_rw(getSilecsDevice()->getRegister("b6_rw")->getVal<uint8_t>());
        // Copy register i2_rw
        int16_t *__i2_rw = new int16_t[block.i2_rwDim1 * block.i2_rwDim2];
        getSilecsDevice()->getRegister("i2_rw")->getValArray2D<int16_t>(__i2_rw, block.i2_rwDim1, block.i2_rwDim2);
        block.setI2_rw(__i2_rw);
        delete[] __i2_rw;
        // Copy register dw2_rw
        uint32_t *__dw2_rw = new uint32_t[block.dw2_rwDim1 * block.dw2_rwDim2];
        getSilecsDevice()->getRegister("dw2_rw")->getValArray2D<uint32_t>(__dw2_rw, block.dw2_rwDim1, block.dw2_rwDim2);
        block.setDw2_rw(__dw2_rw);
        delete[] __dw2_rw;
        // Copy register s1_rw
        block.setS1_rw(getSilecsDevice()->getRegister("s1_rw")->getVal<std::string>());
        // Copy register s2_rw
        std::string *__s2_rw = new std::string[block.s2_rwDim1];
        getSilecsDevice()->getRegister("s2_rw")->getValArray<std::string>(__s2_rw, block.s2_rwDim1);
        block.setS2_rw(__s2_rw);
        delete[] __s2_rw;
        // Copy register s3_rw
        block.setS3_rw(getSilecsDevice()->getRegister("s3_rw")->getVal<std::string>());
        // Copy register s4_rw
        block.setS4_rw(getSilecsDevice()->getRegister("s4_rw")->getVal<std::string>());
        // Copy register r2_rw
        float *__r2_rw = new float[block.r2_rwDim1 * block.r2_rwDim2];
        getSilecsDevice()->getRegister("r2_rw")->getValArray2D<float>(__r2_rw, block.r2_rwDim1, block.r2_rwDim2);
        block.setR2_rw(__r2_rw);
        delete[] __r2_rw;
    }
    
    /*!
     * \brief Set rwBlock block. 
     * \param block RwBlock reference from where value are copied.
     */
    void setRwBlock(RwBlock &block)
    {
        // Copy register b1_rw
        getSilecsDevice()->getRegister("b1_rw")->setVal<uint8_t>(block.getB1_rw());
        // Copy register b2_rw
        getSilecsDevice()->getRegister("b2_rw")->setVal<uint8_t>(block.getB2_rw());
        // Copy register w1_rw
        getSilecsDevice()->getRegister("w1_rw")->setVal<uint16_t>(block.getW1_rw());
        // Copy register b3_rw
        getSilecsDevice()->getRegister("b3_rw")->setVal<uint8_t>(block.getB3_rw());
        // Copy register b4_rw
        uint8_t *__b4_rw = new uint8_t[block.b4_rwDim1];
        block.getB4_rw(__b4_rw);
        getSilecsDevice()->getRegister("b4_rw")->setValArray<uint8_t>(__b4_rw, block.b4_rwDim1);
        delete[] __b4_rw;
        // Copy register dw1_rw
        getSilecsDevice()->getRegister("dw1_rw")->setVal<uint32_t>(block.getDw1_rw());
        // Copy register i1_rw
        int16_t *__i1_rw = new int16_t[block.i1_rwDim1];
        block.getI1_rw(__i1_rw);
        getSilecsDevice()->getRegister("i1_rw")->setValArray<int16_t>(__i1_rw, block.i1_rwDim1);
        delete[] __i1_rw;
        // Copy register c1_rw
        getSilecsDevice()->getRegister("c1_rw")->setVal<int8_t>(block.getC1_rw());
        // Copy register c2_rw
        getSilecsDevice()->getRegister("c2_rw")->setVal<int8_t>(block.getC2_rw());
        // Copy register c3_rw
        getSilecsDevice()->getRegister("c3_rw")->setVal<int8_t>(block.getC3_rw());
        // Copy register di1_rw
        getSilecsDevice()->getRegister("di1_rw")->setVal<int32_t>(block.getDi1_rw());
        // Copy register r1_rw
        float *__r1_rw = new float[block.r1_rwDim1];
        block.getR1_rw(__r1_rw);
        getSilecsDevice()->getRegister("r1_rw")->setValArray<float>(__r1_rw, block.r1_rwDim1);
        delete[] __r1_rw;
        // Copy register b5_rw
        uint8_t *__b5_rw = new uint8_t[block.b5_rwDim1];
        block.getB5_rw(__b5_rw);
        getSilecsDevice()->getRegister("b5_rw")->setValArray<uint8_t>(__b5_rw, block.b5_rwDim1);
        delete[] __b5_rw;
        // Copy register b6_rw
        getSilecsDevice()->getRegister("b6_rw")->setVal<uint8_t>(block.getB6_rw());
        // Copy register i2_rw
        int16_t *__i2_rw = new int16_t[block.i2_rwDim1 * block.i2_rwDim2];
        block.getI2_rw(__i2_rw);
        getSilecsDevice()->getRegister("i2_rw")->setValArray2D<int16_t>(__i2_rw, block.i2_rwDim1, block.i2_rwDim2);
        delete[] __i2_rw;
        // Copy register dw2_rw
        uint32_t *__dw2_rw = new uint32_t[block.dw2_rwDim1 * block.dw2_rwDim2];
        block.getDw2_rw(__dw2_rw);
        getSilecsDevice()->getRegister("dw2_rw")->setValArray2D<uint32_t>(__dw2_rw, block.dw2_rwDim1, block.dw2_rwDim2);
        delete[] __dw2_rw;
        // Copy register s1_rw
        getSilecsDevice()->getRegister("s1_rw")->setVal<std::string>(block.getS1_rw());
        // Copy register s2_rw
        std::string *__s2_rw = new std::string[block.s2_rwDim1];
        block.getS2_rw(__s2_rw);
        getSilecsDevice()->getRegister("s2_rw")->setValArray<std::string>(__s2_rw, block.s2_rwDim1);
        delete[] __s2_rw;
        // Copy register s3_rw
        getSilecsDevice()->getRegister("s3_rw")->setVal<std::string>(block.getS3_rw());
        // Copy register s4_rw
        getSilecsDevice()->getRegister("s4_rw")->setVal<std::string>(block.getS4_rw());
        // Copy register r2_rw
        float *__r2_rw = new float[block.r2_rwDim1 * block.r2_rwDim2];
        block.getR2_rw(__r2_rw);
        getSilecsDevice()->getRegister("r2_rw")->setValArray2D<float>(__r2_rw, block.r2_rwDim1, block.r2_rwDim2);
        delete[] __r2_rw;
    }
    

    /*!
     * \brief Receive readBlock block for current device.
     */
    void receiveReadBlock()
    {
        _silecsDevice->recv("readBlock");
    }
    
    /*!
     * \brief Send writeBlock blocks to current device.
     */
    void sendWriteBlock()
    {
        _silecsDevice->send("writeBlock");
    }
    
    /*!
     * \brief Receive rwBlock block for current device.
     */
    void receiveRwBlock()
    {
        _silecsDevice->recv("rwBlock");
    }
    
    /*!
     * \brief Send rwBlock blocks to current device.
     */
    void sendRwBlock()
    {
        _silecsDevice->send("rwBlock");
    }
    

};

class Controller : public SilecsWrapper::Controller
{
public:
    Controller(const std::string& name, const std::string& domain, Design *design) :
                    SilecsWrapper::Controller(name, domain, (SilecsWrapper::Design*) design)
    {
            _deviceMap.insert(std::pair<std::string, Device*>("0", new Device("0", this)));
            _deviceMap.insert(std::pair<std::string, Device*>("1", new Device("1", this)));
            _deviceMap.insert(std::pair<std::string, Device*>("2", new Device("2", this)));
    }

    ~Controller()
    {
        map<std::string, Device*>::iterator it;
        for (it = _deviceMap.begin(); it != _deviceMap.end(); it++)
        {
            delete it->second;
        }
    }

    /*!
     * \brief Return pointer to the requested device.
     * \param label Device label.
     */
    Device* getDevice(const std::string& label)
    {
        if (_deviceMap.find(label) != _deviceMap.end())
        {
            return _deviceMap[label];
        }
        throw Silecs::SilecsException(__FILE__, __LINE__, Silecs::PARAM_UNKNOWN_DEVICE_NAME, label);
    }

    std::map<std::string, Device*>& getDeviceMap()
    {
        return _deviceMap;
    }
    /*!
     * \brief Receive readBlock blocks from all devices of current controller.
     */
    void receiveReadBlockAllDevices()
    {
        _silecsPLC->recv("readBlock");
    }
    
    /*!
     * \brief Send writeBlock blocks to all devices of current controller.
     */
    void sendWriteBlockAllDevices()
    {
        _silecsPLC->send("writeBlock");
    }
    
    /*!
     * \brief Receive rwBlock blocks from all devices of current controller.
     */
    void receiveRwBlockAllDevices()
    {
        _silecsPLC->recv("rwBlock");
    }
    
    /*!
     * \brief Send rwBlock blocks to all devices of current controller.
     */
    void sendRwBlockAllDevices()
    {
        _silecsPLC->send("rwBlock");
    }
    
private:
    std::map<std::string, Device*> _deviceMap;
};

class Design : public SilecsWrapper::Design
{
public:

    Design(SilecsWrapper::DeployUnit *deployUnit) :
                    SilecsWrapper::Design("SilecsValid", "1.0.0", deployUnit)
    {

            _controllerMap.insert(
                std::pair<std::string, Controller*>("cfp-774-csimatic-s7-02",
                        new Controller("cfp-774-csimatic-s7-02", "TST", this)));

    }

    ~Design()
    {
        map<std::string, Controller*>::iterator it;
        for (it = _controllerMap.begin(); it != _controllerMap.end(); it++)
        {
            delete it->second;
        }
    }

    /*!
     * \brief Return pointer to the requested controller.
     * \param name Controller's name.
     */
    Controller* getController(const std::string& name)
    {
        if (_controllerMap.find(name) != _controllerMap.end())
        {
            return _controllerMap[name];
        }
        throw Silecs::SilecsException(__FILE__, __LINE__, Silecs::PARAM_UNKNOWN_PLC_HOSTNAME, name);
    }

    /*!
     * \brief Return pointer to the controller cfp-774-csimatic-s7-02.
     */
    Controller* getCfp_774_csimatic_s7_02()
    {
        return _controllerMap["cfp-774-csimatic-s7-02"];
    }
    
    /*!
     * \brief Receive readBlock blocks from all connected controllers.
     */
    void receiveReadBlockAllControllers()
    {
        _silecsCluster->recv("readBlock");
    }
    
    /*!
     * \brief Send writeBlock blocks to all connected controllers.
     */
    void sendWriteBlockAllControllers()
    {
        _silecsCluster->send("writeBlock");
    }
    
    /*!
     * \brief Receive rwBlock blocks from all connected controllers.
     */
    void receiveRwBlockAllControllers()
    {
        _silecsCluster->recv("rwBlock");
    }
    
    /*!
     * \brief Send rwBlock blocks to all connected controllers.
     */
    void sendRwBlockAllControllers()
    {
        _silecsCluster->send("rwBlock");
    }
    
    std::map<std::string, Controller*>& getControllerMap()
    {
        return _controllerMap;
    }

private:
    std::map<std::string, Controller*> _controllerMap;

};

} /* namespace SilecsValid_1_0_0 */

#endif /* SILECSVALID_1_0_0_H_ */
