#include "Cfp774Simatic400_1_0_0.h"

#include <iostream>

int main(void)
{

    Cfp774Simatic400_1_0_0::DeployConfig config();
    config.setAutomaticConnect(false);
    const std::string logTopics = "ERROR,COMM,SEND,RECV";
    Cfp774Simatic400_1_0_0::DeployUnit* du = Cfp774Simatic400_1_0_0::DeployUnit::getInstance(logTopics, config);

    Cfp774Simatic400_1_0_0::DesignConfig designConfig(false);
    du->getSilecsValid()->setConfiguration(designConfig);

    du->deleteInstance();
}


//int main(void)
//{
//
//    Cfp774Simatic400_1_0_0::DeployConfig config;
//    config.setAutomaticConnect(false);
//    const std::string logTopics = "ERROR,COMM,SEND,RECV";
//    Cfp774Simatic400_1_0_0::DeployUnit* du = Cfp774Simatic400_1_0_0::DeployUnit::getInstance(logTopics, config);
//
//    Cfp774Simatic400_1_0_0::DesignConfig designConfig(false);
//    du->getSilecsValid()->setConfiguration(designConfig);
//    du->getSilecsValid()->getControllerMap()["cfp-774-csimatic-s7-02"]->connect();
//
//    // read block
//    SilecsValid_1_0_0::RwBlock rwblock;
//    du->getSilecsValid()->getCfp_774_csimatic_s7_02()->getDevice("0")->receiveRwBlock();
//    du->getSilecsValid()->getCfp_774_csimatic_s7_02()->getDevice("0")->getRwBlock(rwblock);
//
//    int32_t b1_rw = rwblock.getB1_rw();
//    uint8_t b4_rw[rwblock.b4_rwDim1];
//    rwblock.getB4_rw(b4_rw);
//    std::cout << "Value of register b1_rw = " << b1_rw << std::endl;
//    std::cout << "Value of register b4_rw[0] = " << (int32_t) b4_rw[0] << std::endl;
//    std::cout << "Value of register b4_rw[1] = " << (int32_t) b4_rw[1] << std::endl;
//
//    // increment b1_rw
//    b4_rw[0] = b4_rw[0] + 1;
//    b4_rw[1] = b4_rw[1] + 2;
//    rwblock.setB4_rw(b4_rw);
//    rwblock.setB1_rw(b1_rw + 2);
//    std::cout << "Increment 2" << std::endl;
//    du->getSilecsValid()->getCfp_774_csimatic_s7_02()->getDevice("0")->setRwBlock(rwblock);
//    du->getSilecsValid()->getCfp_774_csimatic_s7_02()->getDevice("0")->sendRwBlock();
//
//    // read b1_rw
//    du->getSilecsValid()->getCfp_774_csimatic_s7_02()->getDevice("0")->receiveRwBlock();
//    du->getSilecsValid()->getCfp_774_csimatic_s7_02()->getDevice("0")->getRwBlock(rwblock);
//    b1_rw = rwblock.getB1_rw();
//    rwblock.getB4_rw(b4_rw);
//    std::cout << "Value of register b1_rw = " << b1_rw << std::endl;
//    std::cout << "Value of register b4_rw[0] = " << (int32_t) b4_rw[0] << std::endl;
//    std::cout << "Value of register b4_rw[1] = " << (int32_t) b4_rw[1] << std::endl;
//
//    du->deleteInstance();
//}
