/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/wrapper/DeployUnit.h>
#include <silecs-communication/wrapper/Design.h>
#include <silecs-communication/interface/utility/SilecsException.h>

namespace SilecsWrapper
{

Design::Design(const std::string& name, const std::string& version, DeployUnit *deployUnit) :
                _name(name),
                _version(version),
                _deployUnit(deployUnit),
                _silecsPLCHandler(deployUnit->getService()->getPLCHandler())
{
}

Design::~Design()
{

}

const std::string& Design::getName() const
{
    return _name;
}

const std::string& Design::getVersion() const
{
    return _version;
}

Silecs::PLCHandler& Design::getSilecsPLCHandler() const
{
    return _silecsPLCHandler;
}

DeployUnit* Design::getDeployUnit() const
{
    return _deployUnit;
}

} //namespace SilecsWrapper
