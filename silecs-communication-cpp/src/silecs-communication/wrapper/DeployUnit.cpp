/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/wrapper/DeployUnit.h>

namespace SilecsWrapper
{

DeployConfig::DeployConfig() :
                _automaticConnect(true)
{
}

DeployConfig::DeployConfig(bool automaticConnect) :
                _automaticConnect(automaticConnect)
{
}

bool DeployConfig::getAutomaticConnect() const
{
    return _automaticConnect;
}

void DeployConfig::setAutomaticConnect(bool automaticConnect)
{
    _automaticConnect = automaticConnect;
}

//===============================================

DeployUnit* DeployUnit::_instance = NULL;
//SilecsWrapper::DeployConfig DeployUnit::_globalConfig = SilecsWrapper::DeployConfig();

DeployUnit::DeployUnit(const std::string& name, const std::string& version, const std::string& logTopics, const DeployConfig& globalConfig) :
                _name(name),
                _version(version),
                _globalConfig(globalConfig)
{
    // Process log topics
    char *argv[2] = {(char *)"-plcLog", const_cast<char*>(logTopics.c_str())};

    // Get singleton instance
    _silecsService = Silecs::Service::getInstance(2, argv);
}

void DeployUnit::deleteInstance()
{
    if (_instance != NULL)
    {
        delete _instance;
        _instance = NULL;
    }
}

const std::string& DeployUnit::getName() const
{
    return _name;
}

const std::string& DeployUnit::getVersion() const
{
    return _version;
}

Silecs::Service* DeployUnit::getService() const
{
    return _silecsService;
}

DeployConfig& DeployUnit::getGlobalConfig()
{
    return _globalConfig;
}

void DeployUnit::setGlobalConfig(DeployConfig& globalConfig)
{
    _globalConfig = globalConfig;
}

DeployUnit::~DeployUnit()
{
}

}
//namespace SilecsWrapper
