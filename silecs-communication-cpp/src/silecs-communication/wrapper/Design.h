/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef SILECSWRAPER_DESIGN_H_
#define SILECSWRAPER_DESIGN_H_

#include <silecs-communication/interface/equipment/SilecsPLCHandler.h>
#include <silecs-communication/wrapper/DeployUnit.h>

#include <string>

namespace SilecsWrapper
{
class DeployUnit;
class Controller;

class Design
{
public:
    Design(const std::string& name, const std::string& version, DeployUnit *deployUnit);

    const std::string& getName() const;

    const std::string& getVersion() const;

    Silecs::PLCHandler& getSilecsPLCHandler() const;

    DeployUnit* getDeployUnit() const;

    virtual ~Design();

protected:
    const std::string _name;
    const std::string _version;
    DeployUnit* _deployUnit;
    Silecs::PLCHandler& _silecsPLCHandler;
    const DeployConfig _config;

private:
    // Non copyable
    Design(const Design& other);
    Design& operator=(const Design&);
};

} //namespace SilecsWrapper

#endif
