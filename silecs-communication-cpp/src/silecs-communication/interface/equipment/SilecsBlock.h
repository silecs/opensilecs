/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _SILECS_BLOCK_H_
#define _SILECS_BLOCK_H_

#include <silecs-communication/interface/core/SilecsAction.h>
#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <silecs-communication/interface/equipment/SilecsRegister.h>

#include <memory>
#include <vector>
namespace Silecs
{
class SilecsDesign;
class ElementXML;

/*! -----------------------------------------------------------------------
 * \class Block
 * \brief This object maintains a reference of all the device registers.
 * It is responsible to serialize, de-serialize data for the network transfer
 * (alignment, data swapping, ..). It can be used also to trig the communication
 * for a particular device (make sense for DEVICE-MODE only) if needed.
 */
class Block
{
friend class SilecsDesign;
public:
    Block(const ElementXML& blockNode, Connection& plcConnection);
    virtual ~Block();

    virtual void receive() = 0;
    virtual void send() = 0;

    /*!
     * \fn whichAccessType
     * \return the enumeration value of the given access-type string
     */
    static AccessType whichAccessType(const std::string& type);
    static std::string whichAccessType(AccessType type);

    /*!
     * \fn whichAccessArea
     * \return the enumeration value of the given access-area string
     */
    static AccessArea whichAccessArea(std::string area);
    static std::string whichAccessArea(AccessArea area);

    bool isReadable() const
    {
        return (accessType_ != AccessType::Command);
    }
    bool isWritable() const
    {
        return (accessType_ != AccessType::Acquisition);
    }
    bool isConfiguration() const
    {
        return configuration_;
    }
    std::string getName() const
    {
        return name_;
    }
    AccessType getAccessType() const
    {
        return (accessType_);
    }
    AccessArea getAccessArea() const
    {
        return (accessArea_);
    }

    unsigned long getMemSize() const
    {
        return memSize_;
    }

    unsigned long getAddress() const
    {
        return address_;
    }

    /// @cond
    void setCustomAttributes(const unsigned long customAddress, const unsigned long customOffset, const unsigned long customSize);
    void resetCustomAttributes();
    bool withCustomAttributes() const
    {
        return customAttributes_;
    }
    /// @endcond

    /*!
     * \brief Copies input buffer in output buffer
     * The method copies the input buffer in the output buffer for each READ-WRITE register for the block passed as parameter
     * \param blockName name of the block for which the copy must be performed
     */
    void copyInToOut();

    const std::vector<std::unique_ptr<Register>>& getRegisters() const;

protected:

    /*!
     * \fn importRegisters
     * \brief Extract all the concerned registers data of that device from a given block and set the registers value.
     * \param pBlock: block which contain list of registers to be imported
     * \param pBuffer: direct pointer on the buffer which contain the registers data
     * \param ts is the time-of-day to time-stamp each register
     */
    void importRegisters(void* pBuffer, timeval ts);

    /*!
     * \fn exportRegisters
     * \brief Export all the concerned registers value of that device to a given block.
     * \param pBlock: block which contain list of registers to be exported
     * \param pBuffer: direct pointer on the buffer which will contain the registers data
     */
    void exportRegisters(void* pBuffer);

    Connection& plcConnection_;

    /// Block attributes
    std::string name_;

    /// Block access-type: Acquisition, Command or Setting
    AccessType accessType_;

    /// Block access-type: Memory, Digital or Analog
    AccessArea accessArea_;

    /// Block only consists of configuration registers (write once to controller at startup, afterwards only read from controller)
    bool configuration_;

    /// Block size
    unsigned long size_;

    /// Block memory size (including alignments)
    unsigned long memSize_;

    /* Block custom attributes
     * Can be used to limit dynamically the size of sent/received data.
     * Interesting for specific application. (e.g. Cryo) which design maximum size of the block and adjust
     * the real data size at runtime (using PLC::setCustomBlockAttributes() method.
     * !! This mechanism can be used only with device access mode (DEVICE-MODE or BLOCK-MODE from device)
     */
    // Flag used to enable/disable use of custom attributes
    bool customAttributes_;
    unsigned long customAddress_;
    unsigned long customOffset_;
    unsigned long customSize_;

    unsigned long address_;

    std::vector<std::unique_ptr<Register>> registers_;

    // Send/Receive data buffer
    void *pBuffer_;
};

} // namespace

#endif // _SILECS_BLOCK_H_
