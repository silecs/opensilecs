#ifndef _SILECS_PARAM_CONFIG_H_
#define _SILECS_PARAM_CONFIG_H_

#include <silecs-communication/interface/utility/Definitions.h>

#include <string>
#include <vector>

namespace Silecs
{

struct SilecsParamConfig
{
    std::string plcName;
    std::string localRelease; //Release number of the SILECS software
    std::string localOwner; //Owner of the deployment document
    std::string localDate; //Date of the Generation (Client parameters and PLC Sources)
    uint32_t localChecksum; //Checksum of the deployed configuration
    std::string domain; //TST, ADE, PSB, CPS, ..
    std::string brand; //SIEMENS, SCHNEIDER, ..
    std::string system; //STEP-7, UNITY, TWINCAT, ..
    std::string model; //SIMATIC S7-300, Premium, ..
    std::string protocolMode; //Device, Block
    std::string protocolType; //MODBUS-TCP, S7-TCP, ..

    long baseMemAddr;
    long baseDIAddr;
    long baseDOAddr;
    long baseAIAddr;
    long baseAOAddr;
    std::string usedMem;

    PLCType typeID; //BusController (with CPU), BusCoupler (pure IO)
    PLCBrand brandID; //Siemens, Schneider, ... enumeration
    PLCModel modelID; //S7-400, S7-300, ... enumeration
    PLCSystem systemID; //Step7, Unity, TwinCat, ... enumeration
    ProtocolMode protocolModeID; //BlockMode, DeviceMode enumeration
    ProtocolType protocolTypeID; //Modbus, S7 enumeration
};

} // namespace Silecs

#endif // _SILECS_PARAM_CONFIG_H_