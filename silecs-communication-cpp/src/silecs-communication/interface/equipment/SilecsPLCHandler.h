#ifndef _SILECS_PLCHANDLER_H_
#define _SILECS_PLCHANDLER_H_

#include <map>
#include <string>

namespace Silecs
{

class PLC;

/*!
 * \class PLCHandler
 * \brief Takes care of managing all the PLC objects. Provides a way to access the PLCs, in addition
 * it also provides some send and receive functionality that run in parallel on all PLCs.
 */
class PLCHandler
{
public:
    PLCHandler();

    /*!
     * \brief Add a new PLC to the handler. Either a hostname or an IP can be provided as the plcId.
     * If there is an existing PLC with the provided plcId an exception is thrown.
     * When parameterFile is not provided the default paths are searched.
     */
    PLC& addPLC(const std::string& plcId, const std::string& parameterFile="");
    
    /*!
     * \brief Get a PLC with the provided plcId. If the PLC doesn't yet exist, it will be added automatically.
     */
    PLC& getPLC(const std::string& plcId, const std::string& parameterFile="");

    /*!
     * \brief Search for existing PLC with the plcId.
     */
    bool isExistingPLC(const std::string& plcId) const;

    /*!
     * \brief Get hostname of this machine.
     */
    const std::string& getHostname() const;

    /*!
     * \brief Acquires one particular registers block of all devices of all enabled PLCs of that cluster.
     * The method tries to (re)connect each PLC if the connection is not established yet or unexpectedly interrupted (network failure, PLC down, etc.).
     * \param blockName name of the block to be acquired
     */
    void recv(const std::string& blockName);

    /*!
     * \brief Transmits the registers block to all devices of all enabled PLCs of that cluster.
     * The method tries to (re)connect each PLC if the connection is not established yet or unexpectedly interrupted (network failure, PLC down, etc.).
     * \param blockName name of the block to be transmitted
     */
    void send(const std::string& blockName);

    /*!
     * \brief Copies input buffer in output buffer
     * The method calls 'copyInToOut' on the * The method calls 'copyInToOut' on the underlying PLCs.
     * \param blockName name of the block for which the copy must be performed
     */
    void copyInToOut(const std::string& blockName);

private:

    void getHostNameByAddress(const std::string& ipAddress, std::string& hostName) const;
    void getAddressByHostName(const std::string& hostName, std::string& ipAddress) const;

    std::string fecHostName_;

    /* Map of plc hostnames and according PLC objects */
    std::map<std::string, PLC> plcMap_;
};

} // namespace

#endif // _SILECS_PLCHANDLER_H_
