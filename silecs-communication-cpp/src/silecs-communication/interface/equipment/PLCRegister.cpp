/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include "PLCRegister.h"
#include <silecs-communication/interface/communication/MBHardware.h>
#include <silecs-communication/interface/communication/SNAP7Hardware.h>
#include <silecs-communication/interface/communication/SNAP7Utils.h>

namespace Silecs
{

constexpr int kDtlSize = 12;

PLCRegister::PLCRegister(const ElementXML& registerNode, const SilecsParamConfig& paramConfig) :
                Register(registerNode),
                paramConfig_(paramConfig)
{
}
PLCRegister::~PLCRegister()
{
}

BigEndianRegister::BigEndianRegister(const ElementXML& registerNode, const SilecsParamConfig& paramConfig) :
                PLCRegister(registerNode, paramConfig)
{
}
BigEndianRegister::~BigEndianRegister()
{
}

LittleEndianRegister::LittleEndianRegister(const ElementXML& registerNode, const SilecsParamConfig& paramConfig) :
                PLCRegister(registerNode, paramConfig)
{
}
LittleEndianRegister::~LittleEndianRegister()
{
}

S7Register::S7Register(const ElementXML& registerNode, const SilecsParamConfig& paramConfig) :
                BigEndianRegister(registerNode, paramConfig)
{
}
S7Register::~S7Register()
{
}

UnityRegister::UnityRegister(const ElementXML& registerNode, const SilecsParamConfig& paramConfig) :
                LittleEndianRegister(registerNode, paramConfig)
{
}
UnityRegister::~UnityRegister()
{
}

TwinCATRegister::TwinCATRegister(const ElementXML& registerNode, const SilecsParamConfig& paramConfig) :
                UnityRegister(registerNode, paramConfig)
{
}
TwinCATRegister::~TwinCATRegister()
{
}

/// Generic methods for byte-swapping ----------------------------------------------
uint16_t PLCRegister::_swaps(uint16_t s)
{
    return static_cast<uint16_t>( ( (s & 0x00ff) << 8) + ( (s & 0xff00) >> 8));
}

uint32_t PLCRegister::_swapl(uint32_t l)
{
    uint32_t ul;
    void *pl = &l;
    void *pul = &ul;
    * ((uint16_t *)pul) = _swaps(* ((uint16_t *)pl + 1));
    * ((uint16_t *)pul + 1) = _swaps(* ((uint16_t *)pl));
    return (ul);
}

//Beckhoff/BC9020 PLC uses the same memory swapping than Unity PLCs except for float32 data
//This method is used to specialized the 32bits data swapping inherited from the UnityRegister class.
uint32_t TwinCATRegister::_swapl(uint32_t l)
{
    uint32_t ul;
    void *pl = &l;
    void *pul = &ul;

    if ( (paramConfig_.modelID == BC9xxx) && (getFormat() == Silecs::Float32))
    { //Particular case for BC9020 PLC:  swap only bytes together (not the words)
        * ((uint16_t *)pul) = _swaps(* ((uint16_t *)pl));
        * ((uint16_t *)pul + 1) = _swaps(* ((uint16_t *)pl + 1));
    }
    else
    { //Same as base-class methods for other controllers
        ul = PLCRegister::_swapl(l);
    }
    return (ul);
}

float PLCRegister::_swapf(float f)
{
    uint32_t l;
    void *pl = &l;
    void *pf = &f;
    l = _swapl(* ((uint32_t *)pf));
    return (* ((float *)pl));
}

double PLCRegister::_swapd(double d)
{
    uint64_t ld;
    void *pd = &d;
    void *pld = &ld;
    * ((uint32_t *)pld) = _swapl(* ((uint32_t *)pd + 1));
    * ((uint32_t *)pld + 1) = _swapl(* ((uint32_t *)pd));
    return (* ((double *)pld));
}

// BIG-ENDIANNESS  ---------------------------------------------------------------------------
void BigEndianRegister::swapBytesNToH(unsigned char *data, unsigned long size)
{
    switch (size)
    {
        case 1:
            //for CHAR and BYTE, nothing to do in that case.
            break;
        case 2:
        {
            //WORD or INT type
            uint16_t s = ntohs(*(uint16_t *)data);
            //no swap, just convert to network format
            *(uint16_t *)data = s;
            break;
        }
        case 4:
        {
            //DWORD, DINT or REAL type
            uint32_t l = ntohl(*(uint32_t *)data);
            //no swap, just convert to network format
            *(uint32_t *)data = l;
            break;
        }
#ifdef __x86_64__
        case 8:
        {
            //uInt64, Int64 or Float64
            //direct conversion from big-endian to host exists only from SLC6 version
            uint64_t l64 = be64toh(*(uint64_t * )data);
            *(uint64_t *)data = l64;
            break;
        }
#endif
        default:
            //nothing to do unknown type
            break;
    }
}

void BigEndianRegister::swapBytesHToN(unsigned char *data, unsigned long size)
{
    switch (size)
    {
        case 1:
            //for CHAR and BYTE, nothing to do in that case.
            break;
        case 2:
        {
            //WORD or INT type
            uint16_t s = htons(*(uint16_t *)data);
            //no swap, just convert to network format
            *(uint16_t *)data = s;
            break;
        }
        case 4:
        {
            //DWORD, DINT or REAL type
            uint32_t l = htonl(*(uint32_t *)data);
            //no swap, just convert to network format
            *(uint32_t *)data = l;
            break;
        }
#ifdef __x86_64__
        case 8:
        {
            //uInt64, Int64 or Float64
            //direct conversion from host to big-endian exists only from SLC6 version
            uint64_t l64 = htobe64(*(uint64_t * )data);
            *(uint64_t *)data = l64;
            break;
        }
#endif
        default:
            //nothing to do unknown type
            break;
    }
}

// LITTLE-ENDIANNESS  ---------------------------------------------------------------------------
void LittleEndianRegister::swapBytesNToH(unsigned char *data, unsigned long size)
{
    switch (size)
    {
        case 1:
            //for CHAR and BYTE, nothing to do in that case.
            break;
        case 2:
        {
            //WORD or INT type
            uint16_t s = ntohs(*(uint16_t *)data);
            //convert to network format and word swapping
            *(uint16_t *)data = _swaps(s);
            break;
        }
        case 4:
        {
            //DWORD, DINT or REAL type
            uint32_t l = ntohl(*(uint32_t *)data);
            //convert to network format and word swapping
            *(uint32_t *)data = _swapl(l);
            break;
        }
#ifdef __x86_64__
        case 8:
        {
            //uInt64, Int64 or Float64
            //direct conversion from little-endian to host exists only from SLC6 version
            uint64_t l64 = le64toh(*(uint64_t * )data);
            *(uint64_t *)data = l64;
            break;
        }
#endif
        default:
            //nothing to do unknown type
            break;
    }
}

void LittleEndianRegister::swapBytesHToN(unsigned char *data, unsigned long size)
{
    switch (size)
    {
        case 1:
            //for CHAR and BYTE, nothing to do in that case.
            break;
        case 2:
        {
            //WORD or INT type
            uint16_t s = htons(*(uint16_t *)data);
            //convert to network format and word swapping
            *(uint16_t *)data = _swaps(s);
            break;
        }
        case 4:
        {
            //DWORD, DINT or REAL type
            uint32_t l = htonl(*(uint32_t *)data);
            //convert to network format and word swapping
            *(uint32_t *)data = _swapl(l);
            break;
        }
#ifdef __x86_64__
        case 8:
        {
            //uInt64, Int64 or Float64
            //direct conversion from host to little-endian exists only from SLC6 version
            uint64_t l64 = htole64(*(uint64_t * )data);
            *(uint64_t *)data = l64;
            break;
        }
#endif
        default:
            //nothing to do unknown type
            break;
    }
}

// ----------------------------------------------------------------------------------------
void PLCRegister::importValue(void* pBuffer, timeval ts)
{
    std::lock_guard<std::mutex> lock(mutex_);
    //Compute the register address within the block: block-address + register-offset
    unsigned char* pDataBase = ((unsigned char*)pBuffer) + address_;
    unsigned char* pData = pDataBase;

    if ( (type_ == Int8) || (type_ == uInt8))
    {
        //Some PLC use more than 1 byte for this format (exp.: SCHNEIDER aligns char on 16bits)
        unsigned int step = static_cast<unsigned int>(memSize_ / dimension1_ / dimension2_ / size_);
        /*TODO:
         * Optimization a tester: utiliser un memcpy si (step == 1) au lieu du for
         * (idem pour l'export)
         */
        for (unsigned int i = 0; i < (dimension1_); i++)
        {
            for (unsigned int j = 0; j < (dimension2_); j++) // works also for 1d, in this case j is always 0 and points to the element on the array
            {
                // compact the array by removing the alignment added
                ((unsigned char*)pRecvValue_)[ (i * dimension2_) + j] = pData[ (i * step * dimension2_) + j];
            }
        }
    }

    else if ( type_ == Bool)
    {
        bool value = GetBitAt((void *)pBuffer, address_, bitNumber_);
        if (RECV & Log::topics_)
            LOG(RECV) << "Get bit: " << bitNumber_ << " of address " << address_ << ", value: " << value;
        * ((bool*)pRecvValue_) = value;
    }


    else
    {
        if (type_ != Date)
        { //Short, uShort, Long, uLong, Float
            for (unsigned int i = 0; i < (dimension1_ * dimension2_); i++)
            {
                swapBytesNToH(pData, size_);
                pData += size_;
            }

            //The buffer is correctly aligned and swapped now. We can transfer the value to the register cell.
            memcpy(pRecvValue_, (void*)pDataBase, (size_ * dimension1_ * dimension2_));
        }
        else
        {
            //Date&Time
            unsigned long dtSize = size_; //Most of controllers use 64bits coding for DT type
                                          //Beckhoff controller uses 32bits (see below)

            for (unsigned int i = 0; i < dimension1_; i++)
            {
                for (unsigned int j = 0; j < dimension2_; j++) // works also for 1d, in this case j is always 0 and points to the element on the array
                {
                    if (paramConfig_.brandID == Beckhoff)
                    { //Beckhoff controller implement the standard EPOCH on 32bits word
                        dtSize = sizeof(uint32_t);
                        uint32_t *timeVal = (uint32_t *)pData;
                        swapBytesNToH((unsigned char *)& (timeVal[0]), sizeof(uint32_t));
                        ((double *)pRecvValue_)[i * dimension2_ + j] = static_cast<double>(timeVal[0]);
                    }
                    else if (paramConfig_.protocolTypeID == S7Protocol)
                    {
                        if (dtSize == kDtlSize)
                        {
                            // DTL format
                            reinterpret_cast<double *>(pRecvValue_)[i * dimension2_ + j] = DtlGetTime(pData);
                        }
                        else
                        {
                            // DT format
                            reinterpret_cast<double *>(pRecvValue_)[i * dimension2_ + j] = IeRfcGetTime(pData);
                        }
                    }
                    else if (paramConfig_.protocolTypeID == MBProtocol)
                    {
#ifdef MODBUS_SUPPORT_ENABLED
                        ((double *)pRecvValue_)[i*dimension2_+j] = IeMdbGetTime(pData);
#endif //MODBUS_SUPPORT_ENABLED
                    }
                    else if (paramConfig_.protocolTypeID == CNVProtocol)
                    { //TODO for NI
                    }
                    else
                    { //should never occur!
                        throw SilecsException(__FILE__, __LINE__, UNEXPECTED_ERROR, "Controller protocol type undefined!");
                    }
                    pData += dtSize;
                }
            }
        }
    }

    //Set the register time-stamp
    tod_ = ts;

    //DATA topic makes sense with RECV one
    if (RECV & Log::topics_)
        LOG(DATA) << paramConfig_.plcName << "/" << getName() << ", len: " << getLength() << ", dim1: " << getDimension1() << ", dim2: " << getDimension2() << ", ts: " << getTimeStampAsString() << ", value: " << getValAsByteString(pRecvValue_, 10);
}

void PLCRegister::exportValue(void* pBuffer)
{
    std::lock_guard<std::mutex> lock(mutex_);
    //DATA topic makes sense with SEND one
    if (SEND & Log::topics_)
        LOG(DATA) << paramConfig_.plcName << "/" << getName() << ", len: " << getLength() << ", dim1: " << getDimension1() << ", dim2: " << getDimension2() << ", value: " << getValAsByteString(pSendValue_, 10);

    //Compute the register address within the block: block-address + register-offset
    unsigned char* pData = ((unsigned char*)pBuffer) + address_;

    if ( (type_ == Int8) || (type_ == uInt8))
    {
        //Some PLC use more than 1 byte for this format (exp.: SCHNEIDER aligns char on 16bits)
        unsigned int step = static_cast<unsigned int>(memSize_ / dimension1_ / dimension2_ / size_);

        for (unsigned int i = 0; i < dimension1_; i++)
        {
            for (unsigned int j = 0; j < (dimension2_); j++)
            {
                // expand the array by adding required space
                pData[ (i * step * dimension2_) + j] = ((unsigned char*)pSendValue_)[ (i * dimension2_) + j];
            }
        }
    }
    else if ( type_ == Bool)
    {
        bool value = *((bool*) pSendValue_);
        if (SEND & Log::topics_)
            LOG(SEND) << "Set bit: " << bitNumber_ << " of address " << address_ << ", value: "<< value;
        SetBitAt((void *)pBuffer, address_, bitNumber_, value);
    }

    else
    {
        if (type_ != Date)
        {
            //Short, uShort, Long, uLong, Float
            //Transfert the value of the register cell to the buffer
            memcpy((void*)pData, pSendValue_, (size_ * dimension1_ * dimension2_));

            for (unsigned int i = 0; i < (dimension1_ * dimension2_); i++)
            {
                swapBytesHToN(pData, size_);
                pData += size_;
            }
        }
        else
        { //Date&Time
            unsigned long dtSize = size_; //Most of controllers use 64bits coding for DT type
                                          //Beckhoff controller uses 32bits (see below)

            for (unsigned int i = 0; i < (dimension1_ * dimension2_); i++)
            {
                time_t epoch = static_cast<time_t>((double) ((double *)pSendValue_)[i]);

                if (paramConfig_.brandID == Beckhoff)
                { //Beckhoff controller implement the standard EPOCH on 32bits word
                    dtSize = sizeof(uint32_t);
                    uint32_t *timeVal = (uint32_t *)pData;
                    timeVal[0] = static_cast<uint32_t>(epoch);
                    swapBytesHToN((unsigned char *)& (timeVal[0]), sizeof(uint32_t));
                }
                else if (paramConfig_.protocolTypeID == S7Protocol)
                {
                    if (dtSize == kDtlSize)
                    {
                        // DTL format
                        DtlSetTime(pData, epoch);
                    }
                    else
                    {
                        // DT format
                        IeRfcSetTime(pData, epoch);
                    }
                }
                else if (paramConfig_.protocolTypeID == MBProtocol)
                {
#ifdef MODBUS_SUPPORT_ENABLED
                    IeMdbSetTime(pData, epoch);
#endif //MODBUS_SUPPORT_ENABLED
                }
                else if (paramConfig_.protocolTypeID == CNVProtocol)
                { //TODO for NI
                }
                else
                { //should never occur!
                    throw SilecsException(__FILE__, __LINE__, UNEXPECTED_ERROR, "Controller protocol type undefined!");
                }
                pData += dtSize;
            }
        }
    }
}

void PLCRegister::copyValue()
{
    std::lock_guard<std::mutex> lock(mutex_);
    //Transfer the value of the recvBuffer to to the sendBuffer
    if (type_ == String)
    {
        std::string** pRecvStringValue_ = static_cast<std::string**>(pRecvValue_);
        std::string** pSendStringValue_ = static_cast<std::string**>(pSendValue_);
        for (unsigned int i = 0; i < (dimension1_ * dimension2_); i++)
        {
            * (pSendStringValue_[i]) = * (pRecvStringValue_[i]);
        }
    }
    else
    {
        memcpy(pSendValue_, pRecvValue_, (size_ * dimension1_ * dimension2_));
    }
}

// S7 METHODS -------------------------------------------------------------------------------
void S7Register::importString(void* pBuffer, timeval ts)
{
    std::lock_guard<std::mutex> lock(mutex_);
    // Compute the register address within the block: block-address + register-offset
    unsigned char* pDataBase = ((unsigned char*)pBuffer) + address_;
    std::string** pRecvStringValue_ = static_cast<std::string**>(pRecvValue_);

    //Some PLC use more than 1 byte for this format (exp.: SIEMENS aligns string on 16bits address)
    unsigned int step = static_cast<unsigned int>(memSize_ / dimension1_ / dimension2_ / size_);

    // for each string of the single/double array -
    for (unsigned long i = 0; i < (dimension1_ * dimension2_); i++) // scalar has dim1=dim2=1
    {
        unsigned char* pData = & (pDataBase[i * step]);
        pRecvStringValue_[i]->assign((const char*)& (pData[2]), (size_t)pData[1]);
    }
    //Set the register time-stamp
    tod_ = ts;

    //DATA topic makes sense with RECV one
    if (RECV & Log::topics_)
        LOG(DATA) << paramConfig_.plcName << "/" << getName() << ", len: " << getLength() << ", dim1: " << getDimension1() << ", dim2: " << getDimension2() << ", ts: " << getTimeStampAsString() << ", value: " << getValAsByteString(pRecvValue_, 10);
}

void S7Register::exportString(void* pBuffer)
{
    std::lock_guard<std::mutex> lock(mutex_);
    //DATA topic makes sense with SEND one
    if (SEND & Log::topics_)
        LOG(DATA) << paramConfig_.plcName << "/" << getName() << ", len: " << getLength() << ", dim1: " << getDimension1() << ", dim2: " << getDimension2() << ", value: " << getValAsByteString(pSendValue_, 10);

    //Compute the register address within the block: block-address + register-offset
    unsigned char* pData = ((unsigned char*)pBuffer) + address_;
    std::string** pSendStringValue_ = static_cast<std::string**>(pSendValue_);

    //Some PLC use more than 1 byte for this format (exp.: SIEMENS aligns string on 16bits address)
    unsigned int step = static_cast<unsigned int>(memSize_ / dimension1_ / dimension2_ / size_);

    // for each string of the single/double array -
    for (unsigned long i = 0; i < dimension1_ * dimension2_; i++) // scalar has dim1=dim2=1
    { //Current size of the sent string should not exceed the register string max length
        std::string sendStringValue = * (pSendStringValue_[i]);
        if (sendStringValue.size() > length_)
            sendStringValue = sendStringValue.substr(0, length_);
        // S7 requires that the first two bytes of string are reserved for length and max length of string
        pData[ (i * step) + 0] = (unsigned char)length_ & 0XFF; // convert to byte (max length is 254 (+2) = 1 byte)
        pData[ (i * step) + 1] = (unsigned char)sendStringValue.size() & 0XFF;
        memcpy((void *)& (pData[ (i * step) + 2]), (void *)sendStringValue.c_str(), sendStringValue.size());
    }
}

// UNITY METHODS -----------------------------------------------------------------------------------
// TwinCAT object inherit from the same methods than UNITY.
void UnityRegister::importString(void* pBuffer, timeval ts)
{
    std::lock_guard<std::mutex> lock(mutex_);
    // Compute the register address within the block: block-address + register-offset
    unsigned char* pDataBase = ((unsigned char*)pBuffer) + address_;
    std::string** pRecvStringValue_ = static_cast<std::string**>(pRecvValue_);

    //Some PLC use more than 1 byte for this format (exp.: SCHNEIDER aligns char on 16bits)
    unsigned int step = static_cast<unsigned int>(memSize_ / dimension1_ / dimension2_ / size_);

    // for each string of the single/double array -
    for (unsigned long i = 0; i < (dimension1_ * dimension2_); i++) // scalar has dim1=dim2=1
    {
        unsigned char* pData = & (pDataBase[i * step]);
        size_t strSize = std::min((size_t)length_, strlen((const char*)pData));
        pRecvStringValue_[i]->assign((const char*)pData, strSize);
    }
    tod_ = ts;

    //DATA topic makes sense with RECV one
    if (RECV & Log::topics_)
        LOG(DATA) << paramConfig_.plcName << "/" << getName() << ", len: " << getLength() << ", dim1: " << getDimension1() << ", dim2: " << getDimension2() << ", ts: " << getTimeStampAsString() << ", value: " << getValAsByteString(pRecvValue_, 10);
}

void UnityRegister::exportString(void* pBuffer)
{
    std::lock_guard<std::mutex> lock(mutex_);
    //DATA topic makes sense with SEND one
    if (SEND & Log::topics_)
        LOG(DATA) << paramConfig_.plcName << "/" << getName() << ", len: " << getLength() << ", dim1: " << getDimension1() << ", dim2: " << getDimension2() << ", value: " << getValAsByteString(pSendValue_, 10);

    //Compute the register address within the block: block-address + register-offset
    unsigned char* pData = ((unsigned char*)pBuffer) + address_;
    std::string** pSendStringValue_ = static_cast<std::string**>(pSendValue_);

    // Some PLC use more than 1 byte for this format (exp.: SCHNEIDER aligns char on 16bits)
    unsigned int step = static_cast<unsigned int>(memSize_ / dimension1_ / dimension2_ / size_);

    // for each string of the single/double array -
    for (unsigned long i = 0; i < dimension1_ * dimension2_; i++) // scalar has dim1=dim2=1
    { //Current size of the sent string should not exceed the register string max length
        size_t strSize = pSendStringValue_[i]->size();
        std::string sendStringValue = pSendStringValue_[i]->substr(0, std::min((size_t)length_, strSize));
        bzero((void *)& (pData[i * step]), step); //TwinCAT string requires '\0' terminator (mem-size of the string (~step) has been properly dimensioned in the mapping).
        memcpy((void *)& (pData[i * step]), (void *)sendStringValue.c_str(), sendStringValue.size());
    }
}

} // namespace

