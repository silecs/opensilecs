/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _SILECS_DEVICE_H_
#define _SILECS_DEVICE_H_

#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <silecs-communication/interface/equipment/SilecsParamConfig.h>

#include <string>
#include <vector>
#include <map>
#include <memory>
#include <iostream>

namespace Silecs
{

/*!
 * \class Device
 * \brief This object maintains a reference of all the device registers.
 * It is responsible to serialize, de-serialize data for the network transfer (alignment, data swapping, ..).
 * It can be used to trig the data-block exchange for one particular device (make sense for DEVICE-MODE only).
 */
class Device
{
public:

    /*!
     * \brief Returns the label of the device as defined in the Deployment document.
     *  A device label is not visible from the supervision. It's the device identifier within
     *  the PLC/Class scope. If label has not been defined, the device is identified by its
     *  corresponding index [1..n] in the device collection.
     * \return label or index of the device as string
     */
    const std::string& getLabel() const;

    /*!
     * \brief Returns one register instance of the device by its name
     * \param registerName name of the register
     * \return Reference of the register object
     */
    const std::unique_ptr<Register>& getRegister(const std::string& registerName) const;

    /*!
     * \brief Acquires all blocks of the device.
     * The method tries to (re)connect the PLC if the connection is not established yet or unexpectedly interrupted (network failure, PLC down, etc.).
     * It generates an Silecs::SilecsException if an error is produced.
     */
    void recv();

    /*!
     * \brief Acquires one particular registers block of the device.
     * The method tries to (re)connect the PLC if the connection is not established yet or unexpectedly interrupted (network failure, PLC down, etc.).
     * It generates an Silecs::SilecsException if the related PLC has not been enabled (using the connect() method).
     * \param blockName name of the block to be acquired
     * \return 0 if operation was successful else an error code (when available).
     */
    void recv(const std::string& blockName);

    /*!
     * \brief Transmits the registers block to the device
     * The method tries to (re)connect the PLC if the connection is not established yet or unexpectedly interrupted (network failure, PLC down, etc.).
     * It generates an Silecs::SilecsException if the related PLC has not been enabled (using the connect() method).
     * \param blockName name of the block to be transmitted
     * \return 0 if operation was successful else an error code (when available).
     */
    void send(const std::string& blockName);

    /// @cond
    /*!
     * \return all registers of the device for a given block
     */
    const std::vector<std::unique_ptr<Register>>& getRegisterCollection(const std::string& blockName) const;

    /*!
     * \return String list (space separated value) of the Device registers
     */
    std::string getRegisterList() const;
    /// @endcond

    Device(const ElementXML& deviceNode, const std::vector<ElementXML>& blockNodes, Connection& plcConnection, const SilecsParamConfig& paramConfig);
    ~Device();

    Device(Device&& other) :
        label_(std::move(other.label_)),
        address_(other.address_),
        ai_address_(other.ai_address_),
        ao_address_(other.ao_address_),
        di_address_(other.di_address_),
        do_address_(other.do_address_),
        blocks_(std::move(other.blocks_))
    {
        other.address_ = -1;
        other.ai_address_ = -1;
        other.ao_address_ = -1;
        other.di_address_ = -1;
        other.do_address_ = -1;
    };

    const std::vector<std::unique_ptr<Block>>& getBlocks() const;

    /*!
     * \fn getBlock
     * \brief returns one instance of the requested block checking its access-type
     */
    const std::unique_ptr<Block>& getBlock(const std::string& blockName);

    long getInputAddress(AccessArea area)
    {
        if (area == AccessArea::Digital)
        {
            return di_address_;
        }
        else if (area == AccessArea::Analog)
        {
            return ai_address_;
        }
        return address_;
    }

    long getOutputAddress(AccessArea area)
    {
        if (area == AccessArea::Digital)
        {
            return do_address_;
        }
        else if (area == AccessArea::Analog)
        {
            return ao_address_;
        }
        return address_;
    }

    bool hasBlock(const std::string& blockName);

private:

    /// Device attributes
    std::string label_;
    long address_;
    long ai_address_;
    long ao_address_;
    long di_address_;
    long do_address_;

    SilecsParamConfig paramConfig_;

    std::vector<std::unique_ptr<Block>> blocks_;
};

} // namespace

#endif // _SILECS_DEVICE_H_
