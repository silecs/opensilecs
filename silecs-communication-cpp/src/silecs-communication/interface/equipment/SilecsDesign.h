#ifndef _SILECS_DESIGN_H_
#define _SILECS_DESIGN_H_

#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <silecs-communication/interface/equipment/SilecsParamConfig.h>

#include <string>
#include <vector>

namespace Silecs
{

class XMLParser;

class SilecsDesign
{
public:
    SilecsDesign(const XMLParser& xmlParser, const std::string& className, Connection& plcConnection, const SilecsParamConfig& paramConfig);

    /*!
     * \return Name of the Design.
     */
    const std::string& getName() const;

    /*!
     * \return List of the all Device instance names for the Design.
     */
    std::vector<std::string> getDeviceNameList() const;

    /*!
     * \brief If found, returns the device which has the specified name
     * \param deviceName name the device
     * \return Reference of the device object
     */
    Device* getDevice(const std::string& deviceName);

    /*!
     * \return List of the related Device instances for the Design.
     */
    const std::vector<Device>& getDevices() const;

    /*!
     * \brief Provides the list of Blocks defined in the corresponding Design.
     * \param select access-type of the blocks (Input, Output, InOut)
     * \return String list (space separated value) of Blocks name that have selected access-type
     */
    std::string getBlockList(AccessType accessType) const;

    /*!
     * \brief Copies input buffer to output buffer
     * The method calls 'copyInToOut' on the block matching the specified name on all devices
     * \param blockName name of the block for which the copy is performed
     */
    void copyInToOut(const std::string& blockName);

    /*!
     * \brief Receive all blocks of all devices.
     */
    void receiveForAllDevices();

    /*!
     * \brief Receive specified block of all devices.
     */
    void receiveForAllDevices(const std::string& blockName);

    /*!
     * \brief Send specified block of all devices.
     */
    void sendForAllDevices(const std::string& blockName);

    void receiveAll();

private:

    /*!
     * \brief returns instance of the first found block of the first device
     */
    const std::unique_ptr<Block>& getBlockFromFirstDevice(const std::string& blockName) const;

    void recvAllBlockMode(const std::string& blockName);
    void sendAllBlockMode(const std::string& blockName);

    /*!
     * \brief Used for detemining the required block buffer size in case of BLOCK_MODE. In BLOCK_MODE a single block contains a specific register for all the devices,
     * hence when calculating the required buffer size, we need to only reserve the space for the devices which actually contain this block.
     * Should either return 0 or the size of devices_.
     * \param blockName name of the block which should be used to count the devices
     */
    unsigned int getNrDevicesWithBlock(const std::string& blockName);

    std::vector<Device> devices_;
    std::string className_;

    Connection& plcConnection_;

    SilecsParamConfig paramConfig_;
};

} // namespace Silecs

#endif // _SILECS_DESIGN_H