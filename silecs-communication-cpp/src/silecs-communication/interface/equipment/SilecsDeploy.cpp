#include <silecs-communication/interface/equipment/SilecsDeploy.h>

#include <silecs-communication/interface/utility/XMLParser.h>

namespace Silecs
{

SilecsDeploy::SilecsDeploy(const std::string& paramFile, Connection& plcConnection, const SilecsParamConfig& paramConfig)
{
    XMLParser xmlParser(paramFile, true);
    ElementXML mappingNode = xmlParser.getSingleElementFromXPath("/SILECS-Param/SILECS-Mapping");
    auto& classNodes = mappingNode.getChildNodes();

    for (auto& classNode : classNodes)
    {
        std::string className = classNode.getAttribute("name");
        designs_.emplace_back(xmlParser, className, plcConnection, paramConfig);
    }
}

const Device& SilecsDeploy::getHeader() const
{
    for (auto& design : designs_)
    {
        for (auto& device : design.getDevices())
        {
            if (device.getLabel() == "SilecsHeader")
            {
                return device;
            }
        }
    }
    throw SilecsException(__FILE__, __LINE__, UNEXPECTED_ERROR, "Missing SilecsHeader.");
}

const std::vector<SilecsDesign>& SilecsDeploy::getDesigns() const
{
    return designs_;
}

SilecsDesign& SilecsDeploy::getDesign(const std::string& designName)
{
    for (auto& design : designs_)
    {
        if (design.getName() == designName)
        {
            return design;
        }
    }
    throw SilecsException(__FILE__, __LINE__, PARAM_UNKNOWN_DESIGN_NAME, designName);
}

void SilecsDeploy::withAllDesigns(const std::function<void(SilecsDesign&)>& callable)
{
    for (auto& design : designs_)
    {
        callable(design);
    }
}

void SilecsDeploy::withDesign(const std::string& designName, const std::function<void(SilecsDesign&)>& callable)
{
    for (auto& design : designs_)
    {
        if (design.getName() == designName)
        {
            callable(design);
            return;
        }
    }
    throw SilecsException(__FILE__, __LINE__, PARAM_UNKNOWN_DESIGN_NAME, designName);
}

} // namespace Silecs
