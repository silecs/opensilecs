#ifndef _SILECS_DEPLOY_H_
#define _SILECS_DEPLOY_H_

#include <silecs-communication/interface/equipment/SilecsDesign.h>
#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <silecs-communication/interface/equipment/SilecsParamConfig.h>

#include <functional>
#include <vector>
#include <string>

namespace Silecs
{

class SilecsDeploy
{
public:
    SilecsDeploy() = default;
    SilecsDeploy(const std::string& paramFile, Connection& plcConnection, const SilecsParamConfig& paramConfig);

    /*!
     * \return Design of the Silecs Header.
     */
    const Device& getHeader() const;

    /*!
     * \return List of the all available designs in this deployment.
     */
    const std::vector<SilecsDesign>& getDesigns() const;

    /*!
     * \brief Get design with specific name.
     */
    SilecsDesign& getDesign(const std::string& designName);

    /*!
     * \brief Execute a specific action on all the designs of this deployment.
     */
    void withAllDesigns(const std::function<void(SilecsDesign&)>& callable);

    /*!
     * \brief Execute a specific action on the specified design of this deployment.
     */
    void withDesign(const std::string& designName, const std::function<void(SilecsDesign&)>& callable);

private:
    std::vector<SilecsDesign> designs_;
};

} // namespace Silecs

#endif // _SILECS_DEPLOY_H_