/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifdef NI_SUPPORT_ENABLED
#ifndef _SILECS_CNV_BLOCK_H_
#define _SILECS_CNV_BLOCK_H_

#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>

#ifdef __cplusplus
extern "C"
{
#endif

#include <nilibnetv.h>

#ifdef __cplusplus
}
#endif

namespace Silecs
{

    class CNVBlock : public Block
    {

    public:
        CNVBlock(PLC* thePLC, ElementXML blockNode, AccessType accessType);
        ~CNVBlock();

    protected:
        // Check for CNV errors, log and create an exception
        void errChk(int code);

        friend class Device;
        CNVData buffer_;
    };

    /*! -----------------------------------------------------------------------
     * \class InputBlock
     * \brief This is a specific Block for Acquisition data (from NI to client)
     */
    class CNVInputBlock : public CNVBlock
    {

    public:
        CNVInputBlock(PLC* thePLC, ElementXML blockNode, AccessType accessType);
        virtual ~CNVInputBlock();

        /*!
         * \brief Creates subscription
         * Allocates memory for the subscriber, subscribes.
         * if subscription was already in place does nothing.
         * \return True if subscription (all variables) is successful.
         */
        bool doSubscribe();

        /*!
         * \brief Delete subscription
         * Free memory for the subscriber, and unsubscribe.
         */
        void unSubscribe();

        /*!
         * \brief Get buffer Handler
         * Return a pointer to the buffer for current block / required device
         * \param deviceName device name as a string
         * \return pointer to the handler buffer
         */
        CNVBufferedSubscriber* getHandle(std::string deviceName);

    private:
        // Handler
        CNVBufferedSubscriber *handle_;
        // Flag to avoid creating multiple buffer subscriber to the same block
        bool subscriptionFlag_;
    };

    /*! -----------------------------------------------------------------------
     * \class OutputBlock
     * \brief This is a specific Block for Settings (from client to NI)
     */
    class CNVOutputBlock : public CNVBlock
    {

    public:
        CNVOutputBlock(PLC* thePLC, ElementXML blockNode, AccessType accessType);
        virtual ~CNVOutputBlock();
    private:
        void createDataValue(Silecs::Register* regRef, CNVData* builderRef);
    };

} // namespace

#endif // _SILECS_BLOCK_H_
#endif //NI_SUPPORT_ENABLED
