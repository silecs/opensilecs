/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/

#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/equipment/SilecsRegister.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>

#include <arpa/inet.h>
#include <math.h>

namespace Silecs
{

Register::Register(const ElementXML& registerNode) :
                isConfiguration_(false)
{
    name_ = registerNode.getAttribute("name");
    length_ = 1;
    dimension1_ = 1;
    dimension2_ = 1;
    const std::vector<ElementXML>& childNodes = registerNode.getChildNodes();
    for (auto childIter = childNodes.begin(); childIter != childNodes.end(); childIter++)
    {
        // We have to loop, since there is as well the description-element
        
        if (childIter->hasAttribute("custom-type-name-ref"))
        {
            if (!childIter->hasAttribute("type"))
            {
                throw SilecsException(__FILE__, __LINE__, "Register '" + name_+ "' - Can't detect the type of the custom-type.");
            }

            if (childIter->getAttribute("type") == "enum")
            {
                // Enum type.
                type_ = Int32;
                typeString_ = "int32";
            }
            else
            {
                throw SilecsException(__FILE__, __LINE__, "Register '" + name_+ "' - Custom types other than 'enum' are not yet supported.");
            }
        }
        else
        {
            type_ = whichFormatType(childIter->getAttribute("format"));
            typeString_ = childIter->getAttribute("format");
        }
        StringUtilities::toLower(typeString_);

        if (childIter->hasAttribute("dim"))
            StringUtilities::fromString(dimension1_, childIter->getAttribute("dim")); // dim is used for 1d-arrays
        if (childIter->hasAttribute("dim1"))
            StringUtilities::fromString(dimension1_, childIter->getAttribute("dim1"));
        if (childIter->hasAttribute("dim2"))
            StringUtilities::fromString(dimension2_, childIter->getAttribute("dim2"));
        if (childIter->hasAttribute("string-length"))
            StringUtilities::fromString(length_, childIter->getAttribute("string-length"));
    }
    StringUtilities::fromString(size_, registerNode.getAttribute("size"));
    StringUtilities::fromString(memSize_, registerNode.getAttribute("mem-size"));
    StringUtilities::fromString(address_, registerNode.getAttribute("address"));
    if (registerNode.hasAttribute("bit-number"))
        StringUtilities::fromString(bitNumber_, registerNode.getAttribute("bit-number"));


    if(registerNode.getName() == "Configuration-Register")
        isConfiguration_ = true;

    isInitialized_ = false; //register not initialized yet (used for output registers)

    if (length_ > 1)
    {
        LOG(DEBUG) << "Register (create): " << name_ << ", format: " << getFormatAsString() << ", string-length: " << length_ << ", dim: " << dimension1_ << ", dim2: " << dimension2_ << ", offset: " << address_;
    }
    else
    {
        LOG(DEBUG) << "Register (create): " << name_ << ", format: " << getFormatAsString() << ", dim: " << dimension1_ << ", dim2: " << dimension2_ << ", offset: " << address_;
    }
}

Register::~Register()
{
    if (DEBUG & Log::topics_)
        LOG(ALLOC) << "Register (delete): " << name_;

    //Remove the value buffers
    if (pRecvValue_ != NULL)
    {
        if (type_ == String)
        {
            // loop through array of pointers and delete them
            std::string** pRecvStringValue = static_cast<std::string**>(pRecvValue_);
            for (unsigned int i = 0; i < dimension1_ * dimension2_; i++)
            {
                delete pRecvStringValue[i];
            }
            pRecvStringValue = NULL;
        }
        free(pRecvValue_);
        pRecvValue_ = NULL;
    }

    if (pSendValue_ != NULL)
    {
        if (type_ == String)
        {
            // loop through array of pointers and delete them
            std::string** pSendStringValue = static_cast<std::string**>(pSendValue_);
            for (unsigned int i = 0; i < dimension1_ * dimension2_; i++)
            {
                delete pSendStringValue[i];
            }
            pSendStringValue = NULL;
        }
        free(pSendValue_);
        pSendValue_ = NULL;
    }
}

// SET methods =============================================================

void Register::setAccessArea(AccessArea accessArea)
{
    accessArea_ = accessArea;
}

void Register::setAccessType(AccessType accessType)
{
    accessType_ = accessType;

    std::lock_guard<std::mutex> lock(mutex_);
    // By knowing the register type we can allocate 1 or 2 buffers for data exchanges.
    pRecvValue_ = pSendValue_ = NULL;

    if (accessType == AccessType::Acquisition || accessType == AccessType::Setting)
    {
        if (type_ == String)
        {
            pRecvValue_ = calloc(dimension1_ * dimension2_, sizeof(void*));
            std::string** pRecvStringValue = static_cast<std::string**>(pRecvValue_);

            // allocate memory for the required pointers to the strings
            for (unsigned long i = 0; i < dimension1_ * dimension2_; i++)
            {
                pRecvStringValue[i] = new std::string();
            }
        }
        else // If not string register just allocate space for data
        {
            pRecvValue_ = calloc(dimension1_ * dimension2_, size_);
        }
    }

    if (accessType == AccessType::Command || accessType == AccessType::Setting)
    {
        if (type_ == String)
        {
            pSendValue_ = calloc(dimension1_ * dimension2_, sizeof(void*));
            std::string** pSendStringValue = static_cast<std::string**>(pSendValue_);

            //allocate memory for the required pointers to the strings
            for (unsigned long i = 0; i < dimension1_ * dimension2_; i++)
            { // allocate space for the string object
                pSendStringValue[i] = new std::string();
            }
        }
        else // if not string register just allocate space for data
        {
            pSendValue_ = calloc(dimension1_ * dimension2_, size_);
        }
    }
}


// .........................................................................
std::string Register::getName()
{
    return name_;
}
AccessArea Register::getAccessArea()
{
    return accessArea_;
}
unsigned long Register::getDimension(uint16_t whichDim)
{
    return ( (whichDim == 2) ? dimension2_ : dimension1_);
}
uint32_t Register::getDimension1()
{
    return dimension1_;
}
uint32_t Register::getDimension2()
{
    return dimension2_;
}
uint32_t Register::getLength()
{
    return static_cast<uint32_t>(length_);
}
;
FormatType Register::getFormat()
{
    return type_;
}
std::string Register::getBlockName()
{
    return blockName_;
}
std::string Register::getFormatAsString()
{
    return FormatTypeString[type_];
}

AccessType Register::getAccessType() const
{
    return accessType_;
}

bool Register::isReadable()
{
    return (accessType_ != AccessType::Command);
}
bool Register::isWritable()
{
    return (accessType_ != AccessType::Acquisition);
}
bool Register::isIORegister()
{
    return (accessArea_ == AccessArea::Digital || accessArea_ == AccessArea::Analog);
}
bool Register::isDIRegister()
{
    return (accessArea_ == AccessArea::Digital && accessType_ == AccessType::Acquisition);
}
bool Register::isDORegister()
{
    return (accessArea_ == AccessArea::Digital && accessType_ == AccessType::Command);
}
bool Register::isAIRegister()
{
    return (accessArea_ == AccessArea::Analog && accessType_ == AccessType::Acquisition);
}
bool Register::isAORegister()
{
    return (accessArea_ == AccessArea::Analog && accessType_ == AccessType::Command);
}
bool Register::isMemoryRegister()
{
    return (accessArea_ == AccessArea::Memory);
}
bool Register::isConfiguration()
{
    return isConfiguration_;
}
bool Register::isScalar()
{
    return ( (dimension1_ == 1) && (dimension2_ == 1));
}
bool Register::isSingleArray()
{
    return ( (dimension1_ > 1) && (dimension2_ == 1));
}
bool Register::isDoubleArray()
{
    return (dimension2_ > 1);
}
bool Register::isInitialized()
{
    return isInitialized_;
}

std::string Register::whichAccessArea(AccessArea area)
{
    return Block::whichAccessArea(area);
}

FormatType Register::whichFormatType(std::string type)
{
    StringUtilities::toLower(type);
#ifndef __x86_64__
    //SLC5 (32bits platform) still uses deprecated types ========
    if (type == "byte") return uChar;
    else if (type == "int") return Short;
    else if (type == "word") return uShort;
    else if (type == "dint") return Long;
    else if (type == "dword") return uLong;
    else if (type == "real") return Float;
#else
    //SLC6 (64bits platform) ====================================
    if (type == "byte")
        return uInt8;
    else if (type == "int")
        return Int16;
    else if (type == "word")
        return uInt16;
    else if (type == "dint")
        return Int32;
    else if (type == "dword")
        return uInt32;
    else if (type == "real")
        return Float32;
#endif
    // ANY platform =============================================
    if (type == "char")
        return Char;

    else if (type == "dt")
        return Date;
    else if (type == "dtl")
        return Date;
    else if (type == "date")
        return Date;

    else if (type == "int8")
        return Int8;
    else if (type == "uint8")
        return uInt8;
    else if (type == "int16")
        return Int16;
    else if (type == "uint16")
        return uInt16;
    else if (type == "int32")
        return Int32;
    else if (type == "uint32")
        return uInt32;
    else if (type == "int64")
        return Int64;
    else if (type == "uint64")
        return uInt64;
    else if (type == "float32")
        return Float32;
    else if (type == "float64")
        return Float64;
    else if (type == "string")
        return String;
    else if (type == "bool")
        return Bool;
    else
        throw SilecsException(__FILE__, __LINE__, DATA_UNKNOWN_FORMAT_TYPE, type);
}

std::string Register::getValAsByteString(void* pValue, unsigned long maxSize)
{
    std::ostringstream os;
    std::string** pStringValue = static_cast<std::string**>(pValue);
    unsigned long nbElement = dimension1_ * dimension2_;
    unsigned long byteSize = nbElement * size_;
    unsigned long nbLoop = (getFormat() == String) ? nbElement : byteSize;

    for (unsigned int i = 0; i < nbLoop; i++)
    {
        if ( (i == maxSize) && (nbLoop > 2 * maxSize))
        {
            i = static_cast<unsigned int>(nbLoop - maxSize);
            os << ".... ";
        }

        if (getFormat() == String)
        {
            os << *pStringValue[i] << " ";
        }
        else
        {
            os << (unsigned int) ((unsigned char*)pValue)[i] << " ";
        }
    }
    return os.str();
}

void Register::setScalarFromString(std::string stringValue)
{
    if(!isScalar())
        throw SilecsException(__FILE__, __LINE__,"The type of register " + name_ + " is not a scalar type.");

    switch(type_)
    {
        case uInt8:
        {
            //Casting to uint16 and later to uint 8 to avoid it being converted for his ASCII character
            uint16_t val = 0;
            StringUtilities::fromString<uint16_t>(val, stringValue);
            setVal(static_cast<uint8_t>(val));
            break;
        }
        case Int8:
        {
            int8_t val;
            StringUtilities::fromString<int8_t>(val, stringValue);
            setVal(val);
            break;
        }
        case uInt16:
        {
            uint16_t val;
            StringUtilities::fromString<uint16_t>(val, stringValue);
            setVal(val);
            break;
        }
        case Int16:
        {
            int16_t val;
            StringUtilities::fromString<int16_t>(val, stringValue);
            setVal(val);
            break;
        }
        case uInt32:
        {
            uint32_t val;
            StringUtilities::fromString<uint32_t>(val, stringValue);
            setVal(val);
            break;
        }
        case Int32:
        {
            int32_t val;
            StringUtilities::fromString<int32_t>(val, stringValue);
            setVal(val);
            break;
        }
        case uInt64:
        {
            uint64_t val;
            StringUtilities::fromString<uint64_t>(val, stringValue);
            setVal(val);
            break;
        }
        case Int64:
        {
            int64_t val;
            StringUtilities::fromString<int64_t>(val, stringValue);
            setVal(val);
            break;
        }
        case Float32:
        {
            float val;
            StringUtilities::fromString<float>(val, stringValue);
            setVal(val);
            break;
        }
        case Float64:
        {
            double val;
            StringUtilities::fromString<double>(val, stringValue);
            setVal(val);
            break;
        }
        case String:
        {
            setVal(stringValue);
            break;
        }
        case Bool:
        {
            bool val = false;

            if (StringUtilities::evalStringToBool(stringValue))
                val = true;

            setVal(val);
            break;
        }
        case Date:
        {
            double val;
            StringUtilities::fromString<double>(val, stringValue);
            setVal(val);
            break;
        }
        default:
            throw SilecsException(__FILE__, __LINE__, "Register '" + name_+ "' has a unknown format type");
    }
}

void Register::setArrayFromString(const std::vector<std::string>& data)
{
    if(isSingleArray())
    {
        setSingleArrayFromString(data);
    }
    else if (isDoubleArray())
    {
        setDoubleArrayFromString(data);
    }
    else
    {
        throw SilecsException(__FILE__, __LINE__, "The type of register " + name_ + " is not an array type.");
    }
}

void Register::setSingleArrayFromString(const std::vector<std::string>& data)
{
    if (data.size() != dimension1_)
    {
        throw SilecsException(__FILE__, __LINE__, "Passed array size [" + std::to_string(data.size()) + "] doesn't match dimension1_ [" + std::to_string(dimension1_) + "].");
    }

    switch(type_)
    {
        case uInt8:
        {
            uint8_t vector[dimension1_];
            // Cast first to uint16_t to avoid conversion to ASCII character
            uint16_t val;
            for(unsigned long i = 0; i < dimension1_; i++)
            {
                StringUtilities::fromString<uint16_t>(val, data[i]);
                vector[i] = (uint8_t)val;
            }
            setValArray(vector, dimension1_);
            break;
        }
        case Int8:
        {
                int8_t vector[dimension1_];
                for(unsigned long i = 0; i < dimension1_; i++)
                {
                    if(data[i].length() != 1)
                    {
                        throw SilecsException(__FILE__, __LINE__, "Conversion from string '" + data[i] + "'to int8 failed for register '" + name_ + "'");
                    }
                    vector[i] = data[i][0];
                }
                setValArray(vector, dimension1_);
            break;
        }
        case uInt16:
        {
                uint16_t vector[dimension1_];
                uint16_t val;
                for(unsigned long i = 0; i < dimension1_; i++)
                {
                    StringUtilities::fromString<uint16_t>(val, data[i]);
                    vector[i] = val;
                }
                setValArray(vector, dimension1_);
            break;
        }
        case Int16:
        {
                int16_t vector[dimension1_];
                int16_t val;
                for(unsigned long i = 0; i < dimension1_; i++)
                {
                    StringUtilities::fromString<short>(val, data[i]);
                    vector[i] = val;
                }
                setValArray(vector, dimension1_);
            break;
        }
        case uInt32:
        {
                uint32_t vector[dimension1_];
                uint32_t val;
                for(unsigned long i = 0; i < dimension1_; i++)
                {
                    StringUtilities::fromString<uint32_t>(val, data[i]);
                    vector[i] = val;
                }
                setValArray(vector,dimension1_);
            break;
        }
        case Int32:
        {
                int32_t vector[dimension1_];
                int32_t val;
                for(unsigned long i = 0; i < dimension1_; i++)
                {
                    StringUtilities::fromString<int32_t>(val, data[i]);
                    vector[i] = val;
                }
                setValArray(vector,dimension1_);
            break;
        }
        case uInt64:
        {
                uint64_t vector[dimension1_];
                uint64_t val;
                for(unsigned long i = 0; i < dimension1_; i++)
                {
                    StringUtilities::fromString<uint64_t>(val, data[i]);
                    vector[i] = val;
                }
                setValArray(vector,dimension1_);
            break;
        }
        case Int64:
        {
                int64_t vector[dimension1_];
                int64_t val;
                for(unsigned long i = 0; i < dimension1_; i++)
                {
                    StringUtilities::fromString<int64_t>(val, data[i]);
                    vector[i] = val;
                }
                setValArray(vector,dimension1_);
            break;
        }
        case Float32:
        {
                float vector[dimension1_];
                float val;
                for(unsigned long i = 0; i < dimension1_; i++)
                {
                    StringUtilities::fromString<float>(val, data[i]);
                    vector[i] = val;
                }
                setValArray(vector,dimension1_);
            break;
        }
        case Float64:
        {
                double vector[dimension1_];
                double val;
                for(unsigned long i = 0; i < dimension1_; i++)
                {
                    StringUtilities::fromString<double>(val, data[i]);
                    vector[i] = val;
                }
                setValArray(vector,dimension1_);
            break;
        }
        case String:
        {
                std::string vector[dimension1_];
                for(unsigned long i = 0; i < dimension1_; i++)
                {
                    vector[i] = data[i];
                }
                setValArray(vector, dimension1_);
            break;
        }
        case Date:
        {
                double vector[dimension1_];
                double val;
                for(unsigned long i = 0; i < dimension1_; i++)
                {
                    StringUtilities::fromString<double>(val, data[i]);
                    vector[i] = val;
                }
                setValArray(vector,dimension1_);
            break;
        }
        default:
        	throw SilecsException(__FILE__, __LINE__, "Register '" + name_+ "' has an unknown format type");
    }
}

void Register::setDoubleArrayFromString(const std::vector<std::string>& data)
{
    if (data.size() != dimension1_ * dimension2_)
    {
        throw SilecsException(__FILE__, __LINE__, "Passed array size [" + std::to_string(data.size()) + "] doesn't match dimension1_ * dimension2_ [" + std::to_string(dimension1_) + " x " + std::to_string(dimension2_) + "].");
    }

    switch(type_)
    {
        case uInt8:
        {
            uint8_t vector[dimension1_][dimension2_];
            uint16_t val;
            for(unsigned long i = 0; i < dimension1_; i++)
            {
                for(unsigned long j = 0; j < dimension2_; j++)
                {
                    StringUtilities::fromString<uint16_t>(val, data[i * dimension2_ + j]);
                    vector[i][j] = (uint8_t)val;
                }
            }
            setValArray2D((const uint8_t*)vector, dimension1_, dimension2_);
            break;
        }
        case Int8:
        {
            int8_t vector[dimension1_][dimension2_];
            for(unsigned long i = 0; i < dimension1_; i++)
            {
                for(unsigned long j = 0; j < dimension2_; j++)
                {
                    if(data[i * dimension2_ + j].length()!=1)
                    {
                        throw SilecsException(__FILE__, __LINE__,"Conversion from string '" + data[i * dimension2_ + j] + "'to int8 failed for register '" + name_ + "'");
                    }
                    vector[i][j] = data[i * dimension2_ + j][0];
                }
            }
            setValArray2D((const int8_t*)vector, dimension1_, dimension2_);
            break;
        }
        case uInt16:
        {
            uint16_t vector[dimension1_][dimension2_];
            uint16_t val;
            for(unsigned long i = 0; i < dimension1_; i++)
            {
                for(unsigned long j = 0; j < dimension2_; j++)
                {
                    StringUtilities::fromString<uint16_t>(val, data[i * dimension2_ + j]);
                    vector[i][j] = val;
                }
            }
            setValArray2D((const uint16_t*)vector, dimension1_, dimension2_);
            break;
        }
        case Int16:
        {
            int16_t vector[dimension1_][dimension2_];
            int16_t val;
            for(unsigned long i = 0; i < dimension1_; i++)
            {
                for(unsigned long j = 0; j < dimension2_; j++)
                {
                    StringUtilities::fromString<short>(val, data[i * dimension2_ + j]);
                    vector[i][j] = val;
                }
            }
            setValArray2D((const int16_t*)vector, dimension1_, dimension2_);
            break;
        }
        case uInt32:
        {
            uint32_t vector[dimension1_][dimension2_];
            uint32_t val;
            for(unsigned long i = 0; i < dimension1_; i++)
            {
                for(unsigned long j = 0; j < dimension2_; j++)
                {
                    StringUtilities::fromString<uint32_t>(val, data[i * dimension2_ + j]);
                    vector[i][j] = val;
                }
            }
            setValArray2D((const uint32_t*)vector, dimension1_, dimension2_);
            break;
        }
        case Int32:
        {
            int32_t vector[dimension1_][dimension2_];
            int32_t val;
            for(unsigned long i = 0; i < dimension1_; i++)
            {
                for(unsigned long j = 0; j < dimension2_; j++)
                {
                    StringUtilities::fromString<int32_t>(val, data[i * dimension2_ + j]);
                    vector[i][j] = val;
                }
            }
            setValArray2D((const int32_t*)vector, dimension1_, dimension2_);
            break;
        }
        case uInt64:
        {
            uint64_t vector[dimension1_][dimension2_];
            uint64_t val;
            for(unsigned long i = 0; i < dimension1_; i++)
            {
                for(unsigned long j = 0; j < dimension2_; j++)
                {
                    StringUtilities::fromString<uint64_t>(val, data[i * dimension2_ + j]);
                    vector[i][j] = val;
                }
            }
            setValArray2D((const uint64_t*)vector, dimension1_, dimension2_);
            break;
        }
        case Int64:
        {
            int64_t vector[dimension1_][dimension2_];
            int64_t val;
            for(unsigned long i = 0; i < dimension1_; i++)
            {
                for(unsigned long j = 0; j < dimension2_; j++)
                {
                    StringUtilities::fromString<int64_t>(val, data[i * dimension2_ + j]);
                    vector[i][j] = val;
                }
            }
            setValArray2D((const int64_t*)vector, dimension1_, dimension2_);
            break;
        }
        case Float32:
        {
            float vector[dimension1_][dimension2_];
            float val;
            for(unsigned long i = 0; i < dimension1_; i++)
            {
                for(unsigned long j = 0; j < dimension2_; j++)
                {
                    StringUtilities::fromString<float>(val, data[i * dimension2_ + j]);
                    vector[i][j] = val;
                }
            }
            setValArray2D((const float*)vector, dimension1_, dimension2_);
            break;
        }
        case Float64:
        {
            double vector[dimension1_][dimension2_];
            double val;
            for(unsigned long i = 0; i < dimension1_; i++)
            {
                for(unsigned long j = 0; j < dimension2_; j++)
                {
                    StringUtilities::fromString<double>(val, data[i * dimension2_ + j]);
                    vector[i][j] = val;
                }
            }
            setValArray2D((const double*)vector, dimension1_, dimension2_);
            break;
        }
        case String:
        {
            void *vector = calloc(dimension1_*dimension2_, sizeof(void*));
            std::string** stringVector = static_cast<std::string**>(vector);
            for(unsigned long i = 0; i<(dimension1_); i++)
            {
                for(unsigned long j = 0; j < dimension2_; j++)
                {
                    stringVector[(i * dimension2_) + j] = new std::string((data[(i * dimension2_) + j]));
                }
            }
            setValArray2D((const std::string*)stringVector, dimension1_, dimension2_);
            // cleanup
            for (unsigned int i = 0; i < (dimension1_ * dimension2_); i++)
            {
                delete stringVector[i];
            }
            free(vector);
            break;
        }
        case Date:
        {
            double vector[dimension1_][dimension2_];
            double val;
            for(unsigned long i = 0; i < dimension1_; i++)
            {
                for(unsigned long j = 0; j < dimension2_; j++)
                {
                    StringUtilities::fromString<double>(val, data[i * dimension2_ + j]);
                    vector[i][j] = val;
                }
            }
            setValArray2D((const double*)vector, dimension1_, dimension2_);
            break;
        }
        default:
        	throw SilecsException(__FILE__, __LINE__, "Register '" + name_+ "' has an unknown format type");
    }
}

std::string Register::getValAsString(void* pValue, unsigned long i, unsigned long j)
{
    struct tm dscst;
    std::ostringstream os;

    //get element whatever is the value dimension (scalar, single array or flat 2D-array)
    unsigned long index = (i * dimension2_) + j;

    if ( (i >= dimension1_) || (j >= dimension2_))
    {
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    }

    switch (type_)
    { // -------- Deprecated types (32bits platform only)
      //case Char:     os << (char)((char*)pValue)[index];                      break;
      //case uChar:    os << (unsigned int)((unsigned char*)pValue)[index];     break;
      //case Short:    os << (short)((short*)pValue)[index];                    break;
      //case uShort:   os << (unsigned short)((unsigned short*)pValue)[index];  break;
      //case Long:     os << (long)((long*)pValue)[index];                      break;
      //case uLong:    os << (unsigned long)((unsigned long*)pValue)[index];    break;
      //case Float:    os << (float)((float*)pValue)[index];                    break;
      //-------- Supported types (32 and 64bits platform)
        case Int8:
            os << (int8_t) ((int8_t*)pValue)[index];
            break;
        case uInt8:
            os << (uint16_t) ((uint8_t*)pValue)[index];
            break;
        case Int16:
            os << (int16_t) ((int16_t*)pValue)[index];
            break;
        case uInt16:
            os << (uint16_t) ((uint16_t*)pValue)[index];
            break;
        case Int32:
            os << (int32_t) ((int32_t*)pValue)[index];
            break;
        case uInt32:
            os << (uint32_t) ((uint32_t*)pValue)[index];
            break;
        case Int64:
            os << (int64_t) ((int64_t*)pValue)[index];
            break;
        case uInt64:
            os << (uint64_t) ((uint64_t*)pValue)[index];
            break;
        case Float32:
            os << (float) ((float*)pValue)[index];
            break;
        case Float64:
            os << (double) ((double*)pValue)[index];
            break;
        case Bool:
        {
            bool val = ((bool*)pValue)[index];
            if (val)
                os << "true";
            else
                os << "false";
            
            break;
        }
        case String:
        {
            std::string** pStringValue = static_cast<std::string**>(pValue);
            os << * (pStringValue[index]);
            break;
        }
        case Date:
        {
            dscst = getTMDate( ((double*)pValue)[index]);
            os << dscst.tm_year + 1900 << "-" << dscst.tm_mon + 1 << "-" << dscst.tm_mday << "-" << dscst.tm_hour << ":" << dscst.tm_min << ":" << dscst.tm_sec << " ";
            break;
        }
        default:
            throw SilecsException(__FILE__, __LINE__, DATA_UNKNOWN_FORMAT_TYPE, StringUtilities::toString(type_));
    }
    return os.str();
}

std::string Register::getInputValAsString()
{
    return getInputValAsString(0, 0);
}
;
std::string Register::getInputValAsString(unsigned long i)
{
    return getInputValAsString(i, 0);
}
;
std::string Register::getInputValAsString(unsigned long i, unsigned long j)
{
    std::lock_guard<std::mutex> lock(mutex_);
    if (isReadable())
    {
        return getValAsString(pRecvValue_, i, j);
    }
    else
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
}

std::string Register::getOutputValAsString()
{
    return getOutputValAsString(0, 0);
}
;
std::string Register::getOutputValAsString(unsigned long i)
{
    return getOutputValAsString(i, 0);
}
;
std::string Register::getOutputValAsString(unsigned long i, unsigned long j)
{
    std::lock_guard<std::mutex> lock(mutex_);
    if (isWritable())
    {
        return getValAsString(pSendValue_, i, j);
    }
    else
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
}

std::string Register::dumpInputVal(bool asAsciiChar)
{
    std::lock_guard<std::mutex> lock(mutex_);
    std::ostringstream os;
    unsigned long byteSize = dimension1_ * dimension2_ * size_;
    for (unsigned int i = 0; i < byteSize; i++)
    {
        if (asAsciiChar)
        {
            // Returns when end of string is found
            // TODO Release 4.0.0: add this check only for String registers
            if ( ((char) ((unsigned char*)pRecvValue_)[i]) == 0) // returns as soon as it reaches the end of the string
                break;
            os << (char) ((unsigned char*)pRecvValue_)[i];
        }
        else
            os << std::hex << (unsigned int) ((unsigned char*)pRecvValue_)[i] << " ";
    }
    return os.str();
}

std::string Register::dumpOutputVal(bool asAsciiChar)
{
    std::lock_guard<std::mutex> lock(mutex_);
    std::ostringstream os;
    unsigned long byteSize = dimension1_ * dimension2_ * size_;
    for (unsigned int i = 0; i < byteSize; i++)
    {
        if (asAsciiChar)
            os << (char) ((unsigned char*)pSendValue_)[i];
        else
            os << std::hex << (unsigned int) ((unsigned char*)pSendValue_)[i] << " ";
    }
    return os.str();
}

struct tm Register::getTMDate(double date)
{
    struct tm dscst;
    time_t dt = (time_t)date;
    localtime_r(&dt, &dscst);
    return dscst;
}

time_t Register::getEpochDate(double date)
{
    return static_cast<time_t>(round(date));
}

timeval Register::getTimeStamp()
{
    if (isReadable())
        return tod_;

    throw SilecsException(__FILE__, __LINE__, PARAM_NO_TIME_STAMP, getName());
}

std::string Register::getTimeStampAsString()
{
    if (isReadable())
    {
        std::ostringstream os;
        double ts = (double)tod_.tv_sec + (double)tod_.tv_usec / 1000000.0; //second
        os << std::setprecision(20) << ts << "s";
        return os.str();
    }
    else
        return "not defined";
}

void Register::printVal()
{
    std::lock_guard<std::mutex> lock(mutex_);
    std::ostringstream os;
    os << getName() << " " << getFormatAsString() << "[" << dimension1_ << "][" << dimension2_ << "] ";
    if (getFormat() == String)
        os << ", string-length: " << getLength();

    if (isReadable())
    {
        os << ", input: ";
        for (unsigned int i = 0; i < dimension1_; i++)
            for (unsigned int j = 0; j < dimension2_; j++)
                os << getValAsString(pRecvValue_, i, j) << " ";
    }

    if (isWritable())
    {
        os << ", output: ";
        for (unsigned int i = 0; i < dimension1_; i++)
            for (unsigned int j = 0; j < dimension2_; j++)
                os << getValAsString(pSendValue_, i, j) << " ";
    }
    std::cout << os.str() << std::endl;
}

} // namespace
