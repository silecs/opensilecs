/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _SILECS_PLC_REGISTER_H_
#define _SILECS_PLC_REGISTER_H_

#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/equipment/SilecsRegister.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>
#include <silecs-communication/interface/equipment/SilecsParamConfig.h>

#ifdef __x86_64__
#include <endian.h>
#endif

namespace Silecs
{
class PLCRegister : public Register
{
private:

protected:
    PLCRegister(const ElementXML& registerNode, const SilecsParamConfig& paramConfig);
    virtual ~PLCRegister();

    /*!
     * \fn importValue
     * \brief Used to convert and transfered data from the data-block receiver to the register value
     * Overloads the virtual method inherited from the class SilecsRegister.
     * \param pBuffer points the data-block buffer
     * \param ts is the time-of-day to time-stamp the register
     */
    void importValue(void* pBuffer, timeval ts);

    /*!
     * \fn exportValue
     * \brief Used to transfer the register value to the data data-block to be sent
     * Overloads the virtual method inherited from the class SilecsRegister.
     * \param pBuffer points the data-block buffer
     */
    void exportValue(void* pBuffer);

    /*!
     * \fn copyValue
     * \brief Copies the register content from the input buffer the output buffer.
     * Overloads the virtual method inherited from the class SilecsRegister.
     * This method is used for Retentive InOut registers synchronization.
     */
    void copyValue();

    /// Generic methods for byte-swapping ---------------------------------
    virtual uint16_t _swaps(uint16_t s);
    virtual uint32_t _swapl(uint32_t l);
    virtual float _swapf(float f);
    virtual double _swapd(double d);

    virtual void swapBytesNToH(unsigned char *data, unsigned long size) = 0;
    virtual void swapBytesHToN(unsigned char *data, unsigned long size) = 0;

    SilecsParamConfig paramConfig_;
};

/*!
 * \class BigEndianRegister
 * \brief Specific register using Big-endian memory convention
 */
class BigEndianRegister : public PLCRegister
{

public:
    BigEndianRegister(const ElementXML& registerNode, const SilecsParamConfig& paramConfig);
    virtual ~BigEndianRegister();

protected:
    void swapBytesNToH(unsigned char *data, unsigned long size);
    void swapBytesHToN(unsigned char *data, unsigned long size);

};

/*!
 * \class LittleEndianRegister
 * \brief Specific register using Little-endian memory convention
 */
class LittleEndianRegister : public PLCRegister
{

public:
    LittleEndianRegister(const ElementXML& registerNode, const SilecsParamConfig& paramConfig);
    virtual ~LittleEndianRegister();

protected:
    void swapBytesNToH(unsigned char *data, unsigned long size);
    void swapBytesHToN(unsigned char *data, unsigned long size);

};

/*!
 * \class S7Register
 * \brief Specific register for S7 hardware
 */
class S7Register : public BigEndianRegister
{
public:
    S7Register(const ElementXML& registerNode, const SilecsParamConfig& paramConfig);
    virtual ~S7Register();

private:
    /*!
     * \fn importString
     * \brief Used to convert and transfer strings from the data-block receiver to the register value
     * \param pBuffer points the data-block buffer
     * \param ts is the time-of-day to timestamp the register
     */
    void importString(void* pBuffer, timeval ts);

    /*!
     * \fn exportString
     * \brief Used to transfer the register value to the data-block to be sent
     * \param pBuffer points the data-block buffer
     */
    void exportString(void* pBuffer);

};

/*!
 * \class UnityRegister
 * \brief Specific register for Schneider hardware
 */
class UnityRegister : public LittleEndianRegister
{
public:
    UnityRegister(const ElementXML& registerNode, const SilecsParamConfig& paramConfig);
    virtual ~UnityRegister();

private:
    /*!
     * \fn importString
     * \brief Used to convert and transfer strings from the data-block receiver to the register value
     * \param pBuffer points the data-block buffer
     * \param ts is the time-of-day to timestamp the register
     */
    void importString(void* pBuffer, timeval ts);

    /*!
     * \fn exportString
     * \brief Used to transfer the register value to the data-block to be sent
     * \param pBuffer points the data-block buffer
     */
    void exportString(void* pBuffer);

};

/*!
 * \class TwinCATRegister
 * \brief Beckhoff BCxx/CXxx PLC use the same memory swapping than Unity PLCs except for float32 data
 * This class is used to specialized the 32bits data swapping inherited from the UnityRegister class.
 * It overwrites the swapl() method responsible for 32bits data swapping.
 */
class TwinCATRegister : public UnityRegister
{
public:
    TwinCATRegister(const ElementXML& registerNode, const SilecsParamConfig& paramConfig);
    virtual ~TwinCATRegister();

protected:
    //Dedicated method for specific 64bit swapping (other come from the base-class)
    uint32_t _swapl(uint32_t l);
};
/// @endcond

}
#endif // _SILECS_PLC_REGISTER_H_
