/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifdef NI_SUPPORT_ENABLED
#ifndef _SILECS_CNV_REGISTER_H_
#define _SILECS_CNV_REGISTER_H_

#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/equipment/SilecsRegister.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>

#ifdef __cplusplus
extern "C"
{
#endif

#include <nilibnetv.h>

#ifdef __cplusplus
}
#endif

namespace Silecs
{

    /// @cond
    class CNVRegister : public Register
    {
    private:

        void setRegIntoStructBuffer(CNVData *buffer,int bufferSize, CNVData reg, int regOffset);
        CNVData getRegfromStructBuffer(CNVData buffer,int bufferSize, int regOffset);

        void swapInputUInt8Array2D(uint32_t dim1, uint32_t dim2);
        void swapInputInt8Array2D(uint32_t dim1, uint32_t dim2);
        void swapInputUInt16Array2D(uint32_t dim1, uint32_t dim2);
        void swapInputInt16Array2D(uint32_t dim1, uint32_t dim2);
        void swapInputUInt32Array2D(uint32_t dim1, uint32_t dim2);
        void swapInputInt32Array2D(uint32_t dim1, uint32_t dim2);
        void swapInputUInt64Array2D(uint32_t dim1, uint32_t dim2);
        void swapInputInt64Array2D(uint32_t dim1, uint32_t dim2);
        void swapInputFloat32Array2D(uint32_t dim1, uint32_t dim2);
        void swapInputFloat64Array2D(uint32_t dim1, uint32_t dim2);
        void swapInputDateArray2D(uint32_t dim1, uint32_t dim2);

        void swapOutputUInt8Array2D(uint32_t dim1, uint32_t dim2);
        void swapOutputInt8Array2D(uint32_t dim1, uint32_t dim2);
        void swapOutputUInt16Array2D(uint32_t dim1, uint32_t dim2);
        void swapOutputInt16Array2D(uint32_t dim1, uint32_t dim2);
        void swapOutputUInt32Array2D(uint32_t dim1, uint32_t dim2);
        void swapOutputInt32Array2D(uint32_t dim1, uint32_t dim2);
        void swapOutputUInt64Array2D(uint32_t dim1, uint32_t dim2);
        void swapOutputInt64Array2D(uint32_t dim1, uint32_t dim2);
        void swapOutputFloat32Array2D(uint32_t dim1, uint32_t dim2);
        void swapOutputFloat64Array2D(uint32_t dim1, uint32_t dim2);
        void swapOutputDateArray2D(uint32_t dim1, uint32_t dim2);

        // Check for CNV errors, log and create an exception
        void errChk(int code);

    protected:

        // export register extraction methods
        friend class PLC;
        friend class Device;

        /*!
         * \fn importValue
         * \brief Used to convert and transfered data from the data-block receiver to the register value
         * Overloads the virtual method inherited from the class SilecsRegister.
         * \param pBuffer points the data-block buffer
         * \param ts is the time-of-day to time-stamp the register
         */
        void importValue(void* pBuffer, timeval ts);

        /*!
         * \fn exportValue
         * \brief Used to transfered the register value to the data data-block to be sent
         * Overloads the virtual method inherited from the class SilecsRegister.
         * \param pBuffer points the data-block buffer
         */
        void exportValue(void* pBuffer);

        /*!
         * \fn importString
         * \brief Import values from the received buffer to the register. Pure vistual to be implemented by the inheriting classes.
         * \param pBuffer received data buffer
         * \param ts register time-stamp
         */
        void importString(void* pBuffer, timeval ts);

        /*!
         * \fn exportString
         * \brief Export the string from the register to the correct position in the buffer. Pure virtual to be implemented by inheriting classes.
         * \param pBuffer the output buffer where to update the data
         */
        void exportString(void* pBuffer);

        /*!
         * \fn copyValue
         * \brief Copies the register content from the input buffer the output buffer.
         * Overloads the virtual method inherited from the class SilecsRegister.
         * This method is used for Retentive InOut registers synchronization.
         */
        void copyValue();

        /*!
         * \fn swapInputArray2D
         * \brief The CNV library reads the element in row-major order, whereas the SILECS CLIB works in column-major order.
         *  The following code swaps rows and columns, returning the content of pRecvValue_ correctly ordered
         */
        template <typename T> void swapInputArray2D(FormatType F, uint32_t dim1, uint32_t dim2);

        /*!
         * \fn swapOutputArray2D
         * \brief The CNV library reads the element in row-major order, whereas the SILECS CLIB works in column-major order.
         *  The following code swaps rows and columns, returning the content of pSendValue_ correctly ordered
         */
        template <typename T> void swapOutputArray2D(FormatType F, uint32_t dim1, uint32_t dim2);

    public:
        // Constructor from super class
        CNVRegister(Device* theDevice, ElementXML* pDesignEl);

        // Destructor from super class
        ~CNVRegister();

    };
    /// @endcond
}
#endif // _SILECS_CNV_REGISTER_H_
#endif //NI_SUPPORT_ENABLED
