/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _SILECS_PLC_H_
#define _SILECS_PLC_H_

#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/utility/Definitions.h>
#include <silecs-communication/interface/equipment/SilecsParamConfig.h>
#include <silecs-communication/interface/equipment/SilecsDeploy.h>

#include <future>
#include <string>
#include <vector>

namespace Silecs
{
class Connection;

/*!
 * \class PLC
 * \brief This object provides all services related to a PLC. It provides the information
 * of the PLC hardware configuration, connection status and diagnostic.
 * It maintains a reference of all the PLC devices.
 * It can be used to trig the data-block exchanges for all devices of the PLC at the same time.
 */
class PLC
{

public:
    PLC(const std::string& plcName, const std::string& plcIPaddr, const std::string& parameterFile = "");
    virtual ~PLC();
    // Diagnostic features ----------------------------------------

    /*!
     * \brief Returns the PLC network name
     * \return PLC host-name
     */
    std::string getName() const;

    /*!
     * \brief Returns the PLC network address
     * \return PLC TCP-IP address
     */
    std::string getIPAddress() const;

    // Miscellaneous features ----------------------------------------

    /*!
     * \brief Used to Enable the Client/PLC connection and connect the PLC immediately if needed (connectNow=true).
     * Just Enabling the connection (connectNow=false) means the connection is allowed and systematically checked on each PLC access (send/recv).
     * In any case, connect() method downloads the Diagnostic data from the PLC Header memory and checks the Client/PLC configuration consistency.
     * \param connectNow = true: Connection is maintained immediately after the Header data update.
     * connectNow = false: delay the real data connection to the first access attempt (send/recv blocks).
     * \param compareChecksums: Set to false if you want to skip the checksum comparison. Default is true
     */
    void connect(bool connectNow, bool compareChecksums = true);

    /*!
     * \brief Used to disable the Client/PLC connection.
     * Connection is closed (if needed) and send/recv transaction are not possible anymore (PLC connection is Disabled).
     * The PLC object still exists in the PLCHandler and the Registers are available locally.
     */
    void disconnect();

    /*!
     * \brief Returns true if the PLC connection has been requested using connect() method.
     * \return Enable status
     */
    bool isEnabled();

    /*!
     * \brief Check the connection status and reconnect the controller if required (optional)
     * Returns true if the PLC connection is established.
     * \param reconnectNow can be used to force reconnection before checking (false by default).
     * \return Connection status
     */
    bool isConnected(bool reconnectNow = false);

    int recvUnitCode(UnitCodeType& dataStruct);
    int recvUnitStatus(UnitStatusType& dataStruct);
    int recvCPUInfo(CPUInfoType& dataStruct);
    int recvCPInfo(CPInfoType& dataStruct);

    // true if the "recvUnitStatus" is RUN, false otherwise. Throws exception on failure
    bool isRunning();

    int sendColdRestart();
    int sendPlcStop();
    /*!
     * \brief Acquires all blocks of all devices of the PLC.
     * The method tries to (re)connect the PLC if the connection is not established yet or unexpectedly interrupted (network failure, PLC down, etc.).
     * It generates an Silecs::SilecsException if any error is thrown.
     */
    void recvAll();

    /*!
     * \brief Acquires one particular registers block of all devices of that PLC.
     * The method tries to (re)connect the PLC if the connection is not established yet or unexpectedly interrupted (network failure, PLC down, etc.).
     * It generates an Silecs::SilecsException if the PLC has not been enabled (using the connect() method).
     * \param blockName name of the block to be acquired
     * \return 0 if operation was successful else an error code (when available).
     */
    void recv(const std::string& blockName);

    /*!
     * \brief Transmits the registers block to all devices of that PLC.
     * The method tries to (re)connect the PLC if the connection is not established yet or unexpectedly interrupted (network failure, PLC down, etc.).
     * It generates an Silecs::SilecsException if the PLC has not been enabled (using the connect() method).
     * \param blockName name of the block to be transmitted
     * \return 0 if operation was successful else an error code (when available).
     */
    void send(const std::string& blockName);

    /*!
     * \brief Copies input buffer in output buffer
     * The method calls 'copyInToOut' on the underlying designs.
     * \param blockName name of the block for which the copy must be performed
     */
    void copyInToOut(const std::string& blockName);

    /*!
     * \brief Get name of the parameter file.
     */
    std::string getParamsFileName() const;

    /*!
     * \brief Get the parameter configuration of the deployment on this PLC.
     */
    const SilecsParamConfig& getParamConfig() const;

    /*!
     * \fn recvAsync/ recvWait
     * \brief Acquires the 'blockname' data block of all devices of that PLC using asynchronous mode
     */
    std::future<void> recvAsync(const std::string& blockName);

    /*!
     * \fn sendAsync/ sendWait
     * \brief Sends the 'blockname' data block to all devices of that PLC using asynchronous mode
     */
    std::future<void> sendAsync(const std::string& blockName);

    void updateLocalData();

    /*!
     * \brief Get a reference of the deployment on this PLC.
     */
    SilecsDeploy& getDeploy();

private:

    /*!
     * \fn whichPLC, whichProtocol...
     * \return the enumeration value of the given Protocol type/mode string
     */
    // Parameters passed by value since we anyways need a copy. The functions use StringUtilities::toLower.
    static PLCBrand whichPLCBrand(std::string brand); //SIEMENS, SCHNEIDER, BECKHOFF, ...
    static PLCModel whichPLCModel(std::string model); //S7-400, S7-300, ...
    static PLCSystem whichPLCSystem(std::string system); //STEP-7, UNITY, TWINCAT, ...
    static ProtocolType whichProtocolType(std::string system); //S7, Modbus, CNV, ...
    static ProtocolMode whichProtocolMode(std::string mode); //Device or Block

    /*!
     * \brief Initialize the correct connection type for the PLC.
     */
    void createConnection();

    /*!
     * \fn ExtractDatabase
     * \brief Parse the Silecs params file and extract the configuration of that PLC.
     */
    void extractDatabase();

    void populateDeploy();

    /// PLC attributes
    std::string name_;
    std::string IPaddr_;

    std::string parameterFile_;

    /* Diagnostic registers are used to check the consistency between the
     * PLC memory (remote data) and the client parameters (local data).
     */
    //Remote data, uploaded from the PLC memory
    std::string remoteVersion_;
    std::string remoteOwner_;
    std::string remoteDate_;
    uint32_t remoteChecksum_;

    SilecsParamConfig paramConfig_;

    SilecsDeploy deploy_;

    // PLC communication
    std::unique_ptr<Connection> plcConn_;
};

} // namespace

#endif // _SILECS_PLC_H_
