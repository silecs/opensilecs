/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _SILECS_REGISTER_H_
#define _SILECS_REGISTER_H_

#include <stdint.h>
#include <cstring>
#include <mutex>
#include <string>
#include <vector>

#include <silecs-communication/interface/utility/SilecsException.h>

namespace Silecs
{
class ElementXML;

/// @cond
// Register supported format
static const std::string FormatTypeString[] = { //!!Must be synchronized with FormatType enumeration

//"uChar",    /*!<: SLC5 deprecated: 8-bits unsigned char  = PLC BYTE    */
//"Char",     /*!<: 8-bits ascii char                      = PLC CHAR    */
//"uShort",   /*!<: SLC5 deprecated: 16-bits unsigned int  = PLC WORD    */
//"Short",    /*!<: SLC5 deprecated: 16-bits signed int    = PLC INT    */
//"uLong",    /*!<: SLC5 deprecated: 32-bits unsigned long = PLC DWORD*/
//"Long",     /*!<: SLC5 deprecated: 32-bits signed long   = PLC DINT    */
//"Float",    /*!<: SLC5 deprecated: 32-bits signed float  = PLC REAL    */

"uInt8", /*!<: 8-bits unsigned integer*/
"Int8", /*!<: 8-bits integer*/
"uInt16", /*!<: 16-bits unsigned integer*/
"Int16", /*!<: 16-bits integer*/
"uInt32", /*!<: 32-bits unsigned integer*/
"Int32", /*!<: 32-bits integer*/
"Float32",/*!<: 32-bits signed float*/
"Date", /*!<: 64-bits time (elapsed time since the POSIX.1 Epoch (00:00:00 UTC, January 1, 1970)    = dt  */

"uInt64", /*!<: 64-bits unsigned integer*/
"Int64", /*!<: 64-bits integer*/
"Float64", /*!<: 64-bits signed float*/
"String", /*!<: String of ASCII characters*/
"Bool"
};

/*!
 * \brief Enumeration for the Register data type. Used by Register::getFormat() method.
 * Each type has a PLC type equivalent (see below)
 */
typedef enum
{
    //!!Must be synchronized with FormatTypeString[]
    uChar = 0,  /*!<: SLC5 deprecated: 8-bits unsigned char  = PLC BYTE    */
    Char = 1,   /*!<: 8-bits ascii char                      = PLC CHAR    */
    uShort = 2, /*!<: SLC5 deprecated: 16-bits unsigned int  = PLC WORD    */
    Short = 3,  /*!<: SLC5 deprecated: 16-bits signed int    = PLC INT    */
    uLong = 4,  /*!<: SLC5 deprecated: 32-bits unsigned long = PLC DWORD*/
    Long = 5,   /*!<: SLC5 deprecated: 32-bits signed long   = PLC DINT    */
    Float = 6,  /*!<: SLC5 deprecated: 32-bits signed float  = PLC REAL    */
    Date = 7,   /*!<: 64-bits double (elapsed time since the POSIX.1 Epoch (00:00:00 UTC, January 1, 1970)    = dt  */

    uInt8 = 0, /*!<: 8-bits unsigned integer*/
    Int8 = 1, /*!<: 8-bits integer*/
    uInt16 = 2, /*!<: 16-bits unsigned integer*/
    Int16 = 3, /*!<: 16-bits integer*/
    uInt32 = 4, /*!<: 32-bits unsigned integer*/
    Int32 = 5, /*!<: 32-bits integer*/
    Float32 = 6,/*!<: 32-bits signed float*/

    uInt64 = 8, /*!<: 64-bits unsigned integer*/
    Int64 = 9, /*!<: 64-bits integer*/
    Float64 = 10, /*!<: 64-bits signed float*/
    String = 11,
    Bool = 12
} FormatType;

/*!
 * \class Register
 * \brief This object stores the final device data.
 * It provides interface to update and extract the register value.
 * The Register name is unique within the scope of the device class.
 */
class Register
{
    friend class Block;
    friend class PLCBlock;
public:

    /*!
     * \brief Returns the register name as defined in the Class design
     * \return Register name
     */
    std::string getName();

    /*!
     * \brief Returns the register access area (same as the related block as defined in the Class design)
     * \return Register area: Memory, Digital, Analog
     */
    AccessArea getAccessArea();

    /*!
     * \brief Returns the format of the register data
     * \return value from the FormatType enumeration
     */
    FormatType getFormat();

    /*!
     * \brief Returns number of elements in case of array register (1 in case of scalar register)
     * \return Register dimension1
     */
    unsigned long getDimension(uint16_t whichDim = 1 /*1,2*/); //for backward compatibility getDimension() ~= getDimension1()
    uint32_t getDimension1();

    /*!
     * \brief Returns second dimension in case of 2D-array (1 in case of array register)
     * \return Register dimension2
     */
    uint32_t getDimension2();

    /*!
     * \brief Returns length of string in case of a string/array of strings register
     * \return Register length
     */
    uint32_t getLength();

    /*!
     * \brief Returns the name of the block which contains the register as defined in the Class design
     * \return Block name of the register
     */
    std::string getBlockName();

    /*!
     * \brief Time-of-day: number of seconds and microseconds since the POSIX.1 Epoch (00:00:00 UTC, January 1, 1970)
     * Use to time-stamp the register coming from PLC (no time-stamping on send).
     * FEC clock and Timing are synchronized using SNTP (common GPS source)
     * \return Register time-stamping as timeval structure
     */
    timeval getTimeStamp();

    AccessType getAccessType() const;

    /*!
     * \brief A register can have read-only, write-only or read-write access mode as defined in the Class design.
     * \return true if register has read-only or read-write access mode
     */
    bool isReadable();

    /*!
     * \brief A register can have read-only, write-only or read-write access mode as defined in the Class design.
     * \return true if the register has write-only or read-write access mode
     */
    bool isWritable();

    /*!
     * \brief A register can be Memory or IO peripheral register.
     * \return true if the register is an IO register (Digital, Analog)
     */
    bool isIORegister();

    /*!
     * \brief A register can belong to either digital or analog IO
     * \return true if the register is digital input register
     */
    bool isDIRegister();

    /*!
     * \brief A register can belong to either digital or analog IO
     * \return true if the register is digital output register
     */
    bool isDORegister();

    /*!
     * \brief A register can belong to either digital or analog IO
     * \return true if the register is analog input register
     */
    bool isAIRegister();

    /*!
     * \brief A register can belong to either digital or analog IO
     * \return true if the register is analog output register
     */
    bool isAORegister();

    /*!
     * \brief A register can be Memory or IO peripheral register.
     * \return true if the register is a Memory register
     */
    bool isMemoryRegister();

    /*!
     * \brief A register that has constant value is a configuration.
     * Its given default-value is set to the controller at start-up.
     * \return if the register is a configuration register
     */
    bool isConfiguration();

    /*!
     * \brief A scalar is a single value register (dimension1_ and dimension2_ attributes == 1)
     * \return true if register is a scalar
     */
    bool isScalar();

    /*!
     * \brief A single array is a register with dimension1_ attribute > 1
     * \return true if register is a single array
     */
    bool isSingleArray();

    /*!
     * \brief A double array is a register with dimension2_ attribute > 1
     * \return true if register has second dimension
     */
    bool isDoubleArray();

    /*!
     * \brief Converts the double Date type to tm broken-down time structure
     * conforming to POSIX convention (look at localtime() man pages)
     * \param date Floating-point number of second that has elapsed since UTC (00:00) on January 1, 1900
     * \return tm Broken-down time structure
     */
    static struct tm getTMDate(double date);

    /*!
     * \brief Converts (static-cast) the floating-point SILECS Date type to standard time_t,
     * as those returned by Linux function time().
     * \param date Floating-point number of second that has elapsed since UTC (00:00) on January 1, 1900
     * \return time_t value
     */
    static time_t getEpochDate(double date);

    // .........................................................................
    /*!
     * \brief Returns the format of the register data as string value
     * \return Format-type of the register (Enumeration: "char", "uChar", "short", "uShort", ..)
     */
    std::string getFormatAsString();

    /*!
     * \brief Returns the register time-stamping as readable value
     * \return number of seconds since the Epoch (floating point value)
     */
    std::string getTimeStampAsString();

    /*!
     * \brief Interprets the input register value and return the corresponding string.
     * In case of array or 2D-array, it return string value of the appropriate element.
     * Generates an Exception if register is write-only or if index(es) exceed the array dimension(s)
     * \param i,j indexes of the element in the (2D)array if any (optional)
     * \return input register value or element as string
     */
    std::string getInputValAsString(); //for scalar
    std::string getInputValAsString(unsigned long i); //for single array
    std::string getInputValAsString(unsigned long i, unsigned long j); //for 2D array (flat)

    /*!
     * \brief Interprets the output register value and return the corresponding string.
     * In case of array or 2D-array, it return string value of the appropriate element.
     * Generates an Exception if register is read-only or if index(es) exceed the array dimension(s)
     * \param i,j indexes of the element in the (2D)array if any (optional)
     * \return input register value or element as string
     */
    std::string getOutputValAsString(); //for scalar
    std::string getOutputValAsString(unsigned long i); //for single array
    std::string getOutputValAsString(unsigned long i, unsigned long j); //for 2D array (flat)

    /*!
     * \brief Returns the Input register value as sequence of 8-bit words
     * \param asAsciiChar = true to dump the buffer as Ascii characters (default is Hexadecimal dump)
     * \return string blank-separated value
     */
    std::string dumpInputVal(bool asAsciiChar = false);

    /*!
     * \brief Returns the Output register value as sequence of 8-bit word
     * \param asAsciiChar = true to dump the buffer as Ascii characters (default is Hexadecimal dump)
     * \return string blank-separated value
     */
    std::string dumpOutputVal(bool asAsciiChar = false);

    /*!
     * \brief Display all the register information to the standard output for debugging purpose:
     * name, format, dimension, synchro and input/output value(s).
     */
    void printVal();

    /*!
     * \fn whichAccessArea
     * \return the string of the given access-area enum
     */
    std::string whichAccessArea(AccessArea area);

    /// @cond
    /*!
     * \fn whichFormatType
     * \return Enumeration value of the given format-type string
     */
    FormatType whichFormatType(std::string type);

    bool isInitialized();
    /// @endcond

    /*!
     * \fn setScalarFromString
     * \brief transformes the passed string to a register value
     * \param stringValue a scalar value in string representation
     */
    void setScalarFromString(std::string stringValue);


    /*!
     * \fn setArrayFromString
     * \brief transformes the passed vector of strings to a register value
     */
    void setArrayFromString(const std::vector<std::string>& data);

    template<typename T> T getVal();
    template<typename T> void getValArray(T* pValue, uint32_t dim);
    template<typename T> void getValArray2D(T* pValue, uint32_t dim1, uint32_t dim2);
    template<typename T> T* getRefArray(uint32_t& dim);
    template<typename T> T* getRefArray2D(uint32_t& dim1, uint32_t& dim2);

    template<typename T> void setVal(T val);
    template<typename T> void setValArray(const T* pVal, uint32_t dim);
    template<typename T> void setValArray2D(const T* pVal, uint32_t dim1, uint32_t dim2);

    virtual ~Register();

    /// @cond
protected:
    Register(const ElementXML& registerNode);

    /*!
     * \brief Set access type.
     * \param accessType Input, Ouput or InOut
     */
    void setAccessType(AccessType accessType);

    /*!
     * \brief Set access area.
     * \param accessArea Memory, Digital or Analog
     */
    void setAccessArea(AccessArea accessArea);

    inline void setBlockName(const std::string blockName)
    {
        blockName_ = blockName;
    }

    /*!
     * \fn getValAsString
     * \return return a copy of the corresponding string from the buffer (scalar, array or 2D-array)
     */
    std::string getValAsString(void* pValue, unsigned long i, unsigned long j);

    /*!
     * \fn getValAsByteString
     * \brief Debug purpose: return the register value as array of bytes
     * \param maxSize is the used to limit the string size in case of huge data array.
     * \return interprets the register value and return the corresponding string
     */
    std::string getValAsByteString(void* pValue, unsigned long maxSize);

    inline unsigned long getAddress()
    {
        return address_;
    }

    /*!
     * \brief Import values from the received buffer to the register. Pure virtual to be implemented by the inheriting classes.
     * \param pBuffer received data buffer
     * \param ts register time-stamp
     */
    virtual void importValue(void* pBuffer, timeval ts) = 0;

    /*!
     * \brief Export the value from the register to the correct position in the buffer. Pure virtual to be implemented by the inheriting classes.
     * \param pBuffer the output buffer where to update the data
     */
    virtual void exportValue(void* pBuffer) = 0;

    /*!
     * \brief Import values from the received buffer to the register. Pure vistual to be implemented by the inheriting classes.
     * \param pBuffer received data buffer
     * \param ts register time-stamp
     */
    virtual void importString(void* pBuffer, timeval ts) = 0;

    /*!
     * \brief Export the string from the register to the correct position in the buffer. Pure virtual to be implemented by inheriting classes.
     * \param pBuffer the output buffer where to update the data
     */
    virtual void exportString(void* pBuffer) = 0;

    /*!
     * \brief Copies the register content from the input buffer to the output buffer. Pure virtual to be implemented by the inheriting classes.
     */
    virtual void copyValue() = 0;

    void setSingleArrayFromString(const std::vector<std::string>& data);
    
    void setDoubleArrayFromString(const std::vector<std::string>& data);

    /// Register name
    std::string name_;

    /// Register access-type: Acquisition, Command or Setting (following its related block)
    AccessType accessType_;

    /// Register access-area: Memory, Digital or Analog (following its related block)
    AccessArea accessArea_;

    /// Enum format: char, uchar, short, ushort, long, ulong, float, double
    FormatType type_;
    std::string typeString_;

    /// Data first dimension (>1 for single array)
    uint32_t dimension1_;

    /// Data second dimension (>1 for double array)
    uint32_t dimension2_;

    // String length
    unsigned long length_;

    /// Data size (relies on format)
    unsigned long size_;

    /// Data memory size (relies on format, including alignments)
    unsigned long memSize_;

    /// data address within the PLC memory (from the block base-address)
    unsigned long address_;

    /// Bit number (only when format is bool)
    unsigned long bitNumber_;

    /// Flag used to check if the register has been setted once at least.
    /// Uploading Slave registers at connection time is allowed only if all
    /// the retentive Slave registers are initialized!
    bool isInitialized_;

    /// Buffer to store the value of the register
    /// 2 different buffers are used in case of InOut access.
    void* pRecvValue_;
    void* pSendValue_;

    /// Mutex for synchronizing access to pRecvValue_ and pSendValue_ from import/export functions and get/set functions.
    std::mutex mutex_;

    /// Name of the "parent" Block name (for general information)
    std::string blockName_;

    // True if this register is a configuration register ( sent to controller only once suring startup, than only read )
    bool isConfiguration_;

    // Time-of-day: number of seconds and microseconds since the POSIX.1 Epoch (00:00:00 UTC, January 1, 1970)
    // Use to time-stamp the register coming from PLC (no time-stamping on send).
    // FEC clock and Timing are synchronized using SNTP (common GPS source)
    timeval tod_;

    /// @endcond

    //private:
    template<typename T> bool isValidType();
};

template<>
inline bool Register::getVal<bool>()
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if ( !isValidType<bool>() || !isScalar())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());

    std::lock_guard<std::mutex> lock(mutex_);
    if (* ((int8_t*)pRecvValue_) == 0)
        return false;
    else
        return true;
}

template<>
inline std::string Register::getVal<std::string>()
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if (!isValidType<std::string>() || (dimension1_ != 1) || (dimension2_ != 1))
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    // TODO: Fix this string casting.
    std::lock_guard<std::mutex> lock(mutex_);
    std::string** pRecvStringValue = static_cast<std::string**>(pRecvValue_);
    return * (pRecvStringValue[0]);
}

template<typename T>
T Register::getVal()
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if (!isValidType<T>() || !isScalar())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    std::lock_guard<std::mutex> lock(mutex_);
    return * ((T*)pRecvValue_);
}

template<>
inline void Register::getValArray<std::string>(std::string* pVal, uint32_t dim)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if (!isValidType<std::string>())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if ( (dimension1_ != dim) || (dimension2_ > 1))
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    // TODO: Fix this string casting.
    std::lock_guard<std::mutex> lock(mutex_);
    std::string** pRecvStringValue = static_cast<std::string**>(pRecvValue_);
    for (unsigned long i = 0; i < dimension1_; i++)
    {
        pVal[i] = * (pRecvStringValue[i]);
    }
}

template<>
inline void Register::getValArray2D<std::string>(std::string* pVal, uint32_t dim1, uint32_t dim2)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if (!isValidType<std::string>())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if ( (dimension1_ != dim1) || (dimension2_ != dim2))
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    // TODO: Fix this string casting.
    std::lock_guard<std::mutex> lock(mutex_);
    std::string** pRecvStringValue = static_cast<std::string**>(pRecvValue_);
    for (unsigned long i = 0; i < dimension1_; i++)
    {
        for (unsigned long j = 0; j < dimension2_; j++)
        {
            pVal[ (i * dimension2_) + j] = * (pRecvStringValue[ (i * dimension2_) + j]);
        }
    }
}

template<typename T>
void Register::getValArray(T* pValue, uint32_t dim)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if (!isValidType<T>() || !isSingleArray())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if (dimension1_ != dim)
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    std::lock_guard<std::mutex> lock(mutex_);
    memcpy((void *)pValue, pRecvValue_, size_ * dimension1_);
}

template<typename T>
void Register::getValArray2D(T* pValue, uint32_t dim1, uint32_t dim2)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if (!isValidType<T>() || !isDoubleArray())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if ( (dimension1_ != dim1) || (dimension2_ != dim2))
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    std::lock_guard<std::mutex> lock(mutex_);
    memcpy((void *)pValue, pRecvValue_, size_ * dimension1_ * dimension2_);
}

template<>
inline const std::string** Register::getRefArray(uint32_t& dim)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if (dimension1_ <= 1)
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    dim = dimension1_;
    // TODO: Fix this string casting.
    std::lock_guard<std::mutex> lock(mutex_);
    return (const std::string**)pRecvValue_;
}

template<>
inline const std::string** Register::getRefArray2D(uint32_t& dim1, uint32_t& dim2)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if (dimension2_ <= 1)
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    dim1 = dimension1_;
    dim2 = dimension2_;
    // TODO: Fix this string casting.
    std::lock_guard<std::mutex> lock(mutex_);
    return (const std::string**)pRecvValue_;
}

template<typename T>
T* Register::getRefArray(uint32_t& dim)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if (!isValidType<T>() || !isSingleArray())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    dim = dimension1_;
    std::lock_guard<std::mutex> lock(mutex_);
    return (T*)pRecvValue_;
}

template<typename T>
T* Register::getRefArray2D(uint32_t& dim1, uint32_t& dim2)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if (!isValidType<T>() || !isDoubleArray())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    dim1 = dimension1_;
    dim2 = dimension2_;
    std::lock_guard<std::mutex> lock(mutex_);
    return (T*)pRecvValue_;
}

template<>
inline void Register::setVal<std::string>(std::string val)
{
    if (!isWritable())
        throw SilecsException(__FILE__, __LINE__, DATA_WRITE_ACCESS_TYPE_MISMATCH, getName());
    if (!isValidType<std::string>() || (dimension1_ != 1) || (dimension2_ != 1))
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());

    // TODO: Fix this string casting.
    std::lock_guard<std::mutex> lock(mutex_);
    std::string** pSendStringValue = static_cast<std::string**>(pSendValue_);
    * (pSendStringValue[0]) = val;
    isInitialized_ = true; //the register value has been set once at least
}

template<typename T>
void Register::setVal(T val)
{
    if (!isWritable())
        throw SilecsException(__FILE__, __LINE__, DATA_WRITE_ACCESS_TYPE_MISMATCH, getName());
    if (!isValidType<T>() || (dimension1_ != 1))
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    std::lock_guard<std::mutex> lock(mutex_);
    * ((T*)pSendValue_) = val;
    isInitialized_ = true; //the register value has been set once at least
}

template<>
inline void Register::setValArray<std::string>(const std::string* pVal, uint32_t dim)
{
    if (!isWritable())
        throw SilecsException(__FILE__, __LINE__, DATA_WRITE_ACCESS_TYPE_MISMATCH, getName());
    if (!isValidType<std::string>())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if (dimension1_ != dim || dimension2_ > 1)
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());

    // TODO: Fix this string casting.
    std::lock_guard<std::mutex> lock(mutex_);
    std::string** pSendStringValue = static_cast<std::string**>(pSendValue_);
    for (unsigned long i = 0; i < dimension1_; i++)
    {
        * (pSendStringValue[i]) = pVal[i];
    }
    isInitialized_ = true; //the register value has been set once at least
}

template<typename T>
void Register::setValArray(const T* pVal, uint32_t dim)
{
    if (!isWritable())
        throw SilecsException(__FILE__, __LINE__, DATA_WRITE_ACCESS_TYPE_MISMATCH, getName());
    if (!isValidType<T>())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if (dimension1_ != dim)
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    std::lock_guard<std::mutex> lock(mutex_);
    memcpy(pSendValue_, (void *)pVal, size_ * dimension1_);
    isInitialized_ = true; //the register value has been set once at least
}

template<>
inline void Register::setValArray2D<std::string>(const std::string* pVal, uint32_t dim1, uint32_t dim2)
{
    if (!isWritable())
        throw SilecsException(__FILE__, __LINE__, DATA_WRITE_ACCESS_TYPE_MISMATCH, getName());
    if (!isValidType<std::string>())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if (dimension1_ != dim1 || dimension2_ != dim2)
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());

    // TODO: Fix this string casting.
    std::lock_guard<std::mutex> lock(mutex_);
    std::string** pSendStringValue = static_cast<std::string**>(pSendValue_);
    for (unsigned long i = 0; i < dimension1_; i++)
    {
        for (unsigned long j = 0; j < dimension2_; j++)
        {
            * (pSendStringValue[ (i * dimension2_) + j]) = pVal[ (i * dimension2_) + j];
        }
    }
    isInitialized_ = true; //the register value has been set once at least
}

template<typename T>
void Register::setValArray2D(const T* pVal, uint32_t dim1, uint32_t dim2)
{
    if (!isWritable())
        throw SilecsException(__FILE__, __LINE__, DATA_WRITE_ACCESS_TYPE_MISMATCH, getName());
    if (!isValidType<T>())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if ( (dimension1_ != dim1) || (dimension2_ != dim2))
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    std::lock_guard<std::mutex> lock(mutex_);
    memcpy(pSendValue_, (void *)pVal, size_ * dimension1_ * dimension2_);
    isInitialized_ = true; //the register value has been set once at least
}

template<typename T>
bool Register::isValidType()
{
    if (typeString_ == "byte" ||
        typeString_ == "uint8")
        return std::is_same<T, std::uint8_t>::value;
    else if (typeString_ == "char" ||
             typeString_ == "int8")
        return std::is_same<T, std::int8_t>::value;
    else if (typeString_ == "word" ||
             typeString_ == "uint16")
        return std::is_same<T, std::uint16_t>::value;
    else if (typeString_ == "int" ||
             typeString_ == "int16")
        return std::is_same<T, std::int16_t>::value;
    else if (typeString_ == "dword" ||
             typeString_ == "uint32")
        return std::is_same<T, std::uint32_t>::value;
    else if (typeString_ == "dint" ||
             typeString_ == "int32")
        return std::is_same<T, std::int32_t>::value;
    else if (typeString_ == "int64")
        return std::is_same<T, std::int64_t>::value;
    else if (typeString_ == "uint64")
        return std::is_same<T, std::uint64_t>::value;
    else if (typeString_ == "real" ||
             typeString_ == "float32")
        return std::is_same<T, float>::value;
    else if (typeString_ == "dt" ||
             typeString_ == "dtl" ||
             typeString_ == "date" ||
             typeString_ == "float64")
        return std::is_same<T, double>::value;
    else if (typeString_ == "string")
        return std::is_same<T, std::string>::value;
    else if (typeString_ == "bool")
        return std::is_same<T, bool>::value;
    else
        throw SilecsException(__FILE__, __LINE__, DATA_UNKNOWN_FORMAT_TYPE, typeString_);
}

} // namespace

#endif // _SILECS_REGISTER_H_
