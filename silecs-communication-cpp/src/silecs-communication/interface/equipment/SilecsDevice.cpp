/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/equipment/SilecsDevice.h>

#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsRegister.h>
#include <silecs-communication/interface/equipment/PLCRegister.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/equipment/PLCBlock.h>

#include <silecs-communication/interface/equipment/CNVRegister.h>

#include <sstream>

namespace Silecs
{
Device::Device(const ElementXML& deviceNode, const std::vector<ElementXML>& blockNodes, Connection& plcConnection, const SilecsParamConfig& paramConfig) :
        paramConfig_(paramConfig)
{
    // Update Device ElementXML attributes
    label_ = deviceNode.getAttribute("label");
    StringUtilities::fromString(address_, deviceNode.getAttribute("address"));

    StringUtilities::fromString(ai_address_, deviceNode.getAttribute("AI-address"));
    StringUtilities::fromString(ao_address_, deviceNode.getAttribute("AO-address"));
    StringUtilities::fromString(di_address_, deviceNode.getAttribute("DI-address"));
    StringUtilities::fromString(do_address_, deviceNode.getAttribute("DO-address"));

    LOG(ALLOC) << "Creating SilecsDevice: " << label_;

    for (auto blockIter = blockNodes.begin(); blockIter != blockNodes.end(); blockIter++)
    {
        std::string blockName = blockIter->getAttribute("name");
        LOG(ALLOC) << "Adding Block to Device: " << blockName;
        AccessArea accessArea = AccessArea::Memory;
        if (blockIter->hasAttribute("ioType")) // only IO-Blocks have this attribute
            accessArea = Block::whichAccessArea(blockIter->getAttribute("ioType"));

        blocks_.emplace_back(new PLCBlock(*blockIter, getInputAddress(accessArea),
            getOutputAddress(accessArea), plcConnection, paramConfig));
    }
}

Device::~Device()
{
    LOG(ALLOC) << "Device (delete): " << label_;
}

const std::vector<std::unique_ptr<Block>>& Device::getBlocks() const
{
    return blocks_;
}

const std::unique_ptr<Register>& Device::getRegister(const std::string& registerName) const
{
    for (auto& block : blocks_)
    {
        for (auto& reg : block->getRegisters())
        {
            if (reg->getName() == registerName)
            {
                return reg;
            }
        }
    }
    throw SilecsException(__FILE__, __LINE__, PARAM_UNKNOWN_REGISTER_NAME, registerName);
}

const std::vector<std::unique_ptr<Register>>& Device::getRegisterCollection(const std::string& blockName) const
{
    auto blockIt = std::find_if(blocks_.begin(), blocks_.end(),
        [&blockName](const std::unique_ptr<Block>& block)
    {
        return block->getName() == blockName;
    });
    if (blockIt == blocks_.end())
    {
        throw SilecsException(__FILE__, __LINE__, PARAM_UNKNOWN_BLOCK_NAME, blockName);
    }
    return (*blockIt)->getRegisters();
}

void Device::recv()
{
    for (auto& block : blocks_)
    {
        block->receive();
    }
}

void Device::recv(const std::string& blockName)
{
    //Synchronous device data receive
    //Execute the receive action for the required device (from the current thread)
    getBlock(blockName)->receive();
}

void Device::send(const std::string& blockName)
{
    //Synchronous device data send
    //Execute the receive action for the required device (from the current thread)
    getBlock(blockName)->send();
}

const std::string& Device::getLabel() const
{
    return label_;
}

std::string Device::getRegisterList() const
{
    std::string registerList = "";
    for (auto& block : blocks_)
    {
        for (auto& reg : block->getRegisters())
        {
            if (!registerList.empty())
            {
                registerList.append(" ");
            }
            registerList.append(reg->getName());
        }
    }
    return registerList;
}

const std::unique_ptr<Block>& Device::getBlock(const std::string& blockName)
{
    auto blockIt = std::find_if(blocks_.begin(), blocks_.end(),
        [&blockName](const std::unique_ptr<Block>& block)
    {
        return block->getName() == blockName;
    });
    if (blockIt == blocks_.end())
    {
        std::ostringstream errorMessage;
        errorMessage << "Block not found! The block '" << blockName << "' does not exist on the device '" << label_ << "'.";
        throw SilecsException(__FILE__, __LINE__, errorMessage.str());
    }
    return *blockIt;
}

bool Device::hasBlock(const std::string& blockName)
{
    auto blockIt = std::find_if(blocks_.begin(), blocks_.end(),
        [&blockName](const std::unique_ptr<Block>& block)
    {
        return block->getName() == blockName;
    });
    return blockIt != blocks_.end();
}

} // namespace
