/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/equipment/SilecsPLC.h>

#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/equipment/SilecsRegister.h>
#include <silecs-communication/interface/equipment/PLCBlock.h>
#include <silecs-communication/interface/equipment/PLCRegister.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>
#include <silecs-communication/interface/utility/XMLParser.h>

//Supported protocol
#include <silecs-communication/interface/communication/MBConnection.h>
#include <silecs-communication/interface/communication/SNAP7Connection.h>

#include <silecs-communication/interface/communication/CNVConnection.h>
#include <silecs-communication/interface/equipment/CNVBlock.h>

#include <fstream>

namespace Silecs
{
PLC::PLC(const std::string& plcName, const std::string& plcIPaddr, const std::string& parameterFile) :
                IPaddr_(plcIPaddr),
                parameterFile_(parameterFile)
{
    //Force PLC hostname lower-case
    name_ = plcName;
    StringUtilities::toLower(name_);
    paramConfig_.plcName = name_;

    LOG(ALLOC) << "PLC (create): " << name_ << "/ " << IPaddr_;

    if (parameterFile_.empty())
        parameterFile_ = Service::getParamFile(plcName);

    // Upload the parameters file and build the PLC configuration
    extractDatabase();

    // Instantiate the communication interface
    createConnection();

    populateDeploy();
}

PLC::~PLC()
{
    LOG(ALLOC) << "PLC (delete): " << name_;

    //Before removing the PLC resources, we need to interrupt all the related accesses (if connection is not shared)
    disconnect();
}

void PLC::connect(bool connectNow, bool compareChecksums)
{
    //At first, Instantiate the PLC connection object if needed
    LOG((COMM|DIAG)) << "Attempt to establish connection ... ";
    //Check PLC is not already Enabled
    std::string connStr = name_ + "(" + IPaddr_ + ")";
    LOG(COMM) << connStr;

    if ( (connectNow == false) && plcConn_->isEnabled())
    { //Trying to connect a connected PLC should not be necessary an ERROR
      //throw SilecsException(__FILE__, __LINE__, COMM_ALREADY_ENABLED, connStr);
        LOG(ERROR) << "This PLC connection is already enabled: " << connStr;
        return;
    }
    
    LOG(COMM) << (connectNow ? "Open" : "Enable") << " Connection request for " << connStr;

    if (paramConfig_.typeID == BusCoupler)
    { //This is an Ethernet coupler (pure RIO) => immediate connection without SilecsHeader check
        plcConn_->enable(connectNow);
        return;
    }
    
    // Whatever the outcome of the connection that will follow, the PLC must be Enabled in any case.
    // Thus, even if the PLC is off and rejects the first connection to upload the Header,
    // we will automatically attempt to reconnect later at the next PLC accesses (send/recv) ... again and again.
    plcConn_->enable(connectNow);
    
    if (paramConfig_.typeID != BusCoupler && compareChecksums)
    {
        /* PLC memory could have change during a disconnection time (downloading or other).
         The Header registers must be reloaded for consistency checking.
         Do not continue the connection sequence if it is not even possible to load the header.
         */
        LOG(COMM) << "getting checksum";
        auto& header = deploy_.getHeader();

        remoteChecksum_ = header.getRegister("_checksum")->getVal<std::uint32_t>();
        if (remoteChecksum_ != paramConfig_.localChecksum)
        { //Client and PLC mapping are not consistent ... no transaction are possible!
            disconnect();
            std::ostringstream message;
            message << "Checksum comparison failed. Checksum in parameter file: '" << paramConfig_.localChecksum << "' Checksum in PLC: '" << remoteChecksum_ << "' Disconnecting PLC now.";
            throw SilecsException(__FILE__, __LINE__, CONFIG_CLIENT_PLC_NOT_CONSISTENT, message.str());
        }
    }

    TRACE("info") << "Connection setup: PLC host=" << name_ << " : " << IPaddr_ << ", "
        << "Owner=" << paramConfig_.localOwner << ", "
        << "GenerationRelease=" << paramConfig_.localRelease << ", "
        << "GenerationDate=" << paramConfig_.localDate << ", "
        << "PlcChecksum=" << paramConfig_.localChecksum << ", "
        << "PlcDomain=" << paramConfig_.domain << ", "
        << "PlcHostname=" << name_ << ", "
        << "PlcBrand=" << paramConfig_.brand << ", "
        << "PlcSystem=" << paramConfig_.system << ", "
        << "PlcModel=" << paramConfig_.model << ", "
        << "ProtocolAddress=" << paramConfig_.baseMemAddr << ", "
        << "ProtocolMode=" << paramConfig_.protocolMode;
}

void PLC::disconnect()
{
    if (DEBUG & Log::topics_)
        LOG(COMM) << "Disconnection request for " << name_;
    //close the connection
    plcConn_->disable();
}

bool PLC::isEnabled()
{
    return plcConn_->isEnabled();
}
bool PLC::isConnected(bool connectNow)
{
    return (connectNow ? plcConn_->doOpen() : plcConn_->isConnected());
}

std::string PLC::getParamsFileName() const
{
    return parameterFile_;
}

std::string PLC::getName() const
{
    return name_;
}
std::string PLC::getIPAddress() const
{
    return IPaddr_;
}

const SilecsParamConfig& PLC::getParamConfig() const
{
    return paramConfig_;
}

void PLC::updateLocalData()
{
    deploy_.withAllDesigns([](SilecsDesign& design)
    {
        design.receiveForAllDevices();
    });
}

SilecsDeploy& PLC::getDeploy()
{
    return deploy_;
}

int PLC::recvUnitCode(UnitCodeType& dataStruct)
{
    return plcConn_->readUnitCode(dataStruct);
}

int PLC::recvUnitStatus(UnitStatusType& dataStruct)
{
    return plcConn_->readUnitStatus(dataStruct);
}

bool PLC::isRunning()
{
    return plcConn_->isRunning();
}

int PLC::recvCPUInfo(CPUInfoType& dataStruct)
{
    return plcConn_->readCPUInfo(dataStruct);
}

int PLC::recvCPInfo(CPInfoType& dataStruct)
{
    return plcConn_->readCPInfo(dataStruct);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//Puts the CPU in RUN mode performing a COLD START.
int PLC::sendColdRestart()
{
    return plcConn_->coldRestart();
}

int PLC::sendPlcStop()
{
    return plcConn_->plcStop();
}
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void PLC::recvAll()
{
    deploy_.withAllDesigns([&](SilecsDesign& design)
    {
        design.receiveAll();
    });
}

void PLC::recv(const std::string& blockName)
{
    //Synchronous data receive
    //Execute the receive action for all the PLC devices (from the current thread)
    deploy_.withAllDesigns([&](SilecsDesign& design)
    {
        design.receiveForAllDevices(blockName);
    });
}

void PLC::send(const std::string& blockName)
{
    //Synchronous data send
    //Execute the send action for all the PLC devices (from the current thread)
    deploy_.withAllDesigns([&](SilecsDesign& design)
    {
        design.sendForAllDevices(blockName);
    });
}

std::future<void> PLC::recvAsync(const std::string& blockName)
{
    //Schedule task using asynchronous call
    return std::async(std::launch::async, &PLC::recv, this, blockName);
}

std::future<void> PLC::sendAsync(const std::string& blockName)
{
    //Schedule task using asynchronous call
    return std::async(std::launch::async, &PLC::send, this, blockName);
}

void PLC::extractDatabase()
{
    LOG(SETUP) << "Extract SILECS configuration from: " << parameterFile_;
    XMLParser xmlParser(parameterFile_, true);

    // Extract general information ===========================================
    ElementXML rootNode = xmlParser.getSingleElementFromXPath("/SILECS-Param");
    paramConfig_.localRelease = rootNode.getAttribute("silecs-version");
    if( !Service::isVersionSupported(paramConfig_.localRelease) )
    {
      std::ostringstream message;
      message << "The version of the parameter-file: '" << paramConfig_.localRelease << "' is not compartible with the version of the silecs-library: '" << Service::getVersion() << "'";
      throw SilecsException(__FILE__, __LINE__, message.str().c_str());
    }
    ElementXML ownerNode = xmlParser.getSingleElementFromXPath("/SILECS-Param/Mapping-Info/Owner");
    paramConfig_.localOwner = ownerNode.getAttribute("user-login");

    ElementXML generationNode = xmlParser.getSingleElementFromXPath("/SILECS-Param/Mapping-Info/Generation");
    paramConfig_.localDate = generationNode.getAttribute("date");

    ElementXML deploymentNode = xmlParser.getSingleElementFromXPath("/SILECS-Param/Mapping-Info/Deployment");
    StringUtilities::fromString(paramConfig_.localChecksum, deploymentNode.getAttribute("checksum"));

    // Extract PLC general configuration ======================================
    ElementXML mappingNode = xmlParser.getSingleElementFromXPath("/SILECS-Param/SILECS-Mapping");
    //name_ = pEl->getStringAttribute("name"); already done before
    paramConfig_.domain = mappingNode.getAttribute("domain");
    paramConfig_.brand = mappingNode.getAttribute("plc-brand");
    paramConfig_.system = mappingNode.getAttribute("plc-system");
    paramConfig_.protocolMode = mappingNode.getAttribute("protocol");
    paramConfig_.model = mappingNode.getAttribute("plc-model");

    StringUtilities::fromString(paramConfig_.baseMemAddr, mappingNode.getAttribute("address"));
    StringUtilities::fromString(paramConfig_.baseAIAddr, mappingNode.getAttribute("AI-address"));
    StringUtilities::fromString(paramConfig_.baseAOAddr, mappingNode.getAttribute("AO-address"));
    StringUtilities::fromString(paramConfig_.baseDIAddr, mappingNode.getAttribute("DI-address"));
    StringUtilities::fromString(paramConfig_.baseDOAddr, mappingNode.getAttribute("DO-address"));
    paramConfig_.usedMem = mappingNode.getAttribute("used-mem");

    paramConfig_.typeID = BusCoupler; //by default, we consider the controller won't use Memory access

    paramConfig_.brandID = whichPLCBrand(paramConfig_.brand);
    paramConfig_.modelID = whichPLCModel(paramConfig_.model);
    paramConfig_.systemID = whichPLCSystem(paramConfig_.system);
    paramConfig_.protocolModeID = whichProtocolMode(paramConfig_.protocolMode);
    paramConfig_.protocolTypeID = whichProtocolType(paramConfig_.system);

    switch (paramConfig_.protocolTypeID)
    {
        case MBProtocol:
            paramConfig_.protocolType = "MODBUS-TCP";
            break;
        case S7Protocol:
            paramConfig_.protocolType = "SNAP7-TCP";
            break;
        case CNVProtocol:
            paramConfig_.protocolType = "CNV-TCP";
            break;
    };
}

void PLC::populateDeploy()
{
    XMLParser xmlParser(parameterFile_, true);
    ElementXML mappingNode = xmlParser.getSingleElementFromXPath("/SILECS-Param/SILECS-Mapping");
    const std::vector<ElementXML>& classNodes = mappingNode.getChildNodes();
    for (auto& classNode : classNodes)
    {
        std::string className = classNode.getAttribute("name");
        std::vector<ElementXML> blockNodes = xmlParser.getElementsFromXPath("/SILECS-Param/SILECS-Mapping/SILECS-Class[@name='" + className + "']/*[ name()='Acquisition-Block' or name()='Setting-Block' or name()='Command-Block' or name()='Configuration-Block' or name()='Setting-IO-Block' or name()='Acquisition-IO-Block' or name()='Command-IO-Block']");

        for (auto& blockNode : blockNodes)
        {
            AccessArea accessArea = AccessArea::Memory;
            if (blockNode.hasAttribute("ioType")) // only IO-Blocks have this attribute
            {
                accessArea = Block::whichAccessArea(blockNode.getAttribute("ioType"));
            }
            //Force BusController type (with SilecsHeader check), if there is at least one Memory block defined
            if (accessArea == AccessArea::Memory)
            {
                paramConfig_.typeID = BusController;
                break;
            }
        }
    }
    deploy_ = SilecsDeploy(parameterFile_, *plcConn_, paramConfig_);
}

void PLC::createConnection()
{
    switch (paramConfig_.protocolTypeID)
    {
        case S7Protocol:
            plcConn_ = std::unique_ptr<Connection>(new SNAP7Connection(this));
            break;
        case MBProtocol:
#ifdef MODBUS_SUPPORT_ENABLED
            plcConn_ = std::unique_ptr<Connection>(new MBConnection(this));
            break;
#else
            throw SilecsException(__FILE__, __LINE__, "Support for Modbus-Devices is disabled");
#endif
        case CNVProtocol:
#ifdef NI_SUPPORT_ENABLED
            plcConn_ = std::unique_ptr<Connection>(new CNVConnection(this));
            break;
#else
            throw SilecsException(__FILE__, __LINE__, "Support for NI-Devices is disabled");
#endif
        default:
            throw SilecsException(__FILE__, __LINE__, "Unknown protocolTypeID");
    }

    if (DEBUG & Log::topics_)
        LOG(COMM) << "PLC::createConnection()";
}

PLCBrand PLC::whichPLCBrand(std::string brand)
{
    StringUtilities::toLower(brand);
    if (brand == "siemens")
        return Siemens;
    else if (brand == "schneider")
        return Schneider;
    else if (brand == "beckhoff")
        return Beckhoff;
    else if (brand == "rabbit")
        return Digi; //deprecated - release 1.3.0
    else if (brand == "digi")
        return Digi;
    else if (brand == "ni")
        return Ni;
    else
        throw SilecsException(__FILE__, __LINE__, DATA_UNKNOWN_PLC_MANUFACTURER, brand);
}

PLCModel PLC::whichPLCModel(std::string model)
{
    StringUtilities::toLower(model);
    if (model == "simatic_et-200s")
        return ET200S;
    else if (model == "simatic_s7-300")
        return S7300;
    else if (model == "simatic_s7-400")
        return S7400;
    else if (model == "simatic_s7-1200")
        return S71200;
    else if (model == "simatic_s7-1500")
        return S71500;
    else if (model == "simatic_s7-virtual")
        return S7VIRTUAL;
    else if (model == "premium")
        return Premium;
    else if (model == "quantum")
        return Quantum;
    else if (model == "m340")
        return M340;
    else if (model.find("bc9") == 0)
        return BC9xxx;
    else if (model.find("cx9") == 0)
        return CX9xxx;
    else if (model.find("bk9") == 0)
        return BK9xxx;
    else if (model == "rabbit_rcm_4010")
        return RCM4010;
    else if (model == "rabbit_rcm_2000")
        return RCM2000;
    else if (model == "compact_rio")
        return CompactRIO;
    else if (model == "pxi_rt")
        return PXIRT;
    else if (model == "pxi_windows")
        return PXIWindows;
    else if (model == "pc_windows")
        return PCWindows;
    else if (model == "other_support_cnv")
        return OtherSupportCNV;
    else
        throw SilecsException(__FILE__, __LINE__, DATA_UNKNOWN_PLC_MODEL, model);
}

PLCSystem PLC::whichPLCSystem(std::string system)
{
    StringUtilities::toLower(system);
    if (system == "step-7")
        return Step7;
    else if (system == "tia-portal")
        return TiaPortal;
    else if (system == "snap7 linux32")
        return ServerS7;
    else if (system == "snap7 linux64")
        return ServerS7;
    else if (system == "unity pro")
        return Unity;
    else if (system.find("twincat") == 0)
        return TwinCat;
    else if (system == "standard-c")
        return StdC;
    else if (system == "labview")
        return Labview;
    else
        throw SilecsException(__FILE__, __LINE__, DATA_UNKNOWN_PLC_SYSTEM, system);
}

ProtocolType PLC::whichProtocolType(std::string system)
{
    StringUtilities::toLower(system);
    if (system == "step-7")
        return S7Protocol;
    else if (system == "tia-portal")
        return S7Protocol;
    else if (system == "snap7 linux32")
        return S7Protocol;
    else if (system == "snap7 linux64")
        return S7Protocol;
    else if (system == "unity pro")
        return MBProtocol;
    else if (system.find("twincat") == 0)
        return MBProtocol;
    else if (system == "standard-c")
        return MBProtocol;
    else if (system == "labview")
        return CNVProtocol;
    else
        throw SilecsException(__FILE__, __LINE__, DATA_UNKNOWN_PLC_SYSTEM, system);
}

ProtocolMode PLC::whichProtocolMode(std::string mode)
{
    StringUtilities::toLower(mode);
    if (mode == "block_mode")
        return BlockMode;
    else if (mode == "device_mode")
        return DeviceMode;
    else
        throw SilecsException(__FILE__, __LINE__, DATA_UNKNOWN_PROTOCOL_MODE, mode);
}

void PLC::copyInToOut(const std::string& blockName)
{
    deploy_.withAllDesigns([&](SilecsDesign& design)
    {
        design.copyInToOut(blockName);
    });
}

} // namespace
