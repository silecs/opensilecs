#include <silecs-communication/interface/equipment/SilecsPLCHandler.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>

#include <algorithm>
#include <arpa/inet.h>
#include <netdb.h>

namespace Silecs
{

PLCHandler::PLCHandler()
{
    fecHostName_ = Log::getHost();
}

PLC& PLCHandler::addPLC(const std::string& plcId, const std::string& parameterFile)
{
    {
        auto it = std::find_if(plcMap_.begin(), plcMap_.end(), [&](const std::pair<const std::string, PLC>& pair)
        {
            return pair.second.getIPAddress() == plcId || pair.first == plcId;
        });
        if (it != plcMap_.end())
        {
            throw SilecsException(__FILE__, __LINE__, PARAM_INCORRECT_ARGUMENTS, "PLC with ID: " + plcId + " already exists.");
        }
    }

    std::string hostName;
    std::string ipAddress;

    if (plcId.empty())
    {
        throw SilecsException(__FILE__, __LINE__, UNEXPECTED_ERROR, "Provided empty plcId.");
    }
    
    if (isdigit(plcId[0]))
    { //user provided an IP address (start with digit character)
        ipAddress = plcId;
        getHostNameByAddress(ipAddress, hostName);
    }
    else
    { //user provided a PLC network name (start with alpha-character)
        hostName = plcId;
        getAddressByHostName(hostName, ipAddress);
    }

    //Create the new instance of the PLC
    auto it = plcMap_.emplace(std::piecewise_construct, std::forward_as_tuple(hostName), std::forward_as_tuple(hostName, ipAddress, parameterFile));
    return it.first->second;
}

PLC& PLCHandler::getPLC(const std::string& plcId, const std::string& parameterFile)
{
    auto it = std::find_if(plcMap_.begin(), plcMap_.end(), [&](const std::pair<const std::string, PLC>& pair)
    {
        return pair.second.getIPAddress() == plcId || pair.first == plcId;
    });
    if (it != plcMap_.end())
    {
        return it->second;
    }
    return addPLC(plcId, parameterFile);
}

bool PLCHandler::isExistingPLC(const std::string& plcId) const
{
    auto it = std::find_if(plcMap_.begin(), plcMap_.end(), [&](const std::pair<const std::string, PLC>& pair)
    {
        return pair.second.getIPAddress() == plcId || pair.first == plcId;
    });
    return it != plcMap_.end();
}

const std::string& PLCHandler::getHostname() const
{
    return fecHostName_;
}

void PLCHandler::recv(const std::string& blockName)
{
    //Asynchronous data receive
    std::vector<std::future<void>> futures;

    // Schedule task on each PLC using asynchronous call for parallelism
    for (auto it = plcMap_.begin(); it != plcMap_.end(); ++it)
    {
        futures.push_back(it->second.recvAsync(blockName));
    }

    // then wait for the completion of each PLC thread (finally, maximum delay will be the longest one).
    for (auto& future : futures)
    {
        future.get();
    }
}

void PLCHandler::send(const std::string& blockName)
{
    //Asynchronous data send
    std::vector<std::future<void>> futures;

    // Schedule task on each PLC using asynchronous call for parallelism
    for (auto it = plcMap_.begin(); it != plcMap_.end(); ++it)
    {
        futures.push_back(it->second.sendAsync(blockName));
    }

    // then wait for the completion of each PLC thread (finally, maximum delay will be the longest one).
    for (auto& future : futures)
    {
        future.get();
    }
}

void PLCHandler::copyInToOut(const std::string& blockName)
{
    for (auto it = plcMap_.begin(); it != plcMap_.end(); ++it)
    {
        it->second.copyInToOut(blockName);
    }
}

void PLCHandler::getHostNameByAddress(const std::string& ipAddress, std::string& hostName) const
{
    int error;
    struct sockaddr_in ipv4addr;

    ipv4addr.sin_family = AF_INET;
    if ( (error = inet_pton(AF_INET, ipAddress.c_str(), &ipv4addr.sin_addr)) == 1)
    {
        char hname[NI_MAXHOST];
        if ( ( (error = getnameinfo((struct sockaddr*)&ipv4addr, sizeof (ipv4addr), hname, sizeof (hname), NULL, 0, 0)) == 0) && (isalpha(hname[0])))
        {
            hostName = std::string(hname);
            if (DEBUG & Log::topics_)
                LOG(COMM) << "getHostNameByAddress() successful, IPaddr: " << ipAddress << ", host-name: " << hostName;
            return;
        }
    }
    if (DEBUG & Log::topics_)
        LOG(COMM) << "getHostNameByAddress() failed: " << gai_strerror(error);
    throw SilecsException(__FILE__, __LINE__, PARAM_UNKNOWN_IP_ADDRESS, ipAddress);
}

void PLCHandler::getAddressByHostName(const std::string& hostName, std::string& ipAddress) const
{
    int error;
    struct addrinfo hints, *result = NULL;
    struct in_addr ipv4addr;

    memset(&hints, 0, sizeof (hints));
    hints.ai_family = AF_INET;
    if ( (error = getaddrinfo(hostName.c_str(), NULL, &hints, &result)) == 0)
    {
        ipv4addr.s_addr = ((struct sockaddr_in *) (result->ai_addr))->sin_addr.s_addr;
        ipAddress = std::string(inet_ntoa(ipv4addr));
        if (DEBUG & Log::topics_)
            LOG(COMM) << "getAddressByHostName() successful, host-name: " << hostName << ", IPaddr: " << ipAddress;
        if (result)
            freeaddrinfo(result);
        return;
    }
    if (DEBUG & Log::topics_)
        LOG(COMM) << "getAddressByHostName() failed: " << gai_strerror(error);
    if (result)
        freeaddrinfo(result);
    
    throw SilecsException(__FILE__, __LINE__, PARAM_UNKNOWN_PLC_HOSTNAME, hostName);
}

} // namespace Silecs
