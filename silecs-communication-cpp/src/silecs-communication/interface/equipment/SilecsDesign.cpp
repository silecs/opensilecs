#include <silecs-communication/interface/equipment/SilecsDesign.h>

#include <silecs-communication/interface/communication/SNAP7Connection.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/XMLParser.h>

namespace Silecs
{

SilecsDesign::SilecsDesign(const XMLParser& xmlParser, const std::string& className, Connection& plcConnection, const SilecsParamConfig& paramConfig) :
    className_(className),
    plcConnection_(plcConnection),
    paramConfig_(paramConfig)
{
    std::vector<ElementXML> blockNodes = xmlParser.getElementsFromXPath("/SILECS-Param/SILECS-Mapping/SILECS-Class[@name='" + className + "']/*[ name()='Acquisition-Block' or name()='Setting-Block' or name()='Command-Block' or name()='Configuration-Block' or name()='Setting-IO-Block' or name()='Acquisition-IO-Block' or name()='Command-IO-Block']");

    std::vector<ElementXML> instanceNodes = xmlParser.getElementsFromXPath("/SILECS-Param/SILECS-Mapping/SILECS-Class[@name='" + className + "']/Instance");
    for (auto instanceIter = instanceNodes.begin(); instanceIter != instanceNodes.end(); instanceIter++)
    {
        devices_.emplace_back(*instanceIter, blockNodes, plcConnection, paramConfig);
    }
}

const std::string& SilecsDesign::getName() const
{
    return className_;
}

std::vector<std::string> SilecsDesign::getDeviceNameList() const
{
    std::vector<std::string> deviceList;
    for (auto& device : devices_)
    {
        deviceList.push_back(device.getLabel());
    }
    return deviceList;
}

Device* SilecsDesign::getDevice(const std::string& deviceName)
{
    for (auto& device : devices_)
    {
        if (device.getLabel() == deviceName)
        {
            return &device;
        }
    }

    throw SilecsException(__FILE__, __LINE__, PARAM_UNKNOWN_DEVICE_NAME, deviceName + "/" + paramConfig_.plcName + "/" + className_);
}

const std::vector<Device>& SilecsDesign::getDevices() const
{
    return devices_;
}

std::string SilecsDesign::getBlockList(AccessType accessType) const
{
    // Each device has the same named blocks. When collecting all the names we only want each block
    // name once so we first add the names to a set to get unique names.
    std::set<std::string> uniqueBlocks;
    for (auto& device : devices_)
    {
        auto& blockCol = device.getBlocks();
        for (auto& block : blockCol)
        {
            if (block->getAccessType() == accessType)
            {
                uniqueBlocks.insert(block->getName());
            }
        }
    }
    std::string blockList = "";
    for (auto& block : uniqueBlocks)
    {
        blockList.append(block + " ");
    }
    return blockList;
}

void SilecsDesign::copyInToOut(const std::string& blockName)
{
    for (auto& device : devices_)
    {
        if (!device.hasBlock(blockName))
        {
            continue;
        }
        device.getBlock(blockName)->copyInToOut();
    }
}

void SilecsDesign::receiveForAllDevices()
{
    for (auto& device : devices_)
    {
        for (auto& block : device.getBlocks())
        {
            if (block->isReadable())
            {
                LOG(COMM) << "Updating block '" << block->getName() << "' for device '" << device.getLabel() << "'";
                block->receive();
            }
        }
    }
}

void SilecsDesign::receiveForAllDevices(const std::string& blockName)
{
    if (paramConfig_.protocolModeID == ProtocolMode::DeviceMode)
    {
        for (auto& device : devices_)
        {
            if (device.hasBlock(blockName))
            {
                device.recv(blockName);
            }
        }
    }
    else
    {
        recvAllBlockMode(blockName);
    }
}

void SilecsDesign::sendForAllDevices(const std::string& blockName)
{
    if (paramConfig_.protocolModeID == ProtocolMode::DeviceMode)
    {
        for (auto& device : devices_)
        {
            if (device.hasBlock(blockName))
            {
                device.send(blockName);
            }
        }
    }
    else
    {
        sendAllBlockMode(blockName);
    }
}

void SilecsDesign::receiveAll()
{
    if (paramConfig_.protocolModeID == ProtocolMode::DeviceMode)
    {
        for (auto& device : devices_)
        {
            device.recv();
        }
    }
    else
    {
        if (devices_.empty())
        {
            return;
        }
        for (auto& block : devices_.front().getBlocks())
        {
            recvAllBlockMode(block->getName());
        }
    }
}

const std::unique_ptr<Block>& SilecsDesign::getBlockFromFirstDevice(const std::string& blockName) const
{
    for (auto& device : devices_)
    {
        for (auto& block : device.getBlocks())
        {
            if (block->getName() == blockName)
            {
                return block;
            }
        }
    }
    std::ostringstream error;
    error << "The block '" << blockName << "' was not found.";
    throw SilecsException(__FILE__, __LINE__, error.str());
}

void SilecsDesign::recvAllBlockMode(const std::string& blockName)
{
    auto numDevicesWithBlock = getNrDevicesWithBlock(blockName);
    if (numDevicesWithBlock == 0)
    {
        LOG(DEBUG) << "Skipping recvAllBlockMode '" << className_ << "' has no devices with block '" << blockName << "'";
        return;
    }
    
    auto& block = getBlockFromFirstDevice(blockName); // Get first device's block.
    
    unsigned long address = block->getAddress();
    unsigned long memSize = block->getMemSize();
    unsigned long bufferSize = memSize * numDevicesWithBlock;
    unsigned long offset = 0;

    AccessArea area = block->getAccessArea();

    std::vector<unsigned char> buffer(bufferSize, 0);

    int errorCode = 0;

    // Try to open the connection, if it fails. Throw an exception. On success all the
    // PLC blocks will be updated this function will be called recursively within doOpen.
    // Once all blocks are updated continue from here.
    if (!plcConnection_.doOpen())
    {
        throw SilecsException{__FILE__, __LINE__, COMM_CONNECT_FAILURE};
    }

    switch (area)
    {
    case AccessArea::Digital:
        errorCode = plcConnection_.readDIO(address, offset, bufferSize, buffer.data());
        break;
    case AccessArea::Analog:
    {
        errorCode = plcConnection_.readAIO(address, offset, bufferSize, buffer.data());
        break;
    }
    case AccessArea::Memory:
    default:
        errorCode = plcConnection_.readMemory(address, offset, bufferSize, buffer.data());
        break;
    }

    if (plcConnection_.isConnected() && (errorCode == 0))
    {
        timeval tod;
        gettimeofday(&tod, 0);

        //Extract all registers value of all devices of this Design from that block
        for (auto& device : devices_)
        {
            // Only extract if this device actually contains this block, otherwise skip it.
            if (!device.hasBlock(blockName))
            {
                continue;
            }
            auto& devBlock = device.getBlock(blockName);
            unsigned long deviceOffset = device.getInputAddress(area) * memSize;
            unsigned char* pBuffer = buffer.data() + deviceOffset;
            devBlock->importRegisters(pBuffer, tod);
        }
    }
    if (errorCode != 0)
    {
        throw SilecsException(__FILE__, __LINE__, "recvAllBlockMode error for block " + blockName + ". Error code: " + std::to_string(errorCode));
    }
}

void SilecsDesign::sendAllBlockMode(const std::string& blockName)
{
    auto numDevicesWithBlock = getNrDevicesWithBlock(blockName);
    if (numDevicesWithBlock == 0)
    {
        LOG(DEBUG) << "Skipping sendAllBlockMode '" << className_ << "' has no devices with block '" << blockName << "'";
        return;
    }
    auto& block = getBlockFromFirstDevice(blockName); // Get first device's block.
    
    unsigned long address = block->getAddress();
    unsigned long memSize = block->getMemSize();
    unsigned long bufferSize = memSize * numDevicesWithBlock;
    unsigned long offset = 0;

    AccessArea area = block->getAccessArea();

    std::vector<unsigned char> buffer(bufferSize, 0);

    int errorCode = 0;

    // Try to open the connection, if it fails. Throw an exception. On success all the
    // PLC blocks will be updated this function will be called recursively within doOpen.
    // Once all blocks are updated continue from here.
    if (!plcConnection_.doOpen())
    {
        throw SilecsException{__FILE__, __LINE__, COMM_CONNECT_FAILURE};
    }

    if (plcConnection_.isEnabled())
    {
        //PLC context: Export all registers value of all devices of the PLC to that block
        for (auto& device : devices_)
        {
            // Only if this device actually contains this block, otherwise skip it.
            if (!device.hasBlock(blockName))
            {
                continue;
            }
            auto& devBlock = device.getBlock(blockName);
            unsigned long deviceOffset = device.getOutputAddress(area) * memSize;
            unsigned char* pBuffer = buffer.data() + deviceOffset;
            devBlock->exportRegisters(pBuffer);
        }
    }
    switch (area)
    {
    case AccessArea::Digital:
        errorCode = plcConnection_.writeDIO(address, offset, bufferSize, buffer.data());
        break;
    case AccessArea::Analog:
    {
        errorCode = plcConnection_.writeAIO(address, offset, bufferSize, buffer.data());
        break;
    }
    case AccessArea::Memory:
    default:
        errorCode = plcConnection_.writeMemory(address, offset, bufferSize, buffer.data());
        break;
    }

    if (errorCode != 0)
    {
        throw SilecsException(__FILE__, __LINE__, "sendAllBlockMode error for block " + blockName + ". Error code: " + std::to_string(errorCode));
    }
}

unsigned int SilecsDesign::getNrDevicesWithBlock(const std::string& blockName)
{
    unsigned int devicesWithBlock = 0;
    for (auto& device : devices_)
    {
        if (device.hasBlock(blockName))
        {
            devicesWithBlock++;
        }
    }
    return devicesWithBlock;
}

} // namespace Silecs

