/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifdef NI_SUPPORT_ENABLED
#include "CNVRegister.h"

namespace Silecs
{
    // Constructor from super class
    CNVRegister::CNVRegister(Device* theDevice, ElementXML* pDesignEl) : Register(theDevice, pDesignEl)
    {
    }

    // Specialized Destructor
    CNVRegister::~CNVRegister()
    {

    }

    /* Check for errors */
    void CNVRegister::errChk(int code)
    {
        if(code != 0)
        {
            LOG(DEBUG) << "CNV Exception raised: " << CNVGetErrorDescription(code);
            throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, std::string(CNVGetErrorDescription(code))+" in register "+ name_);
        }
    }

    template <typename T>
    inline void CNVRegister::swapInputArray2D(FormatType F, uint32_t dim1, uint32_t dim2)
    {
        T *temp = (T*)calloc(dim1*dim2, size_);
        unsigned long i = 0;
        unsigned long index = 0;
        for (unsigned long j=0; j < dimension1_; j++)
        {
            i = j;
            while(i < (dimension1_*dimension2_))
            {
                temp[index] = ((T*)pRecvValue_)[i];
                index++;
                i += dimension1_;
            }
        }

        memcpy(pRecvValue_, (void *)temp, size_*dimension1_*dimension2_);
        free(temp);
    }

    template <typename T>
    inline void CNVRegister::swapOutputArray2D(FormatType F, uint32_t dim1, uint32_t dim2)
    {
        T *temp = (T*)calloc(dim1*dim2, size_);
        unsigned long i = 0;
        unsigned long index = 0;
        for (unsigned long j=0; j < dimension2_; j++)
        {
            i = j;
            while(i < (dimension1_*dimension2_))
            {
                temp[index] = ((T*)pSendValue_)[i];
                index++;
                i += dimension2_;
            }
        }

        memcpy(pSendValue_, (void *)temp, size_*dimension1_*dimension2_);
        free(temp);
    }

    void CNVRegister::swapInputUInt8Array2D(uint32_t dim1, uint32_t dim2)
    {   swapInputArray2D<uint8_t>(uInt8, dim1, dim2);}
    void CNVRegister::swapInputInt8Array2D(uint32_t dim1, uint32_t dim2)
    {   swapInputArray2D<int8_t>(Int8, dim1, dim2);}
    void CNVRegister::swapInputUInt16Array2D(uint32_t dim1, uint32_t dim2)
    {   swapInputArray2D<uint16_t>(uInt16, dim1, dim2);}
    void CNVRegister::swapInputInt16Array2D(uint32_t dim1, uint32_t dim2)
    {   swapInputArray2D<int16_t>(Int16, dim1, dim2);}
    void CNVRegister::swapInputUInt32Array2D(uint32_t dim1, uint32_t dim2)
    {   swapInputArray2D<uint32_t>(uInt32, dim1, dim2);}
    void CNVRegister::swapInputInt32Array2D(uint32_t dim1, uint32_t dim2)
    {   swapInputArray2D<int32_t>(Int32, dim1, dim2);}
    void CNVRegister::swapInputUInt64Array2D(uint32_t dim1, uint32_t dim2)
    {   swapInputArray2D<uint64_t>(uInt64, dim1, dim2);}
    void CNVRegister::swapInputInt64Array2D(uint32_t dim1, uint32_t dim2)
    {   swapInputArray2D<int64_t>(Int64, dim1, dim2);}
    void CNVRegister::swapInputFloat32Array2D(uint32_t dim1, uint32_t dim2)
    {   swapInputArray2D<float>(Float32, dim1, dim2);}
    void CNVRegister::swapInputFloat64Array2D(uint32_t dim1, uint32_t dim2)
    {   swapInputArray2D<double>(Float64, dim1, dim2);}
    void CNVRegister::swapInputDateArray2D(uint32_t dim1, uint32_t dim2)
    {   swapInputArray2D<double>(Date, dim1, dim2);}

    void CNVRegister::swapOutputUInt8Array2D(uint32_t dim1, uint32_t dim2)
    {   swapOutputArray2D<uint8_t>(uInt8, dim1, dim2);}
    void CNVRegister::swapOutputInt8Array2D(uint32_t dim1, uint32_t dim2)
    {   swapOutputArray2D<int8_t>(Int8, dim1, dim2);}
    void CNVRegister::swapOutputUInt16Array2D(uint32_t dim1, uint32_t dim2)
    {   swapOutputArray2D<uint16_t>(uInt16, dim1, dim2);}
    void CNVRegister::swapOutputInt16Array2D(uint32_t dim1, uint32_t dim2)
    {   swapOutputArray2D<int16_t>(Int16, dim1, dim2);}
    void CNVRegister::swapOutputUInt32Array2D(uint32_t dim1, uint32_t dim2)
    {   swapOutputArray2D<uint32_t>(uInt32, dim1, dim2);}
    void CNVRegister::swapOutputInt32Array2D(uint32_t dim1, uint32_t dim2)
    {   swapOutputArray2D<int32_t>(Int32, dim1, dim2);}
    void CNVRegister::swapOutputUInt64Array2D(uint32_t dim1, uint32_t dim2)
    {   swapOutputArray2D<uint64_t>(uInt64, dim1, dim2);}
    void CNVRegister::swapOutputInt64Array2D(uint32_t dim1, uint32_t dim2)
    {   swapOutputArray2D<int64_t>(Int64, dim1, dim2);}
    void CNVRegister::swapOutputFloat32Array2D(uint32_t dim1, uint32_t dim2)
    {   swapOutputArray2D<float>(Float32, dim1, dim2);}
    void CNVRegister::swapOutputFloat64Array2D(uint32_t dim1, uint32_t dim2)
    {   swapOutputArray2D<double>(Float64, dim1, dim2);}
    void CNVRegister::swapOutputDateArray2D(uint32_t dim1, uint32_t dim2)
    {   swapOutputArray2D<double>(Date, dim1, dim2);}

    void CNVRegister::setRegIntoStructBuffer(CNVData *buffer,int bufferSize, CNVData reg, int regOffset)
    {

        CNVData *temp = (CNVData*)calloc(bufferSize,sizeof(CNVData));
        try
        {
            // split struct into fields
            errChk(CNVGetStructFields (*buffer,temp,bufferSize));

            // Dispose the single value that will be replaced
            errChk(CNVDisposeData(temp[regOffset]));

            // substitute value inside the appropriated field
            temp[regOffset] = reg;

            // re-packs up the struct
            errChk(CNVSetStructDataValue(*buffer,temp,bufferSize));
        }
        catch (const SilecsException& ex)
        {
            // Cleanup before forwarding the exception
            for(int i=0;i<bufferSize;i++)
            CNVDisposeData(temp[i]);
            free(temp);
            throw;
        }
        // Dispose the temporary CNV data
        for(int i=0;i<bufferSize;i++)
        CNVDisposeData(temp[i]);
        // Free the pointer
        free(temp);
    }

    CNVData CNVRegister::getRegfromStructBuffer(CNVData buffer,int bufferSize, int regOffset)
    {

        CNVData *allFields = (CNVData*)calloc(bufferSize,sizeof(CNVData));
        CNVData returnValue;
        try
        {
            // split struct into fields
            errChk(CNVGetStructFields(buffer,allFields,bufferSize));

            returnValue = allFields[regOffset];
        }
        catch (const SilecsException& ex)
        {
            // Cleanup before forwarding the exception
            for(int i=0;i<bufferSize;i++)
            if(i!=regOffset)
            errChk(CNVDisposeData(allFields[i]));
            free(allFields);
            throw;
        }

        for(int i=0;i<bufferSize;i++)
        if(i!=regOffset)
        errChk(CNVDisposeData(allFields[i]));

        free(allFields);
        // return the researched index
        return returnValue;
    }

    void CNVRegister::importValue(void* pBuffer, timeval ts)
    {
        LOG(DEBUG) << "CNVRegister::importValue Reg " << name_ << " dim1: " << dimension1_ << " dim2: " << dimension2_;

        unsigned long structBufferSize = this->getDevice()->getRegisterCollection(this->getBlockName()).size();

        CNVData regContent; // will contain the register value in a CNVData format.

        if (this->getDevice()->getPLC()->getProtocolModeID()==DeviceMode)
        {
            // DEVICE MODE { BlockStruct.reg(1-n) }
            regContent = getRegfromStructBuffer((CNVData)(*(CNVData*) pBuffer), static_cast<int>(structBufferSize), static_cast<int>(address_));
        }
        else
        {
            // BLOCK MODE { Device[BlockStruct.reg(1-n)] }
            LOG(ERROR) << "Block mode not supported for shared variables.";
            throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, "Block mode not supported for shared variables. Error in register "+ name_);
        }

        if( dimension2_ == 1) // scalar or 1d array
        {
            if( dimension1_== 1 ) // the register contains a scalar
            {
                // extract the register out of the struct
                switch(type_)
                {
                    case uInt8:
                    {   errChk(CNVGetScalarDataValue(regContent, CNVUInt8, pRecvValue_)); break;}
                    case Int8:
                    {   errChk(CNVGetScalarDataValue(regContent, CNVInt8, pRecvValue_)); break;}
                    case uInt16:
                    {   errChk(CNVGetScalarDataValue(regContent, CNVUInt16, pRecvValue_)); break;}
                    case Int16:
                    {   errChk(CNVGetScalarDataValue(regContent, CNVInt16, pRecvValue_)); break;}
                    case uInt32:
                    {   errChk(CNVGetScalarDataValue(regContent, CNVUInt32, pRecvValue_)); break;}
                    case Int32:
                    {   errChk(CNVGetScalarDataValue(regContent, CNVInt32, pRecvValue_)); break;}
                    case uInt64:
                    {   errChk(CNVGetScalarDataValue(regContent, CNVUInt64, pRecvValue_)); break;}
                    case Int64:
                    {   errChk(CNVGetScalarDataValue(regContent, CNVInt64, pRecvValue_)); break;}
                    case Float32:
                    {   errChk(CNVGetScalarDataValue(regContent, CNVSingle, pRecvValue_)); break;}
                    case Float64:
                    {   errChk(CNVGetScalarDataValue(regContent, CNVDouble, pRecvValue_)); break;}
                    // CNV does not support date. Double is used instead
                    case Date:
                    {   errChk(CNVGetScalarDataValue(regContent, CNVDouble, pRecvValue_)); break;}
                    default:
                    {
                        LOG(ERROR) << "Unknown shared variable format.";
                        throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, "Unknown shared variable format. Error in register "+ name_);
                    }
                }
            }
            else // the register contains a 1D array
            {
                // retrieve dimension of the array
                long unsigned int dimension[1];
                errChk(CNVGetArrayDataDimensions (regContent, 1, dimension));

                // if received register dimension do not match with one retrieved from the XML throw an exception
                if(dimension1_!=dimension[0])
                {
                    LOG(ERROR) << "Reg " << name_ << " mismatching dimension1. Expected: " << dimension1_ << " received: " << dimension[0];
                    throw SilecsException(__FILE__, __LINE__, DATA_ARRAY_LENGTH_MISMATCH, name_);
                }

                // extract the vector content
                switch(type_)
                {
                    case uInt8:
                    {   errChk(CNVGetArrayDataValue (regContent, CNVUInt8, pRecvValue_, dimension1_)); break;}
                    case Int8:
                    {   errChk(CNVGetArrayDataValue (regContent, CNVInt8, pRecvValue_, dimension1_)); break;}
                    case uInt16:
                    {   errChk(CNVGetArrayDataValue (regContent, CNVUInt16, pRecvValue_, dimension1_)); break;}
                    case Int16:
                    {   errChk(CNVGetArrayDataValue (regContent, CNVInt16, pRecvValue_, dimension1_)); break;}
                    case uInt32:
                    {   errChk(CNVGetArrayDataValue (regContent, CNVUInt32, pRecvValue_, dimension1_)); break;}
                    case Int32:
                    {   errChk(CNVGetArrayDataValue (regContent, CNVInt32, pRecvValue_, dimension1_)); break;}
                    case uInt64:
                    {   errChk(CNVGetArrayDataValue (regContent, CNVUInt64, pRecvValue_, dimension1_)); break;}
                    case Int64:
                    {   errChk(CNVGetArrayDataValue (regContent, CNVInt64, pRecvValue_, dimension1_)); break;}
                    case Float32:
                    {   errChk(CNVGetArrayDataValue (regContent, CNVSingle, pRecvValue_, dimension1_)); break;}
                    case Float64:
                    {   errChk(CNVGetArrayDataValue (regContent, CNVDouble, pRecvValue_, dimension1_)); break;}
                    // CNV does not support date. Double is used instead
                    case Date:
                    {   errChk(CNVGetArrayDataValue (regContent, CNVDouble, pRecvValue_, dimension1_)); break;}
                    default:
                    {
                        LOG(ERROR) << "Unknown shared variable format.";
                        throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, "Unknown shared variable format. Error in register "+ name_);
                    }
                }
            }
        }
        else // dim2 >1: the register contains a 2d array
        {
            long unsigned int dimension[2];
            errChk(CNVGetArrayDataDimensions (regContent, 2, dimension));
            LOG(DEBUG) << "CNVRegister::importValue Reg " << name_ << " received dim1: " << dimension[0] << " dim2: " << dimension[1];

            // if received register dimension do not match with one retrieved from the XML throw an exception
            if((dimension1_!=dimension[0]) || (dimension2_ != dimension[1]))
            throw SilecsException(__FILE__, __LINE__, DATA_ARRAY_LENGTH_MISMATCH, name_);

            // extract the vector content
            switch(type_)
            {
                case uInt8:
                {
                    errChk(CNVGetArrayDataValue (regContent, CNVUInt8, pRecvValue_, dimension1_*dimension2_));
                    swapInputUInt8Array2D(dimension1_, dimension2_);
                    break;
                }
                case Int8:
                {
                    errChk(CNVGetArrayDataValue (regContent, CNVInt8, pRecvValue_, dimension1_*dimension2_));
                    swapInputInt8Array2D(dimension1_, dimension2_);
                    break;
                }
                case uInt16:
                {
                    errChk(CNVGetArrayDataValue (regContent, CNVUInt16, pRecvValue_, dimension1_*dimension2_));
                    swapInputUInt16Array2D(dimension1_, dimension2_);
                    break;
                }
                case Int16:
                {
                    errChk(CNVGetArrayDataValue (regContent, CNVInt16, pRecvValue_, dimension1_*dimension2_));
                    swapInputInt16Array2D(dimension1_, dimension2_);
                    break;
                }
                case uInt32:
                {
                    errChk(CNVGetArrayDataValue (regContent, CNVUInt32, pRecvValue_, dimension1_*dimension2_));
                    swapInputUInt32Array2D(dimension1_, dimension2_);
                    break;
                }
                case Int32:
                {
                    errChk(CNVGetArrayDataValue (regContent, CNVInt32, pRecvValue_, dimension1_*dimension2_));
                    swapInputInt32Array2D(dimension1_, dimension2_);
                    break;
                }
                case uInt64:
                {
                    errChk(CNVGetArrayDataValue (regContent, CNVUInt64, pRecvValue_, dimension1_*dimension2_));
                    swapInputUInt64Array2D(dimension1_, dimension2_);
                    break;
                }
                case Int64:
                {
                    errChk(CNVGetArrayDataValue (regContent, CNVInt64, pRecvValue_, dimension1_*dimension2_));
                    swapInputInt64Array2D(dimension1_, dimension2_);
                    break;
                }
                case Float32:
                {
                    errChk(CNVGetArrayDataValue (regContent, CNVSingle, pRecvValue_, dimension1_*dimension2_));
                    swapInputFloat32Array2D(dimension1_, dimension2_);
                    break;
                }
                case Float64:
                {
                    errChk(CNVGetArrayDataValue (regContent, CNVDouble, pRecvValue_, dimension1_*dimension2_));
                    swapInputFloat64Array2D(dimension1_, dimension2_);
                    break;
                }
                case Date: // CNV does not support date. Double is used instead
                {
                    errChk(CNVGetArrayDataValue (regContent, CNVDouble, pRecvValue_, dimension1_*dimension2_));
                    swapInputDateArray2D(dimension1_, dimension2_);
                    break;
                }
                default:
                {
                    LOG(ERROR) << "Unknown shared variable format.";
                    throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, "Unknown shared variable format. Error in register "+ name_);
                }
            }
        }
        //Set the register time-stamp
        tod_ = ts;
        // free the register content temporary variable
        errChk(CNVDisposeData(regContent));
    }

    void CNVRegister::exportValue(void* pBuffer)
    {
        CNVData regContent; // will contain the register value in a CNVData format.

        if(dimension2_ == 1)// scalar or 1d arrray
        {
            if( dimension1_== 1 ) // the register contains a scalar
            {
                // extract the register out of the struct
                // generate the correct CNVData register content converting from the SILECS Type
                switch(type_)
                {
                    case uInt8:
                    {
                        errChk(CNVCreateScalarDataValue (&regContent, CNVUInt8, (uint8_t)(*((uint8_t *)pSendValue_))));
                        errChk(CNVSetScalarDataValue(regContent, CNVUInt8, (uint8_t)(*((uint8_t *)pSendValue_))));
                        break;
                    }
                    case Int8:
                    {
                        errChk(CNVCreateScalarDataValue (&regContent, CNVInt8, (int8_t)(*((int8_t *)pSendValue_))));
                        errChk(CNVSetScalarDataValue(regContent, CNVInt8, (int8_t)(*((int8_t *)pSendValue_))));
                        break;
                    }
                    case uInt16:
                    {
                        errChk(CNVCreateScalarDataValue (&regContent, CNVUInt16,(uint16_t)(*((uint16_t *)pSendValue_))) );
                        errChk(CNVSetScalarDataValue(regContent, CNVUInt16, (uint16_t)(*((uint16_t *)pSendValue_))));
                        break;
                    }
                    case Int16:
                    {
                        errChk(CNVCreateScalarDataValue (&regContent, CNVInt16, (int16_t)(*((int16_t *)pSendValue_))));
                        errChk(CNVSetScalarDataValue(regContent, CNVInt16, (int16_t)(*((int16_t *)pSendValue_))));
                        break;
                    }
                    case uInt32:
                    {
                        errChk(CNVCreateScalarDataValue (&regContent, CNVUInt32, (uint32_t)(*((uint32_t *)pSendValue_))));
                        errChk(CNVSetScalarDataValue(regContent, CNVUInt32, (uint32_t)(*((uint32_t *)pSendValue_))));
                        break;
                    }
                    case Int32:
                    {
                        errChk(CNVCreateScalarDataValue (&regContent, CNVInt32, (int32_t)(*((int32_t *)pSendValue_))));
                        errChk(CNVSetScalarDataValue(regContent, CNVInt32, (int32_t)(*((int32_t *)pSendValue_))));
                        break;
                    }
                    case uInt64:
                    {
                        errChk(CNVCreateScalarDataValue (&regContent, CNVUInt64, (uint64_t)(*((uint64_t *)pSendValue_))));
                        errChk(CNVSetScalarDataValue(regContent, CNVUInt64, (uint64_t)(*((uint64_t *)pSendValue_))));
                        break;
                    }
                    case Int64:
                    {
                        errChk(CNVCreateScalarDataValue (&regContent, CNVInt64, (int64_t)(*((int64_t *)pSendValue_))));
                        errChk(CNVSetScalarDataValue(regContent, CNVInt64, (int64_t)(*((int64_t *)pSendValue_))));
                        break;
                    }
                    case Float32:
                    {
                        errChk(CNVCreateScalarDataValue (&regContent, CNVSingle, (float)(*((float*)pSendValue_))));
                        errChk(CNVSetScalarDataValue(regContent, CNVSingle, (float)(*((float*)pSendValue_))));
                        break;
                    }
                    case Float64:
                    {
                        errChk(CNVCreateScalarDataValue (&regContent, CNVDouble, (double)(*((double *)pSendValue_))));
                        errChk(CNVSetScalarDataValue(regContent, CNVDouble, (double)(*((double *)pSendValue_))));
                        break;
                    }
                    case Date: // CNV does not support date. Double is used instead
                    {
                        errChk(CNVCreateScalarDataValue (&regContent, CNVDouble, (double)(*((double *)pSendValue_))));
                        errChk(CNVSetScalarDataValue(regContent, CNVDouble, (double)(*((double *)pSendValue_))));
                        break;
                    }
                    default:
                    {
                        LOG(ERROR) << "Unknown shared variable format.";
                        throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, "Unknown shared variable format. Error in register "+ name_);
                    }
                }
            }
            else // the register contains an array
            {
                long unsigned int dimensions[1];
                dimensions[0] = dimension1_;

                switch(type_)
                {
                    case uInt8:
                    {
                        errChk(CNVCreateArrayDataValue (&regContent, CNVUInt8, (uint8_t *)pSendValue_, 1, dimensions));
                        errChk(CNVSetArrayDataValue (regContent, CNVUInt8, (uint8_t *)pSendValue_, 1, dimensions));
                        break;
                    }
                    case Int8:
                    {
                        errChk(CNVCreateArrayDataValue (&regContent, CNVInt8, (int8_t *)pSendValue_, 1, dimensions));
                        errChk(CNVSetArrayDataValue (regContent, CNVInt8, (int8_t *)pSendValue_, 1, dimensions));
                        break;
                    }
                    case uInt16:
                    {
                        errChk(CNVCreateArrayDataValue (&regContent, CNVUInt16, (uint16_t *)pSendValue_, 1, dimensions));
                        errChk(CNVSetArrayDataValue (regContent, CNVUInt16, (uint16_t *)pSendValue_, 1, dimensions));
                        break;
                    }
                    case Int16:
                    {
                        errChk(CNVCreateArrayDataValue (&regContent, CNVInt16, (int16_t *)pSendValue_, 1, dimensions));
                        errChk(CNVSetArrayDataValue (regContent, CNVInt16, (int16_t *)pSendValue_, 1, dimensions));
                        break;
                    }
                    case uInt32:
                    {
                        errChk(CNVCreateArrayDataValue (&regContent, CNVUInt32, (uint32_t *)pSendValue_, 1, dimensions));
                        errChk(CNVSetArrayDataValue (regContent, CNVUInt32, (uint32_t *)pSendValue_, 1, dimensions));
                        break;
                    }
                    case Int32:
                    {
                        errChk(CNVCreateArrayDataValue (&regContent, CNVInt32, (int32_t *)pSendValue_, 1, dimensions));
                        errChk(CNVSetArrayDataValue (regContent, CNVInt32, (int32_t *)pSendValue_, 1, dimensions));
                        break;
                    }
                    case uInt64:
                    {
                        errChk(CNVCreateArrayDataValue (&regContent, CNVUInt64, (uint64_t *)pSendValue_, 1, dimensions));
                        errChk(CNVSetArrayDataValue (regContent, CNVUInt64, (uint64_t *)pSendValue_, 1, dimensions));
                        break;
                    }
                    case Int64:
                    {
                        errChk(CNVCreateArrayDataValue (&regContent, CNVInt64, (int64_t *)pSendValue_, 1, dimensions));
                        errChk(CNVSetArrayDataValue (regContent, CNVInt64, (int64_t *)pSendValue_, 1, dimensions));
                        break;
                    }
                    case Float32:
                    {
                        errChk(CNVCreateArrayDataValue (&regContent, CNVSingle, (float *)pSendValue_, 1, dimensions));
                        errChk(CNVSetArrayDataValue (regContent, CNVSingle, (float *)pSendValue_, 1, dimensions));
                        break;
                    }
                    case Float64:
                    {
                        errChk(CNVCreateArrayDataValue (&regContent, CNVDouble, (double *)pSendValue_, 1, dimensions));
                        errChk(CNVSetArrayDataValue (regContent, CNVDouble, (double *)pSendValue_, 1, dimensions));
                        break;
                    }
                    case Date: // CNV does not support date. Double is used instead
                    {
                        errChk(CNVCreateArrayDataValue (&regContent, CNVDouble, (double *)pSendValue_, 1, dimensions));
                        errChk(CNVSetArrayDataValue (regContent, CNVDouble, (double *)pSendValue_, 1, dimensions));
                        break;
                    }
                    default:
                    {
                        LOG(ERROR) << "Unknown shared variable format.";
                        throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, "Unknown shared variable format. Error in register "+ name_);
                    }
                }
            }
        }
        else // 2d array
        {
            long unsigned int dimensions[2];
            dimensions[0] = dimension1_;
            dimensions[1] = dimension2_;

            switch(type_)
            {
                case uInt8:
                {
                    swapOutputUInt8Array2D(dimension1_, dimension2_);
                    errChk(CNVCreateArrayDataValue (&regContent, CNVUInt8, (uint8_t *)pSendValue_, 2, dimensions));
                    errChk(CNVSetArrayDataValue (regContent, CNVUInt8, (uint8_t *)pSendValue_, 2, dimensions));
                    break;
                }
                case Int8:
                {
                    swapOutputInt8Array2D(dimension1_, dimension2_);
                    errChk(CNVCreateArrayDataValue (&regContent, CNVInt8, (int8_t *)pSendValue_, 2, dimensions));
                    errChk(CNVSetArrayDataValue (regContent, CNVInt8, (int8_t *)pSendValue_, 2, dimensions));
                    break;
                }
                case uInt16:
                {
                    swapOutputUInt16Array2D(dimension1_, dimension2_);
                    errChk(CNVCreateArrayDataValue (&regContent, CNVUInt16, (uint16_t *)pSendValue_, 2, dimensions));
                    errChk(CNVSetArrayDataValue (regContent, CNVUInt16, (uint16_t *)pSendValue_, 2, dimensions));
                    break;
                }
                case Int16:
                {
                    swapOutputInt16Array2D(dimension1_, dimension2_);
                    errChk(CNVCreateArrayDataValue (&regContent, CNVInt16, (int16_t *)pSendValue_, 2, dimensions));
                    errChk(CNVSetArrayDataValue (regContent, CNVInt16, (int16_t *)pSendValue_, 2, dimensions));
                    break;
                }
                case uInt32:
                {
                    swapOutputUInt32Array2D(dimension1_, dimension2_);
                    errChk(CNVCreateArrayDataValue (&regContent, CNVUInt32, (uint32_t *)pSendValue_, 2, dimensions));
                    errChk(CNVSetArrayDataValue (regContent, CNVUInt32, (uint32_t *)pSendValue_, 2, dimensions));
                    break;
                }
                case Int32:
                {
                    swapOutputInt32Array2D(dimension1_, dimension2_);
                    errChk(CNVCreateArrayDataValue (&regContent, CNVInt32, (int32_t *)pSendValue_, 2, dimensions));
                    errChk(CNVSetArrayDataValue (regContent, CNVInt32, (int32_t *)pSendValue_, 2, dimensions));
                    break;
                }
                case uInt64:
                {
                    swapOutputUInt64Array2D(dimension1_, dimension2_);
                    errChk(CNVCreateArrayDataValue (&regContent, CNVUInt64, (uint64_t *)pSendValue_, 2, dimensions));
                    errChk(CNVSetArrayDataValue (regContent, CNVUInt64, (uint64_t *)pSendValue_, 2, dimensions));
                    break;
                }
                case Int64:
                {
                    swapOutputInt64Array2D(dimension1_, dimension2_);
                    errChk(CNVCreateArrayDataValue (&regContent, CNVInt64, (int64_t *)pSendValue_, 2, dimensions));
                    errChk(CNVSetArrayDataValue (regContent, CNVInt64, (int64_t *)pSendValue_, 2, dimensions));
                    break;
                }
                case Float32:
                {
                    swapOutputFloat32Array2D(dimension1_, dimension2_);
                    errChk(CNVCreateArrayDataValue (&regContent, CNVSingle, (float *)pSendValue_, 2, dimensions));
                    errChk(CNVSetArrayDataValue (regContent, CNVSingle, (float *)pSendValue_, 2, dimensions));
                    break;
                }
                case Float64:
                {
                    swapOutputFloat64Array2D(dimension1_, dimension2_);
                    errChk(CNVCreateArrayDataValue (&regContent, CNVDouble, (double *)pSendValue_, 2, dimensions));
                    errChk(CNVSetArrayDataValue (regContent, CNVDouble, (double *)pSendValue_, 2, dimensions));
                    break;
                }
                case Date:
                {
                    swapOutputDateArray2D(dimension1_, dimension2_);
                    // CNV does not support date. Double is used instead
                    errChk(CNVCreateArrayDataValue (&regContent, CNVDouble, (double *)pSendValue_, 2, dimensions));
                    errChk(CNVSetArrayDataValue (regContent, CNVDouble, (double *)pSendValue_, 2, dimensions));
                    break;
                }
                default:
                {
                    LOG(ERROR) << "Unknown shared variable format.";
                    throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, "Unknown shared variable format. Error in register "+ name_);
                }
            }
        }

        unsigned long structBufferSize = this->getDevice()->getRegisterCollection(this->getBlockName()).size();
        if (this->getDevice()->getPLC()->getProtocolModeID()==DeviceMode)
        {
            // DEVICE MODE { BlockStruct.reg(1-n) }
            setRegIntoStructBuffer((CNVData*) pBuffer, static_cast<int>(structBufferSize), regContent, static_cast<int>(address_));
        }
        else
        {
            // BLOCK MODE { Device[BlockStruct.reg(1-n)] }

            LOG(ERROR) << "Block mode not supported for shared variables.";
            throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, "Block mode not supported for shared variables. Error in register "+ name_);
        }
    }

    void CNVRegister::importString(void* pBuffer, timeval ts)
    {
        unsigned long structBufferSize = this->getDevice()->getRegisterCollection(this->getBlockName()).size();
        std::string** pRecvStringValue_ = static_cast<std::string**>(pRecvValue_);
        CNVData regContent; // will contain the register value in a CNVData format

        if (this->getDevice()->getPLC()->getProtocolModeID()==DeviceMode)
        {
            // DEVICE MODE { BlockStruct.reg(1-n) }
            regContent = getRegfromStructBuffer((CNVData)(*(CNVData*) pBuffer), static_cast<int>(structBufferSize), static_cast<int>(address_));
        }
        else
        {
            // BLOCK MODE { Device[BlockStruct.reg(1-n)] }
            LOG(ERROR) << "Block mode not supported for shared variables.";
            throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, "Block mode not supported for shared variables. Error in register "+ name_);
        }

        char *temp[dimension1_*dimension2_];

        if( dimension2_ == 1) // scalar or 1D array
        {
            if( dimension1_== 1 ) // the register contains a scalar
            {
                errChk(CNVGetScalarDataValue(regContent, CNVString, temp));
                pRecvStringValue_[0]->assign((const char*)temp[0]);
            }
            else // the register contains a 1D array
            {
                // retrieve dimension of the mono-dimensional array
                long unsigned int dimension[1];
                errChk(CNVGetArrayDataDimensions (regContent, 1, dimension));

                // if received register dimension do not match with one retrieved from the XML throw an exception
                if(dimension1_!=dimension[0])
                throw SilecsException(__FILE__, __LINE__, DATA_ARRAY_LENGTH_MISMATCH, name_);

                errChk(CNVGetArrayDataValue(regContent, CNVString, temp, dimension1_));
                for (unsigned long i = 0; i < dimension1_; i++)
                pRecvStringValue_[i]->assign((const char*)temp[i]);
            }
        }
        else // the register contains a 2D array
        {
            // retrieve dimensions of double array
            long unsigned int dimensions[2];
            errChk(CNVGetArrayDataDimensions (regContent, 2, dimensions));

            // if received register dimensions do not match with the ones retrieved from the XML throw an exception
            if((dimension1_!=dimensions[0]) || (dimension2_!=dimensions[1]))
            throw SilecsException(__FILE__, __LINE__, DATA_ARRAY_LENGTH_MISMATCH, name_);

            // Warning: this method returns the element of the matrix in row-major order
            errChk(CNVGetArrayDataValue(regContent, CNVString, temp, dimension1_*dimension2_));

            // The CNV library reads the element in row-major order, whereas the SILECS CLIB works in column-major order.
            // The following code swaps rows and columns, returning the content of pRecvValue_ correctly ordered
            unsigned long i = 0;
            unsigned long index = 0;
            for (unsigned long j=0; j < dimension1_; j++)
            {
                i = j;
                while(i < (dimension1_*dimension2_))
                {
                    pRecvStringValue_[index]->assign((const char*)temp[i]);
                    index++;
                    i += dimension1_;
                }
            }
        }

        // Set the register time-stamp
        tod_ = ts;

        // free the register content temporary variable
        errChk(CNVDisposeData(regContent));
    }

    void CNVRegister::exportString(void* pBuffer)
    {
        std::string** pSendStringValue_ = static_cast<std::string**>(pSendValue_);
        CNVData regContent; // will contain the register value in a CNVData format.
        char *temp[dimension1_*dimension2_];

        if(dimension2_ == 1)// scalar or 1D arrray
        {
            if( dimension1_== 1 ) // the register contains a scalar
            {
                // generate the correct CNVData register content
                errChk(CNVCreateScalarDataValue (&regContent, CNVString, (*pSendStringValue_[0]).data()));
                errChk(CNVSetScalarDataValue(regContent, CNVString, (*pSendStringValue_[0]).data()));
            }
            else // the register contains a 1D array
            {
                long unsigned int dimensions[1];
                dimensions[0] = dimension1_;

                for (unsigned long i = 0; i < dimension1_; i++)
                temp[i] = (char*)(*pSendStringValue_[i]).data();

                errChk(CNVCreateArrayDataValue (&regContent, CNVString, temp, 1, dimensions));
                errChk(CNVSetArrayDataValue(regContent, CNVString, temp, 1, dimensions));
            }
        }
        else // the register contains a 2D array
        {
            long unsigned int dimensions[2];
            dimensions[0] = dimension1_;
            dimensions[1] = dimension2_;

            // The CNV library reads the element in row-major order, whereas the SILECS CLIB works in column-major order.
            // The following code swaps rows and columns, returning the content of pSendValue_ correctly ordered
            unsigned long i = 0;
            unsigned long index = 0;
            for (unsigned long j=0; j < dimension2_; j++)
            {
                i = j;
                while(i < (dimension1_*dimension2_))
                {
                    temp[index] = (char*)(*pSendStringValue_[i]).c_str();
                    index++;
                    i += dimension2_;
                }
            }

            errChk(CNVCreateArrayDataValue (&regContent, CNVString, temp, 2, dimensions));
            errChk(CNVSetArrayDataValue(regContent, CNVString, temp, 2, dimensions));
        }

        unsigned long structBufferSize = this->getDevice()->getRegisterCollection(this->getBlockName()).size();
        if (this->getDevice()->getPLC()->getProtocolModeID()==DeviceMode)
        {
            // DEVICE MODE { BlockStruct.reg(1-n) }
            setRegIntoStructBuffer((CNVData*) pBuffer, static_cast<int>(structBufferSize), regContent, static_cast<int>(address_));
        }
        else
        {
            // BLOCK MODE { Device[BlockStruct.reg(1-n)] }
            LOG(ERROR) << "Block mode not supported for shared variables.";
            throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, "Block mode not supported for shared variables. Error in register "+ name_);
        }

        // free the register content temporary variable
//        errChk(CNVDisposeData(regContent));
    }

    //Transfer the value of the recvBuffer to to the sendBuffer
    void CNVRegister::copyValue()
    {
        if (type_ == String)
        {
            std::string** pRecvStringValue_ = static_cast<std::string**>(pRecvValue_);
            std::string** pSendStringValue_ = static_cast<std::string**>(pSendValue_);
            for(unsigned int i=0; i<(dimension1_*dimension2_); i++)
            {
                *(pSendStringValue_[i]) = *(pRecvStringValue_[i]);
            }
        }
        else
        {
            memcpy(pSendValue_, pRecvValue_, (size_*dimension1_*dimension2_));
        }
    }
}

#endif //NI_SUPPORT_ENABLED
