/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _SILECS_PLC_BLOCK_H_
#define _SILECS_PLC_BLOCK_H_

#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/equipment/SilecsParamConfig.h>

namespace Silecs
{

class PLCBlock : public Block
{

public:
    PLCBlock(const ElementXML& blockNode, long deviceInputAddress, long deviceOutputAddress, Connection& plcConnection, const SilecsParamConfig& paramConfig);
    virtual ~PLCBlock();

    void send() override;
    void receive() override;

private:

    void recvBlockMode();
    void recvDeviceMode();
    void sendBlockMode();
    void sendDeviceMode();

    void readBlock(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    void sendBlock(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);

    /// Send/Receive data size (size of block * nb of device)
    unsigned long bufferSize_;

    long deviceInputAddress_;
    long deviceOutputAddress_;

    SilecsParamConfig paramConfig_;
};

} // namespace

#endif // _SILECS_BLOCK_H_
