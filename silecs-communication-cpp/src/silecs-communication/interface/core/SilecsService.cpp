/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/utility/StringUtilities.h>
#include <unistd.h>

#include <cstring>

#include <libgen.h>

/* **********************************************************************
 * Global data definition related to the software install and versioning
 * Must be up-to-date for each release in connection with others packages
 * (Java tool and Environment software (Python scripts, ..)
 * **********************************************************************
 */
#ifndef MAJOR
#error "Silecs::Service: MAJOR SYMBOL NOT DEFINED! Make sure to define symbols MAJOR, MINOR and PATCH in your Makefile."
#endif

namespace Silecs
{
#define STRINGIFY2(X) #X
#define STRINGIFY(X) STRINGIFY2(X)

//Semantic versioning for SILECS packages (Tools, Environment and client library have consistent major number)
const std::string Service::semverMajor_ = STRINGIFY(MAJOR); //Major release, not backward compatible
const std::string Service::semverMinor_ = STRINGIFY(MINOR); //Minor release, backward compatible
const std::string Service::semverPatch_ = STRINGIFY(PATCH); //Bug fixes, backward compatible
const std::string Service::developmentVersion_ = "DEV"; // Version which is used for unreleased code

} // namespace

/* **********************************************************************
 * **********************************************************************
 */
namespace Silecs
{
// static definition
Service* Service::instance_ = NULL;
bool Service::isShutingDown_ = false;
std::string Service::userParamFilePath_;

Service* Service::getInstance(int argc, char ** argv)
{
    if (instance_ == NULL)
    {
        instance_ = new Service(argc, argv);
        LOG((ALLOC)) << "SILECS Service create";

        XMLParser::init();

        //ACET start-up message (once per process)
        TRACE("info") << "SILECS service startup: libsilecs-comm " << semverMajor_ << "." << semverMinor_ << "." << semverPatch_;
    }
    return instance_;
}

void Service::deleteInstance()
{
    if (!isShutingDown_)
    {
        isShutingDown_ = true;
        LOG((ALLOC)) << "SILECS Resources release";

        TRACE("info") << "SILECS service shutdown: libsilecs-comm " << semverMajor_ << "." << semverMinor_ << "." << semverPatch_;

        XMLParser::cleanUp();

        if (instance_ != NULL)
            delete instance_;
        instance_ = NULL;
    }
}

void Service::setArguments(const std::string& usrArgs)
{
    size_t pos = usrArgs.find("-plcLog");
    if (pos != std::string::npos)
    {
        std::string topics = usrArgs.substr(pos + 8);
        char *argv[3] = {(char*)"dummy", (char*)"-plcLog", (char*)topics.c_str()};
        if (setArgs(3, argv) == false)
        {
            throw SilecsException(__FILE__, __LINE__, PARAM_INCORRECT_ARGUMENTS);
        }
    }

    std::string silecsParamStr = "-silecsParamPath";
    pos = usrArgs.find(silecsParamStr);
    if (pos != std::string::npos)
    {
        auto start = pos + silecsParamStr.size();
        start = usrArgs.find_first_not_of(" ", start); // Move start by as many whitespaces as there are.
        std::string temp = usrArgs.substr(start);
        auto end = temp.find(" ");
        if (end == std::string::npos)
        {
            userParamFilePath_ = temp;
        }
        else
        {
            userParamFilePath_ = temp.substr(0, end);
        }
    }
}

bool Service::setLogTopics(const std::string& topics)
{
    bool isOK = Log::getTopicsFromString(topics);
    return isOK;
}

Service::Service(int argc, char ** argv)
{
    isShutingDown_ = false;

    //Default client process name (in case arguments are not transmitted)
    char *ident = (char*)"SILECS-Client";

    //Start the sys-logger at first
    if (argc > 0)
    { //Real process name
        ident = basename(argv[0]);
    }

    //No particular topics to log to syslog in addition to the mandatory ones (if any)
    Log::startSyslog(ident, 0);

    //Then, interpret and set the 'plc' parameters of the command line, if any
    if ( (argc > 0) && (argv != NULL))
    {
        if (setArgs(argc, argv) == false)
        {
            printArgs();
            /* or as following if required
             std::cerr << std::endl;
             std::cerr << "PLC expected arguments: " << std::endl;
             std::cerr << Silecs::whichArgs() << std::endl;
             */
            throw SilecsException(__FILE__, __LINE__, PARAM_INCORRECT_ARGUMENTS);
        }
    }
}

Service::~Service()
{
    //close the syslog resource
    Log::stopSyslog();
}

PLCHandler& Service::getPLCHandler()
{
    return plcHandler_;
}

std::string Service::getSemverMajor()
{
    return semverMajor_;
}

std::string Service::getSemverMinor()
{
    return semverMinor_;
}

std::string Service::getSemverPatch()
{
    return semverPatch_;
}

std::string Service::getVersion()
{
    if( semverMajor_ == "0" && semverMinor_ == "0" && semverPatch_ == "0" )
    {
        return developmentVersion_;
    }

    return semverMajor_ + "." + semverMinor_ + "." + semverPatch_;
}

bool Service::isVersionSupported(const std::string& version)
{
    if( version == getVersion())
        return true;

    std::size_t pos = version.find(".");
    if (pos == std::string::npos)
    {
        return false;
    }
    std::string major_string = version.substr(0,pos);
    int major_int, lib_major_int;
    std::istringstream ( major_string ) >> major_int;
    std::istringstream ( semverMajor_ ) >> lib_major_int;
    return major_int == lib_major_int;
}

const std::string Service::getBinaryFolderPath()
{
    char buf[1024] = {0};
    ssize_t size = readlink("/proc/self/exe", buf, sizeof (buf));
    if (size == 0 || size == sizeof (buf))
    {
        throw SilecsException(__FILE__, __LINE__, "Failed to get binary folder");
    }
    if (size == -1)
    {
        throw SilecsException{__FILE__, __LINE__, errno};
    }
    return dirname(buf);
}

const std::string Service::getParamFile(const std::string& controllerName)
{
    const std::string extension = ".silecsparam";

    if (!userParamFilePath_.empty())
    {
        std::string userParamFile = userParamFilePath_ + "/" + controllerName + extension;
        if (access(userParamFile.c_str(), F_OK) != -1)
        {
            LOG(DEBUG) << "Using param file: " << userParamFile;
            return userParamFile;
        }
    }

    // directly next to the binary, used on GSI frontends
    std::string localParameterFile1 = getBinaryFolderPath() + "/" + controllerName + extension;

    if (access(localParameterFile1.c_str(), F_OK) != -1)
    {
        LOG(DEBUG) << "Using local param file: " << localParameterFile1;
        return localParameterFile1;
    }

    // if FESA-class is not deployed yet and started on development-system ( used on GSI development-systems )
    std::string localParameterFile2 = getBinaryFolderPath() + "/../../../generated-silecs/client/" + controllerName + extension;
    //for debugging printf("localParameterFile2: %s",localParameterFile2.c_str());
    if (access(localParameterFile2.c_str(), F_OK) != -1)
    {
        LOG(DEBUG) << "Using local param file: " << localParameterFile2;
        return localParameterFile2;
    }

    std::string message = "No parameter-file found for controller '" + controllerName + "'";
    throw SilecsException(__FILE__, __LINE__, message.c_str());
}

bool Service::fileExists(const std::string& filename)
{
    struct stat fileInfo;

    // Attempt to get the file attributes
    if (stat(filename.c_str(), &fileInfo) == 0)
    { //No error: the file exists
        return true;
    }
    else
    {
        if (errno == ENOENT)
        { //No such file or directory
            return false;
        }
    }
    //Unexpected error
    throw SilecsException(__FILE__, __LINE__, errno);
}

bool Service::checkArgs(int argc, char ** argv)
{
    if ( (argc >= 2) && (argv != NULL))
    {
        for (int i = 0; i < argc; i++)
            if (strcmp(argv[i], "-plcHelp") == 0)
                return false;
    }
    return true; //help is not required
}

bool Service::setArgs(int argc, char ** argv)
{
    if (checkArgs(argc, argv))
        if (Log::setLogArguments(argc, argv))
            //if ..
            //if ..
            return true;
    return false;
}

std::string Service::whichArgs()
{
    return std::string("-plcHelp") + std::string("\n") + Log::getLogArguments() + std::string("\n")
    // + ..
    ;
}

void Service::printArgs()
{
    std::cerr << std::endl;
    std::cerr << "SILECS expected arguments: " << std::endl;
    std::cerr << whichArgs() << std::endl;
}

} // namespace

