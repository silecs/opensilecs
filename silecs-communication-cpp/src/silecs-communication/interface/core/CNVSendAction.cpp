/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifdef NI_SUPPORT_ENABLED
#include <silecs-communication/interface/core/SilecsAction.h>
#include <silecs-communication/interface/core/PLCAction.h>
#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/core/CNVSendAction.h>
#include <silecs-communication/interface/core/Context.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include "../communication/CNVConnection.h.obsolete"
#include "../equipment/CNVBlock.h.obsolete"

namespace Silecs
{

    CNVSendBlockMode::CNVSendBlockMode(Block* block) : PLCAction(block)
    {
        if (DEBUG & Log::topics_) LOG(ALLOC) << "Action CNVSendBlockMode (create): " << theBlock_->getName();
    }

    CNVSendBlockMode::~CNVSendBlockMode()
    {
        if (DEBUG & Log::topics_) LOG(ALLOC) << "Action CNVSendBlockMode (delete): " << theBlock_->getName();
    }

    CNVSendDeviceMode::CNVSendDeviceMode(Block* block) : PLCAction(block)
    {
        if (DEBUG & Log::topics_) LOG(ALLOC) << "Action CNVSendDeviceMode (create): " << theBlock_->getName();
    }

    CNVSendDeviceMode::~CNVSendDeviceMode()
    {
        if (DEBUG & Log::topics_) LOG(ALLOC) << "Action CNVSendDeviceMode (delete): " << theBlock_->getName();
    }

    int CNVSendBlockMode::execute(Context* pContext)
    {
        return 0;
    }

    int CNVSendDeviceMode::execute(Context* pContext)
    {
        CNVData* pBuffer;
        int errorCode = 0;
        Connection* pConn = theBlock_->getPLC()->getConnection();

        //(re)connect the PLC if needed and (re)synchronize the retentive registers
        if (pConn->doOpen())
        {
            try
            {
                //if transaction is for one device only, pContext contains its reference
                if (pContext->isForOneDevice())
                {

                    Device* pDev = pContext->getDevice();

                    pBuffer = (CNVData*)theBlock_->getBuffer();

                    { //critical section
                        Lock lock(bufferMux_);
                        if (pConn->isEnabled())
                        {
                            LOG(SEND) << "SendAction (execute/ DeviceMode): " << theBlock_->getName() << ", device: " << pDev->getLabel();

                            //Device context: Export all the device registers to that block
                            pDev->exportRegisters(theBlock_, pBuffer, pContext);
                        }

                        // compute the block address
                        std::string blockAddress = "\\\\"+pDev->getPLC()->getName()+"\\'"
                        +pDev->getPLC()->theCluster_->getClassName()+"-"+pDev->getPLC()->theCluster_->getClassVersion()+"'\\"
                        + pDev->getLabel()+"\\"+theBlock_->getName();

                        // call CNV write
                        ((CNVConnection*)pConn)->writeData(theBlock_->getPLC(), blockAddress.c_str(),*pBuffer);
                    }

                    if (SEND & Log::topics_)
                    {
                        if (pConn->isEnabled())
                        {
                            Log(SEND).getLogDelay() << "done: " << theBlock_->getName();
                        }
                    }
                }
                else
                {
                    //Cluster/PLC context: set block of all devices one by one =========================
                    deviceVectorType::iterator pDeviceIter;
                    deviceVectorType& deviceCol = theBlock_->getPLC()->getDeviceMap();
                    for(pDeviceIter = deviceCol.begin(); pDeviceIter != deviceCol.end(); ++pDeviceIter)
                    {
                        Device* pDev = pDeviceIter->second;

                        pBuffer = (CNVData*)theBlock_->getBuffer();

                        { //critical section
                            Lock lock(bufferMux_);
                            if (pConn->isEnabled())
                            {   LOG(SEND) << "SendAction (execute/ DeviceMode): " << theBlock_->getName() << ", device:" << pDev->getLabel();

                                //PLC context: Export all registers value of the current device of the PLC to that block
                                pDev->exportRegisters(theBlock_, pBuffer, pContext);
                            }

                            // compute the block address
                            std::string blockAddress = "\\\\"+pDev->getPLC()->getName()+"\\'"
                            +pDev->getPLC()->theCluster_->getClassName()+"-"+pDev->getPLC()->theCluster_->getClassVersion()+"'\\"
                            + pDev->getLabel()+"\\"+theBlock_->getName();

                            // call CNV write
                            ((CNVConnection*)pConn)->writeData(theBlock_->getPLC(), blockAddress.c_str(),*pBuffer);
                        }

                        if (SEND & Log::topics_)
                        {   if (pConn->isEnabled())
                            {   Log(SEND).getLogDelay() << "done: " << theBlock_->getName();
                            }
                        }
                    }
                }
            }
            catch (const SilecsException& ex)
            {
                //LOG(DEBUG) << "CNV SendAction (execute/ DeviceMode) failed: "<< ex.getMessage();
                //errorCode = ex.getCode();
                LOG(DEBUG) << "CNV SendAction (execute/ DeviceMode) failed";
                errorCode = -1;//no SilecsException expected so far
            }

        } //doOpen

        return errorCode;
    }

    void CNVSendBlockMode::errChk(int code) throw (std::string*)
    {
        if(code != 0)
        {
            throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, std::string(CNVGetErrorDescription(code))+" in CNVSendBlockMode");
        }
    }

    void CNVSendDeviceMode::errChk(int code) throw (std::string*)
    {
        if(code != 0)
        {
            throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, std::string(CNVGetErrorDescription(code))+" in CNVSendDeviceMode");
        }
    }

} // namespace
#endif //NI_SUPPORT_ENABLED
