/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _SILECS_SERVICE_H_
#define _SILECS_SERVICE_H_

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <set>
#include <map>

namespace Silecs
{
/// @cond
// Register and Block access types
enum class AccessType
{
    Command,
    Acquisition,
    Setting
};

// Register and Block access types
enum class AccessArea
{
    Memory,
    Digital,
    Analog
};
/// @endcond

}

#include <silecs-communication/interface/equipment/SilecsPLCHandler.h>
#include <string>

namespace Silecs
{

/*!
 * \class Service
 * \brief This class is the entry point of the SILECS service.
 * Service class is a singleton that the client process must instantiate at first.
 * It provides services to set-up the general tasks (set process arguments for instance)
 * and create the required PLC Clusters and related resources of each SILECS class:
 * PLC, Device and Register.
 */
class Service
{

public:
    /*!
     * \brief Use this method to create/get the unique process instance of the SILECS Service.
     * By creating that object it's possible to transmit specific arguments to the SILECS Service,
     * typically the SILECS logging options:
     * -plcLog ERROR[,INFO,DEBUG,SETUP,ALLOC,RECV,SEND,COMM,DATA,LOCK]
     *
     * (argc=0, argv=NULL) means no arguments for SILECS Service
     */
    static Service* getInstance(int argc = 0, char ** argv = NULL);

    /*!
     * \brief This method releases all the SILECS resources and finally removes
     * the Service singleton itself.
     * Attention! This method is responsible to disconnect all the PLCs but mostly to remove
     * all the SILECS resources (Clusters and related components: PLCs, Devices, Registers, ..)
     * Client implementation must ensure that no process is currently accessing to
     * these resources before calling.
     */
    static void deleteInstance();

    PLCHandler& getPLCHandler();

    /*!
     * \brief Use this method to get the library Semantic version (Major)
     * \return the Major number of the current release: <Major>.<Minor>.<Patch>
     */
    static std::string getSemverMajor();

    /*!
     * \brief Use this method to get the library Semantic version (Minor)
     * \return the Minor number of the current release: <Major>.<Minor>.<Patch>
     */
    static std::string getSemverMinor();

    /*!
     * \brief Use this method to get the library Semantic version (Patch)
     * \return the Patch number of the current release: <Major>.<Minor>.<Patch>
     */
    static std::string getSemverPatch();

    /*!
     * \brief Returns the version of this library in the form <Major>.<Minor>.<Patch>
     * \return the version
     */
    static std::string getVersion();

    /*!
     * \brief checks if the passed version is compartible to this library
     * \return true if compartible
     */
    static bool isVersionSupported(const std::string& version);

    /*!
     * \brief get the parameter-file for this controller
     * \return path to the parameter-file
     */
    static const std::string getParamFile(const std::string& controllerName);

    /*!
     * \brief Use this method to propagate the input arguments to the SILECS library (-plcLog in particular)
     */
    void setArguments(const std::string& usrArgs);

    /// @cond
    // TODO: For python Diagnostic only, to be improved
    bool setLogTopics(const std::string& topics);
    /// @endcond

private:

    Service(int argc, char ** argv);
    virtual ~Service();

    bool checkArgs(int argc, char ** argv);
    bool setArgs(int argc, char ** argv);
    std::string whichArgs();
    void printArgs();

    // returns folder of the binary which currently uses this library
    static const std::string getBinaryFolderPath();

    static bool fileExists(const std::string& filename);

    PLCHandler plcHandler_;

    //unique instance of the Silecs service
    static Service* instance_;

    //Semantic versioning for SILECS packages (Tools, Environment and client library consistency)
    static const std::string semverMajor_; //Major release, not backward compatible
    static const std::string semverMinor_; //Minor release, backward compatible
    static const std::string semverPatch_; //Bug fixes, backward compatible
    static const std::string developmentVersion_; // Version which is used if not released

    static std::string userParamFilePath_; // User specific path, pointing to the folder in which parameter files are searched. Can be passed via application argument

    static bool isShutingDown_;
};

} // namespace

#endif // _SILECS_SERVICE_H_
