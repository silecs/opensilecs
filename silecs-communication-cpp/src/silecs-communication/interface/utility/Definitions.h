#ifndef _SILECS_UTILITY_DEFINITIONS_H_
#define _SILECS_UTILITY_DEFINITIONS_H_

#include <string>

namespace Silecs
{

// PLC type, brand, system, protocol type and mode
typedef enum
{
    BusController,
    BusCoupler
} PLCType;

// PLC brand, system, protocol type and mode
typedef enum
{
    Siemens,
    Schneider,
    Ni,
    Digi,
    Beckhoff
} PLCBrand;

typedef enum
{
    ET200S,
    S7300,
    S7400,
    S71200,
    S71500,
    S7VIRTUAL,
    Premium,
    Quantum,
    M340,
    BC9xxx,
    CX9xxx,
    BK9xxx,
    RCM4010,
    RCM2000,
    CompactRIO,
    PXIRT,
    PXIWindows,
    PCWindows,
    OtherSupportCNV
} PLCModel;

typedef enum
{
    Step7,
    TiaPortal,
    Unity,
    TwinCat,
    StdC,
    ServerS7,
    Labview
} PLCSystem;

typedef enum
{
    MBProtocol,
    S7Protocol,
    CNVProtocol
} ProtocolType;

typedef enum
{
    BlockMode,
    DeviceMode
} ProtocolMode;
/// @endcond

// PLC unit status
typedef struct
{
    int status; //bit-pattern hardware specific (look at the related documentation)
} UnitStatusType;

// Order code
typedef struct
{
    std::string code;
    std::string version;
} UnitCodeType;

// CPU Info
typedef struct
{
    std::string moduleName;
    std::string moduleTypeName;
    std::string serialNumber;
    std::string asName;
    std::string copyright;
} CPUInfoType;

// CP Info
typedef struct
{
    int maxPduLength;
    int maxConnections;
    int maxMPIRate;
    int maxBusRate;
} CPInfoType;

} // namespace Silecs

#endif // _SILECS_UTILITY_DEFINITIONS_H_