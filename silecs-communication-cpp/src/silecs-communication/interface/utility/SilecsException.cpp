/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include "SilecsException.h"
#include "SilecsLog.h"

#include <cstring>
#include <string>

namespace Silecs
{

SilecsException::SilecsException(const std::string& file, const uint32_t line, const ErrorCode code, const std::string& extMsg)
{
    errFile_ = file;
    errLine_ = line;
    errCode_ = code;
    fillCategoryMessageFromCode(code);
    errMessage_ += extMsg;
    buildFullMessage();
    //Log the error to the std. output (if ERROR topic is enable)
    log();
}

SilecsException::SilecsException(const std::string& file, const uint32_t line, const int err)
{
    errFile_ = file;
    errLine_ = line;
    errCategory_ = CLIENT_SYSTEM_FAULT;
    errCode_ = UNEXPECTED_ERROR;
    errMessage_ = std::strerror(err);
    buildFullMessage();

    //Log the error to the std. output (if ERROR topic is enable)
    log();
}

SilecsException::SilecsException(const std::string& file, const uint32_t line, const std::string& msg)
{
    errFile_ = file;
    errLine_ = line;
    errCategory_ = CLIENT_SYSTEM_FAULT;
    errCode_ = UNEXPECTED_ERROR;
    errMessage_ = msg;
    buildFullMessage();
    //Log the error to the std. output (if ERROR topic is enable)
    log();
}

SilecsException::SilecsException(const SilecsException& ex)
{
    errMessage_ = ex.errMessage_;

    //----------------------------------
    // Not used... initialized for safety. suggested by static analysis
    errFile_ = "";
    errLine_ = -1;
    errCategory_ = CLIENT_SYSTEM_FAULT;
    errCode_ = UNEXPECTED_ERROR;
    //----------------------------------

    //Log the error to the std. output (if ERROR topic is enable)
    log();
}

SilecsException::~SilecsException() throw()
{
}

const char* SilecsException::what() const throw()
{
    return errFullMessage_.c_str();
}

ErrorCategory SilecsException::getCategory() const
{
    return errCategory_;
}

ErrorCode SilecsException::getCode() const
{
    return errCode_;
}

const char* SilecsException::getMessage() const
{
    return errMessage_.c_str();
}

void SilecsException::fillCategoryMessageFromCode(ErrorCode code)
{
    switch (code)
    {
        case XML_FILE_NOT_WELLFORMED:
            errCategory_ = XML_PARSING_FAULT;
            errMessage_ = "This XML parameters file is not well-formed: ";
            break;
        case XML_ELEMENT_NOT_FOUND:
            errCategory_ = XML_PARSING_FAULT;
            errMessage_ = "XML Element not found within the parameters file: ";
            break;
        case XML_EXPRESSION_MISMATCH:
            errCategory_ = XML_PARSING_FAULT;
            errMessage_ = "The following rule does not match any XML Element within the parameters file:\n";
            break;
        case XML_CREATE_XPATH_FAULT:
            errCategory_ = XML_PARSING_FAULT;
            errMessage_ = "Cannot create the XML path context.";
            break;
        case XML_EVALUATE_XPATH_FAULT:
            errCategory_ = XML_PARSING_FAULT;
            errMessage_ = "Cannot find evaluate xPath expression.";
            break;
        case XML_ATTRIBUTE_NOT_FOUND:
            errCategory_ = XML_PARSING_FAULT;
            errMessage_ = "XML Attribute not found.";
            break;
        case XML_DATA_NOT_CONSISTENT:
            errCategory_ = XML_PARSING_FAULT;
            errMessage_ = "Design and Deployment data are not consistent (Registers and Blocks list.";
            break;
        case XML_DATA_TYPE_MISMATCH:
            errCategory_ = XML_PARSING_FAULT;
            errMessage_ = "XML data type mismatch within the parameters file.";
            break;
        case DATA_UNKNOWN_FORMAT_TYPE:
            errCategory_ = DATA_FAULT;
            errMessage_ = "Register Design has a wrong data format: ";
            break;
        case DATA_UNKNOWN_ACCESS_TYPE:
            errCategory_ = DATA_FAULT;
            errMessage_ = "Block Design has a wrong access type: ";
            break;
        case DATA_UNKNOWN_ACCESS_AREA:
            errCategory_ = DATA_FAULT;
            errMessage_ = "Block Design has a wrong access area: ";
            break;
        case DATA_UNKNOWN_PLC_MANUFACTURER:
            errCategory_ = DATA_FAULT;
            errMessage_ = "Controller Deployment has a wrong manufacturer: ";
            break;
        case DATA_UNKNOWN_PLC_MODEL:
            errCategory_ = DATA_FAULT;
            errMessage_ = "Controller Deployment has a wrong model: ";
            break;
        case DATA_UNKNOWN_PLC_SYSTEM:
            errCategory_ = DATA_FAULT;
            errMessage_ = "Controller Deployment has a wrong system: ";
            break;
        case DATA_UNKNOWN_PROTOCOL_MODE:
            errCategory_ = DATA_FAULT;
            errMessage_ = "Controller Deployment has a wrong protocol mode: ";
            break;
        case DATA_READ_ACCESS_TYPE_MISMATCH:
            errCategory_ = DATA_FAULT;
            errMessage_ = "Trying to read a write-only register: ";
            break;
        case DATA_WRITE_ACCESS_TYPE_MISMATCH:
            errCategory_ = DATA_FAULT;
            errMessage_ = "Trying to write a read-only register: ";
            break;
        case DATA_ARRAY_LENGTH_MISMATCH:
            errCategory_ = DATA_FAULT;
            errMessage_ = "ArrCOMM_BLOCK_RESIZING_NOT_ALLOWEDay length is not compatible: ";
            break;
        case PARAM_INCORRECT_ARGUMENTS:
            errCategory_ = PARAM_FAULT;
            errMessage_ = "Incorrect SILECS service arguments.";
            break;
        case PARAM_FILE_NOT_FOUND:
            errCategory_ = PARAM_FAULT;
            errMessage_ = "This parameters file does not exist: ";
            break;
        case PARAM_UNKNOWN_BLOCK_NAME:
            errCategory_ = PARAM_FAULT;
            errMessage_ = "Unknown block-name: ";
            break;
        case PARAM_UNKNOWN_DEVICE_NAME:
            errCategory_ = PARAM_FAULT;
            errMessage_ = "Unknown device-name: ";
            break;
        case PARAM_UNKNOWN_DESIGN_NAME:
            errCategory_ = PARAM_FAULT;
            errMessage_ = "Unknown design-name: ";
            break;
        case PARAM_UNKNOWN_REGISTER_NAME:
            errCategory_ = PARAM_FAULT;
            errMessage_ = "Unknown register-name: ";
            break;
        case PARAM_ACCESS_TYPE_MISMATCH:
            errCategory_ = PARAM_FAULT;
            errMessage_ = "Use of inappropriate method (access-type) to access the block: ";
            break;
        case PARAM_FORMAT_TYPE_MISMATCH:
            errCategory_ = PARAM_FAULT;
            errMessage_ = "Use of inappropriate method (format/dimension) to access the register: ";
            break;
        case PARAM_ARRAY_DIMENSION_MISMATCH:
            errCategory_ = PARAM_FAULT;
            errMessage_ = "Trying to access the following array-register with wrong index or dimension: ";
            break;
        case PARAM_NO_TIME_STAMP:
            errCategory_ = PARAM_FAULT;
            errMessage_ = "Trying to access the Time-Stamp of a Write-Only register: ";
            break;
        case PARAM_INCORRECT_BLOCK_ADDRESS:
            errCategory_ = PARAM_FAULT;
            errMessage_ = "Schneider uses 16bit alignment addressing. The Block address should be an even value: ";
            break;
        case PARAM_EXCEEDED_BLOCK_SIZE:
            errCategory_ = PARAM_FAULT;
            errMessage_ = "The specified block-size exceeds the one allocated: ";
            break;
        case PARAM_UNKNOWN_IP_ADDRESS:
            errCategory_ = PARAM_FAULT;
            errMessage_ = "This IP address is unknown or not well-formed: ";
            break;
        case PARAM_UNKNOWN_PLC_HOSTNAME:
            errCategory_ = PARAM_FAULT;
            errMessage_ = "This controller hostname is unknown or not well-formed: ";
            break;
        case COMM_CONNECT_FAILURE:
            errCategory_ = COMM_FAULT;
            errMessage_ = "Unable to connect the controller: ";
            break;
        case COMM_RECONNECT_FAILURE:
            errCategory_ = COMM_FAULT;
            errMessage_ = "Unable to re-connect the controller: ";
            break;
        case COMM_CONNECT_CLOSED:
            errCategory_ = COMM_FAULT;
            errMessage_ = "Connection has been closed with the controller: ";
            break;
        case COMM_ALREADY_ENABLED:
            errCategory_ = COMM_FAULT;
            errMessage_ = "Controller connection already enabled: ";
            break;
        case COMM_NOT_CONNECTED_YET:
            errCategory_ = COMM_FAULT;
            errMessage_ = "Trying to access (send/recv) a controller that is not connected yet: ";
            break;
        case COMM_ALREADY_CONNECTED:
            errCategory_ = COMM_FAULT;
            errMessage_ = "Controller already connected: ";
            break;

        case COMM_BLOCK_RESIZING_NOT_ALLOWED:
            errCategory_ = COMM_FAULT;
            errMessage_ = "Data-block resizing is allowed only with single device access (send/recv).";
            break;
        case CONFIG_CLASS_NOT_DEPLOYED:
            errCategory_ = CONFIG_FAULT;
            errMessage_ = "This controller/Design deployment does not exist: ";
            break;
        case CONFIG_CLIENT_PLC_NOT_CONSISTENT:
            errCategory_ = CONFIG_FAULT;
            errMessage_ = "The client configuration is not consistent with the mapping of the controller: ";
            break;
        case CONFIG_CLIENT_PLC_NOT_AVAILABLE:
            errCategory_ = CONFIG_FAULT;
            errMessage_ = "Reading the controller Header data has failed: IP address of that client is not allowed or the controller memory is empty: ";
            break;
        case DIAG_SLAVE_REGISTER_NOT_INITIALIZED:
            errCategory_ = DIAG_FAULT;
            errMessage_ = "Connection is not allowed now because some of retentive slave registers are not initialized yet: ";
            break;
        case DIAG_PLC_REPORT_NOT_SUPPORTED:
            errCategory_ = DIAG_FAULT;
            errMessage_ = "Diagnostic and status report are not supported with this controller type: ";
            break;
        case CNV_INTERNAL_ERROR:
            errCategory_ = CNV_SYSTEM_FAULT;
            errMessage_ = "CNV library error: ";
            break;
        case UNEXPECTED_ERROR:
            errCategory_ = CLIENT_SYSTEM_FAULT;
            errMessage_ = "Unexpected error: ";
            break;
        default:
            errCategory_ = UNKNOWN_FAULT;
            errMessage_ = "Unknown fault.";
    }
}

void SilecsException::buildFullMessage()
{
    errFullMessage_ = errCategory_ + "/" + errCode_;
    errFullMessage_ += " ";

    char errLineAsString[5] = {0};
    sprintf(errLineAsString, "%d", int(errLine_));

    errFullMessage_ += errFile_ + ":" + errLineAsString;
    errFullMessage_ += " " + errMessage_;
}

void SilecsException::print() const
{
    std::cerr << "Silecs::SilecsException [" << errCategory_ << "/" << errCode_ << "] " << errFile_ << ":" << errLine_ << ", " << errMessage_ << std::endl;
}

void SilecsException::log() const
{
    unsigned long topic = ERROR; //Exception means ERROR topic
    if (errCategory_ == DIAG_FAULT)
        topic |= DIAG; //and DIAG topic if required for syslog purpose
    LOG(topic) << "[" << errCategory_ << "/" << errCode_ << "] " << errFile_ << ":" << errLine_ << ", " << errMessage_;
}

} // namespace
