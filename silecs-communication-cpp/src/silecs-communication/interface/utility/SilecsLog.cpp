/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>
#include <silecs-communication/interface/utility/TimeStamp.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <syslog.h>
#include <string.h>
#include <errno.h>

namespace Silecs
{
//TRACE is a particular topic used to feed ACET tracing service with static information
//of the running program (SILECS library in that case). Sys-logging of it is mandatory and
//cannot be disabled to insure ACET does not miss sources.
//DIAG is a particular topic used to feed the standard syslog with minimal information for
//debugging purpose, sys-logging cannot be disabled as well
unsigned long Log::topics_ = 0;
unsigned long Log::systopics_ = TRACE | DIAG;

std::string Log::host_ = "";
pid_t Log::pid_ = -1;
std::string Log::process_ = "";

bool Log::syslogIsStarted_ = false; //syslog not yet started
TsCounter Log::timeStamp_(true);

Log::Log(unsigned long topic)
{
    topic_ = topic;
}

Log::~Log()
{
    if (topic_ & topics_)
    {
        //this topic is enabled for std. output
        std::cerr << os.str().c_str() << std::endl;
    }

    if (topic_ & systopics_)
        //this topic is enabled for syslog output
        syslog(LOG_INFO, "%s", os.str().c_str());
}

bool Log::getTopicsFromString(const std::string& topicArgv)
{
    unsigned long topicsTemp = NO_TOPIC; // Remove all the topics

    unsigned int i;
    int j;
    bool wrongTopic = false;
    std::vector<std::string> topicCol;
    StringUtilities::tokenize(topicArgv, topicCol, ",");
    for (i = 0; i < topicCol.size(); ++i)
    {
        for (j = 0; j < topicNb; ++j)
        {
            if (topicCol[i] == topicList[j])
            {
                topicsTemp |= 1 << j;
                break;
            }
        }
        if (j == topicNb)
        {
            wrongTopic = true;
            std::cerr << "SILECS setup failed: Unknown diagnostic topic: " << topicCol[i] << std::endl;
            break;
        }

    }
    //Return false if topic parameters are not correct or empty
    if (! (wrongTopic || topicCol.empty()))
    {
        topics_ = topicsTemp;
        return true;
    }
    return false;
}

std::string Log::setTopicToString(unsigned long topic)
{
    int i = 0;
    while (topic > 1)
    {
        topic >>= 1;
        i++;
    }
    if (i >= topicNb)
    {
        std::cerr << "Unknown topic id: '" << topic << "'. Giving ERROR topic as default." << std::endl;
        i = 0;
    }
    return ("/" + topicList[i]);
}

std::ostringstream& Log::getLog()
{
    os << "TIME " << std::setprecision(5) << timeStamp_.getValue(S_UNIT);
    os << "s SILECS" << setTopicToString(topic_) << "(thread " << pthread_self() << "): ";
    return os;
}

std::ostringstream& Log::getTrace(const std::string& level)
{
    os << "host=" << getHost() << ",level=" << level << ",pid=" << getPid() << ",system=SILECS" << ",subsystem=libsilecs-comm" << ",process=" << getProcess() << ",body=";
    return os;
}

std::ostringstream& Log::getLogDelay()
{
    os << "DELAY " << std::setprecision(5) << timeStamp_.getDelay(MS_UNIT);
    os << "ms SILECS" << setTopicToString(topic_) << "(thread " << pthread_self() << "): ";
    return os;
}

bool Log::setLogArguments(int argc, char ** argv)
{
    bool argsOk = true;
    for (int i = 0; i < argc; i++)
    {
        if ( (strcmp(argv[i], "-plcLog") == 0) && (i + 1 < argc))
        {
            argsOk = Log::getTopicsFromString(argv[i + 1]);
            break;
        }
    }
    return argsOk;
}

std::string Log::getLogArguments()
{
    int j;
    std::string argsList;
    argsList = "-plcLog " + topicList[0] + "[,";
    for (j = 1; j < topicNb - 1; ++j)
        argsList += topicList[j] + ",";
    argsList += topicList[j] + "] ";
    return argsList;
}

void Log::startSyslog(char* ident, unsigned long topics)
{
    if (!syslogIsStarted_)
    {
        char hostName[128];
        if (gethostname(hostName, 128) == -1)
            throw SilecsException(__FILE__, __LINE__, errno);

        //remove domain name (.cern.ch) if any
        host_ = std::string(hostName);
        size_t domain = host_.find_first_of('.');
        if (domain != std::string::npos)
            host_.erase(domain);

        pid_ = getpid();
        process_ = std::string(ident);

        systopics_ |= topics;

        /* syslog service uses file configuration (required root access):
         * /etc/<syslog.conf>     : to define the ouput channel (file, console, host)
         * /etc/<sysconfig/syslog>: to set-up the syslogd daemon
         * For SILECS log we use LOCAL2 facility that is required for ACET service.
         * LOCAL2 is also compliant with SILECS diagnostic purpose since by default
         * LOG_LOCAL[1..5] are stored into /var/log/messages NFS files, as required.
         * Ask to system expert (Nmn) to know about syslogd redirection details.
         */
        openlog(ident, LOG_NDELAY, LOG_LOCAL2);
        syslogIsStarted_ = true;
    }
}

void Log::stopSyslog()
{
    if (syslogIsStarted_)
    {
        syslogIsStarted_ = false;
        //remove syslog resources
        closelog();

        LOG(SETUP) << "syslog service has been stopped.";
    }
}

} // namespace
