/*
 *  Piece of code adapted for SILECS.
 *  Original version references:
 *
 *  Portable Agile C++ Classes (PACC)
 *  Copyright (C) 2004 by Marc Parizeau
 *  http://manitou.gel.ulaval.ca/~parizeau/PACC
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Contact:
 *  Laboratoire de Vision et Systemes Numeriques
 *  Departement de genie electrique et de genie informatique
 *  Universite Laval, Quebec, Canada, G1K 7P4
 *  http://vision.gel.ulaval.ca
 *
 */

#include <silecs-communication/interface/utility/TimeStamp.h>
#include <iostream>

namespace Silecs
{

double TsCounter::mPeriod = 0;

void TsCounter::calibrateCountPeriod(unsigned int inDelay/*ms*/, unsigned int inTimes)
{

    if (mHardware)
    {
#if defined (__GNUG__) && (defined (__i386__) || defined (__PPC__))
        double lPeriod = 0;
        // calibrate by matching the time-stamps with the micro-seconds of gettimeofday
        for(unsigned int i = 0; i < inTimes; ++ i)
        {
            timeval lStartTime, lTime;
            ::gettimeofday(&lStartTime, 0);
            unsigned long long lStartCount = getCount();

            struct timespec theDelay, time_left_before_wakeup;
            int sec = inDelay/1000; // number of seconds since delay is expressed in ms
            long nsec = 0;
            if ((nsec = inDelay%1000))// check remainder
            nsec *= (long)1E6;// convert remainder into ns
            theDelay.tv_sec = sec;
            theDelay.tv_nsec = nsec;
#ifdef __Lynx__
            //UNIX variants often use a kernel timer resolution (HZ value)
            //of about 10ms. So it' not possible to manage delays less than 10ms
            //to wait 10ms you need specify a delay 0 othewhise your delay will b 20ms
            if((inDelay%10)<5)
            {
                theDelay.tv_nsec -= 20000000;
            }
            else
            {
                theDelay.tv_nsec -= 10000000;
            }
            if (theDelay.tv_nsec < 0)
            theDelay.tv_nsec = 0;
#endif
            nanosleep(&theDelay, &time_left_before_wakeup);

            ::gettimeofday(&lTime, 0);
            unsigned long long lCount = getCount() - lStartCount;
            lTime.tv_sec -= lStartTime.tv_sec;
            lTime.tv_usec -= lStartTime.tv_usec;
            // dismiss the first run of the loop
            if(i != 0) lPeriod += (lTime.tv_sec + lTime.tv_usec*0.000001)/lCount;
        }
        mPeriod = lPeriod/(inTimes-1);
#else
        // Cast to void to avoid unsued warnings.
        static_cast<void>(inDelay);
        static_cast<void>(inTimes);
        // use the microseconds of gettimeofday
        mPeriod = 0.000001;
#endif
    }
    else
    {
        // use the microseconds of gettimeofday
        mPeriod = 0.000001;
    }
}

unsigned long long TsCounter::getCount(void) const
{
    unsigned long long lCount = 0;

    if (mHardware)
    {
#if defined (__GNUG__) && defined (__i386__)
        __asm__ volatile("rdtsc" : "=A" (lCount));
#else
#if defined (__GNUG__) && defined (__PPC__)
        register unsigned int lLow;
        register unsigned int lHigh1;
        register unsigned int lHigh2;
        do
        {
            // make sure that high bits have not changed
            __asm__ volatile ( "mftbu %0" : "=r" (lHigh1) );
            __asm__ volatile ( "mftb  %0" : "=r" (lLow) );
            __asm__ volatile ( "mftbu %0" : "=r" (lHigh2) );
        }while(lHigh1 != lHigh2);
        // transfer to lCount
        unsigned int *lPtr = (unsigned int*) &lCount;
        *lPtr++ = lHigh1; *lPtr = lLow;
#else
        timeval lCurrent;
        ::gettimeofday(&lCurrent, 0);
        lCount = (unsigned long long)lCurrent.tv_sec * 1000000 + lCurrent.tv_usec;
#endif
#endif
    }
    else
    {
        timeval lCurrent;
        ::gettimeofday(&lCurrent, 0);
        lCount = (unsigned long long)lCurrent.tv_sec * 1000000 + lCurrent.tv_usec;
    }

    return lCount;
}

} // namespace
