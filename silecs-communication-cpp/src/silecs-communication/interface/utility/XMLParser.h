/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _XMLPARSER_HPP_
#define _XMLPARSER_HPP_

#include <string>
#include <vector>
#include <iostream>
#include <unistd.h>
#include <sstream>

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <libxml/encoding.h>

#include <silecs-communication/interface/utility/SilecsException.h>

#define MY_ENCODING "UTF-8"

namespace Silecs
{

/*!
 * \class AttributeXML
 * \brief This class represents an XML attribute containing name and value
 */
class AttributeXML
{
public:
    /*!
     * \brief name_ of the attribute
     */
    std::string name_;

    /*!
     * \brief value_ of the attribute
     */
    std::string value_;
};

/*!
 * \class ElementXML
 * \brief This class represents an XML element containing name, value, a list of attributes
 * and a list of child XML elements
 */
class ElementXML
{
friend class XMLParser;
public:
    bool hasAttribute(const std::string& attributeName) const;

    std::string getAttribute(const std::string& attributeName) const;

    const std::vector<ElementXML>& getChildNodes() const;

    const std::string& getName() const;
private:
    /*!
     * \brief name of the xml-node
     */
    std::string name_;

    /*!
     * \brief value of the element
     */
    std::string value_;

    /*!
     * \brief list of attributes of the element
     */
    std::vector<AttributeXML> attributeList_;

    /*!
     * \brief list of children of the element
     */
    std::vector<ElementXML> childList_;
};

/*!
 * \class XMLParser
 * \brief This class offers functions to extract information from an XML file.
 * It uses the xPath functions of the library libXML
 */
class XMLParser
{
public:

    /*!
     * \brief constructor
     * \param fileName path of the file that represents the xml element
     * \param validateFile if true, file is validated
     */
    XMLParser(const std::string& fileName, bool validateFile);

    /*!
     * \brief destructor
     */
    ~XMLParser();

    /*!
     * \brief Retrieve the elements identified by the XPath expression from the XML file
     * \param xpathExpression The XPath expression
     */

    ElementXML getSingleElementFromXPath(const std::string& xpathExpression) const;

    ElementXML getFirstElementFromXPath(const std::string& xpathExpression) const;

    /*!
     * \brief Retrieve the elements identified by the XPath expression from the XML file. If no
              elements are found an exception is thrown.
     * \param xpathExpression The XPath expression
     */
    std::vector<ElementXML> getElementsFromXPath(const std::string& xpathExpression) const;

    /*!
     * \brief Has to be executed from the main-thread before this class can be used
     */
    static void init();

    /*!
     * \brief Has to be executed from the main-thread on shutdown
     */
    static void cleanUp();

private:

    /*!
     * \brief Retrieve the elements identified by the XPath expression from the XML file. If no
              elements are found an empty vector is returned. This method is used by the public
              facing getElementsFromXPath which throws if no elements are found.
     * \param xpathExpression The XPath expression
     */
    std::vector<ElementXML> getElementsFromXPathInner(const std::string& xpathExpression) const;

    /*!
     * \brief removes carrier return from string
     * \param s the string to remove the carrier from
     */
    void trimCarrierReturn(std::string& s) const;

    /*!
     * \brief fills instance of the class ElementXML with nodes from an XML file
     * \param node that contains the xml info
     * \param [out] pElement into which the info will be put
     */
    void fillElement(xmlNodePtr node, ElementXML& element) const;

    /*!
     * \brief converts a char array to a xmlChar array
     * \param in The char array
     * \param encoding the coding format
     * \return the xmlChararray
     */
    xmlChar* ConvertInput(const char *in, const char *encoding);

    /*!
     * \brief path of the file
     */
    std::string fileName_;
};

} // fesa

#endif // XML_PARSER_H_
