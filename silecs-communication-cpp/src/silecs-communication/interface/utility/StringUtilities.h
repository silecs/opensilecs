/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _STRING_UTILITIES_H_
#define _STRING_UTILITIES_H_

#include <string>
#include <vector>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <sstream>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>

namespace Silecs
{

class StringUtilities
{
public:
    static void tokenize(const std::string& str, std::vector<std::string>& tokens, const std::string& delimiters = " ");

    static void trimWhiteSpace(std::string& str);

    // define an array of one pointer on an empty string
    static const unsigned int c_emptyStringArraySize;
    static const char* c_emptyStringArray[1];
    // define an empty string
    static const char c_emptyString[];

    static std::string toString(unsigned int data);
    static std::string toString(int data);
    static std::string toString(unsigned long data);
    static std::string toString(long data);
    static std::string toString(long long data);
    static std::string toString(double data);
    static std::string toString(const void * ptr);
    static void toLower(std::string& str);
    static bool evalStringToBool(std::string stringVal);
    /*!
     * \brief transform a string into any integer-type
     * \param str the string to transform
     * \param value that will receive the conversion
     */
    template <typename T> static void fromString(T& value, const std::string& str);
};

template <typename T>
inline void StringUtilities::fromString(T& value, const std::string& str)
{
    std::istringstream iss(str);
    if ( (iss >> value).fail())
    {
        std::ostringstream message;
        message << "Failed to convert the string: '" << str << "' to numeric type";
        throw SilecsException(__FILE__, __LINE__, message.str().c_str());
    }
}

/* trim from start */
static inline std::string &ltrim(std::string &s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
    std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

/* trim from end */
static inline std::string &rtrim(std::string &s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(),
        std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

/* trim from both sides */
static inline std::string &trim(std::string &s)
{
    return ltrim(rtrim(s));
}

} // namespace

#endif //_STRING_UTILITIES_H_
