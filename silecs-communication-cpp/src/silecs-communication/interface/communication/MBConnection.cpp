/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/

#ifdef MODBUS_SUPPORT_ENABLED
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>
#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/communication/MBConnection.h>
#include <silecs-communication/interface/utility/SilecsException.h>

#include <bitset>

// Modbus max data size definition (byte counting)
#define MAX_WRITE_DATA_SIZE MODBUS_MAX_WRITE_REGISTERS * 2
#define MAX_READ_DATA_SIZE MODBUS_MAX_READ_REGISTERS * 2

namespace Silecs
{

MBConnection::MBConnection(PLC *thePLC) :
                Connection{thePLC}
{
    LOG(ALLOC) << "MBConnection (create): " << thePLC_->getName();

    // Connection use IP address to limit the naming-server accesses
    readCtx_ = modbus_new_tcp((char *)thePLC_->getIPAddress().c_str(), MODBUS_TCP_DEFAULT_PORT /*=502*/);
    writeCtx_ = modbus_new_tcp((char *)thePLC_->getIPAddress().c_str(), MODBUS_TCP_DEFAULT_PORT /*=502*/);

    /*TODO: To be adjusted with the next stable libmodbus release (>3.1.1)
     which will fix the current timeout response time issue (see libmodbus
     forum).

     Define Modbus response timeout
     struct timeval response_timeout;
     response_timeout.tv_sec = 0;
     response_timeout.tv_usec = 10000;

     modbus_set_response_timeout(readCtx_ , &response_timeout);
     modbus_set_response_timeout(writeCtx_ , &response_timeout);
     */

    modbus_set_slave(readCtx_, 1);
    // modbus_set_debug(readCtx_, TRUE);
}

MBConnection::~MBConnection()
{
    // Close the connection before removing resources
    // disable(); must be done before removing resource
    modbus_free(writeCtx_);
    modbus_free(readCtx_);
}

bool MBConnection::open()
{
    int readErr = modbus_connect(readCtx_);
    int writeErr = modbus_connect(writeCtx_);
    return ( (readErr != -1) && (writeErr != -1));
}

bool MBConnection::close()
{
    modbus_close(readCtx_);
    modbus_close(writeCtx_);
    return true;
}

int MBConnection::readFrames(modbus_t *ctx, long start_addr, uint16_t count, uint8_t *data, bool isIO)
{
    uint16_t *destp = (uint16_t *)data;
    int word_count, byte_count;

    while (count)
    {

        if (count > MAX_READ_DATA_SIZE)
        {
            /*'word_count' is a word counter*/
            word_count = MAX_READ_DATA_SIZE / 2;
        }
        else
        {
            /*'word_count' is a word counter*/
            word_count = (count / 2) + (count % 2);
        }

        byte_count = (word_count * 2);

        // libmodbus offers different functions for reading input registers and memory
        if (isIO)
        {
            if (modbus_read_input_registers(ctx, static_cast<int>(start_addr), static_cast<int>(word_count), destp) != word_count)
                return -1;
        }
        else
        {
            if (modbus_read_registers(ctx, static_cast<int>(start_addr), static_cast<int>(word_count), destp) != word_count)
                return -1;
        }

        // destp += (byte_count-(count%2));
        destp += word_count;
        start_addr += word_count;
        count = static_cast<uint16_t>(count - (byte_count - (count % 2)));
    }

    return 0;
}

int MBConnection::writeFrames(modbus_t *ctx, long start_addr, uint16_t count, uint8_t *data)
{
    uint16_t *srcp = (uint16_t *)data;
    uint16_t word_count, byte_count;

    while (count)
    {
        word_count = static_cast<uint16_t>( (count > MAX_WRITE_DATA_SIZE) ? MAX_WRITE_DATA_SIZE / 2 : (count / 2) + (count % 2));
        byte_count = static_cast<uint16_t>(word_count * 2);

        if (modbus_write_registers(ctx, static_cast<int>(start_addr), static_cast<int>(word_count), srcp) != word_count)
        {
            return -1;
        }

        // srcp += byte_count-(count%2);
        srcp += word_count;
        start_addr += word_count;
        count = static_cast<uint16_t>(count - (byte_count - (count % 2)));
    }

    return 0;
}

int MBConnection::readCoils(modbus_t *ctx, long start_addr, uint16_t count, uint8_t *data)
{
    // read the status of N bits (coils) at the bit address of the remote device.

    // The dest array must contain bytes set to TRUE or FALSE. We need one byte for each bit in data, e.g 2 bytes would require a 16 byte bool array 
    uint8_t dest[count * 8];
    uint16_t bit_count;
    uint16_t current_count;
    uint16_t current_bit_count;

    int j = 0;
    int i = 0;
    current_count = static_cast<uint16_t>(count * 8);
    j = 0;

    while (current_count)
    {
        bit_count = static_cast<uint16_t>( (current_count > MODBUS_MAX_READ_BITS) ? MODBUS_MAX_READ_BITS : current_count);

        if (modbus_read_input_bits(ctx, static_cast<int>(start_addr), bit_count, &dest[j]) < 0)
        {
            return -1;
        }

        j += bit_count;
        start_addr += bit_count;
        current_count = static_cast<uint16_t>(current_count - bit_count);
    }

    // The buffer is filled with boolean (8 bits) data that need to be converted back to a bitstream

    for (i = 0; i < count; i++)
    {
        current_bit_count = static_cast<uint16_t>(i * 8);
        data[i] = 0x00;
        for (j = 0; j < 8; j++)
        {
            if (dest[current_bit_count + j])
            {
                data[i] |= static_cast<unsigned char>(1 << (7 - j));
            }
        }
    }

    return 0;
}

int MBConnection::writeCoils(modbus_t *ctx, long start_addr, uint16_t count, uint8_t *data)
{
    // write the status of N bits (coils) from src at the bit address of the remote device.

    // The src array must contain bytes set to TRUE or FALSE. We need one byte for each bit in data, e.g 2 bytes would require a 16 byte bool array 
    uint8_t src[count * 8];
    uint16_t bit_count;
    std::string tmp;

    int i, k;
    int j = 0;

    // Convert the bitstream to a bool array
    for (i = 0; i < count; i++)
    {
        std::bitset<8> bitSet(data[i]);
        tmp = bitSet.to_string();
        std::copy(tmp.begin(), tmp.end(), &src[j]);
        for (k = j; k < j + 8; k++)
        {
            // Convert char to int
            src[k] = static_cast<uint8_t>(src[k] - '0');
        }
        j += 8;
    }

    count = static_cast<uint16_t>(count * 8);
    j = 0;

    while (count)
    {
        bit_count = static_cast<uint16_t>( (count > MODBUS_MAX_WRITE_BITS) ? MODBUS_MAX_WRITE_BITS : count);

        if (modbus_write_bits(ctx, static_cast<int>(start_addr), bit_count, &src[j]) < 0)
        {
            return -1;
        }

        j += bit_count;
        start_addr += bit_count;
        count = static_cast<uint16_t>(count - bit_count);
    }
    return 0;
}

int MBConnection::readRegisters(long address, unsigned long offset, unsigned long size, unsigned char *pBuffer, bool isIO)
{
    // Schneider uses 16bit alignment memory. Block address is expressed in
    // bytes, must be an even value!
    if (address % 2)
        throw SilecsException(__FILE__, __LINE__, PARAM_INCORRECT_BLOCK_ADDRESS, StringUtilities::toString(address));

    if (isConnected())
    {
        // connection is established then acquire data
        // Schneider uses 16bit alignment memory. Block address is expressed in
        // bytes (==> /2)
        long addr = (address + offset) / 2;

        // DATA topic makes sense with RECV one
        if (RECV & Log::topics_)
            LOG(DATA) << "Read data, address: %MW" << addr << ", byte-size: " << size;

        int err = readFrames(readCtx_, addr, (unsigned short)size, pBuffer, isIO);
        checkError(err); // close the connection, will try again
                                        // at the next access
        return err;
    }
    throw SilecsException{__FILE__, __LINE__, COMM_NOT_CONNECTED_YET};
}

int MBConnection::writeRegisters(long address, unsigned long offset, unsigned long size, unsigned char *pBuffer)
{
    // Schneider uses 16bit alignment memory. Block address is expressed in
    // bytes, must be an even value!
    if (address % 2)
        throw SilecsException(__FILE__, __LINE__, PARAM_INCORRECT_BLOCK_ADDRESS, StringUtilities::toString(address));

    if (isConnected())
    {
        // connection is established then send data
        // Schneider uses 16bit alignment memory. Block address is expressed in
        // bytes (==> /2)
        long addr = (address + offset) / 2;

        // DATA topic makes sense with SEND one
        if (SEND & Log::topics_)
            LOG(DATA) << "Write data, address: %MW" << addr << ", byte-size: " << size;

        int err = writeFrames(writeCtx_, (unsigned short)addr, (unsigned short)size, pBuffer);
        checkError(err); // close the connection, will try again
                                        // at the next access
        return err;
    }
    throw SilecsException{__FILE__, __LINE__, COMM_NOT_CONNECTED_YET};
}

int MBConnection::readBits(long address, unsigned long offset, unsigned long size, unsigned char *pBuffer)
{
    if (isConnected())
    {
        // connection is established then acquire data
        // addr is a byte address, therefore we don't need to convert to a word address
        // However, we need to calculate the coil address from the register address 
        long addr = (address - thePLC_->getParamConfig().baseDIAddr) + offset;

        // Coils address is a bit address, but in our case we only address the first bit of the byte
        // addr should me a multiple of 8
        addr *= 8;

        // DATA topic makes sense with RECV one
        if (RECV & Log::topics_)
            LOG(DATA) << "Read bits, address: %MB" << addr / 8 << ", byte-size: " << size;

        int err = readCoils(readCtx_, addr, (unsigned short)size, pBuffer);
        checkError(err); // close the connection, will try again
                                        // at the next access
        return err;
    }
    throw SilecsException{__FILE__, __LINE__, COMM_NOT_CONNECTED_YET};
}

int MBConnection::writeBits(long address, unsigned long offset, unsigned long size, unsigned char *pBuffer)
{

    if (isConnected())
    {
        // connection is established then send data
        // addr is a byte address, therefore we don't need to convert to a word address
        // However, we need to calculate the coil address from the register address 
        long addr = (address - thePLC_->getParamConfig().baseDOAddr) + offset;

        // Coils address is a bit address, but in our case we only address the first bit of the byte
        // addr should me a multiple of 8
        addr *= 8;

        // DATA topic makes sense with SEND one
        if (SEND & Log::topics_)
            LOG(DATA) << "Write data, address: %MB" << addr / 8 << ", byte-size: " << size;
        int err = writeCoils(writeCtx_, (unsigned short)addr, (unsigned short)size, pBuffer);
        checkError(err); // close the connection, will try again
                                        // at the next access
        return err;
    }
    throw SilecsException{__FILE__, __LINE__, COMM_NOT_CONNECTED_YET};
}

int MBConnection::readMemory(long address, unsigned long offset, unsigned long size, unsigned char *pBuffer)
{
    std::lock_guard<std::mutex> lock(readMux_);
    return readRegisters(address, offset, size, pBuffer, 0);
}

int MBConnection::writeMemory(long address, unsigned long offset, unsigned long size, unsigned char *pBuffer)
{
    std::lock_guard<std::mutex> lock(writeMux_);
    return writeRegisters(address, offset, size, pBuffer);
}

int MBConnection::readAIO(long address, unsigned long offset, unsigned long size, unsigned char *pBuffer)
{
    std::lock_guard<std::mutex> lock(readMux_);
    return readRegisters(address, offset, size, pBuffer, 1);
}

int MBConnection::writeAIO(long address, unsigned long offset, unsigned long size, unsigned char *pBuffer)
{
    std::lock_guard<std::mutex> lock(writeMux_);
    return writeRegisters(address, offset, size, pBuffer);
}

int MBConnection::readDIO(long address, unsigned long offset, unsigned long size, unsigned char *pBuffer)
{
    std::lock_guard<std::mutex> lock(readMux_);
    return readBits(address, offset, size, pBuffer);
}

int MBConnection::writeDIO(long address, unsigned long offset, unsigned long size, unsigned char *pBuffer)
{
    std::lock_guard<std::mutex> lock(writeMux_);
    return writeBits(address, offset, size, pBuffer);
}

//-------------------------------------------------------------------------------------------------------------------
bool MBConnection::checkError(int err)
{
    if (err != 0)
    {
        LOG(COMM) << "Transaction failure with the controller: " << thePLC_->getName() << ". MODBUS[" << errno << "]: " << modbus_strerror(errno);
        // no retry, we just want to close (use default case)
        doClose();
    }
    return false;
}

} // namespace
#endif //MODBUS_SUPPORT_ENABLED
