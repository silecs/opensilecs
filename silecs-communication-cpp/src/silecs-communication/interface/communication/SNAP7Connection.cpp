/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>
#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/communication/SNAP7Connection.h>
#include <silecs-communication/interface/utility/SilecsException.h>

namespace Silecs
{

SNAP7Connection::SNAP7Connection(PLC* thePLC) :
                Connection{thePLC}
{
    rackNb_ = 0; //rackNb - common rack is 0 so far
    slotNb_ = -1; //slotNb - depends on hardware configuration (scan is required the first connect)

    recvClient_ = Cli_Create();
    sendClient_ = Cli_Create();

    LOG(ALLOC) << "SNAP7Connection (create) PLC: " << thePLC_->getName();
}

SNAP7Connection::~SNAP7Connection()
{
    Cli_Destroy(&recvClient_);
    Cli_Destroy(&sendClient_);
}

bool SNAP7Connection::open()
{
    int err = 0;
    bool ret = false;
    int slotNb;

    //Slot number is not required, so try to connect on different slots (1..31), depending on hardware layout.
    int *slotsScan;
    int slotsS7400[4] = {3, 2, 4, 1}; //slot scan - most common layout S7-400
    int slotsS71x00[4] = {1, 2, 3, 4}; //slot scan - most common layout S7-1200, S7-1500
    int slotsDefault[4] = {2, 3, 4, 1}; //slot scan - most common layout S7-other (S7-300, ET200S)
    int nbMaxSlot = ( (slotNb_ == -1) ? 4 : 1); //slots scan only the first time (slotNb_ == -1)

    switch (thePLC_->getParamConfig().modelID)
    {
        case S7400:
            slotsScan = slotsS7400;
            break;
        case S71200:
        case S71500:
            slotsScan = slotsS71x00;
            break;
        default:
            slotsScan = slotsDefault; //S7-300, ET200S
    }

    for (int i = 0; i < nbMaxSlot; i++)
    {
        slotNb = slotsScan[i];

        err = ( (slotNb_ == -1) ? Cli_ConnectTo(recvClient_, thePLC_->getIPAddress().c_str(), rackNb_, slotNb) : Cli_Connect(recvClient_));

        if (err == 0)
        {
            LOG(DEBUG) << "SNAP7 connect (channel-1) successful on PLC/rack/slot: " << thePLC_->getName() << "/" << rackNb_ << "/" << slotNb;

            //We managed to open the first channel, just open the second one on the same slot.
            err = ( (slotNb_ == -1) ? Cli_ConnectTo(sendClient_, thePLC_->getIPAddress().c_str(), rackNb_, slotNb) : Cli_Connect(sendClient_));

            if (err != 0)
            {
                LOG(DEBUG) << "SNAP7 connection (channel-2) failed on PLC/rack/slot: " << thePLC_->getName() << "/" << rackNb_ << "/" << slotNb << ". SNAP7[" << err << "]: " << getErrorMessage(err);
                continue;
            }

            LOG(DEBUG) << "SNAP7 connect (channel-2) successful on PLC/rack/slot: " << thePLC_->getName() << "/" << rackNb_ << "/" << slotNb;

            slotNb_ = slotNb; //connection is ok we can store the valid slot number for the next (re)connection (will be faster).
            ret = true;
            break;
        }
        LOG(DEBUG) << "SNAP7 connection (channel-1) failed on PLC/rack/slot: " << thePLC_->getName() << "/" << rackNb_ << "/" << slotNb << ". SNAP7[" << err << "]: " << getErrorMessage(err);
    }
    return (ret);
}

bool SNAP7Connection::close()
{
    bool ret = (Cli_Disconnect(recvClient_) == 0) && (Cli_Disconnect(sendClient_) == 0);
    return ret;
}

std::string SNAP7Connection::getErrorMessage(int err)
{
    char text[TextLen];
    Cli_ErrorText(err, text, TextLen);
    return std::string(text);
}

//-------------------------------------------------------------------------------------------------------------------
int SNAP7Connection::readUnitCode(UnitCodeType& dataStruct)
{
    int err;
    TS7OrderCode S7data;
    std::lock_guard<std::mutex> lock(readMux_);
    if ( (err = Cli_GetOrderCode(recvClient_, &S7data)) == 0)
    {
        std::ostringstream os;
        os << S7data.Code;
        dataStruct.code = os.str();
        os.str().clear();
        os << (int)S7data.V1 << "." << (int)S7data.V2 << "." << (int)S7data.V3;
        dataStruct.version = os.str();
        LOG(DEBUG) << "Unit order-code received: " << dataStruct.code << ", " << dataStruct.version;
        return 0;
    }
    checkError(err);
    return err;
}

int SNAP7Connection::readUnitStatus(UnitStatusType& dataStruct)
{
    int err;
    int status;
    std::lock_guard<std::mutex> lock(readMux_);
    if ( (err = Cli_GetPlcStatus(recvClient_, &status)) == 0)
    {
        dataStruct.status = status;
        LOG(DEBUG) << "Unit status received: " << (int)dataStruct.status;
        return 0;
    }
    checkError(err);
    return err;
}

int SNAP7Connection::readCPUInfo(CPUInfoType& dataStruct)
{
    int err;
    TS7CpuInfo S7data;
    std::lock_guard<std::mutex> lock(readMux_);
    if ( (err = Cli_GetCpuInfo(recvClient_, &S7data)) == 0)
    {
        dataStruct.moduleName = std::string(S7data.ModuleName);
        dataStruct.moduleTypeName = std::string(S7data.ModuleTypeName);
        dataStruct.serialNumber = std::string(S7data.SerialNumber);
        dataStruct.asName = std::string(S7data.ASName);
        dataStruct.copyright = std::string(S7data.Copyright);
        LOG(DEBUG) << "CPU info received: " << dataStruct.moduleName << ", " << dataStruct.moduleTypeName << ", " << dataStruct.serialNumber << ", " << dataStruct.asName << ", " << dataStruct.copyright;
        return 0;
    }
    checkError(err);
    return err;
}

int SNAP7Connection::readCPInfo(CPInfoType& dataStruct)
{
    int err;
    TS7CpInfo S7data;
    std::lock_guard<std::mutex> lock(readMux_);
    if ( (err = Cli_GetCpInfo(recvClient_, &S7data)) == 0)
    {
        dataStruct.maxPduLength = S7data.MaxPduLengt;
        dataStruct.maxConnections = S7data.MaxConnections;
        dataStruct.maxMPIRate = S7data.MaxMpiRate;
        dataStruct.maxBusRate = S7data.MaxBusRate;
        LOG(DEBUG) << "CP info received: " << dataStruct.maxPduLength << ", " << dataStruct.maxConnections << ", " << dataStruct.maxMPIRate << ", " << dataStruct.maxBusRate;
        return 0;
    }
    checkError(err);
    return err;
}

bool SNAP7Connection::isRunning()
{
    UnitStatusType statusStruct;
    readUnitStatus(statusStruct);
    switch (statusStruct.status)
    {
        case S7CpuStatusRun:
            return true;
        case S7CpuStatusStop:
            return false;
        default:
            throw SilecsException(__FILE__, __LINE__, UNKNOWN_ERROR, std::string("PLC Status is: UNKNOWN"));
    }
}
int SNAP7Connection::readMemory(long DBn, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    std::lock_guard<std::mutex> lock(readMux_);
    if (DBn < 0)
    {
        LOG(COMM) << "Invalid DB number: " << DBn;
        throw SilecsException(__FILE__, __LINE__, PARAM_INCORRECT_BLOCK_ADDRESS, StringUtilities::toString(DBn));
    }

    if (isConnected())
    {
        //connection is established then acquire data
        //DATA topic makes sense with RECV one
        if (RECV & Log::topics_)
            LOG(DATA) << "Read memory data, DBn: " << DBn << ", ofs: " << offset << ", byte-size: " << size;

        int err = Cli_DBRead(recvClient_, (int)DBn, (int)offset, (int)size, (void *)pBuffer);
        checkError(err); // close the connection, will try again at the next access
        return err;
    }
    throw SilecsException{__FILE__, __LINE__, COMM_NOT_CONNECTED_YET};
}
int SNAP7Connection::writeMemory(long DBn, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    std::lock_guard<std::mutex> lock(writeMux_);

    if (DBn < 0)
    {
        LOG(COMM) << "Invalid DB number: " << DBn;
        throw SilecsException(__FILE__, __LINE__, PARAM_INCORRECT_BLOCK_ADDRESS, StringUtilities::toString(DBn));
    }

    if (isConnected())
    {
        //connection is established then send data
        //DATA topic makes sense with SEND one
        if (SEND & Log::topics_)
            LOG(DATA) << "Write memory data, DBn: " << DBn << ", ofs: " << offset << ", byte-size: " << size;

        int err = Cli_DBWrite(sendClient_, (int)DBn, (int)offset, (int)size, (void *)pBuffer);
        checkError(err); // close the connection, will try again at the next access
        return err;
    }
    throw SilecsException{__FILE__, __LINE__, COMM_NOT_CONNECTED_YET};
}

int SNAP7Connection::readIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    if (address < 0)
    {
        LOG(COMM) << "Invalid address: " << address;
        throw SilecsException(__FILE__, __LINE__, PARAM_INCORRECT_BLOCK_ADDRESS, StringUtilities::toString(address));
    }
    if (isConnected())
    {
        //connection is established then acquire data
        //DATA topic makes sense with RECV one
        if (RECV & Log::topics_)
            LOG(DATA) << "Read IO data, address: " << address << ", ofs: " << offset << ", byte-size: " << size;

        address += offset;
        int err = Cli_EBRead(recvClient_, (int)address, (int)size, (void *)pBuffer);
        checkError(err); // close the connection, will try again at the next access
        return err;
    }
    throw SilecsException{__FILE__, __LINE__, COMM_NOT_CONNECTED_YET};
}

int SNAP7Connection::writeIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    if (address < 0)
    {
        LOG(COMM) << "Invalid address: " << address;
        throw SilecsException(__FILE__, __LINE__, PARAM_INCORRECT_BLOCK_ADDRESS, StringUtilities::toString(address));
    }

    if (isConnected())
    {
        //connection is established then send data
        //DATA topic makes sense with SEND one
        if (SEND & Log::topics_)
            LOG(DATA) << "Write IO data, address: " << address << ", ofs: " << offset << ", byte-size: " << size;

        address += offset;
        int err = Cli_ABWrite(sendClient_, (int)address, (int)size, (void *)pBuffer);
        checkError(err); // close the connection, will try again at the next access
        return err;
    }
    throw SilecsException{__FILE__, __LINE__, COMM_NOT_CONNECTED_YET};
}

int SNAP7Connection::readAIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    std::lock_guard<std::mutex> lock(readMux_);
    return readIO(address, offset, size, pBuffer);
}
int SNAP7Connection::writeAIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    std::lock_guard<std::mutex> lock(writeMux_);
    return writeIO(address, offset, size, pBuffer);
}
int SNAP7Connection::readDIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    std::lock_guard<std::mutex> lock(readMux_);
    return readIO(address, offset, size, pBuffer);
}
int SNAP7Connection::writeDIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    std::lock_guard<std::mutex> lock(writeMux_);
    return writeIO(address, offset, size, pBuffer);
}

//-------------------------------------------------------------------------------------------------------------------
bool SNAP7Connection::checkError(int err)
{
    if (err != 0)
    {
        LOG(COMM) << "Transaction failure with the controller: " << thePLC_->getName() << ". SNAP7[" << err << "]: " << getErrorMessage(err);
        doClose();
    }
    return false;
}

int SNAP7Connection::coldRestart()
{
    std::lock_guard<std::mutex> lock(writeMux_);
    if (isConnected())
    {
        int error = Cli_PlcColdStart(sendClient_);
        if (error)
        {
            throw SilecsException(__FILE__, __LINE__, UNEXPECTED_ERROR, " SNAP7 Error: " + getErrorMessage(error));
        }
        return 0;
    }
    throw SilecsException{__FILE__, __LINE__, COMM_NOT_CONNECTED_YET};
}

int SNAP7Connection::plcStop()
{
    std::lock_guard<std::mutex> lock(writeMux_);
    if (isConnected())
    {
        int error = Cli_PlcStop(sendClient_);
        if (error)
        {
            throw SilecsException(__FILE__, __LINE__, UNEXPECTED_ERROR, " SNAP7 Error: " + getErrorMessage(error));
        }
        return 0;
    }
    throw SilecsException{__FILE__, __LINE__, COMM_NOT_CONNECTED_YET};
}

} // namespace
