
#ifndef _SILECS_ENDIANNESS_H_
#define _SILECS_ENDIANNESS_H_

#include <cstdint>

namespace Silecs
{

std::int16_t bigEndianToInt16(const std::int8_t* data);

std::int32_t bigEndianToInt32(const std::int8_t* data);

void int16ToBigEndianBytes(std::int16_t data, std::uint8_t* dest);

} // namespace Silecs

#endif /* _SILECS_ENDIANNESS_H_ */

