/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _SNAP7_CONNECTION_H_
#define _SNAP7_CONNECTION_H_

#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <snap7.h>

namespace Silecs
{
class PLC;

/*!
 * \class SNAP7Connection
 * \brief Plc-communication object for specific SNAP7 protocol
 */
class SNAP7Connection : public Connection
{

public:
    SNAP7Connection(PLC* thePLC);
    virtual ~SNAP7Connection();

    int readUnitCode(UnitCodeType& dataStruct) override;
    int readUnitStatus(UnitStatusType& dataStruct) override;
    int readCPUInfo(CPUInfoType& dataStruct) override;
    int readCPInfo(CPInfoType& dataStruct) override;

    bool isRunning() override;

    int readMemory(long DBn, unsigned long offset, unsigned long size, unsigned char* pBuffer) override;
    int writeMemory(long DBn, unsigned long offset, unsigned long size, unsigned char* pBuffer) override;
    int readAIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer) override;
    int writeAIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer) override;
    int readDIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer) override;
    int writeDIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer) override;

    //Extension Silecs methods
    int coldRestart() override;
    int plcStop() override;

private:
    S7Object recvClient_;
    S7Object sendClient_;
    bool open() override;
    bool close() override;
    int readIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);

    int rackNb_; //rackNb - common rack is 0 by default
    int slotNb_; //slotNb - depends on hardware configuration (scan is required the first connect)

    std::string getErrorMessage(int err);
    bool checkError(int err) override;
};

} // namespace

#endif // _SNAP7_CONNECTION_H_

