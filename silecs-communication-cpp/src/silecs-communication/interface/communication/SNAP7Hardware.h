/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/

#ifndef _SNAP7HARDWARE_H_
#define _SNAP7HARDWARE_H_

#include <silecs-communication/interface/communication/ietype.h>

/*----------------------------------------------------------*/
/* Time funtion
 * IeRfcSetTime: Convert time_t epoch date to PLC _DT format
 * (SIMATIC).
 *
 * Details:
 * PLC DATE_AND_TIME (or DT) format is coded on 8 byte in BCD
 * format:
 * 0      1      2      3      4      5      6       7
 * YY     MM     DD     HH     MN     SC     SC/100  SC/10e4
 */
void IeRfcSetTime(unsigned char *dt, time_t epoch); // to PLC

/*----------------------------------------------------------*/
/* Time funtion
 * IeRfcGetTime: Convert PLC _DT format to lynxOS time_t format
 * (SIMATIC).
 *
 * Details:
 * PLC DATE_AND_TIME (or DT) format is coded on 8 byte in BCD
 * format:
 * 0      1      2      3      4      5      6       7
 * YY     MM     DD     HH     MN     SC     SC/100  SC/10e4
 */
double IeRfcGetTime(unsigned char *dt); // from PLC

/*----------------------------------------------------------*/
/* Time funtion
 * DtlGetTime: Convert PLC _DTL format to time_t format
 * (SIMATIC).
 *
 * Details:
 * DTL format expects 12 bytes
 * format:
 * 0      1      2      3      4       5      6      7        8        9      10     11
 * YY     YY     MM     DD  DayOfWeek  HH     MN     SC        NS (bytes 8 - 11)
 */
double DtlGetTime(unsigned char *dt); // from PLC

/*----------------------------------------------------------*/
/* Time funtion
 * DtlGetTime: Convert time_t epoch date to PLC _DTL format
 * (SIMATIC).
 *
 * Details:
 * DTL format expects 12 bytes
 * format:
 * 0      1      2      3      4       5      6      7        8        9      10     11
 * YY     YY     MM     DD  DayOfWeek  HH     MN     SC        NS (bytes 8 - 11)
 */
void DtlSetTime(unsigned char *dt, time_t epoch); // to PLC

#endif /* _SNAP7HARDWARE_H_ */
