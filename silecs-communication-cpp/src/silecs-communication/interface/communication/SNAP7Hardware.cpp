/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include "SNAP7Hardware.h"
#include "silecs-communication/interface/communication/Endianness.h"

/*----------------------------------------------------------*/
/* Time funtion
 * IeRfcSetTime: Convert lynxos time_t format to PLC _DT format
 * (SIMATIC).
 *
 * Details:
 * PLC DATE_AND_TIME (or DT) format is coded on 8 byte in BCD
 * format:
 * 0      1      2      3      4      5      6       7
 * YY     MM     DD     HH     MN     SC     SC/100  SC/10e4
 */
void IeRfcSetTime(unsigned char *dt, time_t epoch) // to PLC
{
    struct tm *dscst;
    dscst = localtime(&epoch);

    dt[7] = 0; // not used at the moment
    dt[6] = 0; // not used at the moment
    dt[5] = static_cast<char>(_tobcd(dscst->tm_sec));
    dt[4] = static_cast<char>(_tobcd(dscst->tm_min));
    dt[3] = static_cast<char>(_tobcd(dscst->tm_hour));
    dt[2] = static_cast<char>(_tobcd(dscst->tm_mday));
    dt[1] = static_cast<char>(_tobcd(dscst->tm_mon + 1));
    dt[0] = static_cast<char>(_tobcd(dscst->tm_year - 100));
}

/*----------------------------------------------------------*/
/* Time funtion
 * IeRfcGetTime: Convert PLC _DT format to time_t format (SIMATIC).
 *
 * Details:
 * PLC DATE_AND_TIME (or DT) format is coded on 8 byte in BCD
 * format:
 * 0      1      2      3      4      5      6       7
 * YY     MM     DD     HH     MN     SC     SC/100  SC/10e4
 */
double IeRfcGetTime(unsigned char *dt) // from PLC
{
    struct tm plcst;
    time_t plctm;
    double ms;
    int year;

    plcst.tm_sec = _frombcd(dt[5]);
    plcst.tm_min = _frombcd(dt[4]);
    plcst.tm_hour = _frombcd(dt[3]);
    plcst.tm_mday = _frombcd(dt[2]);
    plcst.tm_mon = _frombcd(dt[1]) - 1;
    year = _frombcd(dt[0]);
    //look at Siemens DATA_AND_TIME type documentation
    plcst.tm_year = ( (year >= 90) ? year : year + 100);

    ms = static_cast<double>(_frombcd(dt[6]) / 100.) + static_cast<double>(_frombcd(dt[7]) / 10000.);
    plcst.tm_isdst = -1; // daylight saving time unavailable
    plctm = mktime(&plcst);

    return static_cast<double>(plctm) + ms;
}

double DtlGetTime(unsigned char *dt)
{
    struct tm plcst;
    time_t plctm;
    plcst.tm_year = Silecs::bigEndianToInt16(reinterpret_cast<std::int8_t*>(dt));
    plcst.tm_year -= 1900;
    plcst.tm_mon = dt[2] - 1;
    plcst.tm_mday = dt[3];
    plcst.tm_hour = dt[5];
    plcst.tm_min = dt[6];
    plcst.tm_sec = dt[7];
    auto nanoseconds = Silecs::bigEndianToInt32(reinterpret_cast<std::int8_t*>(dt + 8));

    double ms = static_cast<double>(nanoseconds) / 1e9;
    plcst.tm_isdst = -1; // daylight saving time unavailable
    plctm = mktime(&plcst);

    return static_cast<double>(plctm) + ms;
}

void DtlSetTime(unsigned char *dt, time_t epoch)
{
    struct tm *dscst;
    dscst = localtime(&epoch);

    Silecs::int16ToBigEndianBytes(dscst->tm_year + 1900, dt);
    dt[2] = static_cast<unsigned char>(dscst->tm_mon + 1);
    dt[3] = static_cast<unsigned char>(dscst->tm_mday);
    dt[4] = static_cast<unsigned char>(dscst->tm_wday + 1);
    dt[5] = static_cast<unsigned char>(dscst->tm_hour);
    dt[6] = static_cast<unsigned char>(dscst->tm_min);
    dt[7] = static_cast<unsigned char>(dscst->tm_sec);
    dt[8] = 0;
    dt[9] = 0;
    dt[10] = 0;
    dt[11] = 0;
}