/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _SILECS_CONNECTION_H_
#define _SILECS_CONNECTION_H_

#include <mutex>

#include <silecs-communication/interface/utility/Definitions.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/utility/TimeStamp.h>

namespace Silecs
{
class PLC;

typedef enum
{
  shortDelay  = 0,
  mediumDelay = 1,
  longDelay   = 2
} ReconnectionDelay;

// Time to wait (second) before next reconnection attempt
const static double RECONNECTION_DELAYS[3] = {2, 20, 60};

// Maximum muber of connection attempts before using longer delay
const static unsigned int MAX_CONNECTION_ATTEMPTS_PER_DELAY = 3;

#ifdef __x86_64__
typedef long ChannelID; //pointer is 64bits word
#else
typedef int ChannelID; //pointer is 32bits word
#endif

class Connection
{

public:
    Connection(PLC* thePLC);
    virtual ~Connection() = default;

    virtual int readMemory(long address, unsigned long offset, unsigned long size, unsigned char* buffer) = 0;

    virtual int writeMemory(long address, unsigned long offset, unsigned long size, unsigned char* buffer) = 0;

    virtual int readAIO(long address, unsigned long offset, unsigned long size, unsigned char* buffer) = 0;

    virtual int writeAIO(long address, unsigned long offset, unsigned long size, unsigned char* buffer) = 0;

    virtual int readDIO(long address, unsigned long offset, unsigned long size, unsigned char* buffer) = 0;

    virtual int writeDIO(long address, unsigned long offset, unsigned long size, unsigned char* buffer) = 0;

    /*!
     * \fn enable/disable
     * \brief The client can suspend the data transmission by disabling the connection if required.
     * enable/disable methods are used from the high-level Cluster API: connect/disconnect.
     * \param connectNow can be used to force immediate connection (true)
     */
    bool enable(bool connectNow);
    void disable();
    bool isEnabled();
    bool isConnected();

    virtual int readUnitCode(UnitCodeType& dataStruct);
    virtual int readUnitStatus(UnitStatusType& dataStruct);
    virtual int readCPUInfo(CPUInfoType& dataStruct);
    virtual int readCPInfo(CPInfoType& dataStruct);

    // true if the "recvUnitStatus" is RUN, false otherwise. Throws exception on failure
    virtual bool isRunning();

    //SET PLC COLD RESTART
    virtual int coldRestart();
    virtual int plcStop();

    /*!
     * \fn doOpen
     * \brief This method check the current state of the connection and open it if required.
     * doOpen that is called on each transaction is reponsible of the automatic reconnection mechanism.
     */
    bool doOpen();

protected:
    void doClose();

    virtual bool open() = 0; //open the connection for a particular PLC brand
    virtual bool close() = 0; //close the connection for a particular PLC brand

    /*!
     * \fn checkError
     * \brief This method attempts to reconnect the PLC in case of "expected" transaction failure.
     * If the returned value is true the request must be repeated.
     * The retry flag defines whether the transaction will be repeated or not
     */
    virtual bool checkError(int err) = 0;

    /* 
     * read-channel and write channel are independent and can be accessed in parallel.
     * Each channel must be protected against respective concurrent access.
     * The global action (open,close,etc.) must be protected from any concurrent access. This is
     * done by locking both the read and write mutex.
     */
    std::mutex readMux_; //Mutex used to protect the PLC read-channel resource
    std::mutex writeMux_; //Mutex used to protect the PLC write-channel resource
    std::mutex connMux_; //Mutex used to protect the global PLC connection resource (for open/close, etc.)

    PLC* thePLC_;

private:

    /*
     * Structure containing information if the connection is opened or not, and if it was opened
     * with the current call whether the register should be updated (read from) the device.
     */
    struct OpenStatus
    {
        OpenStatus(bool connected, bool update);
        bool isConnected;
        bool updateRegisters;
    };
    OpenStatus doOpenUnlocked();
    void doCloseUnlocked();

    void logError(bool isReachable);

    /*!
     * \fn isTimeToReconnect
     * \brief In order to not overload the network the reconnection attempts are gradually slowed
     * down from second scale to several minutes. This method checks the elapsed time since connection.
     * \return true if it's time to try to reconnect (time unit is second).
     */
    bool isTimeToReconnect();

    // flag used to enable/disable the transactions independently from the scheduling
    bool isEnabled_;

    time_t lastReconnectionAttempt_;
    ReconnectionDelay reconnectDelay_; // time to wait before invoking reconnect (Seconds)
    unsigned int reconnectAttempts_; // number of reconnection attempts ( will be cleared on successfull connect)

    /* ping function is a function used to check
     * if PLC is ON (~ping) before trying to connect it.
     */
    /*----------------------------------------------------------*/
    /* This is a function used to connect an host on a port.
     * 'ip' is the ip address string
     * 'port' is the port number (102 for rfc1006 server)
     * 'dst' is the TSAP destination string (exp: "TCP-1")
     * 'src' is the TSAP source string (exp: "TCP-1")
     * 'ts' timeout in second (1s minimum)
     * return a socket descriptor or <0 on error
     * (see constant error)
     *
     * Details:
     * intermediate connect_nonb() function is used to perform a non-blockant connection.
     * intermediate rfcPing() function is used to check if PLC is ON
     */
    static int ping(const char *hostName, char *plcIP);

    // Communication Diagnostic & Monitoring
    bool isConnected_; // State of this particular connection: FEC/PLC/Class

    // not copyable object
    Connection(const Connection&);
    Connection& operator=(const Connection&);
};

} // end namespace

#endif // _SILECS_CONNECTION_H_
