#include <silecs-communication/interface/communication/Endianness.h>

namespace Silecs
{

std::int16_t bigEndianToInt16(const std::int8_t* data)
{
    return static_cast<std::int16_t>(
            static_cast<std::int16_t>(data[0]) << 8 |
            (static_cast<std::int16_t>(data[1]) & 0xFF));
}

std::int32_t bigEndianToInt32(const std::int8_t* data)
{
    return static_cast<std::int32_t>(
            static_cast<std::int32_t>(data[0]) << 24 |
            (static_cast<std::int32_t>(data[1]) & 0xFF) << 16 |
            (static_cast<std::int32_t>(data[2]) & 0xFF) << 8 |
            (static_cast<std::int32_t>(data[3]) & 0xFF));
}

void int16ToBigEndianBytes(std::int16_t data, std::uint8_t* dest)
{
    dest[0] = static_cast<std::uint8_t>(data >> 8);
    dest[1] = static_cast<std::uint8_t>(data);
}

} // namespace Silecs