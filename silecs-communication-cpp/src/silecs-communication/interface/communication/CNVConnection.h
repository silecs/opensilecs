/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifdef NI_SUPPORT_ENABLED
#ifndef _CNV_CONNECTION_H_
#define _CNV_CONNECTION_H_

#include <silecs-communication/interface/communication/SilecsConnection.h>

#include <iostream>
#include <string.h>

#ifdef __cplusplus
extern "C"
{
#endif

#include <nilibnetv.h>

#ifdef __cplusplus
}
#endif

namespace Silecs
{
/*!
 * \class CNVConnection
 * \brief CNV-communication object for PXI Shared Variable Protocol
 */
class CNVConnection : public Connection
{

public:
    CNVConnection(PLC* thePLC);
    virtual ~CNVConnection();

    /*!
     * \brief Read Data from the specified buffer handler
     * \return the read CNVData and error code (0 = everything is ok)
     */
    //CNVData readData(CNVBufferedSubscriber *handle);
    int readData(CNVBufferedSubscriber *handle, CNVData *data);

    /*!
     * \brief Write the specified variable, data must be already in CNVData format.
     * In case of error raises a std::string* exception containing the error message
     * \return error code (0 = everything is ok)
     */
    int writeData(const char *networkVariablePathname, CNVData data);

    /*!
     * \brief Subscribes for changes on the the specified variable.
     * In case of error raises a std::string* exception containing the error message
     */
    void monitorData(const char *networkVariablePathname);

    int readMemory(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer) override;
    int writeMemory(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer) override;
    int readAIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer) override;
    int writeAIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer) override;
    int readDIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer) override;
    int writeDIO(long address, unsigned long offset, unsigned long size, unsigned char* pBuffer) override;

private:
    // Subscriber
    CNVSubscriber subscriber;

    /*!
     * \brief Print the error message related to the code and throw exception if needed
     * \return the error code itself (0 everything is ok)
     */
    int errChk(int code);

    /*!
     * \brief Open the connection
     * It actually return a boolean which indicates if the server responded to a ping operation
     */
    bool open() override;

    /*!
     * \brief Close the connection with the server
     */
    bool close() override;

    bool checkError(int err)  override
    {
        return false;
    }

};

} // namespace

#endif // _CNV_CONNECTION_H_

#endif //NI_SUPPORT_ENABLED
