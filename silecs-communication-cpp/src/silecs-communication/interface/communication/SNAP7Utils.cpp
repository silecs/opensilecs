#include "snap7.h"

//******************************************************************************
//           HELPER DATA ACCESS FUNCTIONS IMPLEMENTATION FOR SNAP 7
//  They were no accessible in SNAP7 version 1.4.x
//******************************************************************************

//==============================================================================
// Helper GET routines
//==============================================================================

bool GetBitAt(void *Buffer, unsigned long Pos, unsigned long Bit)
{
    byte Mask[] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
    if (Bit > 7) Bit = 7;
    return (*(pbyte(Buffer) + Pos) & Mask[Bit]) != 0;
}

//==============================================================================
// Helper SET routines
//==============================================================================
void SetBitAt(void *Buffer, unsigned long Pos, unsigned long Bit, bool Value)
{
    byte Mask[] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
    pbyte p = pbyte(Buffer)+Pos;
    if (Bit > 7) Bit = 7;
    (Value) ? *p |= Mask[Bit] : *p &= ~Mask[Bit];
}

