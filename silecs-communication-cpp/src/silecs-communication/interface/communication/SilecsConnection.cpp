/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <fcntl.h>
#include <signal.h>
#include <cstring>
#include <thread>

#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/communication/ietype.h>

namespace Silecs
{

//------------------------------------------------------------------------------------------------------------------------------------------------
Connection::Connection(PLC* thePLC) :
    thePLC_(thePLC)
{
    //Not yet allowed, not yet connected!
    isEnabled_ = false;
    isConnected_ = false;

    //Reset the Reconnection mechanism
    lastReconnectionAttempt_ = 0;
    reconnectDelay_ = shortDelay; //initial reconnection delay
    reconnectAttempts_ = 0;
}

//------------------------------------------------------------------------------------------------------------------------------------------------
bool Connection::enable(bool connectNow)
{
    std::unique_lock<std::mutex> lock(connMux_);
    isEnabled_ = true;
    if (connectNow)
    {
        auto status = doOpenUnlocked();
        if (status.updateRegisters)
        {
            LOG((COMM|DIAG)) << "First connection - performing registers synchronization";
            // Due to the current design following is a call which would recursively lock connMux_.
            // For that reason we unlock the connMux to avoid deadlocks. updateLocalData will
            // reacquire the locks which it needs. It will call execute on all the blocks which will
            // in turn a doOpen (which locks the connMux_) before reading the data.
            lock.unlock();
            thePLC_->updateLocalData();
            lock.lock();
            return isConnected_;
        }
        return status.isConnected;
    }   
    return isConnected_;
}

//------------------------------------------------------------------------------------------------------------------------------------------------
void Connection::disable()
{
    std::lock_guard<std::mutex> readLock(readMux_);
    std::lock_guard<std::mutex> writeLock(writeMux_);
    std::lock_guard<std::mutex> connLock(connMux_);
    if (!isEnabled_)
    {
        LOG(DEBUG) << "Trying to disconnect a PLC that is not enabled: " << thePLC_->getName();
        return;
    }

    isEnabled_ = false;
    doCloseUnlocked();
}

//------------------------------------------------------------------------------------------------------------------------------------------------
bool Connection::doOpen()
{
    std::unique_lock<std::mutex> lock(connMux_);
    OpenStatus status = doOpenUnlocked();
    
    if (status.updateRegisters)
    {
        /* 
        * Process the Retentive registers synchronization each time the PLC is just (re)connected.
        * This would recursively lock connMux_: performs task::execute method which calls the doOpen again.
        * To avoid deadlocks unlock the connMux now. updateLocalData will reacquire the locks which it needs.
        */
        LOG((COMM|DIAG)) << "First connection - performing registers synchronization";
        lock.unlock();
        thePLC_->updateLocalData();
        lock.lock();
        return isConnected_;
    }
    return status.isConnected;
}

Connection::OpenStatus::OpenStatus(bool connected, bool update)
    : isConnected(connected),
      updateRegisters(update)
{
}

Connection::OpenStatus Connection::doOpenUnlocked()
{
    if (isConnected_)
    {
        if (!isEnabled_)
        {
            LOG((COMM|DIAG)) << "The PLC is connected but the client wants to disable the transactions";
            doCloseUnlocked();
        }
        return {isConnected_, false};
    }

    if (!isEnabled_)
    {
        LOG((COMM|DIAG)) << "The communication currently is not enabled";
        return {isConnected_, false};
    }

    if (!isTimeToReconnect())
    { // Do nothing, just wait a bit to dont pullute the log
        return {isConnected_, false};
    }

    if (ping(thePLC_->getName().c_str(), NULL))
    {
        logError(false);
        return {isConnected_, false};
    }
    // It's time to open the connection according to the (re)connection timing
    // Let's try several times with limited delay (some ms).
    // It allows wake-up frozen PLC (SIEMENS in particular) after long stop period.
    bool isOpen = false;
    unsigned int nbConn = 2; //for fast low-level iteration
    for (unsigned int i = 0; i < nbConn; i++)
    {
        LOG((COMM|DIAG)) << "Attempt to open PLC connection ....";
        isOpen = open();
        if (isOpen)
        {
            //reset reconnection settings
            reconnectDelay_ = shortDelay;
            reconnectAttempts_ = 0;

            isConnected_ = true;
            LOG((COMM|DIAG)) << "Connection opened successfully";
            break;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds{100}); // wait 100ms
    }

    if (!isOpen)
    {
        logError(true);
        return {isConnected_, false};
    }

    std::ostringstream os;
    os << "Connection with " << thePLC_->getName() << " is established.";
    LOG(COMM) << os.str();

    return {isConnected_, true};
}

//------------------------------------------------------------------------------------------------------------------------------------------------
void Connection::doClose()
{
    std::lock_guard<std::mutex> lock(connMux_);
    doCloseUnlocked();
}

void Connection::doCloseUnlocked()
{
    if (isConnected_)
    {
        if (close())
        {
            isConnected_ = false;

            std::ostringstream os;
            os << "Connection with " << thePLC_->getName() << " is closed.";
            LOG(COMM) << os.str();
        }
        else
        {
            LOG(COMM) << "Close connection with " << thePLC_->getName() << " has failed.";
        }
    }

}

//------------------------------------------------------------------------------------------------------------------------------------------------
bool Connection::isEnabled()
{
    std::lock_guard<std::mutex> lock(connMux_);
    return isEnabled_;
}
bool Connection::isConnected()
{
    std::lock_guard<std::mutex> lock(connMux_);
    return isConnected_;
}

//-------------------------------------------------------------------------------------------------------------------
int Connection::readUnitCode(UnitCodeType&)
{
    throw SilecsException(__FILE__, __LINE__, DIAG_PLC_REPORT_NOT_SUPPORTED, thePLC_->getName());
}

int Connection::readUnitStatus(UnitStatusType&)
{
    throw SilecsException(__FILE__, __LINE__, DIAG_PLC_REPORT_NOT_SUPPORTED, thePLC_->getName());
}

int Connection::readCPUInfo(CPUInfoType&)
{
    throw SilecsException(__FILE__, __LINE__, DIAG_PLC_REPORT_NOT_SUPPORTED, thePLC_->getName());
}

int Connection::readCPInfo(CPInfoType&)
{
    throw SilecsException(__FILE__, __LINE__, DIAG_PLC_REPORT_NOT_SUPPORTED, thePLC_->getName());
}

bool Connection::isRunning()
{
    throw SilecsException(__FILE__, __LINE__, DIAG_PLC_REPORT_NOT_SUPPORTED, thePLC_->getName());
}

int Connection::coldRestart()
{
    throw SilecsException(__FILE__, __LINE__, DIAG_PLC_REPORT_NOT_SUPPORTED, thePLC_->getName());
}

//PERFORM COLD RESTART
int Connection::plcStop()
{
    throw SilecsException(__FILE__, __LINE__, DIAG_PLC_REPORT_NOT_SUPPORTED, thePLC_->getName());
}

//------------------------------------------------------------------------------------------------------------------------------------------------
void Connection::logError(bool isReachable)
{
    std::string errorMsg = isReachable ? "Connection with " + thePLC_->getName() + " has failed.\n" : "Controller " + thePLC_->getName() + " does not respond to ping, might be OFF!\n";
    errorMsg += "Error: " + std::string(std::strerror(errno)) + ". ";
    if (reconnectDelay_ == longDelay)
    {
        if (reconnectAttempts_ <  MAX_CONNECTION_ATTEMPTS_PER_DELAY)
        {
            std::ostringstream os;
            os << errorMsg << "Periodic attempt to reconnect, delay " << RECONNECTION_DELAYS[reconnectDelay_] << " seconds (tracing off).";
            LOG(COMM) << os.str();
        }
        /*else
         PLC does not respond anymore. It's probably stopped for a long time.
         Do not log error anymore (but still try to reconnect, with long-term delay).
         */
    }
    else
    {
        std::ostringstream os;
        os << errorMsg << "Next attempt to reconnect in " << RECONNECTION_DELAYS[reconnectDelay_] << " seconds.";
        LOG(COMM) << os.str();
    }
}

//------------------------------------------------------------------------------------------------------------------------------------------------
bool Connection::isTimeToReconnect()
{
  time_t now;
  time(&now);
  double secondsElapsed = difftime(now, lastReconnectionAttempt_);

  if ( secondsElapsed < RECONNECTION_DELAYS[reconnectDelay_])
    return false;

  lastReconnectionAttempt_ = now;
  reconnectAttempts_ ++;

  if( reconnectAttempts_ < MAX_CONNECTION_ATTEMPTS_PER_DELAY)
    return true;

  if( reconnectDelay_ < longDelay)
  {
    reconnectAttempts_ = 0;
    reconnectDelay_ =  static_cast<ReconnectionDelay>(reconnectDelay_+ 1);
  }
  return true;
}

/* This macro is used to trap the unexpected broken pipe and
 return an error instead of exit process.
 */
static __sighandler_t sigpipeHandler = (__sighandler_t )-1;
#define _DISABLE_SIGPIPE sigpipeHandler = signal(SIGPIPE, SIG_IGN)
#define _ENABLE_SIGPIPE signal(SIGPIPE, sigpipeHandler)

//------------------------------------------------------------------------------------------------------------------------------------------------
int connect_nonb(int sockfd, struct sockaddr *saptr, socklen_t salen, int nsec)
{
    int flags, n, error;
    socklen_t len;
    fd_set rset, wset;
    struct timeval tval;

    flags = fcntl(sockfd, F_GETFL, 0);
    fcntl(sockfd, F_SETFL, flags | O_NONBLOCK);

    error = 0;
    /*v1.1*/
    _DISABLE_SIGPIPE;
    n = connect(sockfd, (struct sockaddr *)saptr, salen);
    _ENABLE_SIGPIPE;
    if (n < 0)
    {
        if (errno != EINPROGRESS)
            return (-1);
    }

    /* Do whatever we want while the connect is taking place. */

    if (n == 0)
        goto done;
    /* connect completed immediately */

    FD_ZERO(&rset);
    FD_SET(sockfd, &rset);
    wset = rset;
    tval.tv_sec = nsec;
    tval.tv_usec = 0;

    if ( (n = select(sockfd + 1, &rset, &wset, NULL, nsec ? &tval : NULL)) == 0)
    {
        close(sockfd); /* timeout */
        errno = ETIMEDOUT;
        return (-1);
    }

    if (FD_ISSET(sockfd, &rset) || FD_ISSET(sockfd, &wset))
    {
        len = sizeof (error);
        if (getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, &len) < 0)
        {
            return (-1); /* Solaris pending error */
        }
    }

    done: fcntl(sockfd, F_SETFL, flags); /* restore file status flags */

    if (error)
    {
        /*close(sockfd); just in case */
        errno = error;
        return (-1);
    }

    return (0);
}

/*----------------------------------------------------------*/
int rfcPing(char *ip, long ts)
{
    int s, val = 1;
    struct protoent *pent;
    struct sockaddr_in rsock;

    /* Socket create/connect */
    memset((char *)&rsock, 0, sizeof (rsock));

    if ( (s = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        LOG(ERROR) << "Can't create socket: " << strerror(errno);
        return -1;
    }

    /* Set TCP_NODELAY option to force immediate TCP/IP acknowledgement */
    if ( (pent = getprotobyname("TCP")) == NULL)
    {
        LOG(ERROR) << "Can't configure socket: " << strerror(errno);
        return -1;
    }
    if (setsockopt(s, pent->p_proto, TCP_NODELAY, &val, 4) == -1)
    {
        LOG(ERROR) << "Can't configure socket: " << strerror(errno);
        return -1;
    }

    rsock.sin_addr.s_addr = inet_addr(ip);
    rsock.sin_family = AF_INET;
    /*check any port to detect if the host is OFF*/
    rsock.sin_port = htons(102);

    if (connect_nonb(s, (struct sockaddr *) (&rsock), sizeof (rsock), static_cast<int>(ts)) == -1)
    {
        /*if hostname is OFF, connect() fails on TIMEOUT*/
        if ( (errno == ETIMEDOUT) || (errno == EHOSTDOWN) || (errno == EHOSTUNREACH))
        {
            LOG(ERROR) << "Error: " << strerror(errno);
            return -1;
        }
    }

    close(s);
    return 0; //controller was reachable
}

/*..........................................................*/
int Connection::ping(const char *hostName, char *plcIP)
{
    struct in_addr addr;
    struct hostent *hp;
    char *ipstr = plcIP;
    std::string errorMsg;
    std::ostringstream os;

    if ( (hostName == NULL) && (plcIP == NULL))
    {
        os << errorMsg << "Bad parameter(s) value/format";
        LOG(COMM) << os.str();
        return -1;
    }

    // use hostName reference in priority else plcIP
    if (hostName)
    {
        hp = gethostbyname(hostName);
        if (hp)
        {
            addr.s_addr = static_cast<in_addr_t>(* ((unsigned long int *)hp->h_addr));
            ipstr = inet_ntoa(addr);
        }
    }

    if (ipstr == NULL)
    {
        os << errorMsg << "PLC hostname unknown";
        LOG(COMM) << os.str();
        return -1;
    }

    /*trying to connect PLC (1 second max)*/
    return (rfcPing(ipstr, 1));
}

} // end namespace
