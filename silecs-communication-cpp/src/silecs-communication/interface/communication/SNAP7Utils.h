#include <string>


//******************************************************************************
//                 HELPER DATA ACCESS FUNCTIONS FOR SNAP 7
//  They were no accessible in SNAP7 version 1.4.x
//******************************************************************************
// GET 
bool GetBitAt(void *Buffer, unsigned long Pos, unsigned long Bit);
// SET
void SetBitAt(void *Buffer, unsigned long Pos, unsigned long Bit, bool Value);



