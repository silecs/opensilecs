# silecs-communication-cpp

This component of the opensilecs PLC-framework provides a C++ library to establishes PLC-connections by using brand-specific protocols and provides an generic API to get/set values on the PLC.