#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import libxml2

class DeployDeviceNumberMigrator(object):

    def migrateDeviceNumbers(self, context):
        modified = False
        oldHeader = context.xpathEval('/SILECS-Deploy/Deploy-Classes/Class[name/text()="SilecsHeader"]')
        if len(oldHeader) == 1:
            deviceList = libxml2.newNode("device-list")
            device = libxml2.newNode("Device")
            device.newProp('label', "SilecsHeader")
            deviceList.addChild(device)
            classNameNode = oldHeader[0].xpathEval("name")[0]
            classNameNode.addPrevSibling(deviceList)
            print("Info: Device-number replaced by device-list for Class SilecsHeader")
            modified = True
            deviceNumber = oldHeader[0].xpathEval("device-number")
            deviceNumber[0].unlinkNode()
            
        silecsClasses = context.xpathEval("/SILECS-Deploy/Deploy-Classes/Class")
        for silecsClass in silecsClasses:
            deviceNumber = silecsClass.xpathEval("device-number")
            if len(deviceNumber) == 1 :  # not possible to have more than one
                deviceList = libxml2.newNode("device-list")
                classNameNode = silecsClass.xpathEval("name")[0]
                classNameNode.addPrevSibling(deviceList)
                numberOfDevices = deviceNumber[0].getContent()
                modified = True
                print("Info: Device-number replaced by device-list for Class: " + classNameNode.getContent())
                for i in range(0, int(numberOfDevices)):
                    device = libxml2.newNode("Device")
                    device.newProp('label', "GenericDevice" + str(i))
                    deviceList.addChild(device)
                    print("Info: Added device: '" + device.prop('label') + "' for Class: " + classNameNode.getContent())
                deviceNumber[0].unlinkNode()

        return modified

    def migrate(self, context):
        modified = False
        modified |= self.migrateDeviceNumbers(context)
        return modified
