#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

import libxml2
from migration.migration_0_9_0to0_10_0.migrateDeployDomain import *
from migration.migration_0_9_0to0_10_0.migrateDeployDeviceNumber import *

SilecsDeployOld = '''<?xml version="1.0" encoding="UTF-8"?>
<SILECS-Deploy silecs-version="0.9.0" created="03/04/16" updated="03/04/16" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:noNamespaceSchemaLocation="/common/home/bel/schwinn/lnx/workspace-silecs/silecs-model/src/xml/DeploySchema.xsd">
    <Information>
        <Owner user-login="schwinn"/>
        <Editor user-login="schwinn"/>
    </Information>
    <Deploy-Unit name="Test123DU" version="0.1.0">
        <Siemens-PLC system="STEP-7" model="SIMATIC_S7-300" protocol="BLOCK_MODE" base-DB-number="0"/>
    </Deploy-Unit>
    <Deploy-Instances>
      <Controller host-name="asl733" domain="ADE"/>
      <Controller host-name="asl734" domain="SDFSDAFF"/>
    </Deploy-Instances>
    <Deploy-Classes>
       <Class>
            <device-number>1</device-number>
            <name>SilecsHeader</name>
            <version>1.0.0</version>
       </Class>
       <Class>
           	<device-number>5</device-number>
           	<name>Test123</name>
           	<version>0.1.0</version>
       </Class>
       <Class>
       	    <device-list>
                <Device label="MyDevice1"/>
                <Device label="MyDevice2"/>
            </device-list>
       	<name>Test345</name>
       	<version>0.1.0</version>
       </Class>
    </Deploy-Classes>
</SILECS-Deploy>
'''

class TestMigration0_9_0to0_10_0(unittest.TestCase):

    def setUp(self):
        self.deployDoc = libxml2.parseDoc(SilecsDeployOld)

    def test_migrateDeployDeviceNumber(self):
        migrator = DeployDeviceNumberMigrator()
        migrator.migrate(self.deployDoc)
        deviceNumbers = self.deployDoc.xpathEval("/SILECS-Deploy/Deploy-Classes/Class/device-number")
        self.assertEqual(len(deviceNumbers),0)
        deviceLists = self.deployDoc.xpathEval("/SILECS-Deploy/Deploy-Classes/Class/device-list")
        self.assertEqual(len(deviceLists),3)
        newHeader = self.deployDoc.xpathEval('/SILECS-Deploy/Deploy-Classes/Class/device-list/Device[@label="SilecsHeader"]')
        self.assertEqual(len(newHeader),1)
        test123 = self.deployDoc.xpathEval('/SILECS-Deploy/Deploy-Classes/Class/name[text()="Test123"]')
        self.assertEqual(len(test123),1)

        deviceListOnFirstPosition = test123[0].xpathEval('../*')[0]
        self.assertTrue(deviceListOnFirstPosition.get_name() == 'device-list')

        genericDevices = test123[0].xpathEval('../device-list/Device')
        self.assertEqual(len(genericDevices),5)

    def test_MigrateDeployDomain(self):
        migrator = DeployDomainMigrator()
        migrator.migrate(self.deployDoc)
        controllers = self.deployDoc.xpathEval("/SILECS-Deploy/Deploy-Instances/Controller")
        for controller in controllers:
            self.assertFalse(controller.hasProp('domain'))
