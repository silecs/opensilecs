#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import libxml2

class DeployDomainMigrator(object):

    def migrateDomains(self, context):
        modified = False
        controllers = context.xpathEval("/SILECS-Deploy/Deploy-Instances/Controller")
        for controller in controllers:
            if controller.hasProp('domain'):
                controller.unsetProp('domain')
                print("Info: Domain node in Controller %r removed" % controller.prop("host-name"))
                modified = True
        return modified

    def migrate(self, context):
        modified = False
        modified |= self.migrateDomains(context)
        return modified
