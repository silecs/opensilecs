
from argparse import ArgumentParser

def parse_arguments():
    parser = ArgumentParser(description='Migration script')
    parser.add_argument("silecsDocument", action="store", help="The SILECS document to migrate")
    parser.add_argument("xmlSchema", action="store", help="Path to the new schema")
    parser.add_argument("versionOld", action="store", help="old silecs version")
    parser.add_argument("versionNew", action="store", help="new silecs version")
    parser.add_argument('--backup', dest='createBackup', action='store_true')
    parser.add_argument('--no-backup', dest='createBackup', action='store_false')
    parser.set_defaults(createBackup=False)
    
    arguments = parser.parse_args()
    return arguments
