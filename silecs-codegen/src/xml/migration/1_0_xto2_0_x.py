#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import ParseMigrationArgs

from migrationBase import MigrationBase
from migration1_0_Xto2_0_0.migrators import *

import libxml2
import sys
import FileUtils
import shutil

class Migration(MigrationBase):
    def __init__(self, silecsDocument, xmlSchema, versionOld, versionNew, createBackup):
        super(Migration, self).__init__(silecsDocument, xmlSchema, versionOld, versionNew, createBackup)

    def migrateClass(self, context, projectDir):
        modified = designValueTypeMigrator(context)
        modified |= designBlockRegisterMigrator(context)
        return modified

def main_parse():
    arguments = ParseMigrationArgs.parse_arguments()
    run_migrate(arguments.silecsDocument,
                arguments.xmlSchema,
                arguments.versionOld, 
                arguments.versionNew,
                arguments.createBackup)

def run_migrate(silecsDocument, xmlSchema, versionOld, versionNew, createBackup):
    migration = Migration(silecsDocument, xmlSchema, versionOld, versionNew, createBackup)
    migration.migrate()
    migration.backupOldFESAMakeSpecific()

if __name__ == "__main__":
    main_parse()
