#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import libxml2
from migration.migration2_0_Xto2_1_X.migrators import *

class TestMigration2_0_Xto2_1_X(unittest.TestCase):

    def test_silecsDeployMigrator(self):
        silecsDeployDocOld = '''<?xml version="1.0" encoding="UTF-8"?>
    <SILECS-Deploy silecs-version="1.0.1" created="05/03/17" updated="05/03/17" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="/common/usr/cscofe/silecs/silecs-model/1.0.1/xml/DeploySchema.xsd">
        <Information>
            <Owner user-login="schwinn" />
            <Editor user-login="schwinn" />
        </Information>
        <Deploy-Unit name="AlexTestDU" version="0.1.0" />
        <Controller host-name="asl733">
            <Siemens-PLC system="TIA-PORTAL" model="SIMATIC_S7-300" protocol="DEVICE_MODE" base-DB-number="1" />
            <SilecsDesign silecs-design-version="0.1.0" silecs-design-name="AlexTest1">
                <Device device-name="myDev1" />
                <Device device-name="myDev2" />
            </SilecsDesign>
            <SilecsDesign silecs-design-version="0.1.0" silecs-design-name="AlexTest2">
                <Device device-name="myDev3" />
                <Device device-name="myDev4" />
            </SilecsDesign>
        </Controller>
        <Controller host-name="asl744">
            <Siemens-PLC system="TIA-PORTAL" model="SIMATIC_S7-300" protocol="DEVICE_MODE" base-DB-number="1" />
            <SilecsDesign silecs-design-version="0.1.0" silecs-design-name="AlexTest1">
                <Device device-name="myDev1" />
                <Device device-name="myDev2" />
            </SilecsDesign>
            <SilecsDesign silecs-design-version="0.1.0" silecs-design-name="AlexTest2">
                <Device device-name="myDev3" />
                <Device device-name="myDev4" />
            </SilecsDesign>
        </Controller>
    </SILECS-Deploy>'''
        context = libxml2.parseDoc(silecsDeployDocOld)
        silecsDeployMigrator(context)
        print(context)
        silecsDesigns = context.xpathEval("/SILECS-Deploy/SilecsDesign")
        self.assertEqual(len(silecsDesigns), 2)
        devices = context.xpathEval("/SILECS-Deploy/Controller[@host-name='asl733']/*/Device")
        self.assertEqual(len(devices),4)
        self.assertEqual(devices[0].prop("silecs-design-ref"), "AlexTest1")
        self.assertEqual(devices[0].prop("silecs-device-label"), "myDev1")
        
    def test_removeFesaConfigurationFieldParameterFile(self):
        FesaDocOld = '''<?xml version="1.0" encoding="UTF-8"?>
    <equipment-model xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="file:/opt/fesa/fesa-model-gsi/4.2.0/xml/design/design-gsi.xsd">
        <data>
            <device-data>
                <configuration>
                    <field name="parameterFile" id="_170711152634_36"><description>ParameterFile of the PLC (*.silecsparam)</description><array type="char"><dim>512</dim></array><default>../../../generated/client/MyControllerName.silecsparam</default></field><field name="plcDeviceLabel" id="_170711152634_37"><description>Name of the related SILECS instance within the PLC mapping</description><array type="char"><dim>128</dim></array></field><field name="plcHostName" id="_170711152634_38"><description>Hostname of the PLC that contains the related SILECS class device</description><array type="char"><dim>128</dim></array></field><GSI-detailed-status-labels-field name="detailedStatus_labels" id="_170503152015_8">
                        <array2D type="char">
                            <custom-constant-dim1 constant-name-ref="DETAILED_STATUS_SIZE"/>
                            <custom-constant-dim2 constant-name-ref="MAX_DETAILED_STATUS_LABEL_LENGTH"/>
                        </array2D>
                        <default>{myStatusLabel1,myStatusLabel2}</default>
                    </GSI-detailed-status-labels-field>
                    <GSI-detailed-status-severity-field name="detailedStatus_severity" id="_170503152015_9">
                        <custom-type-array data-type-name-ref="DETAILED_STATUS_SEVERITY">
                            <custom-constant-dim constant-name-ref="DETAILED_STATUS_SIZE"/>
                        </custom-type-array>
                        <default>{INFO,INFO}</default>
                    </GSI-detailed-status-severity-field>
                    <GSI-module-status-labels-field name="moduleStatus_labels" id="_170503152015_10">
                        <array2D type="char">
                            <custom-constant-dim1 constant-name-ref="MODULE_STATUS_SIZE"/>
                            
                            <custom-constant-dim2 constant-name-ref="MAX_MODULE_STATUS_LABEL_LENGTH"/>
                        </array2D>
                        <default>{myModule1,myModule2}</default>
                    </GSI-module-status-labels-field>
                </configuration>
            </device-data>
        </data>
    </equipment-model>
    '''
        context = libxml2.parseDoc(FesaDocOld)
        removeFesaConfigurationFieldParameterFile(context)
        paramFields = context.xpathEval("//field[@name='parameterFile']")
        self.assertEqual(len(paramFields), 0)
    