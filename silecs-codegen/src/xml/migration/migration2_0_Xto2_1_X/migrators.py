#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import libxml2
#from migration import FileUtils
import os
import shutil

#------------------- def fesaClassIncludeHeaderMigrator(context,silecsDocument):
    # print("Migration-Info: The include calls in all FESA source files will be updated to use the new 'generated-silecs' folder")
    #---------------------------------------------------------- modified = False
    #-------------------- silecsClassNodes = context.xpathEval("//SILECS-Class")
    #-------------------------------------------------- if not silecsClassNodes:
        #---------------------- raise Exception('Node "SILECS-Class" not found')
    #------------------------------------ silecsClassNodes = silecsClassNodes[0]
    #--------------------------- silecsClassName = silecsClassNodes.prop("name")
    #---------------------- projectDir = FileUtils.getProjectDir(silecsDocument)
    #--- sourceFiles = FileUtils.getFesaSourceFiles(silecsClassName, projectDir)
#------------------------------------------------------------------------------ 
    # searchString = "<" + os.path.join(silecsClassName,"Common",silecsClassName) + ".h>"
    # replaceString = "<" + os.path.join(silecsClassName,"GeneratedCode",silecsClassName) + ".h>"
    #-------------------------------------------- for sourceFile in sourceFiles:
        #-------- FileUtils.replaceInFile(sourceFile,searchString,replaceString)
        #------------------------------------------------------- modified = True
    #----------------------------------------------------------- return modified

#---------------------------- def fesaClassMakeSpecificMigrator(silecsDocument):
    # print("Migration-Info:  Compiler flags for new codegen-folder will be added into the Makefile.specific of your FESA class")
    #---------------------------------------------------------- modified = False
    #---------------------- projectDir = FileUtils.getProjectDir(silecsDocument)
    #--- makeSpecific = os.path.join(projectDir,"myProject","Makefile.specific")
    #------------------------------------------ if os.path.isfile(makeSpecific):
        #------------------------------- with open(makeSpecific, "a") as myfile:
            #------ myfile.write("\nCOMPILER_FLAGS += -I./generated-silecs/cpp")
        #------------------------------------------------------- modified = True
    #----------------------------------------------------------- return modified

#----------------------- def removeSilecsDesignGenCode(context, silecsDocument):
    # print("Migration-Info: The silecs generated C++ code which was located in 'Common' moved to 'generated-silecs' in the root directory. The old code will be removed")
    #-------------------- silecsClassNodes = context.xpathEval("//SILECS-Class")
    #-------------------------------------------------- if not silecsClassNodes:
        #---------------------- raise Exception('Node "SILECS-Class" not found')
    #------------------------------------ silecsClassNodes = silecsClassNodes[0]
    #--------------------------- silecsClassName = silecsClassNodes.prop("name")
    #---------------------- projectDir = FileUtils.getProjectDir(silecsDocument)
    #---- commonFolder = os.path.join(projectDir,"src",silecsClassName,"Common")
    #------------- cppFile = os.path.join(commonFolder,silecsClassName + ".cpp")
    #--------------- hppFile = os.path.join(commonFolder,silecsClassName + ".h")
    #----------------------------------------------- if os.path.isfile(cppFile):
        #---------------------------------------------------- os.remove(cppFile)
    #----------------------------------------------- if os.path.isfile(hppFile):
        #---------------------------------------------------- os.remove(hppFile)
        
def removeSilecsDeployGenCode(context, projectDir):
    print("Migration-Info: the silecsdeploy folder 'generated' will be named 'generated-silecs' to be consistant with silecsdesign. The old folder will be removed.")
    clientFolder = os.path.join(projectDir,"generated","client")
    controllerFolder = os.path.join(projectDir,"generated","controller")
    wrapperFolder = os.path.join(projectDir,"generated","wrapper")
    if os.path.isdir(clientFolder):
        shutil.rmtree(clientFolder)
    if os.path.isdir(controllerFolder):
        shutil.rmtree(controllerFolder)
    if os.path.isdir(wrapperFolder):
        shutil.rmtree(wrapperFolder)
        
def removeFesaConfigurationFieldParameterFile(context):
    paramFields = context.xpathEval("//field[@name='parameterFile']")
    for paramField in paramFields:
        paramField.unlinkNode()
        
        
def silecsDeployMigrator(silecsDocument):
    print("Migration-Info: re-order .silecsdeploy document to provide more configuration-options")
    modified = False
    deployUnitNodes = silecsDocument.xpathEval("/SILECS-Deploy/Deploy-Unit")
    if not len(deployUnitNodes) == 1:
        return
    deployUnitNode = deployUnitNodes[0]
    controllers = silecsDocument.xpathEval("/SILECS-Deploy/Controller")
    for controller in controllers:
        silecsDesigns = controller.xpathEval("SilecsDesign")
        plc = controller.xpathEval("*[@system]")[0]
        for silecsDesign in silecsDesigns:
            devices = silecsDesign.xpathEval("Device")
            for device in devices:
                modified = True
                device.newProp("silecs-design-ref",silecsDesign.prop("silecs-design-name"))
                device.newProp("silecs-device-label",device.prop("device-name"))
                device.unsetProp("device-name")
                device.unlinkNode()
                plc.addChild(device)
            silecsDesign.unlinkNode()
            alreadyThere = silecsDocument.xpathEval("/SILECS-Deploy/SilecsDesign[@silecs-design-name='" + silecsDesign.prop("silecs-design-name") + "']")
            if len(alreadyThere) == 0:
                newDesign = libxml2.newNode("SilecsDesign") #need to re-build, otherwise error for empty </SilecsDesign>
                newDesign.newProp("silecs-design-version",silecsDesign.prop("silecs-design-version"))
                newDesign.newProp("silecs-design-name",silecsDesign.prop("silecs-design-name"))
                deployUnitNode.addNextSibling(newDesign)
            modified = True
    return modified
        
