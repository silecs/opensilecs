#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import ParseMigrationArgs

from migrationBase import MigrationBase
from migration2_0_Xto2_1_X.migrators import *

import libxml2
import sys
import FileUtils
import shutil

class Migration(MigrationBase):
    def __init__(self, silecsDocument, xmlSchema, versionOld, versionNew, createBackup):
        super(Migration, self).__init__(silecsDocument, xmlSchema, versionOld, versionNew, createBackup)

    def migrateClass(self, context, projectDir ):
        modified = False
        
        # fix reverted, since FESA does not allow to have code outside of the "src" folder
        #modified = fesaClassIncludeHeaderMigrator(context,self.silecsDocument)
        #removeSilecsDesignGenCode(context, self.silecsDocument)
        #modified |= fesaClassMakeSpecificMigrator(self.silecsDocument)
        
        fesaClassName = os.path.dirname(projectDir)
        fesaDocument = os.path.join("src", fesaClassName + ".design")
        if os.path.isfile(fesaDocument):
            contextFesa = self._parse(fesaDocument)
            modified |= removeFesaConfigurationFieldParameterFile(contextFesa)
        self.updateFESAMakeSpecific()
        return modified
    
    def migrateDeployUnit(self, context, projectDir ):
        modified = False
        modified |= silecsDeployMigrator(context)
        removeSilecsDeployGenCode(context, projectDir)
        self.updateFESAMakeSpecific()
        return modified

def main_parse():
    arguments = ParseMigrationArgs.parse_arguments()
    run_migrate(arguments.silecsDocument,
                arguments.xmlSchema,
                arguments.versionOld, 
                arguments.versionNew,
                arguments.createBackup)

def run_migrate(silecsDocument, xmlSchema, versionOld, versionNew, createBackup):
    migration = Migration(silecsDocument, xmlSchema, versionOld, versionNew, createBackup)
    migration.migrate()

if __name__ == "__main__":
    main_parse()
