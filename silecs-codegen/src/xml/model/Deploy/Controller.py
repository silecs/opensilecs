#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from model.Deploy.Device import *
from model.Deploy.SilecsDesign import *
from model.Class.Block import *
from Mapping import *
import libxml2

#-------------------------------------------------------------------------
# Return the highest bit-alignment of the given address
def whichDataAlignment(value):
    if (value % 2 != 0): return 8
    if (value % 4 != 0): return 16
    if (value % 8 != 0): return 32
    if (value % 16 != 0): return 64
    return 128
    
class Controller(object):

    BLOCK_MODE = "BLOCK_MODE"
    DEVICE_MODE = "DEVICE_MODE"
    
    def __init__(self, xmlNode):
        self.hostName = ""
        self.domain = ""
        self.system = ""
        self.model = ""
        self.protocol = ""
        self.brand = ""
        self._baseAddress =  { MEM_TYPE: None, DI_TYPE: None, DO_TYPE: None, AI_TYPE: None, AO_TYPE: None }
        self.address = { MEM_TYPE: int(-1), DI_TYPE: int(-1), DO_TYPE: int(-1), AI_TYPE: int(-1), AO_TYPE: int(-1) }
        self.offset = { MEM_TYPE: int(0), DI_TYPE: int(0), DO_TYPE: int(0), AI_TYPE: int(0), AO_TYPE: int(0) }
        self.addressFactor = 1
        
        # some variable used to calculate class/block/register-memory
        self.memSize = 0
        self.memLast = 0
        self.msize = 0
        self.regCounter = 0
        self.blockCounter = 0
        
        # The following constant are used to align the address of a device block {set of registers}.
        # With SCHNEIDER PLC, block is aligned depending on his 'worst-case' alignement {16bits or 32bits}.
        # This logic allows having a unique adress computing whatever the mode {DEVICE/BLOCK} and the PLC model.
        # Concerning SIEMENS PLC, block alignment should respect the 16bits alignement constraint for Struct&Array.
        self.baseAlignment = int(16) # the default
        self.alligment = { IOType.MEMORY: int(16), IOType.DIGITAL: int(8), IOType.ANALOG: int(16) } # the default
        self.xmlNode = xmlNode
        self.plcNode = None
        self.devices = []
    
        self.hostName = xmlNode.prop("host-name")
        if xmlNode.hasProp("domain"):
            self.domain = xmlNode.prop("domain")
        self.plcNode = Controller.getPLCNode(xmlNode)
        self.system = self.plcNode.prop('system')
        self.model = self.plcNode.prop('model')
        self.protocol = self.plcNode.prop('protocol')
        
        if self.plcNode.hasProp("base-address"):
            self._baseAddress[MEM_TYPE] = int(self.plcNode.prop('base-address'))
        if self.plcNode.hasProp("base-DB-number"):
            self._baseAddress[MEM_TYPE] = int(self.plcNode.prop('base-DB-number'))
        if self.plcNode.hasProp("DI-base-address"):
            self._baseAddress[DI_TYPE] = int(self.plcNode.prop("DI-base-address"))
        if self.plcNode.hasProp("DO-base-address"):
            self._baseAddress[DO_TYPE] = int(self.plcNode.prop("DO-base-address"))
        if self.plcNode.hasProp("AI-base-address"):
            self._baseAddress[AI_TYPE] = int(self.plcNode.prop("AI-base-address"))
        if self.plcNode.hasProp("AO-base-address"):
            self._baseAddress[AO_TYPE] = int(self.plcNode.prop("AO-base-address"))
        
        silecsHeader = Device()
        silecsHeader.silecsDeviceLabel = "SilecsHeader"
        silecsHeader.silecsDesignRef = "SilecsHeader"
        self.devices.append(silecsHeader)
        
        for deviceNode in self.plcNode.xpathEval('Device'):
            device = Device()
            device.initFromXMLNode(deviceNode)
            self.devices.append(device)
    
    def computeAddresses(self):
        for index, baseAddress in enumerate(self._baseAddress):
            if self._baseAddress[index] != None:
                self.address[index] = ( self._baseAddress[index] + self.offset[index] ) * self.addressFactor

    def connectDesignObjects(self,silecsDesigns):
        for device in self.devices:
            for design in silecsDesigns:
                if device.silecsDesignRef == design.name:
                    device.silecsDesign = design
    
    def getDevicesOfDesign(self, designName):
        deviceList = []
        for device in self.devices:
            if device.silecsDesignRef == designName:
                deviceList.append(device)
        return deviceList
    
    def getUsedDesigns(self):
        designList = []
        for device in self.devices:
            if not device.silecsDesign in designList:
                designList.append(device.silecsDesign)
        return designList
        
    @staticmethod
    def getPLCNode(controllerNode):
        hostName = controllerNode.prop("host-name")
        plcNodes = controllerNode.xpathEval('*')
        if len(plcNodes) > 1:
            raise Exception( "Error: multiple plc-nodes found for controller: " + hostName )
        if len(plcNodes) < 1:
            raise Exception( "Error: no plc-node found for controller: " + hostName )
        return plcNodes[0]
        
    @staticmethod
    def createController(controllerNode):
        plcNode = Controller.getPLCNode(controllerNode)
        type = plcNode.get_name()
        if   type == "Siemens-PLC":
            return SiemensController(controllerNode)
        elif type == "Schneider-PLC":
            return SchneiderController(controllerNode)
        elif type == "Beckhoff-PLC":
            return BeckhoffController(controllerNode)
        elif type == "Rabbit-uC":
            return RabbitController(controllerNode)
        elif type == "NI-Controller":
            return NIController(controllerNode)
        elif type == "Virtual-Controller":
            return SiemensController(controllerNode)#currently there is only S7-virtual
        else:
            raise Exception( "Controller-Type " + type + " not supported" )
        
    def hasMappingFor(self,type):
        return self._baseAddress[type] != None
    
    def computeInstAddress(self, mapping):
        if self.protocol == self.BLOCK_MODE:
            return self.computeBlockInstAddress(mapping)
        elif self.protocol == self.DEVICE_MODE:
            return self.computeDeviceInstAddress(mapping)
        else:
            raise Exception("The protocol '" + self.protocol + "' of controller '" + self.hostName + "is not known.")
    
    def computeBlockInstAddress(self, mapping):
        raise NotImplementedError()
        
    def computeDeviceInstAddress(self, mapping):
        raise NotImplementedError()
        
    def computeBlockInstAddressDefault(self, mapping):
        if not mapping.isDefined:
            return mapping.instanceAddress
        tmpInstAddr = mapping.instanceAddress
        mapping.instanceAddress += 1     #device-index
        return tmpInstAddr
    
    def computeDeviceInstAddressDefault(self, mapping):
        if not mapping.isDefined:
            return mapping.instanceAddress
        tmpInstAddr = mapping.instanceAddress
        mapping.instanceAddress = mapping.instanceAddress + mapping.deviceDataSize #absolute address
        return tmpInstAddr + mapping.classBaseAddress

    def computeNextBaseAddress(self, mapping, nbDev):
        raise NotImplementedError()
    
    # Compute the base-address of the next class
    # DEVICE or BLOCK mode use the same memory size: next base address should be the same
    # 'used-mem' info is expressed in words (/2) but address is computed in bytes
    def computeNextBaseAddressDefault(self, mapping, nbDev):
        mapping.usedData = "0 word"
        if not mapping.isDefined or ( mapping.nbBlock == 0 ) or not self.hasMappingFor(mapping.type):
            return mapping.classBaseAddress
        wordSize = (nbDev * mapping.deviceDataSize) / self.addressFactor
        self.memSize += wordSize  # global size of the plc configuration
        startAddr = (mapping.classBaseAddress - self.offset[mapping.type] * self.addressFactor) / self.addressFactor  # first word address used for this class
        self.memLast = startAddr + wordSize - 1  # last word address used for this class
        mapping.usedData = "MW%d..MW%d/%d words" % (startAddr, self.memLast, wordSize);
        mapping.classBaseAddress  = (mapping.classBaseAddress + (wordSize * self.addressFactor))  # next word address expressed in bytes

    def alignRegAddress(self, block, register, regAddress):
        raise NotImplementedError()

    # Adjust the register address relying on the SCHNEIDER Premium/Quantum and/or Beckhoff alignment constraints
    # Unity Premium/Quantum is 16bits processor and base-address is interpreted as 16bit address
    # Byte elements of array (including STRING) use 8bit alignment.
    def alignRegAddressDefault(self, block, register, regAddress):
        algnt = 16
        while(whichDataAlignment(regAddress) < algnt):
            regAddress += 1
        self.msize = register.dim1 * register.dim2 * register.stringLength * register.getDataSize()  # compute memory data size
        if register.format == 'int8' or register.format == 'char':# 8bit signed type use word alignment (16bit)
            self.msize = self.msize * 2  # while string and uint8-array use 8bit alignment
        return regAddress

    def computeNextRegAddress(self, block, register, regAddress ):
        raise NotImplementedError()
    #-------------------------------------------------------------------------
    # Compute the next register address relying on the data start-address and size.
    def computeNextRegAddressDefault(self, block, register, regAddress ):
        regAddress = regAddress + self.msize;
        return regAddress

    def computeBlkAddress(self, block, mapping, regAddr, nbDev):
        if self.protocol == self.BLOCK_MODE:
            return self.computeBlockBlkAddress(block, mapping, regAddr, nbDev)
        elif self.protocol == self.DEVICE_MODE:
            return self.computeDeviceBlkAddress(block, mapping, regAddr, nbDev)
        else:
            raise Exception("The protocol '" + self.protocol + "' of controller '" + self.hostName + "is not known.")
        
    def computeBlockBlkAddress(self, block, mapping, regAddr, nbDev):
        raise NotImplementedError()
    
    def computeDeviceBlkAddress(self, block, mapping, regAddr, nbDev):
        raise NotImplementedError()
    
    # Block-address = absolute address in the PLC memory
    def computeBlockBlkAddressDefault(self, block, mapping, regAddr, nbDev):
        # Compute block mem-size at first:
        # It corresponds to the next potential address that is 16bits or 32bits.
        nextBlockAddr = regAddr
        # Adjust the next block-address by respecting the  alignment depending on the area 
        while int(whichDataAlignment(nextBlockAddr)) < self.alligment[block.ioType]:
            nextBlockAddr += 1
            
        mapping.blockDataSize = nextBlockAddr  
        # then compute the next block DB-number and return the current one
        tmpblockAddress = mapping.blockAddress   
        # next absolute address: relies on the global class-address
        mapping.blockAddress += (nbDev * mapping.blockDataSize)
        # Count the number of block per area type,
        # accumulate the block data size to compute the total device data size
        mapping.nbBlock += 1
        mapping.deviceDataSize += mapping.blockDataSize
        return mapping.classBaseAddress + tmpblockAddress
    
    # Block-address = offset in the device memory
    def computeDeviceBlkAddressDefault(self, block, mapping, regAddr, nbDev):
        # Compute block mem-size at first:
        # it corresponds to the next potential address that is 16bits or 32bits
        nextBlockAddr = regAddr
        # Adjust the next block-address by respecting the  alignment depending on the area 
        while int(whichDataAlignment(nextBlockAddr)) < self.alligment[block.ioType]:
            nextBlockAddr += 1
            
        mapping.blockDataSize = nextBlockAddr
        # then compute the next block DB-number and return the current one
        tmpblockAddress = mapping.blockAddress   
        # next relative address: independent from the global class-address
        mapping.blockAddress +=  mapping.blockDataSize
        # Count the number of block per area type,
        # accumulate the block data size to compute the total device data size
        mapping.nbBlock += 1
        mapping.deviceDataSize += mapping.blockDataSize
        return tmpblockAddress

class SiemensController(Controller):
    def __init__(self, xmlNode):
        super(SiemensController, self).__init__(xmlNode)
        self.brand = "SIEMENS"
        self.computeAddresses()
        
    def computeDeviceInstAddress(self, mapping):
        if not mapping.isDefined:
            return mapping.instanceAddress
        
        tmpInstAddr = mapping.instanceAddress
        mapping.instanceAddress += 1     #device DB-number
        return tmpInstAddr + mapping.classBaseAddress

    def computeBlockInstAddress(self, mapping):
        return self.computeBlockInstAddressDefault(mapping)
    
    def computeNextBaseAddress(self, mapping, nbDev):
        mapping.usedData = "0 byte"
        if not mapping.isDefined or ( mapping.nbBlock == 0 ) or not self.hasMappingFor(mapping.type):
            return mapping.classBaseAddress
        if mapping.type != MEM_TYPE:
            return self.computeIONextBaseAddress(mapping, nbDev)
        if self.protocol == self.BLOCK_MODE:
            return self.computeBlockNextBaseAddress(mapping, nbDev)
        elif self.protocol == self.DEVICE_MODE:
            return self.computeDeviceNextBaseAddress(mapping, nbDev)
        else:
            raise Exception("The protocol '" + self.protocol + "' of controller '" + self.hostName + "is not known.")
        
    # Compute the base DB-number of the next instance in the SIEMENS PLC memory
    # BLOCK mode requires 1 DB per class block
    def computeBlockNextBaseAddress(self, mapping, nbDev):
        byteSize = nbDev * mapping.deviceDataSize
        self.memSize = self.memSize + byteSize  # global size of the plc configuration
        startDB = mapping.classBaseAddress  # first DB used for this class
        self.memLast = mapping.classBaseAddress + mapping.nbBlock - 1  # last DB used
        mapping.usedData = "DB%d..DB%d/%d bytes" % (startDB, self.memLast, byteSize)
        mapping.classBaseAddress =  self.memLast + 1  # next DB number
    
    # Compute the base DB-number of the next instance in the SIEMENS PLC memory
    # DEVICE mode requires 1 DB per class instance
    def computeDeviceNextBaseAddress(self, mapping, nbDev):
        byteSize = nbDev * mapping.deviceDataSize
        self.memSize = self.memSize + byteSize  # global size of the plc configuration
        startDB = mapping.classBaseAddress  # first DB used for this class
        self.memLast = mapping.classBaseAddress + nbDev - 1  # last DB used
        mapping.usedData = "DB%d..DB%d/%d bytes" % (startDB, self.memLast, byteSize)
        mapping.classBaseAddress =  self.memLast + 1  # next DB number

    # Compute the base-address of the next class in the SIEMENS PLC IO area
    # DEVICE or BLOCK mode use the same data size: next base address should be the same
    # 'used-IO' info is expressed in bytes
    def computeIONextBaseAddress(self, mapping, nbDev):
        byteSize = nbDev * mapping.deviceDataSize
        self.memSize += byteSize  # global size of the plc configuration
        startAddr = mapping.classBaseAddress              # first byte address used for this class
        self.memLast = startAddr + byteSize - 1  # last byte address used for this class
        if mapping.type == DI_TYPE or mapping.type == AI_TYPE:
            mapping.usedData = "IB%d..IB%d/%d bytes" % (startAddr, self.memLast, byteSize);
        else:             #in ['DO-', 'AO-']
            mapping.usedData = "QB%d..QB%d/%d bytes" % (startAddr, self.memLast, byteSize);
        mapping.classBaseAddress =  (mapping.classBaseAddress + byteSize)  # next byte address expressed in bytes

    # Adjust the register relying on the SIEMENS Simatic alignment constraints
    # MEMORY area: uses 8bits alignment except for array and >8bits data (16bits alignment)
    # DIGITAL area: uses 8bit alignment without any exception.
    # ANALOG area: uses 16bit alignment.
    # In case of string, its length has to be +2 to hold info on string
    def alignRegAddress(self, block, register, regAddress):
        algnt = 8
        if ( block.isAnanlogIO() or ( block.isMEMBlock() and ((register.getDataSize() > 1) or (register.dim1 > 1) or (register.dim2 > 1) or (register.stringLength > 1))) ):
                algnt = self.alligment[block.ioType]
        while(whichDataAlignment(regAddress) < algnt):
            regAddress += 1
        if register.stringLength > 1:  # register is a string, 
            if ((register.stringLength % 2) != 0): register.stringLength += 1  # adjusts the global size for byte type (word alignment)
            register.stringLength += 2  # add increment to register.stringLength first two bytes for info on string (len, maxlen)
        self.msize = register.dim1 * register.dim2 * register.stringLength * register.getDataSize()  # compute memory data size
        return regAddress

    def computeNextRegAddress(self, block, register, regAddress ):
        regAddress = regAddress + self.msize;    #compute the next address for any case
            # SIEMENS requires specific treatment in case of array.
        if register.isArrayType():
            #SIEMENS array is always followed by 16bits adressing
            algnt = 16
            while(whichDataAlignment(regAddress) < algnt):
                regAddress+=1
                
        # SIEMENS ANALOG-IO area uses 16bit addressing
        # SIEMENS MEMORY array is always followed by 16bits address
        if block.isAnanlogIO() or ( block.isMEMBlock() and register.isArrayType() ):
            while(whichDataAlignment(regAddress) < 16):
                regAddress += 1
        return regAddress
    
    def computeBlockBlkAddress(self, block, mapping, regAddr, nbDev):
        if mapping.type == MEM_TYPE:
            return self.computeBlockBlkAddressMemory(block, mapping, regAddr, nbDev)
        else:
            return self.computeBlockBlkAddressDefault(block, mapping, regAddr, nbDev)
    
    def computeDeviceBlkAddress(self, block, mapping, regAddr, nbDev):
        return self.computeDeviceBlkAddressDefault(block, mapping, regAddr, nbDev)
    
    # block-address = block DB-Number
    def computeBlockBlkAddressMemory(self, block, mapping, regAddr, nbDev):
        # Compute block mem-size at first: each block has a dedicated DB-number which contains an array of devices
        # each element of the array is a structure which must be 16bits aligned!
        nextBlockAddr = regAddr
        # Adjust the next block-address by respecting the  alignment depending on the area 
        while int(whichDataAlignment(nextBlockAddr)) < self.alligment[block.ioType]:
            nextBlockAddr += 1
        mapping.blockDataSize = nextBlockAddr
        # then compute the next block DB-number and return the current one
        tmpblockAddress = mapping.blockAddress    
        mapping.blockAddress += 1
        # Count the number of block per area type,
        # accumulate the block data size to compute the total device data size
        mapping.nbBlock += 1
        mapping.deviceDataSize += mapping.blockDataSize
        return mapping.classBaseAddress + tmpblockAddress
    
class BeckhoffController(Controller):
    addressFactor_default = 2 # Beckhoff default is 16 bit addressing
    addressFactor_CX9020_Twincat2 = 1
    addressFactor_CX9020_Twincat3 = 2
    offset_mem_BC9020_base = 0x8000 # some controllers use a memory-offset
    offset_mem_CX9020_base = 0x6000
    offset_output_base = 0x1000 # offset for analog and digital output registers
    memoryAlignment_CX9020 = 32 
    def __init__(self, xmlNode):
        super(BeckhoffController, self).__init__(xmlNode)
        self.brand =            "BECKHOFF"
        self.addressFactor = self.addressFactor_default
        if 'CX90' in self.model:
            if self.system == 'TWINCat-2':
                self.addressFactor = self.addressFactor_CX9020_Twincat2
            elif self.system == 'TWINCat-3':
                self.addressFactor = self.addressFactor_CX9020_Twincat3
            else:
                raise Exception("system '" + self.system + "' of controller '" + self.hostName + "'not supported")
            self.offset[MEM_TYPE] = self.offset_mem_CX9020_base / self.addressFactor
            self.alligment[IOType.MEMORY] = self.memoryAlignment_CX9020
        if self.model == 'BC9020':
            self.offset[MEM_TYPE] = self.offset_mem_BC9020_base / self.addressFactor
        self.offset[AO_TYPE] = self.offset_output_base / self.addressFactor
        self.offset[DO_TYPE] = self.offset_output_base / self.addressFactor
        self.computeAddresses()
            
    def computeDeviceInstAddress(self, mapping):
        return self.computeDeviceInstAddressDefault(mapping)
    
    def computeBlockInstAddress(self, mapping):
        return self.computeBlockInstAddressDefault(mapping)
    
    def computeNextBaseAddress(self, mapping, nbDev):
        return self.computeNextBaseAddressDefault(mapping, nbDev)

    def alignRegAddress(self, block, register, regAddress):
        if self.model == 'BC9020':
            return self.alignBCxxRegAddress(block, register, regAddress);
        elif self.model == 'CX9020':
            return self.alignCXxxRegAddress(block, register, regAddress);
        elif self.model == 'BK9000' or self.model == 'BK9050' or self.model == 'BK9100':
            return self.alignRegAddressDefault(block, register, regAddress);
        else:
            raise Exception("The plc-model '" + self.model + "' is not yet supported.")
    
    # Adjust the register address relying on the BECKHOFF Twincat BC9020 alignment constraints.
    # TwinCAT BCxx PLCs are 16bits processor and base-address is interpreted as 16bit address
    # 8bits alignment between elements of 8bit-data array (including strings).
    # In case of string, its length has to be +1 to hold end-terminator.
    def alignBCxxRegAddress(self, block, register, regAddress):
        algnt = 16
        while(whichDataAlignment(regAddress) < algnt):
            regAddress += 1
        if register.stringLength > 1:  # register is a string, 
            register.stringLength += 1  # TwinCAT  requires  '\0' string terminator.                             
        self.msize = register.dim1 * register.dim2 * register.stringLength * register.getDataSize()  # compute memory data size
        return regAddress
    
    # Adjust the register address relying on the BECKHOFF Twincat CX9020 alignment constraints.
    # TwinCAT CXxx PLCs are 32bits processor but base-address is interpreted as 16bit address (required even address in the mapping)
    # TWINCAT-2: 32bits alignment except for 8/16bits (16bits alignment).
    # TWINCAT-3: 16bits alignment
    # BOTH: 8bits alignment between elements of 8bit-data array (including strings).
    # In case of string, its length has to be +1 to hold end-terminator.
    def alignCXxxRegAddress(self, block, register, regAddress):
        algnt = 16
        if (register.getDataSize() > 2):
            algnt = self.alligment[block.ioType]  #depends on TwinCAT platform
        while(whichDataAlignment(regAddress) < algnt):
            regAddress += 1
        if register.stringLength > 1:  # register is a string, 
            register.stringLength += 1  # TwinCAT  requires  '\0' string terminator.
        self.msize = register.dim1 * register.dim2 * register.stringLength * register.getDataSize()  # compute memory data size
        return regAddress
    
    def computeNextRegAddress(self, block, register, regAddress):
        return self.computeNextRegAddressDefault(block, register, regAddress)
    
    def computeBlockBlkAddress(self, block, mapping, regAddr, nbDev):
        return self.computeBlockBlkAddressDefault(block, mapping, regAddr, nbDev)
    
    def computeDeviceBlkAddress(self, block, mapping, regAddr, nbDev):
        return self.computeDeviceBlkAddressDefault(block, mapping, regAddr, nbDev)
    
class SchneiderController(Controller):
    # Premium/Quantum uses 16bit adressing for 32bit values
    # Unity M340 is interpreted as 32bit address
    addressFactor_Schneider_default = 2
    memoryAlignment_M340 = 32 
    def __init__(self, xmlNode):
        super(SchneiderController, self).__init__(xmlNode)
        self.brand = "SCHNEIDER"
        self.addressFactor = self.addressFactor_Schneider_default
        if self.model == 'M340':
            self.alligment[IOType.MEMORY] = self.memoryAlignment_M340
        self.computeAddresses()

    def computeDeviceInstAddress(self, mapping):
        return self.computeDeviceInstAddressDefault(mapping)
    
    def computeBlockInstAddress(self, mapping):
        return self.computeBlockInstAddressDefault(mapping)

    def computeNextBaseAddress(self, mapping, nbDev):
        return self.computeNextBaseAddressDefault(mapping, nbDev)

    def alignRegAddress(self, block, register, regAddress):
        if self.model == 'M340':
            return self.alignM340RegAddress(block, register, regAddress);
        elif self.model == 'Premium' or self.model == 'Quantum':
            return self.alignRegAddressDefault(block, register, regAddress);
        else:
            raise Exception("The plc-model '" + self.model + "' is not yet supported.")
        
    # Adjust the register address relying on the SCHNEIDER M340 alignment constraints
    # Unity M340 is 32bits processor but base-address is interpreted as 16bit address (required even address in the mapping)
    # 32bits alignment except for 8/16bits (16bits alignment)
    # Byte elements of array (including STRING) use 8bit alignment.
    
    def alignM340RegAddress(self, block, register, regAddress):
        algnt = 16
        if (register.getDataSize() > 2):
            algnt = self.alligment[block.ioType]
        while(whichDataAlignment(regAddress) < algnt):
            regAddress += 1
        self.msize = register.dim1 * register.dim2 * register.stringLength * register.getDataSize()  # compute memory data size
        if register.format == 'int8' or register.format == 'char':# 8bit signed type use word alignment (16bit)
            self.msize = self.msize * 2  # while string and uint8-array use 8bit alignment
        return regAddress
    
    def computeNextRegAddress(self, block, register, regAddress):
        return self.computeNextRegAddressDefault(block, register, regAddress)
    
    def computeBlockBlkAddress(self, block, mapping, regAddr, nbDev):
        return self.computeBlockBlkAddressDefault(block, mapping, regAddr, nbDev)
    
    def computeDeviceBlkAddress(self, block, mapping, regAddr, nbDev):
        return self.computeDeviceBlkAddressDefault(block, mapping, regAddr, nbDev)
    
class RabbitController(Controller):
    addressFactor_DIGI_default = 2
    def __init__(self, xmlNode):
        super(RabbitController, self).__init__(xmlNode)
        self.brand = "DIGI"
        self.addressFactor = self.addressFactor_DIGI_default
        self.computeAddresses()
            
    def computeDeviceInstAddress(self, mapping):
        return self.computeDeviceInstAddressDefault(mapping)
    
    def computeBlockInstAddress(self, mapping):
        return self.computeBlockInstAddressDefault(mapping)

    def computeNextBaseAddress(self, mapping, nbDev):
        return self.computeNextBaseAddressDefault(mapping, nbDev)
    
    # Adjust the register address relying on the DIGI-Rabbit RCMx alignment constraints
    # Use  16bits processor and base-address is interpreted as 16bit address
    def alignRegAddress(self, block, register, regAddress):
        algnt = 16
        while(whichDataAlignment(regAddress) < algnt):
            regAddress += 1
        self.msize = register.dim1 * register.dim2 * register.stringLength * register.getDataSize()  # compute memory data size
        if not register.isStringType():
            if (register.getDataSize() == 1):  # but it's a 8bit type
                self.msize = self.msize * 2  # so, it's a word alignment (only string use byte alignment)
        return regAddress
    
    def computeNextRegAddress(self, block, register, regAddress):
        return self.computeNextRegAddressDefault(block, register, regAddress)
    
    def computeBlockBlkAddress(self, block, mapping, regAddr, nbDev):
        return self.computeBlockBlkAddressDefault(block, mapping, regAddr, nbDev)
    
    def computeDeviceBlkAddress(self, block, mapping, regAddr, nbDev):
        return self.computeDeviceBlkAddressDefault(block, mapping, regAddr, nbDev)

class NIController(Controller):
    def __init__(self, xmlNode):
        super(NIController, self).__init__(xmlNode)
        self.brand = "NI"
        self.computeAddresses()
        
    def computeDeviceInstAddress(self, mapping):
        return 0
        
    def computeNextBaseAddress(self, mapping, nbDev):
        mapping.usedData = "0 byte"
        mapping.classBaseAddress = 0
        
    def alignRegAddress(self, block, register, regAddress):
        return self.regCounter
    
    def computeNextRegAddress(self, block, register, regAddress):
        regAddress = regAddress + self.msize;    #compute the next address for any case
        self.msize = 0
        return regAddress
    
    def computeBlockBlkAddress(self, block, mapping, regAddr, nbDev):
        raise Exception("Block-mode not possible for NI-Controllers")
    
    def computeDeviceBlkAddress(self, block, mapping, regAddr, nbDev):
        self.blockCounter += 1
        # Count the number of block per area type,
        # accumulate the block data size to compute the total device data size
        mapping.nbBlock += 1
        mapping.deviceDataSize += mapping.blockDataSize
        return self.blockCounter
    #xmlNode.shellPrintNode()
