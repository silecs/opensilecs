#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import libxml2

from iecommon import *
from model.Deploy.Controller import *
from model.Deploy.SilecsDesign import *

class Deploy(object):

    def ___initInformation(self):
        ownerNodes = self.xmlNode.xpathEval('Information/Owner')
        if len(ownerNodes) > 1:
            raise Exception( "Error: multiple owner-nodes found in deploy-document" )
        if len(ownerNodes) < 1:
            raise Exception( "Error: no owner-node found in deploy-document" )
        if not ownerNodes[0].hasProp("user-login"):
            raise Exception( "Error: no name property found on owner-node" )
        self.owner = ownerNodes[0].prop("user-login")
        editorNodes = self.xmlNode.xpathEval('Information/Editor')
        for editorNode in editorNodes:
            if editorNode.hasProp("user-login"):
                self.editors.append(editorNode.prop("user-login"))
        
        
    def __init__(self, xmlNode):
        self.name = ""
        self.owner = ""
        self.editors = []
        self.silecsVersion = ""
        self.controllers = []
        self.silecsDesigns = []
        self.xmlNode = xmlNode
        self.silecsVersion = xmlNode.prop("silecs-version")
        self.created = xmlNode.prop("created")
        self.updated = xmlNode.prop("updated")
        self.___initInformation()

        deployUnitNodes = self.xmlNode.xpathEval('Deploy-Unit')
        if len(deployUnitNodes) > 1:
            raise Exception( "Error: multiple Deploy-Unit nodes found in deploy-document" )
        if len(deployUnitNodes) < 1:
            raise Exception( "Error: no Deploy-Unit node found in deploy-document" )
        if not deployUnitNodes[0].hasProp("name"):
            raise Exception( "Error: no name property found on Deploy-Unit node" )
        if not deployUnitNodes[0].hasProp("version"):
            raise Exception( "Error: no version property found on Deploy-Unit node" )
        self.name = deployUnitNodes[0].prop("name")
        self.version = deployUnitNodes[0].prop("version")
        
        ownerNodes = self.xmlNode.xpathEval('Information/Owner')
            
        silecsHeader = SilecsDesign()
        silecsHeader.name = "SilecsHeader"
        silecsHeader.version = "1.0.0"
        self.silecsDesigns.append(silecsHeader)
        
        for designNode in self.xmlNode.xpathEval('SilecsDesign'):
            design = SilecsDesign()
            design.initFromXMLNode(designNode)
            self.silecsDesigns.append(design)

        for controllerNode in self.xmlNode.xpathEval('Controller'):
            controller = Controller.createController(controllerNode)
            self.controllers.append(controller)
            controller.connectDesignObjects(self.silecsDesigns)
            
        #xmlNode.shellPrintNode()

    
    @staticmethod
    def getDeployFromFile(deployFile):
        root = libxml2.parseFile(deployFile)
        deployNodes = root.xpathEval('/SILECS-Deploy')
        if len(deployNodes) > 1:
            raise Exception( "Error: multiple Deploy nodes found in deploy-document" )
        if len(deployNodes) < 1:
            raise Exception( "Error: no Deploy node found in deploy-document" )
        return Deploy(deployNodes[0])