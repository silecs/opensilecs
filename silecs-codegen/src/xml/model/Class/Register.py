#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from iecommon import *
import libxml2

class Register(object):
    ___settingRegisterType = "Setting-Register"
    ___acquisitionRegisterType = "Acquisition-Register"
    ___configurationRegisterType = "Configuration-Register"
    ___settingIORegisterType = "Setting-IO-Register"
    ___acquisitionIORegisterType = "Acquisition-IO-Register"
    
    
    def __init__(self, xmlNode, customTypesXmlNode):
        self.xmlNode = xmlNode
        self.customTypesXmlNode = customTypesXmlNode
        #xmlNode.shellPrintNode()
        self.name = xmlNode.prop("name")
        self.___type = xmlNode.get_name()
        self.valueTypeNode = None
        self.valueType = ""
        self.dim1 = 1
        self.dim2 = 1
        self.stringLength = 1 # ... currently needs to be default because of some old convention
        self.format = ""
        self.custom_type_name = ""

        valueTypes = xmlNode.xpathEval("*[name()='scalar' or name()='array' or name()='array2D' or name()='string' or name()='stringArray' or name()='stringArray2D' or name()='custom-type-scalar' or name()='custom-type-array' or name()='custom-type-array2D']")
        if not valueTypes:
            iecommon.logError('ERROR: The register '+ self.name +' has no valueTypes.', True, {'errorlog': True})
        if len(valueTypes) < 1:
            iecommon.logError('ERROR: The register '+ self.name +' has no valueTypes.', True, {'errorlog': True})
        if len(valueTypes) > 1:
            iecommon.logError('ERROR: The register '+ self.name +' has multiple valueTypes.', True, {'errorlog': True})
        self.valueTypeNode = valueTypes[0]
        self.valueType = self.valueTypeNode.get_name()

        if self.valueTypeNode.hasProp("dim1"):
            self.dim1 = int(self.valueTypeNode.prop("dim1"))
        elif self.valueTypeNode.hasProp("dim"):
            self.dim1 = int(self.valueTypeNode.prop("dim"))
            
        if self.valueTypeNode.hasProp("dim2"):
            self.dim2 = int(self.valueTypeNode.prop("dim2"))

        if self.valueTypeNode.hasProp("string-length"):
            self.stringLength = int(self.valueTypeNode.prop("string-length"))

        if self.isCustomType():
            self.custom_type_name = self.valueTypeNode.prop("custom-type-name-ref")

            if self.isEnumType():
                self.format = "int32"
            else:
                raise Exception("Custom types other than 'enum' are not yet supported.")
        else:
            self.format = self.valueTypeNode.prop("format")
            

    def getNameCapitalized(self):
        return  iecommon.capitalizeString(self.name)
    
    def isUnsigned(self):
        type = self.getSilecsDataType()
        if type[0] == 'u':
            return True
        return False
    
    def isScalar(self):
        return self.valueType == 'scalar'
        
    def isString(self):
        return self.valueType == 'string'
    
    def isStringArray(self):
        return self.valueType == 'stringArray'
        
    def isArray(self):
        return self.valueType == 'array' or self.valueType == 'stringArray'
    
    def isArray2D(self):
        return self.valueType == 'array2D' or self.valueType == 'stringArray2D'
    
    def isStringType(self):
        return self.valueType == 'string' or self.valueType == 'stringArray' or self.valueType == 'stringArray2D'
    
    def isCustomScalar(self):
        return self.valueType == 'custom-type-scalar'

    def isCustomArray(self):
        return self.valueType == 'custom-type-array'

    def isCustomArray2D(self):
        return self.valueType == 'custom-type-array2D'

    def isCustomType(self):
        return self.isCustomScalar() or self.isCustomArray() or self.isCustomArray2D()

    def isEnumType(self):
        if self.customTypesXmlNode is None:
            return False

        enums = self.customTypesXmlNode.xpathEval("enum")
        for e in enums:
            if e.prop("name") == self.custom_type_name:
                return True

        return False

    def isArrayType(self):
        return self.isArray() or self.isArray2D()
    
    def isAcquisition(self):
        return self.___type == self.___acquisitionRegisterType or self.___type == self.___acquisitionIORegisterType
    
    def isSetting(self):
        return self.___type == self.___settingRegisterType or self.___type == self.___settingIORegisterType
    
    def isConfiguration(self):
        return self.___type == self.___configurationRegisterType
    
    def getCType(self):
        return  {
                 'bool'     :'bool',
                 'int8'     :'int8_t',
                 'uint8'    :'uint8_t',
                 'int16'    :'int16_t',
                 'uint16'   :'uint16_t',
                 'int32'    :'int32_t',
                 'uint32'   :'uint32_t',
                 'int64'    :'int64_t',
                 'uint64'   :'uint64_t',
                 'float32'  :'float',
                 'float64'  :'double',
                 'date'     :'double',
                 'char'     :'int8_t',
                 'byte'     :'int16_t',
                 'word'     :'int32_t',
                 'dword'    :'int64_t',
                 'int'      :'int16_t',
                 'dint'     :'int32_t',
                 'real'     :'float',
                 'dt'       :'double',
                 'dtl'      :'double',
                 'string'   :'std::string'
        }[self.getSilecsDataType()]
        
    #-------------------------------------------------------------------------
    # Given PLC data type, returns the corresponding
    # SILECS data type
    #-------------------------------------------------------------------------
    def getSilecsDataType(self):
        return {
                'byte'          :'uint8',
                'char'          :'int8',
                'unsigned char' :'uint8',
                'short'         :'int16',
                'unsigned short':'uint16',
                'long'          :'int32',   # conversion relies on 32-bit platform (FESA 3 does not support long type anymore)
                'unsigned long' :'uint32',    
                'double'        :'float64',
                'float'         :'float32',
                'word'          :'uint16',
                'dword'         :'uint32',
                 'int'          :'int16',
                 'dint'         :'int32',
                 'real'         :'float32',
                 'dt'           :'date',
                 'dtl'          :'date',
                 'uint8'        :'uint8',
                 'int8'         :'int8',
                 'uint16'       :'uint16',
                 'int16'        :'int16',
                 'uint32'       :'uint32',
                 'int32'        :'int32',
                 'uint64'       :'uint64',
                 'int64'        :'int64',
                 'float32'      :'float32',
                 'float64'      :'float64',
                 'date'         :'date',
                 'string'       : 'string',
                 'bool'         : 'bool'
        }[self.format]

    #Needed for SilecsMethodNames

    def getDataSize(self):
        return {
            'bool'        : 1,
            'uint8'       : 1,
            'int8'        : 1,
            'uint16'      : 2,
            'int16'       : 2,
            'uint32'      : 4,
            'int32'       : 4,
            'float32'     : 4,
            'uint64'      : 8,
            'int64'       : 8,
            'float64'     : 8,
            'string'      : 1,
            'date'        : 8,
            'dtl'         : 12,
            # deprecated formats
            'char'        : 1,
            'byte'        : 1,
            'word'        : 2,
            'dword'       : 4,
            'int'         : 2,
            'dint'        : 4,
            'real'        : 4,
            'dt'          : 8
        }[self.format]
        
#has some additionalValues
class ParamRegister(Register):

            
    def __init__(self ):
        self.size = 0
        self.address = 0
        self.memSize = 0
        self.bitNumber = 0
        
    def initWithParamRegisterNode(self, xmlNode, customTypesXmlNode):
        super(ParamRegister, self).__init__(xmlNode, customTypesXmlNode)
        self.size = int(self.xmlNode.prop("size"))
        self.address = int(self.xmlNode.prop("address"))
        self.memSize = int(self.xmlNode.prop("mem-size"))
        
        if self.xmlNode.prop("bit-number"):
            self.bitNumber = int(self.xmlNode.prop("bit-number"))
        


    def initWithDesignRegister(self, designRegister,address,memSize,bitNumber=None):
        newNodeTree = designRegister.xmlNode.copyNode(1) # 1 is for recursive copy
        if ( newNodeTree == None ):
            iecommon.logError('ERROR: Failed to copy register node: '+ designRegister.name +'.', True, {'errorlog': True})
        super(ParamRegister, self).__init__(designRegister.xmlNode, designRegister.customTypesXmlNode)
        self.xmlNode = newNodeTree
        self.size = self.getDataSize()
        newNodeTree.newProp("size", str(self.size))
        newNodeTree.newProp("address", str(address))
        self.address = address
        newNodeTree.newProp("mem-size", str(memSize))
        self.memSize = memSize

        if bitNumber is not None:
            newNodeTree.newProp("bit-number", str(bitNumber))
            self.bitNumber = bitNumber

        # Add an attribute to facilitate detecting the type of the custom-type.
        if self.isEnumType():
            for elem in self.xmlNode:
                if elem.hasProp("custom-type-name-ref"):
                    elem.newProp("type", "enum")
        
    def setSize(self,size):
        self.xmlNode.setProp("size", str(size))
        self.size = size
        
    def setAddress(self,address):
        self.xmlNode.setProp("address", str(int(address)))
        self.address = address
        
    def setMemSize(self,memSize):
        self.xmlNode.setProp("mem-size", str(memSize))
        self.memSize = memSize
        
        
class DesignRegister(Register):
    
    def __init__(self, xmlNode, customTypesXmlNode):
        super(DesignRegister, self).__init__(xmlNode, customTypesXmlNode)
        self.fesaFieldName = self.name
        self.generateFesaValueItem=True
        if self.xmlNode.hasProp("fesaFieldName"):
            self.fesaFieldName = xmlNode.prop("fesaFieldName")
        if self.xmlNode.hasProp("generateFesaValueItem"): #SilecsHEader does not have this attribute 
            self.generateFesaValueItem = xsdBooleanToBoolean(xmlNode.prop("generateFesaValueItem"))
            
    def getFesaFieldName(self):
        return self.fesaFieldName
    
    #-------------------------------------------------------------------------
    # Given an SILECS data type, returns the corresponding FESA data type
    # For the time being FESA does not support unsigned data types in properties
    # so here unsigned types are considered as signed to allow users to link
    # any field to properties
    #-------------------------------------------------------------------------
    def getFesaType(self):
        return  {
                 'int8'     :'int8_t',
                 'uint8'    :'int16_t',
                 'int16'    :'int16_t',
                 'uint16'   :'int32_t',
                 'int32'    :'int32_t',
                 'uint32'   :'int64_t',
                 'int64'    :'int64_t',
                 'uint64'   :'uint64_t',  #for PXI and Beckhoff, but not supported from Java! Not possible to use it in FESA properties
                 'float32'  :'float',
                 'float64'  :'double',
                 'date'     :'double',
                 'char'     :'int8_t',
                 'byte'     :'int16_t',
                 'word'     :'int32_t',
                 'dword'    :'int64_t',
                 'int'      :'int16_t',
                 'dint'     :'int32_t',
                 'real'     :'float',
                 'dt'       :'double',
                 'string'   :'char',
                 'bool'     :'bool'
        }[self.getSilecsDataType()]

    def getFesaFieldNameCapitalized(self):
        return  iecommon.capitalizeString(self.fesaFieldName)

    def getFesaDataType(self,silecsDataType):
        return  {
                 'int8'     :'int8_t',
                 'uint8'    :'int16_t',
                 'int16'    :'int16_t',
                 'uint16'   :'int32_t',
                 'int32'    :'int32_t',
                 'uint32'   :'int64_t',
                 'int64'    :'int64_t',
                 'uint64'   :'uint64_t',  # only for PXI, but not supported from Java! Not possible to use it in FESA properties
                 'float32'  :'float',
                 'float64'  :'double',
                 'date'     :'double',
                 'char'     :'int8_t',
                 'byte'     :'int16_t',
                 'word'     :'int32_t',
                 'dword'    :'int64_t',
                 'int'      :'int16_t',
                 'dint'     :'int32_t',
                 'real'     :'float',
                 'dt'       :'double',
                 'string'   :'char',
                 'bool'     :'bool'
        }[silecsDataType]
