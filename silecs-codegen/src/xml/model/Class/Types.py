class EnumItem(object):
    def __init__(self, xmlNode) -> None:
        self.xmlNode = xmlNode
        self.access = xmlNode.prop("access")
        self.symbol = xmlNode.prop("symbol")
        self.value = xmlNode.prop("value")

    def __str__(self) -> str:
        return f"EnumItem: {self.access}, {self.symbol}, {self.value}"

class Enum(object):
    def __init__(self, xmlNode) -> None:
        self.xmlNode = xmlNode

        self.name: str = xmlNode.prop("name")
        self.items: EnumItem = []
        items = xmlNode.xpathEval("item")
        for item in items:
            self.items.append(EnumItem(item))
    
    def __str__(self) -> str:
        items = ""
        for item in self.items:
            items += "\t"+ str(item) + "\n"
        return f"Enum {self.name}.\nItems:\n {items}"