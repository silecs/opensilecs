#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from iecommon import *
from model.Class.Block import *
from Mapping import *
import libxml2
from model.Class.Types import Enum

class Class(object):
    
    def __init__(self, xmlNode):
        self.xmlNode = xmlNode
        customTypes = self.xmlNode.xpathEval("custom-types")
        if customTypes is not None and len(customTypes) == 1:
            self.customTypesXmlNode = customTypes[0]
        else:
            self.customTypesXmlNode = None

        self.name = xmlNode.prop("name")
        self.version = xmlNode.prop("version")
        #xmlNode.shellPrintNode()

    def getNameCapitalized(self):
        return  iecommon.capitalizeString(self.name)

    def getBlockNodes(self):
        return self.getIOBlockNodes() + self.getMemoryBlockNodes()
    
    def getIOBlockNodes(self):
        return self.xmlNode.xpathEval("*[name()='Acquisition-IO-Block' or name()='Setting-IO-Block' or name()='Command-IO-Block']")
    
    def getMemoryBlockNodes(self):
        return self.xmlNode.xpathEval("*[name()='Acquisition-Block' or name()='Setting-Block' or name()='Command-Block' or name()='Configuration-Block']")
    
    def getEnumNodes(self):
        return self.xmlNode.xpathEval("custom-types/enum")

class ParamClass(Class):
    ___addressAttribute = { MEM_TYPE: "address", DI_TYPE: "DI-address", DO_TYPE: "DO-address", AI_TYPE: "AI-address", AO_TYPE: "AO-address" }
    ___usedDataAttribute = { MEM_TYPE: "used-mem", DI_TYPE: "used-DI", DO_TYPE: "used-DO", AI_TYPE: "used-AI", AO_TYPE: "used-AO" }

    def __init__(self):
        self.address = { MEM_TYPE: int(-1), DI_TYPE: int(-1), DO_TYPE: int(-1), AI_TYPE: int(-1), AO_TYPE: int(-1) }
        self.usedData = { MEM_TYPE: "", DI_TYPE: "", DO_TYPE: "", AI_TYPE: "", AO_TYPE: "" }
            
    def initWithParamClassNode(self, xmlNode):
        super(ParamClass, self).__init__(xmlNode)
        for index, address in enumerate(self.address):
            self.address[index] = int(self.xmlNode.prop(self.___addressAttribute[index]))
        for index, usedData in enumerate(self.usedData):
            self.usedData[index] = self.xmlNode.prop(self.___usedDataAttribute[index])
        
    def initWithDesignClass(self, designClass):
        newNode = libxml2.newNode(designClass.xmlNode.get_name())
        newNode.newProp("name", designClass.name)
        newNode.newProp("version", designClass.version)
        if designClass.customTypesXmlNode is not None:
            customNode = designClass.customTypesXmlNode.copyNode(1)
            newNode.addChild(customNode)
        super(ParamClass, self).__init__(newNode)
        for index, address in enumerate(self.address):
            newNode.newProp(self.___addressAttribute[index], str(self.address[index]))
        for index, usedData in enumerate(self.usedData):
            newNode.newProp(self.___usedDataAttribute[index], str(self.usedData[index]))

    def setAddress(self,address,type):
        self.xmlNode.setProp(self.___addressAttribute[type], str(int(address)))
        self.address[type] = address

    def setUsedData(self,usedData,type):
        self.xmlNode.setProp(self.___usedDataAttribute[type], usedData)
        self.usedData[type] = usedData
        
    def getParamBlocks(self):
        paramBlocks = []
        for blockNode in self.getBlockNodes():
            paramBlock = ParamBlock()
            paramBlock.initWithParamBlockNode(blockNode, self.customTypesXmlNode)
            paramBlocks.append(paramBlock)
        return paramBlocks
    
    def getParamMemoryBlocks(self):
        paramMemBlocks = []
        for blockNode in self.getMemoryBlockNodes():
            paramBlock = ParamBlock()
            paramBlock.initWithParamBlockNode(blockNode, self.customTypesXmlNode)
            paramMemBlocks.append(paramBlock)
        return paramMemBlocks
    
    def getDeviceInstanceNodes(self):
        deviceNodes = self.xmlNode.xpathEval("Instance")
        return deviceNodes
    
                
class DesignClass(Class):
    def __init__(self, xmlNode):
        super(DesignClass, self).__init__(xmlNode)
        self.fesaPropertyName = self.name

    def getFesaName(self):
        return self.fesaPropertyName
    
    def getDesignFesaBlocks(self):
        designFesaBlocks = []
        for block in self.getDesignBlocks():
            if block.generateFesaProperty:
                designFesaBlocks.append(block)
        return designFesaBlocks
    
    def getDesignBlocks(self):
        designBlocks = []
        for blockNode in self.getBlockNodes():
            designBlocks.append(DesignBlock(blockNode, self.customTypesXmlNode))
        return designBlocks
    
    def getDesignMemoryBlocks(self):
        designBlocks = []
        for blockNode in self.getMemoryBlockNodes():
            designBlocks.append(DesignBlock(blockNode, self.customTypesXmlNode))
        return designBlocks
    
    def getEnums(self):
        enums = []
        for node in self.getEnumNodes():
            enums.append(Enum(node))
        return enums

    @staticmethod
    def getDesignClassFromRootNode(silecsRoot):
        classNodes = silecsRoot.xpathEval('/SILECS-Design/SILECS-Class')
        if len(classNodes) > 1:
            raise "Error: multiple class-nodes found in design-document"
        if len(classNodes) < 1:
            raise "Error: no class-node found in design-document"
        return DesignClass(classNodes[0])
