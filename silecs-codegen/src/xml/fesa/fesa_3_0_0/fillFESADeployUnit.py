#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import libxml2
import iecommon

import fesa.fillFesaDeployUnitBase

class FESADeployUnitGenerator3_0_0(object):

    def fillXML(self,fesaDeployParsed,silecsDeployParsed,deployName, deployUnitSchemaPath, fesaVersion,logTopics={'errorlog': True} ):

        fesaRoot = fesaDeployParsed.xpathEval("/*")[0]
        fesaRoot.setProp("xsi:noNamespaceSchemaLocation",deployUnitSchemaPath)

        fesaVersionNode = fesaDeployParsed.xpathEval("/deploy-unit/information/fesa-version")[0]
        fesaVersionNode.setContent(fesaVersion)

        fesaDUNameNode = fesaDeployParsed.xpathEval("/deploy-unit/information/deploy-unit-name")[0]
        fesaDUNameNode.setContent(deployName)

        fesaOwnershipNode = fesaDeployParsed.xpathEval("/deploy-unit/ownership")[0]

        defaultClassTemplate = fesaDeployParsed.xpathEval('/deploy-unit/class[class-name/text()="class-name"]')
        if len(defaultClassTemplate) > 0:
            defaultClassTemplate[0].unlinkNode()

        silecsClasses = silecsDeployParsed.xpathEval("/SILECS-Deploy/Controller/SilecsDesign")
        for silecsClass in silecsClasses:

            existingClassEntry = fesaDeployParsed.xpathEval('/deploy-unit/class/class-name[text()="' + silecsClass.prop('silecs-design-name') + '"]')
            if len(existingClassEntry) > 0:
                continue # dont overwrite existing entrys

            iecommon.logDebug("Adding class '" + silecsClass.prop('silecs-design-name') + "' to DeployUnit document", {'debuglog': True})
            fesaClassNode = libxml2.newNode("class")

            fesaClassName = libxml2.newNode("class-name")
            fesaClassName.setContent(silecsClass.prop('silecs-design-name'))
            fesaClassNode.addChild(fesaClassName)

            fesaClassMajorVersion = libxml2.newNode("class-major-version")
            fesaClassMajorVersion.setContent('0')
            fesaClassNode.addChild(fesaClassMajorVersion)

            fesaClassMinorVersion = libxml2.newNode("class-minor-version")
            fesaClassMinorVersion.setContent('1')
            fesaClassNode.addChild(fesaClassMinorVersion)

            fesaClassTinyVersion = libxml2.newNode("class-tiny-version")
            fesaClassTinyVersion.setContent('0')
            fesaClassNode.addChild(fesaClassTinyVersion)

            fesaClassDeviceInstance = libxml2.newNode("device-instance")
            fesaClassDeviceInstance.setContent('required')
            fesaClassNode.addChild(fesaClassDeviceInstance)

            fesaOwnershipNode.addNextSibling(fesaClassNode)

def fillDeployUnit(fesaVersion, className, workspacePath,fesaDesignXSDPath,logTopics={'errorlog': True}):
    generator = FESADeployUnitGenerator3_0_0()
    fesa.fillFesaDeployUnitBase.fillFesaDeployUnitBase(generator,fesaVersion, className, workspacePath,fesaDesignXSDPath,logTopics)
