#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import fesa.fillFesaDesignBase

import os
import sys
import time
import zlib
import glob
import re
import datetime
import socket

import iecommon
import fesa.fesa_3_0_0.fesaTemplates
import iefiles

from iecommon import *
from model.Class.Register import DesignRegister
from model.Class.Block import DesignBlock
from model.Class.Class import DesignClass

import libxml2


class FESADesignGenerator3_0_0(object):

    #=========================================================================
    # Sub-functions
    #=========================================================================
        
    def isGSITemplate(self,parent):
        if isChildElement(parent,"GSI-Status-Property"):
            return True
        if isChildElement(parent,"GSI-Init-Property"):
            return True
        return False

    def getOrCreateStringArrayType(self,fieldNode,register):
        array2DNode = getOrCreateChildElement(fieldNode,'array2D')
        fillAttributes(array2DNode, {'type': register.getFesaType()})
        dim1Node = getOrCreateChildElement(array2DNode,'dim1')
        dim2Node = getOrCreateChildElement(array2DNode,'dim2')
        dim1Node.setContent(str(register.dim1)) # first dimension is the dimension of the array
        dim2Node.setContent(str(register.stringLength))
        return array2DNode

    def getOrCreateStringType(self,fieldNode,register):
        stringNode = getOrCreateChildElement(fieldNode,'array')
        fillAttributes(stringNode, {'type': register.getFesaType()})
        dimNode = getOrCreateChildElement(stringNode,'dim')
        dimNode.setContent(str(register.stringLength)) # first dimension is the dimension of the array
        return stringNode

    def getOrCreate2DArrayType(self,fieldNode,register):
        array2DNode = getOrCreateChildElement(fieldNode,'array2D')
        fillAttributes(array2DNode, {'type': register.getFesaType()})
        dim1Node = getOrCreateChildElement(array2DNode,'dim1')
        dim2Node = getOrCreateChildElement(array2DNode,'dim2')
        dim1Node.setContent(str(register.dim1))
        dim2Node.setContent(str(register.dim2))
        return array2DNode

    def getOrCreateArrayType(self,fieldNode,register):
        arrayNode = getOrCreateChildElement(fieldNode,'array')
        fillAttributes(arrayNode, {'type': register.getFesaType()})
        dimNode = getOrCreateChildElement(arrayNode,'dim')
        dimNode.setContent(str(register.dim1))
        return arrayNode

    def getOrCreateScalarType(self,fieldNode,register):
        scalarNode = getOrCreateChildElement(fieldNode,'scalar')
        fillAttributes(scalarNode, {'type': register.getFesaType()})
        return scalarNode

    def getOrCreateCustomScalarType(self, fieldNode, register):
        scalarNode = getOrCreateChildElement(fieldNode,'custom-type-scalar')
        fillAttributes(scalarNode, {'data-type-name-ref': register.custom_type_name})
        return scalarNode

    def getOrCreateCustomArrayType(self,fieldNode,register):
        arrayNode = getOrCreateChildElement(fieldNode,'custom-type-array')
        fillAttributes(arrayNode, {'data-type-name-ref': register.custom_type_name})
        dimNode = getOrCreateChildElement(arrayNode,'dim')
        dimNode.setContent(str(register.dim1))
        return arrayNode
    
    def getOrCreateCustom2DArrayType(self,fieldNode,register):
        array2DNode = getOrCreateChildElement(fieldNode,'custom-type-array2D')
        fillAttributes(array2DNode, {'data-type-name-ref': register.custom_type_name})
        dim1Node = getOrCreateChildElement(array2DNode,'dim1')
        dim2Node = getOrCreateChildElement(array2DNode,'dim2')
        dim1Node.setContent(str(register.dim1))
        dim2Node.setContent(str(register.dim2))
        return array2DNode

    def getOrCreateType(self,fieldNode, register):
        if register.valueType == "scalar":
            return self.getOrCreateScalarType(fieldNode,register)
        elif register.valueType == "array":
            return self.getOrCreateArrayType(fieldNode,register)
        elif register.valueType == "array2D":
            return self.getOrCreate2DArrayType(fieldNode,register)
        elif register.valueType == "string":
            return self.getOrCreateStringType(fieldNode,register)
        elif register.valueType == "stringArray":
            return self.getOrCreateStringArrayType(fieldNode,register)
        elif register.valueType == "stringArray2D":
            iecommon.logError('ERROR: In register '+register.name+' - 2D array of strings not supported in FESA.', True, {'errorlog': True})
        elif register.valueType == "custom-type-scalar":
            return self.getOrCreateCustomScalarType(fieldNode, register)
        elif register.valueType == "custom-type-array":
            return self.getOrCreateCustomArrayType(fieldNode, register)
        elif register.valueType == "custom-type-array2D":
            return self.getOrCreateCustom2DArrayType(fieldNode, register)
        else:
            iecommon.logError('ERROR: Unknown data-type:' + register.valueType, True, {'errorlog': True})
        return None

    def getOrCreateFieldRef(self,valueItemNode,FieldNameRef):
        fieldRefNode = getOrCreateNamedChildElement(valueItemNode,'data-field-ref',FieldNameRef,'field-name-ref')
        return fieldRefNode
        
    def getOrCreateAcqStampItem(self,fieldNode):
        item = getOrCreateChildElement(fieldNode,'acq-stamp-item')
        fillAttributes(item, {'direction': 'OUT', 'name': 'acqStamp'})
        scalar = getOrCreateChildElement(item,'scalar')
        fillAttributes(scalar, {'type': 'int64_t'})
        return item

    def getOrCreateUpdateFlagItem(self,fieldNode):
        item = getOrCreateChildElement(fieldNode,'update-flag-item')
        fillAttributes(item, {'direction': 'OUT', 'name': 'updateFlags', 'optional': 'true'})
        scalar = getOrCreateChildElement(item,'builtin-type-scalar')
        fillAttributes(scalar, {'data-type-name-ref': 'NOTIFICATION_UPDATE'})
        return item

    def getOrCreateCyleNameItem(self,fieldNode):
        item = getOrCreateChildElement(fieldNode,'cycle-name-item')
        fillAttributes(item, {'direction': 'OUT', 'name': 'cycleName', 'optional': 'true'})
        array = getOrCreateChildElement(item,'array')
        fillAttributes(array, {'type': 'char'})
        dim = getOrCreateChildElement(array,'dim')
        dim.setContent('32')
        return item

    def getOrCreateCyleStampItem(self,fieldNode):
        item = getOrCreateChildElement(fieldNode,'cycle-stamp-item')
        fillAttributes(item, {'direction': 'OUT', 'name': 'cycleStamp', 'optional': 'true'})
        scalar = getOrCreateChildElement(item,'scalar')
        fillAttributes(scalar, {'type': 'int64_t'})
        return item

    def getOrCreateAcquisitionContextItem(self,propertyNode):
        item = propertyNode.xpathEval('acquisition-context-item')
        if item != None:
            return item
            
        item = getOrCreateChildElement(propertyNode,'acquisition-context-item')
        item.setProp("direction","OUT")
        acqStamp = getOrCreateChildElement(item,'acqStamp')
        acqStamp.setProp("direction","OUT")
        acqStamp.setProp("name","acqStampGSI")
        acqStampScalar = getOrCreateChildElement(acqStamp,'scalar')
        acqStampScalar.setProp('type','int64_t')
        
        cycleStamp = getOrCreateChildElement(item,'cycleStamp')
        cycleStamp.setProp("direction","OUT")
        cycleStamp.setProp("name","cycleStampGSI")
        cycleStampScalar = getOrCreateChildElement(cycleStamp,'scalar')
        cycleStampScalar.setProp('type','int64_t')
        
        cycleName = getOrCreateChildElement(item,'cycleName')
        cycleName.setProp("direction","OUT")
        cycleName.setProp("name","cycleNameGSI")
        cycleNameArray = getOrCreateChildElement(cycleName,'array')
        cycleNameArray.setProp('type','char')
        cycleNameDim = getOrCreateChildElement(cycleNameArray,'custom-constant-dim')
        cycleNameDim.setProp('constant-name-ref', 'MAX_CYCLE_NAME_LENGTH')
        
        beamProcessID = getOrCreateChildElement(item,'beamProcessID')
        beamProcessID.setProp("direction","OUT")
        beamProcessID.setProp("name","beamProcessID")
        beamProcessIDScalar = getOrCreateChildElement(beamProcessID,'scalar')
        beamProcessIDScalar.setProp('type','int32_t')
        
        sequenceID = getOrCreateChildElement(item,'sequenceID')
        sequenceID.setProp("direction","OUT")
        sequenceID.setProp("name","sequenceID")
        sequenceIDScalar = getOrCreateChildElement(sequenceID,'scalar')
        sequenceIDScalar.setProp('type','int32_t')
        
        fieldRef = getOrCreateChildElement(item,'acquisition-context-field-ref')
        fieldRef.setProp("field-name-ref","acquisitionContext")
                        
    def getOrCreateAction(self,propNode,name,type,actionsNode,implementation):
        iecommon.logDebug('Generating Server-Action: ' + name, {'debuglog': True})
        action = getOrCreateChildElement(propNode,type +'-action')
        if not isChildElement(action,'server-action-ref'):
            actionSub = getOrCreateChildElement(action,'server-action-ref')
            fillAttributes(actionSub, {'server-action-name-ref': name})
        finalName = action.xpathEval("server-action-ref")[0].prop('server-action-name-ref') # could be different from name if already exists
        getActionNode = getOrCreateNamedChildElement(actionsNode,type + '-server-action',finalName)
        fillAttributes(getActionNode, {'implementation': implementation})

    def getOrCreateFieldNode(self,parent,register): #in order to place them before any GSI-specifc fields (order matters!)
        iecommon.logDebug('Generating Data-Field for register: ' + register.name, {'debuglog': True})
        if( hasChildren(parent) ):
            return getOrCreateNamedPreviousSiblingElement(parent,getFirstChild(parent),'field',register.getFesaFieldName())
        else:
            return getOrCreateNamedChildElement(parent,'field',register.getFesaFieldName())

    def addDefaultNode(self, fieldNode, defaultValue):
        defaultNode = getOrCreateChildElement(fieldNode,'default')
        defaultNode.setContent(defaultValue)
        
    def getOrCreatePLCHostNameField(self,configurationNode):
        fieldNode = getOrCreateNamedFirstChild(configurationNode,'field','plcHostName')
        descriptionNode = getOrCreateChildElement(fieldNode,'description')
        descriptionNode.setContent('Hostname of the PLC that contains the related SILECS class device')
        array = getOrCreateChildElement(fieldNode,'array')
        fillAttributes(array, {'type': 'char'})
        dim = getOrCreateChildElement(array,'dim')
        dim.setContent('128')
        return fieldNode

    def getOrCreatePLCDeviceLabelField(self,configurationNode):
        fieldNode = getOrCreateNamedFirstChild(configurationNode,'field','plcDeviceLabel')
        descriptionNode = getOrCreateChildElement(fieldNode,'description')
        descriptionNode.setContent('Name of the related SILECS instance within the PLC mapping')
        array = getOrCreateChildElement(fieldNode,'array')
        fillAttributes(array, {'type': 'char'})
        dim = getOrCreateChildElement(array,'dim')
        dim.setContent('128')
        return fieldNode

    def getOrCreatePLCClassVersionField(self,configurationNode,plcClassVersion):
        fieldNode = getOrCreateNamedFirstChild(configurationNode,'field','plcClassVersion')
        descriptionNode = getOrCreateChildElement(fieldNode,'description')
        descriptionNode.setContent('Version of the SILECS class that needs to be deployed in the controller')
        array = getOrCreateChildElement(fieldNode,'array')
        fillAttributes(array, {'type': 'char'})
        dim = getOrCreateChildElement(array,'dim')
        self.addDefaultNode(fieldNode, plcClassVersion)
        dim.setContent(str(len(plcClassVersion)))
        return fieldNode
                
    def getOrCreateRegisterValueItems(self,prop,block,direction):
        for register in block.getDesignRegisters():
            if register.generateFesaValueItem:
                self.getOrCreateValueItem(prop,register,direction)

    def getOrCreateSettingProperty(self,parent,actionsNode,block):
        iecommon.logDebug('Generating SettingProperty for Block: ' + block.name, {'debuglog': True})
        if( hasChildren(parent)):
            propsAfterCommand = parent.xpathEval("*[not(name()='command-property')]")
            prop = getOrCreateNamedPreviousSiblingElement(parent,propsAfterCommand[0], 'setting-property',block.getFesaName())
        else:
            prop = getOrCreateNamedChildElement(parent,'setting-property',block.getFesaName())
        fillAttributes(prop, {'visibility': 'development', 'multiplexed': 'false'})
        self.getOrCreateRegisterValueItems(prop,block,"INOUT")
        self.getOrCreateUpdateFlagItem(prop)
        self.getOrCreateCyleNameItem(prop)
        self.getOrCreateAction(prop,block.fesaSetServerActionName,'set',actionsNode,'custom')
        self.getOrCreateAction(prop,block.fesaGetServerActionName,'get',actionsNode,'custom')
        return prop
    
    def getOrCreateCommandProperty(self,parent,actionsNode,block):
        iecommon.logDebug('Generating CommandProperty for Block: ' + block.name, {'debuglog': True})
        if( hasChildren(parent)):
            prop = getOrCreateNamedPreviousSiblingElement(parent,getFirstChild(parent), 'command-property',block.getFesaName())
        else:
            prop = getOrCreateNamedChildElement(parent,'command-property',block.getFesaName())
        fillAttributes(prop, {'visibility': 'development', 'multiplexed': 'false'})
        self.getOrCreateRegisterValueItems(prop,block,"IN")
        self.getOrCreateAction(prop,block.fesaSetServerActionName,'set',actionsNode,'custom')
        return prop
            
    def getOrCreateAcquisitionProperty(self,parent,actionsNode,block):
        iecommon.logDebug('Generating AcquisitionProperty for Block: ' + block.name, {'debuglog': True})
        prop = ""
        if( hasChildren(parent)):
            prop = getOrCreateNamedPreviousSiblingElement(parent,getFirstChild(parent), 'acquisition-property',block.getFesaName())
        else:
            prop = getOrCreateNamedChildElement(parent,'acquisition-property',block.getFesaName())
        fillAttributes(prop, {'visibility': 'development', 'subscribable': 'true', 'multiplexed': 'false', 'on-change': 'true'})
        self.getOrCreateRegisterValueItems(prop,block,"OUT")
        self.getOrCreateAcqStampItem(prop)
        self.getOrCreateUpdateFlagItem(prop)
        self.getOrCreateCyleNameItem(prop)
        self.getOrCreateCyleStampItem(prop)
        self.getOrCreateAction(prop,block.fesaGetServerActionName,'get',actionsNode,'custom')
        return prop
                
    def getOrCreateGSISettingProperty(self,parent,actionsNode,block):
        iecommon.logDebug('Generating GSISettingProperty for Block: ' + block.name, {'debuglog': True})
        powerProps = parent.xpathEval('GSI-Power-Property')
        if len(powerProps) == 0:
            raise Exception("Error: A GSI-Power-Property needs to be available to generate a GSISettingProperty")
        powerProp = powerProps[0] # template is used --> there has to be a power prop
        prop =  getOrCreateNamedPreviousSiblingElement(parent,powerProp, 'GSI-Setting-Property',block.getFesaName())
        fillAttributes(prop, {'visibility': 'development', 'multiplexed': 'false'})
        self.getOrCreateRegisterValueItems(prop,block,"INOUT")
        self.getOrCreateUpdateFlagItem(prop)
        self.getOrCreateCyleNameItem(prop)
        self.getOrCreateAction(prop,block.fesaSetServerActionName,'set',actionsNode,'custom')
        self.getOrCreateAction(prop,block.fesaGetServerActionName,'get',actionsNode,'custom')
        return prop
            
    def getOrCreateGSIAcquisitionProperty(self,parent,actionsNode,block):
        iecommon.logDebug('Generating GSIAcquisitionProperty for Block: ' + block.name, {'debuglog': True})
        versionProps = parent.xpathEval('GSI-Version-Property')
        if len(versionProps) == 0:
            raise Exception("Error: A GSI-Version-Property needs to be available to generate a GSIAcquisitionProperty")
        prop =  getOrCreateNamedPreviousSiblingElement(parent,versionProps[0], 'GSI-Acquisition-Property',block.getFesaName())
        fillAttributes(prop, {'visibility': 'development', 'subscribable': 'true', 'multiplexed': 'false', 'on-change': 'true'})
        self.getOrCreateRegisterValueItems(prop,block,"OUT")
        self.getOrCreateAcqStampItem(prop)
        self.getOrCreateUpdateFlagItem(prop)
        self.getOrCreateCyleNameItem(prop)
        self.getOrCreateCyleStampItem(prop)
        self.getOrCreateAction(prop,block.fesaGetServerActionName,'get',actionsNode,'custom')
        self.getOrCreateAcquisitionContextItem(prop)
        return prop

    # propertyType only used during creation ! If named property already exists, it is just returned, no matter which type
    def getOrCreateFESAProperty(self,parent,actionsNode,block):
        if self.isGSITemplate(parent):
            if block.isSetting():
                if block.getFesaName() == 'Power':
                    raise Exception("Error: Please use '@generateFesaProperty=false' for the GSI-Power-Property and connect silecs-fields manually")
                else:
                    return self.getOrCreateGSISettingProperty(parent,actionsNode,block)
            elif block.isCommand():
                if block.getFesaName() == 'Init':
                    raise Exception("Error: Please use '@generateFesaProperty=false' for the GSI-Init-Property and connect silecs-fields manually")
                elif block.getFesaName() == 'Reset':
                    raise Exception("Error: Please use '@generateFesaProperty=false' for the GSI-Reset-Property and connect silecs-fields manually")
                else:
                    return self.getOrCreateCommandProperty(parent,actionsNode,block)
            elif block.isAcquisition():
                if block.getFesaName() == 'Status':
                    raise Exception("Error: Please use '@generateFesaProperty=false' for the GSI-Status-Property and connect silecs-fields manually")
                elif block.getFesaName() == 'ModuleStatus':
                    raise Exception("Error: Please use '@generateFesaProperty=false' for the GSI-ModuleStatus-Property and connect silecs-fields manually")
                elif block.getFesaName() == 'Version':
                    raise Exception("Error: Please use '@generateFesaProperty=false' for the GSI-Version-Property and connect silecs-fields manually")
                else:
                    return self.getOrCreateGSIAcquisitionProperty(parent,actionsNode,block)
            elif block.isConfiguration():
                return self.getOrCreateGSIAcquisitionProperty(parent,actionsNode,block);
            else:
                raise Exception( "Cannot identify FESA GSI Property-type to use for block:" + block.name )
        else:
            if block.isSetting():
                return self.getOrCreateSettingProperty(parent,actionsNode,block)
            elif block.isCommand():
                return self.getOrCreateCommandProperty(parent,actionsNode,block)
            elif block.isAcquisition():
                return self.getOrCreateAcquisitionProperty(parent,actionsNode,block)
            elif block.isConfiguration():
                return self.getOrCreateAcquisitionProperty(parent,actionsNode,block);
            else:
                raise Exception( "Cannot identify FESA Property-type to use for block:" + block.name )

    def getOrCreateValueItem(self,propertyNode,register,direction):
        iecommon.logDebug('Generating ValueItem for Register: ' + register.name, {'debuglog': True})
        result = propertyNode.xpathEval("value-item[@name='" + register.getFesaFieldName() + "']")
        if len(result):
            return result[0]
        if( hasChildren(propertyNode)):
            result = propertyNode.xpathEval("*[not(name()='description') and not(name()='export') and not(name()='filter-item')]") # skip possible first elements to get right position
            valueItemNode =  getOrCreateNamedPreviousSiblingElement(propertyNode,result[0],'value-item',register.getFesaFieldName())
        else:
            valueItemNode = getOrCreateNamedChildElement(propertyNode,'value-item',register.getFesaFieldName())
        fillAttributes(valueItemNode, {'direction': direction})
        self.getOrCreateType(valueItemNode,register)
        self.getOrCreateFieldRef(valueItemNode,register.getFesaFieldName())

    #-------------------------------------------------------------------------
    # Generates the data node of the FESA design document
    # parsing the SILECS class document
    #-------------------------------------------------------------------------
    def genData(self,designClass, doc,logTopics):
        iecommon.logDebug("Creating data fields", logTopics)
        
        dataNode = doc.xpathEval('/equipment-model/data')[0]
        iecommon.logError("len(dataNode)" + dataNode.get_name(), logTopics)
        deviceDataNode = getOrCreateChildElement(dataNode,'device-data')
        configurationNode = getOrCreateChildElement(deviceDataNode,'configuration')
        settingNode = getOrCreateChildElement(deviceDataNode,'setting')
        acquisitionNode = getOrCreateChildElement(deviceDataNode,'acquisition')
        self.getOrCreatePLCHostNameField(configurationNode)
        self.getOrCreatePLCDeviceLabelField(configurationNode)

        for block in designClass.getDesignBlocks():
            for reg in block.getDesignRegisters():
                if reg.isSetting():
                    fieldNode = self.getOrCreateFieldNode(settingNode,reg)
                    fillAttributes(fieldNode, {'multiplexed': 'false', 'persistent': 'true'})
                elif reg.isAcquisition():
                    fieldNode = self.getOrCreateFieldNode(acquisitionNode,reg)
                    fillAttributes(fieldNode, {'multiplexed': 'false', 'persistent': 'false'})
                elif reg.isConfiguration():
                    fieldNode = self.getOrCreateFieldNode(configurationNode,reg)
                else:
                    raise Exception( "Cannot identify register-type to use for register:" + reg.name )
                self.getOrCreateType(fieldNode,reg)

        globalDataNode = getOrCreateChildElement(dataNode,'global-data')
        globalConfigurationNode = ""
        if len(globalDataNode.xpathEval("configuration")) == 0:
            if len( globalDataNode.xpathEval("*")) == 0:
                globalConfigurationNode = getOrCreateChildElement(globalDataNode,'configuration')
            else:
                globalConfigurationNode = getOrCreatePreviousSiblingElement(getFirstChild(globalDataNode),'configuration')
        else:
            globalConfigurationNode = globalDataNode.xpathEval("configuration")[0]
        self.getOrCreatePLCClassVersionField(globalConfigurationNode,designClass.version)

    #-------------------------------------------------------------------------
    # Generates the interface/device-interface node of the 
    # FESA design document parsing the SILECS class document
    #-------------------------------------------------------------------------
    def genDeviceInterface(self,designClass, doc,logTopics):
        # parse the FESA design document to find the device-interface node and create children
        interfaceNode = doc.xpathEval('/equipment-model/interface')[0]
        globalInterfaceNode = getOrCreateChildElement(interfaceNode,'global-interface')
        deviceInterfaceNode = getOrCreatePreviousSiblingElement(globalInterfaceNode,'device-interface')
        settingNode = getOrCreateChildElement(deviceInterfaceNode,'setting')
        acquisitionNode = getOrCreateChildElement(deviceInterfaceNode,'acquisition')
        actionsNode = doc.xpathEval('/equipment-model/actions')[0]
        for block in designClass.getDesignBlocks():
            if not block.generateFesaProperty:
                continue #skip this block
            if block.isSetting() or block.isCommand():
                propertyNode = self.getOrCreateFESAProperty(settingNode, actionsNode, block)
            else:
                propertyNode = self.getOrCreateFESAProperty(acquisitionNode,actionsNode,block)
            
            
    def fillXML(self,fesaVersion, className, fesaRoot, silecsRoot,logTopics={'errorlog': True}):

        #print etree.tostring(silecsRoot, pretty_print=True)
        #-------------------------------------------------------------------------
        # Fill section <information>
        #-------------------------------------------------------------------------
        informationNode = fesaRoot.xpathEval("/equipment-model/information")[0]
        informationNode.xpathEval("class-name")[0].setContent(className)
        informationNode.xpathEval("fesa-version")[0].setContent(fesaVersion)

        #-------------------------------------------------------------------------
        # Fill section <ownership>
        #-------------------------------------------------------------------------
        creatorNode = fesaRoot.xpathEval("//ownership/creator")[0]
        owner = silecsRoot.xpathEval('//Information/Owner')[0]

        creatorNode.setProp('login', owner.prop('user-login'))    # == Owner in SILECS design document
        designClass = DesignClass.getDesignClassFromRootNode(silecsRoot)
        # Generate data fields
        self.genData(designClass, fesaRoot,logTopics)

        # Generate properties, Actions, Events, Scheduling Units
        self.genDeviceInterface(designClass, fesaRoot,logTopics)

        return fesaRoot

#-------------------------------------------------------------------------
# Fill the FESA design document - entry point of the plugin
#-------------------------------------------------------------------------
def fillDesignFile(fesaVersion, className, workspacePath,fesaDesignXSDPath,logTopics={'errorlog': True}):
    generator = FESADesignGenerator3_0_0()
    fesa.fillFesaDesignBase.fillDesignFileBase(generator,fesaVersion, className, workspacePath,fesaDesignXSDPath,logTopics)

