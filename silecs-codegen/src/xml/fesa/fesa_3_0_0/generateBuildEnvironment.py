#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import time
import zlib
import glob
import re
import datetime
import socket

import iecommon
import fesaTemplates
import iefiles

from iecommon import *

#-------------------------------------------------------------------------    
# Generates two Makefile.specific, one for the class design
# and another for the deploy unit
#-------------------------------------------------------------------------    
def genMakefileClass(projectPath, logTopics={'errorlog': True} ):
    # Generate makefile for class design
    source = fesaTemplates.genMakeDesign()
    makefile = projectPath + "/" + "Makefile.specific"
    if os.path.isfile(makefile): #dont overwrite
        return
    fdesc = open(makefile, "w")
    iecommon.logInfo("Generate makefile.specific for class design:  " + makefile, logTopics)
    fdesc.write(source)      
    fdesc.close()

def genMakefileDU(projectPath, logTopics={'errorlog': True}):
    # Generate makefile for class design
    source = fesaTemplates.genMakeDeploy()
    # Write file and save
    makefile = projectPath + "/" + "Makefile.specific"
    if os.path.isfile(makefile): #dont overwrite
        return
    fdesc = open(makefile, "w")
    iecommon.logInfo("Generate makefile.specific for deploy unit:  " + makefile, logTopics)
    fdesc.write(source)      
    fdesc.close()

def genCProjectFile(projectPath, logTopics={'errorlog': True}):
    # Generate makefile for class design
    source = fesaTemplates.genCProject()
    
    # Write file and save
    newFile = projectPath + "/" + ".cproject"
    if os.path.isfile(newFile): #dont overwrite
        return
    fdesc = open(newFile, "w")
    iecommon.logInfo("Generate .cproject", logTopics)
    fdesc.write(source)      
    fdesc.close()
