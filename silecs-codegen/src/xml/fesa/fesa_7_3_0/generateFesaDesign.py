#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import fesa.fillFesaDesignBase
import fesa.fesa_3_1_0.generateFesaDesign

from iecommon import *

def fillDesignFile(fesaVersion, className, workspacePath,fesaDesignXSDPath,logTopics={'errorlog': True}):
    generator = FESADesignGenerator7_3_0()
    fesa.fillFesaDesignBase.fillDesignFileBase(generator,fesaVersion, className, workspacePath,fesaDesignXSDPath,logTopics)
        
class FESADesignGenerator7_3_0(fesa.fesa_3_1_0.generateFesaDesign.FESADesignGenerator3_1_0):

    #overwrite 3_0_0 version (multiplexed --> cycle-bound)
    def getOrCreateAcquisitionProperty(self,parent,actionsNode,block):
        iecommon.logDebug('Generating AcquisitionProperty for Block: ' + block.name, {'debuglog': True})
        prop = ""
        if( hasChildren(parent)):
            prop = getOrCreateNamedPreviousSiblingElement(parent,getFirstChild(parent), 'acquisition-property',block.getFesaName())
        else:
            prop = getOrCreateNamedChildElement(parent,'acquisition-property',block.getFesaName())
        fillAttributes(prop, {'visibility': 'development', 'subscribable': 'true', 'cycle-bound': 'false', 'on-change': 'true'})
        self.getOrCreateRegisterValueItems(prop,block,"OUT")
        self.getOrCreateAcqStampItem(prop)
        self.getOrCreateUpdateFlagItem(prop)
        self.getOrCreateCyleNameItem(prop)
        self.getOrCreateCyleStampItem(prop)
        self.getOrCreateAction(prop,block.fesaGetServerActionName,'get',actionsNode,'custom')
        return prop

    #overwrite 3_0_0 version (multiplexed --> cycle-bound)
    def getOrCreateGSIAcquisitionProperty(self,parent,actionsNode,block):
        iecommon.logDebug('Generating GSIAcquisitionProperty for Block: ' + block.name, {'debuglog': True})
        versionProps = parent.xpathEval('GSI-Version-Property')
        if len(versionProps) == 0:
            raise Exception("Error: A GSI-Version-Property needs to be available to generate a GSIAcquisitionProperty")
        prop =  getOrCreateNamedPreviousSiblingElement(parent,versionProps[0], 'GSI-Acquisition-Property',block.getFesaName())
        fillAttributes(prop, {'visibility': 'development', 'subscribable': 'true', 'cycle-bound': 'false', 'on-change': 'true'})
        self.getOrCreateRegisterValueItems(prop,block,"OUT")
        self.getOrCreateAcqStampItem(prop)
        self.getOrCreateUpdateFlagItem(prop)
        self.getOrCreateCyleNameItem(prop)
        self.getOrCreateCyleStampItem(prop)
        self.getOrCreateAction(prop,block.fesaGetServerActionName,'get',actionsNode,'custom')
        self.getOrCreateAcquisitionContextItem(prop)
        return prop

    #overwrite 3_0_0 version (multiplexed --> cycle-bound for acquisition fileds)
    def genData(self,designClass, doc,logTopics):
        iecommon.logDebug("Creating data fields", logTopics)
        
        dataNode = doc.xpathEval('/equipment-model/data')[0]
        iecommon.logError("len(dataNode)" + dataNode.get_name(), logTopics)
        deviceDataNode = getOrCreateChildElement(dataNode,'device-data')
        configurationNode = getOrCreateChildElement(deviceDataNode,'configuration')
        settingNode = getOrCreateChildElement(deviceDataNode,'setting')
        acquisitionNode = getOrCreateChildElement(deviceDataNode,'acquisition')
        self.getOrCreatePLCHostNameField(configurationNode)
        self.getOrCreatePLCDeviceLabelField(configurationNode)

        for block in designClass.getDesignBlocks():
            for reg in block.getDesignRegisters():
                if reg.isSetting():
                    fieldNode = self.getOrCreateFieldNode(settingNode,reg)
                    fillAttributes(fieldNode, {'multiplexed': 'false', 'persistent': 'true'})
                elif reg.isAcquisition():
                    fieldNode = self.getOrCreateFieldNode(acquisitionNode,reg)
                    fillAttributes(fieldNode, {'cycle-bound': 'false', 'persistent': 'false'})
                elif reg.isConfiguration():
                    fieldNode = self.getOrCreateFieldNode(configurationNode,reg)
                else:
                    raise Exception( "Cannot identify register-type to use for register:" + reg.name )
                self.getOrCreateType(fieldNode,reg)

        globalDataNode = getOrCreateChildElement(dataNode,'global-data')
        globalConfigurationNode = ""
        if len(globalDataNode.xpathEval("configuration")) == 0:
            if len( globalDataNode.xpathEval("*")) == 0:
                globalConfigurationNode = getOrCreateChildElement(globalDataNode,'configuration')
            else:
                globalConfigurationNode = getOrCreatePreviousSiblingElement(getFirstChild(globalDataNode),'configuration')
        else:
            globalConfigurationNode = globalDataNode.xpathEval("configuration")[0]
        self.getOrCreatePLCClassVersionField(globalConfigurationNode,designClass.version)

        # Fill custom-types with enums.
        enums = designClass.getEnums()
        customTypesNode = doc.xpathEval('/equipment-model/custom-types')[0]
        for enum in enums:
            enumElement = getOrCreateNamedChildElement(customTypesNode, "enum", enum.name)
            existing_items = enumElement.xpathEval("item")
            to_add = []
            to_edit = []

            # Loop through all new enums and check which should be added and which edited.
            for item in enum.items:
                if item.symbol in [item.prop("symbol") for item in existing_items]:
                    to_edit.append(item)
                else:
                    to_add.append(item)

            # Loop though all existing enums and remove any which are not in new enums.
            to_add_edit = to_add + to_edit
            for existing in existing_items:
                if existing.prop("symbol") not in [item.symbol for item in to_add_edit]:
                    existing.unlinkNode()
                    existing.freeNode()

            # Edit existing items.
            for item in to_edit:
                for existing in existing_items:
                    if existing.prop("symbol") != item.symbol:
                        continue
                    existing.setProp("access", item.access)
                    existing.setProp("value", item.value)
            
            # Add missing items.
            for item in to_add:
                itemElement = libxml2.newNode("item")
                enumElement.addChild(itemElement)
                itemElement.setProp("access", item.access)
                itemElement.setProp("value", item.value)
                itemElement.setProp("symbol", item.symbol)
