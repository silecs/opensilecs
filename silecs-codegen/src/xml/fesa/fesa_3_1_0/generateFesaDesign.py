#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import fesa.fillFesaDesignBase
import fesa.fesa_3_0_0.generateFesaDesign

from iecommon import *

def fillDesignFile(fesaVersion, className, workspacePath,fesaDesignXSDPath,logTopics={'errorlog': True}):
    generator = FESADesignGenerator3_1_0()
    fesa.fillFesaDesignBase.fillDesignFileBase(generator,fesaVersion, className, workspacePath,fesaDesignXSDPath,logTopics)

class FESADesignGenerator3_1_0(fesa.fesa_3_0_0.generateFesaDesign.FESADesignGenerator3_0_0):

    #overwrite 3_0_0 version
    def getOrCreateAcquisitionContextItem(self,propertyNode):
        items = propertyNode.xpathEval('acquisition-context-item')
        if items:
            return items[0]
            
        item = getOrCreateChildElement(propertyNode,'acquisition-context-item')
        item.setProp("direction","OUT")

        processIndex = getOrCreateChildElement(item,'processIndex')
        processIndex.setProp("direction","OUT")
        processIndex.setProp("name","processIndex")
        processIndex = getOrCreateChildElement(processIndex,'scalar')
        processIndex.setProp('type','int32_t')

        sequenceIndex = getOrCreateChildElement(item,'sequenceIndex')
        sequenceIndex.setProp("direction","OUT")
        sequenceIndex.setProp("name","sequenceIndex")
        sequenceIndex = getOrCreateChildElement(sequenceIndex,'scalar')
        sequenceIndex.setProp('type','int32_t')

        chainIndex = getOrCreateChildElement(item,'chainIndex')
        chainIndex.setProp("direction","OUT")
        chainIndex.setProp("name","chainIndex")
        chainIndex = getOrCreateChildElement(chainIndex,'scalar')
        chainIndex.setProp('type','int32_t')

        eventNumber = getOrCreateChildElement(item,'eventNumber')
        eventNumber.setProp("direction","OUT")
        eventNumber.setProp("name","eventNumber")
        eventNumber = getOrCreateChildElement(eventNumber,'scalar')
        eventNumber.setProp('type','int32_t')

        timingGroupID = getOrCreateChildElement(item,'timingGroupID')
        timingGroupID.setProp("direction","OUT")
        timingGroupID.setProp("name","timingGroupID")
        timingGroupID = getOrCreateChildElement(timingGroupID,'scalar')
        timingGroupID.setProp('type','int32_t')

        acquisitionStamp = getOrCreateChildElement(item,'acquisitionStamp')
        acquisitionStamp.setProp("direction","OUT")
        acquisitionStamp.setProp("name","acquisitionStamp")
        acquisitionStamp = getOrCreateChildElement(acquisitionStamp,'scalar')
        acquisitionStamp.setProp('type','int64_t')

        eventStamp = getOrCreateChildElement(item,'eventStamp')
        eventStamp.setProp("direction","OUT")
        eventStamp.setProp("name","eventStamp")
        eventStamp = getOrCreateChildElement(eventStamp,'scalar')
        eventStamp.setProp('type','int64_t')

        processStartStamp = getOrCreateChildElement(item,'processStartStamp')
        processStartStamp.setProp("direction","OUT")
        processStartStamp.setProp("name","processStartStamp")
        processStartStamp = getOrCreateChildElement(processStartStamp,'scalar')
        processStartStamp.setProp('type','int64_t')

        sequenceStartStamp = getOrCreateChildElement(item,'sequenceStartStamp')
        sequenceStartStamp.setProp("direction","OUT")
        sequenceStartStamp.setProp("name","sequenceStartStamp")
        sequenceStartStamp = getOrCreateChildElement(sequenceStartStamp,'scalar')
        sequenceStartStamp.setProp('type','int64_t')

        chainStartStamp = getOrCreateChildElement(item,'chainStartStamp')
        chainStartStamp.setProp("direction","OUT")
        chainStartStamp.setProp("name","chainStartStamp")
        chainStartStamp = getOrCreateChildElement(chainStartStamp,'scalar')
        chainStartStamp.setProp('type','int64_t')

        fieldRef = getOrCreateChildElement(item,'acquisition-context-field-ref')
        fieldRef.setProp("field-name-ref","acquisitionContext")

