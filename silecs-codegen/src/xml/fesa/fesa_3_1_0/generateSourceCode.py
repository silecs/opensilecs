#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import fesa.fesa_3_0_0.generateSourceCode

def genHSource(className, silecsRoot, fesaRoot, sourcePath,logTopics):
    return fesa.fesa_3_0_0.generateSourceCode.genHSource(className, silecsRoot, fesaRoot, sourcePath,logTopics)

def genCppSource(className, silecsRoot, fesaRoot, sourcePath,logTopics):
    return fesa.fesa_3_0_0.generateSourceCode.genCppSource(className, silecsRoot, fesaRoot, sourcePath,logTopics)

def genCppFiles(className, workspacePath, silecsDesignFilePath,logTopics={'errorlog': True}):
    return fesa.fesa_3_0_0.generateSourceCode.genCppFiles(className, workspacePath, silecsDesignFilePath,logTopics)
