#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import libxml2
import os

import iefiles
import iecommon

def fillFesaDeployUnitBase(generator,workspacePath,deployName, deployUnitSchemaPath, fesaVersion, logTopics):
    iecommon.logDebug("deployUnitSchemaPath: " + deployUnitSchemaPath, {'debuglog': True})
    iecommon.logDebug("fesaVersion: " + fesaVersion, {'debuglog': True})
    fesaDeployFile = iefiles.getFesaDeployFilePath(workspacePath,deployName)
    silecsDeployFile = iefiles.getSilecsDeployFilePath(workspacePath,deployName)
    if (not os.path.isfile(fesaDeployFile)):
        raise Exception( "FESA DeployUnit Missing: " + fesaDeployFile )
    if (not os.path.isfile(silecsDeployFile)):
        raise Exception( "SILECS Deploy File Missing: " + silecsDeployFile )
    fesaDeployParsed = libxml2.parseFile(fesaDeployFile)
    silecsDeployParsed = libxml2.parseFile(silecsDeployFile)
    generator.fillXML(fesaDeployParsed,silecsDeployParsed,deployName, deployUnitSchemaPath, fesaVersion, logTopics)
    iefiles.saveXMLToFile(fesaDeployFile,fesaDeployParsed)
