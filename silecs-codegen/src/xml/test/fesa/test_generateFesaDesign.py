#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import fesa.fesa_7_3_0.generateFesaDesign

fesa_version = "7.5.0"

import libxml2
import unittest

simpleSilecsDesign = '''<?xml version="1.0" encoding="UTF-8"?>
<SILECS-Design silecs-version="0.10.0" created="03/02/16" updated="03/02/16" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:noNamespaceSchemaLocation="/common/home/bel/schwinn/lnx/workspace-silecs/silecs-model/src/xml/DesignSchema.xsd">
    <Information>
        <Owner user-login="schwinn"/>
        <Editor user-login="schwinn"/>
    </Information>
    <SILECS-Class name="Test123" version="0.1.0" domain="OPERATIONAL">
        <Setting-Block name="Setting" generateFesaProperty="true">
            <Setting-Register name="mySettingRegister" generateFesaValueItem="true">
                <scalar format="uint8"/>
            </Setting-Register>
        </Setting-Block>
         <Command-Block name="Command" generateFesaProperty="true">
            <Setting-Register name="mySettingRegister" generateFesaValueItem="true">
                <scalar format="uint8"/>
            </Setting-Register>
        </Command-Block>
        <Acquisition-Block name="Acquisition" generateFesaProperty="true">
            <Acquisition-Register name="myAcqRegister" generateFesaValueItem="true">
                <scalar format="uint8"/>
            </Acquisition-Register>
        </Acquisition-Block>
    </SILECS-Class>
</SILECS-Design>'''
simpleSilecsDesignRoot = libxml2.parseDoc(simpleSilecsDesign)

class TestGenerateFesaDesign(unittest.TestCase):
    def setUp(self):
        self.generator = fesa.fesa_7_3_0.generateFesaDesign.FESADesignGenerator7_3_0()

    def test_fillXML_EmptyTemplate(self):
        fesaRoot = libxml2.parseFile("test/fesa/emptyTemplate.xml")
        self.generator.fillXML(fesa_version, 'AllTypesFESA', fesaRoot, simpleSilecsDesignRoot, logTopics={'errorlog': True})
        self.assertTrue(fesaRoot.xpathEval('/equipment-model/events') is not None)
        self.assertTrue(fesaRoot.xpathEval('/equipment-model/scheduling-units') is not None)

    def test_fillXML_GSITemplate(self):
        fesaRoot = libxml2.parseFile("test/fesa/GSIClassTemplate.xml")
        self.generator.fillXML(fesa_version, 'AllTypesFESA', fesaRoot, simpleSilecsDesignRoot, logTopics={'errorlog': True})
        self.assertTrue(fesaRoot.xpathEval('/equipment-model/events') is not None)
        self.assertTrue(fesaRoot.xpathEval('/equipment-model/scheduling-units') is not None)

        acquisition = fesaRoot.xpathEval('/equipment-model/interface/device-interface/acquisition')[0]
        firstGSIAcqProp = acquisition.xpathEval('GSI-Acquisition-Property')[0]

        self.assertTrue(acquisition.xpathEval('*')[0].prop('name') != firstGSIAcqProp.prop('name')) # check if generated at right position
        self.assertEqual(firstGSIAcqProp.prop('name'), "Acquisition")
        valueItems = firstGSIAcqProp.xpathEval('value-item')
        self.assertTrue(valueItems is not None)

        setting = fesaRoot.xpathEval('/equipment-model/interface/device-interface/setting')[0]
        firstGSISettingProp = setting.xpathEval('GSI-Setting-Property')[0]
        self.assertTrue(acquisition.xpathEval('*')[0].prop('name') != firstGSISettingProp.prop('name')) # check if generated at right position

    def test_fillXML_GSITemplate_Exceptions(self):
        fesaRoot = libxml2.parseFile("test/fesa/GSIClassTemplate.xml")
        myRoot = simpleSilecsDesignRoot
        commandBlock = myRoot.xpathEval('/SILECS-Design/SILECS-Class/Command-Block')[0]

        commandBlock.setProp("name", "Init")
        self.assertRaises(Exception, lambda: self.generator.fillXML(fesa_version, 'AllTypesFESA', fesaRoot, myRoot, logTopics={'errorlog': True}))

        commandBlock.setProp("name", "Reset")
        self.assertRaises(Exception, lambda: self.generator.fillXML(fesa_version, 'AllTypesFESA', fesaRoot, myRoot, logTopics={'errorlog': True}))

    def testFillXML_AllTypes(self):
        fesaRoot = libxml2.parseFile("test/fesa/GSIClassTemplate.xml")
        silecsRoot = libxml2.parseFile("test/AllTypesFESA.silecsdesign")
        fesaRoot = self.generator.fillXML(fesa_version, 'AllTypesFESA', fesaRoot, silecsRoot, logTopics={'errorlog': True})
        fesaNewDocPath = "test/generated_temp/AllTypesFESA.design"
        fesaCompareDocPath = "test/generated_correct/AllTypesFESA.design"
        fesaCompareTestDocPath = "test/generated_correct/AllTypesFESA.design.testing"
        with open(fesaNewDocPath, 'w') as fd:
            fesaRoot.saveTo(fd, format = True)

        correct = libxml2.readFile(fesaCompareDocPath, "utf-8", 0)
        for node in correct.xpathEval("//*"):
            if node.hasProp("id"):
                node.unsetProp("id")
            if node.name == "equipment-model":
                node.setProp("xsi:noNamespaceSchemaLocation", "../design-gsi.xsd")

        correct.saveFormatFile(fesaCompareTestDocPath, format=True)

        print('FESA design document saved successfully')
        # Compare files.
        with open(fesaNewDocPath) as a, open(fesaCompareTestDocPath) as b:
            self.assertListEqual(list(a), list(b))
