#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import libxml2
import unittest

import fesa.fesa_3_0_0.fillFESADeployUnit

simpleSilecsDeploy = '''<?xml version="1.0" encoding="UTF-8"?>
<SILECS-Deploy silecs-version="0.10.0" created="04/05/16" updated="04/05/16" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:noNamespaceSchemaLocation="">
    <Information>
        <Owner user-login="MaxMustermann"/>
        <Editor user-login="MaxMustermann"/>
    </Information>
    <Deploy-Unit name="MyTestDeploy" version="0.1.0">
        <Siemens-PLC system="STEP-7" model="SIMATIC_S7-300" protocol="BLOCK_MODE" base-DB-number="0"/>
    </Deploy-Unit>
    <Controller host-name="asl1234">
      <SilecsDesign silecs-design-version="0.1.0" silecs-design-name="MyTestClass1">
            <Device device-name="myDevice1" />
      </SilecsDesign>
      <SilecsDesign silecs-design-version="0.1.0" silecs-design-name="MyTestClass2">
            <Device device-name="myDevice2" />
      </SilecsDesign>
    </Controller>
</SILECS-Deploy>'''

simpleSilecsDeployParsed = libxml2.parseDoc(simpleSilecsDeploy)
simpleFesaDeployParsed = libxml2.parseFile("test/fesa/DeploymentUnitTemplateFESA300.xml")

class TestFillDeployUnit(unittest.TestCase):

    def setUp(self):
        self.generator = fesa.fesa_3_0_0.fillFESADeployUnit.FESADeployUnitGenerator3_0_0()

    def test_fillDU_3_0_0(self):
        self.generator.fillXML(simpleFesaDeployParsed,simpleSilecsDeployParsed,"MyTestDeploy", "some/schema.xsd", "1.2.3")

        # check that default class is removed
        defaultClassTemplate = simpleFesaDeployParsed.xpathEval('/deploy-unit/class[class-name/text()="class-name"]')
        self.assertEqual(len(defaultClassTemplate), 0)

        # test 2 classes available
        classes = simpleFesaDeployParsed.xpathEval('/deploy-unit/class')
        self.assertEqual(len(classes), 2)

        # test 2 classes available after overwrite
        self.generator.fillXML(simpleFesaDeployParsed,simpleSilecsDeployParsed,"MyTestDeploy", "some/schema.xsd", "1.2.3")
        classes = simpleFesaDeployParsed.xpathEval('/deploy-unit/class')
        self.assertEqual(len(classes), 2)
