#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

import fesa.fesa_3_0_0.generateSourceCode
import fesa.fesa_3_1_0.generateSourceCode

import libxml2

testFolder = "test"
generationFolder = testFolder + "/generated_temp"
comparisonFolder = testFolder + "/generated_correct"
generatedHeaderName = "AllTypes.h"
generatedCppName = "AllTypes.cpp"

silecsDesignRoot = libxml2.parseFile("test/AllTypesFESA.silecsdesign")

class TestGenerateSourceCode(unittest.TestCase):
    def setUp(self):
        self.fesaDesignRoot = libxml2.parseFile(generationFolder + "/AllTypesFESA.design" )

    def assertFileEqual(self, filePath1, filePath2):
        with open(filePath1) as a, open(filePath2) as b:
            self.assertListEqual(list(a), list(b))

    def test_genHSource(self):
        fesa.fesa_3_1_0.generateSourceCode.genHSource('AllTypes', silecsDesignRoot, self.fesaDesignRoot, generationFolder,logTopics={'errorlog': True})
        self.assertFileEqual(generationFolder + "/" + generatedHeaderName, comparisonFolder + "/" + generatedHeaderName)

    def test_genCppSource(self):
        fesaDesignRoot = libxml2.parseFile(generationFolder + "/AllTypesFESA.design" )
        fesa.fesa_3_1_0.generateSourceCode.genCppSource('AllTypes', silecsDesignRoot,fesaDesignRoot,generationFolder,logTopics={'errorlog': True})
        self.assertFileEqual(generationFolder + "/" + generatedCppName, comparisonFolder + "/" + generatedCppName)
