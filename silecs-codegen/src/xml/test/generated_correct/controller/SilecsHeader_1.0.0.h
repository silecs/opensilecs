
/* Copyright CERN 2015
 *
 * WARNING: This code is automatically generated from your SILECS deploy unit document.
 * You should never modify the content of this file as it would break consistency.
 * Furthermore, any changes will be overwritten in the next code generation.
 * Any modification shall be done using the SILECS development environment
 * and regenerating this source code.
 */

#ifndef SILECSHEADER_1_0_0_H_
#define SILECSHEADER_1_0_0_H_

#include <silecs-virtual-controller/interface/Block.h>
#include <silecs-virtual-controller/interface/DeployUnit.h>
#include <silecs-virtual-controller/interface/Design.h>
#include <silecs-virtual-controller/interface/Device.h>

namespace SilecsHeader_1_0_0
{

class Design;

class HdrBlk : public SilecsServer::Block
{
public:
    /*!
     * \brief hdrBlk constructor. It creates an empty block.
     */
    HdrBlk() : SilecsServer::Block("SilecsHeader:hdrBlk")
    {
        set_version("DEV");
        set_checksum(0XF0DA17E);
        set_user("schwinn");
        set_date(0.0);
    }

    ~HdrBlk()
    {
    }
        
    /*!
     * \brief Get _version register.
     * \return value.
     */
    std::string get_version() const
    {
        size_t len = (size_t)structData_._version[1];
        return std::string((char*)&(structData_._version[2]), len);
    }

    /*!
     * \brief Set _version register.
     * \param value to be set.
     */
    void set_version(const std::string &value)
    {
        size_t len = (value.length() < _versionLen_) ? value.length() : _versionLen_;
        memcpy((char*)&(structData_._version[2]), value.c_str(), len);
        structData_._version[0] = char(0);
        structData_._version[1] = char(len);
    }
    
    /*!
     * \brief Get _checksum register.
     * \return value.
     */
    uint32_t get_checksum() const
    {
        return structData_._checksum;
    }

    /*!
     * \brief Set _checksum register.
     * \param value to be set.
     */
    void set_checksum(uint32_t value)
    {
        structData_._checksum = value;
    }
    
    /*!
     * \brief Get _user register.
     * \return value.
     */
    std::string get_user() const
    {
        size_t len = (size_t)structData_._user[1];
        return std::string((char*)&(structData_._user[2]), len);
    }

    /*!
     * \brief Set _user register.
     * \param value to be set.
     */
    void set_user(const std::string &value)
    {
        size_t len = (value.length() < _userLen_) ? value.length() : _userLen_;
        memcpy((char*)&(structData_._user[2]), value.c_str(), len);
        structData_._user[0] = char(0);
        structData_._user[1] = char(len);
    }
    
    /*!
     * \brief Get _date register.
     * \return value.
     */
    double get_date() const
    {
        return structData_._date;
    }

    /*!
     * \brief Set _date register.
     * \param value to be set.
     */
    void set_date(double value)
    {
        structData_._date = value;
    }

    virtual inline size_t getSize() const
    {
        return sizeof(structData_);
    }

    virtual void getData(unsigned char * data) const
    {
        memcpy(data, &structData_, this->getSize());
    }

    virtual void setData(unsigned char * data)
    {
        memcpy(&structData_, data, this->getSize());
    }

    virtual inline size_t getOffset() const { return 0; }

    static const std::size_t _versionLen_ = 16;
    static const std::size_t _userLen_ = 16;

private:

#pragma pack(push, 1)
    struct
    {
        char _version[_versionLen_+2];
        uint32_t _checksum;
        char _user[_userLen_+2];
        double _date;

    } structData_;
#pragma pack(pop)
    
};



class Device : public SilecsServer::Device
{
public:
    Device(const std::string& label, size_t number):SilecsServer::Device(label, number)
    {    
        blockMap_["SilecsHeader:hdrBlk"] = new HdrBlk();    
    }

    ~Device()
    {    
        delete (blockMap_["SilecsHeader:hdrBlk"]);    
    }
};


class Design : public SilecsServer::Design
{
public:

    Design():SilecsServer::Design("SilecsHeader", "1.0.0")
    {    
        deviceMap_["SilecsHeader:SilecsHeader"] = new Device("SilecsHeader:SilecsHeader", 0);    
    }

    ~Design()
    {    
        delete(deviceMap_["SilecsHeader:SilecsHeader"]);    
    }

    /*!
     * \brief Return pointer to the requested device.
     * \param label Device label.
     */
    SilecsHeader_1_0_0::Device* getDevice(const std::string& label)
    {
        if (deviceMap_.find(label) != deviceMap_.end())
        {
            return dynamic_cast<SilecsHeader_1_0_0::Device*>(deviceMap_[label]);
        }
        return NULL;
    }
};

} /* namespace */
#endif
