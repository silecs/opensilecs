#include "AllTypes.h"
#include "Siemens_Step7Device.h"

int main()
{
	std::string logTopic = "ERROR";
	AllTypesDU::DeployUnit* du = AllTypesDU::DeployUnit::getInstance(logTopic);
	std::cout << du->getName() << std::endl;
	AllTypes::Design* allTypesDesign = du->getAllTypes();
	AllTypesDU::Controller controller(allTypesDesign,"fakeParamFile.silecsparam");
	controller.connect();

    return 0;
}
