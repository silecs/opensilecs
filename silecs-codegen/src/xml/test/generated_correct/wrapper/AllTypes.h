/* Copyright CERN 2015
 *
 * WARNING: This code is automatically generated from your SILECS deploy unit document.
 * You should never modify the content of this file as it would break consistency.
 * Furthermore, any changes will be overwritten in the next code generation.
 * Any modification shall be done using the SILECS development environment
 * and regenerating this source code.
 */

#ifndef ALLTYPES_H_
#define ALLTYPES_H_

#include <silecs-communication/wrapper/Block.h>
#include <silecs-communication/wrapper/DeployUnit.h>
#include <silecs-communication/wrapper/Design.h>
#include <silecs-communication/wrapper/Device.h>

namespace AllTypes
{


class Design : public SilecsWrapper::Design
{
public:

    Design(SilecsWrapper::DeployUnit *deployUnit) :
                    SilecsWrapper::Design("AllTypes", "0.1.0", deployUnit)
    {
    }

    ~Design()
    {
    }
};

} /* namespace AllTypes */

#endif /* ALLTYPES_H_ */
