#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from genduwrapper import genDuWrapperBase

testFolder = "test"
generationFolder = testFolder + "/generated_temp/wrapper"
comparisonFolder = testFolder + "/generated_correct/wrapper"
deployFilePath = testFolder + "/AllTypesDU.silecsdeploy"
duControllerWrapperGenerated = generationFolder + "/Siemens_Step7Device.h" # just check one of them, codegen is identical for all 
duControllerWrapperCorrect = comparisonFolder + "/Siemens_Step7Device.h"
duDesignWrapperGenerated = generationFolder + "/AllTypes.h"
duDesignWrapperCorrect = comparisonFolder + "/AllTypes.h"

def fakeGetSilecsDesignFilePath(workspacePath,designName):
    return testFolder + "/AllTypes.silecsdesign"

def fakeGetDuDesignWrapperFile(workspacePath,deployName,designName):
    return generationFolder + "/" + designName + ".h"

def fakeGetDuWrapperFile(workspacePath, deployName, controllerName):
    return generationFolder + "/" + controllerName + ".h"

class TestGenDuWrapperBase(unittest.TestCase):

    def setUp(self):
        genDuWrapperBase(deployFilePath, fakeGetDuWrapperFile, "fake", fakeGetSilecsDesignFilePath, fakeGetDuDesignWrapperFile)

    def assertFileEqual(self, filePath1, filePath2):
        with open(filePath1) as a, open(filePath2) as b:
            self.assertListEqual(list(a), list(b))

    def test_compareGeneratedFiles(self):
        self.assertFileEqual(duDesignWrapperGenerated, duDesignWrapperCorrect)
        self.assertFileEqual(duControllerWrapperGenerated, duControllerWrapperCorrect)
