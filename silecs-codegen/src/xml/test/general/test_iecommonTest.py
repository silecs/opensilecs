#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from iecommon import hasChildren, getFirstChild

import libxml2

class TestIeCommon(unittest.TestCase):
    def test_hasChildren(self):
        document = '''<?xml version="1.0"?>
                    <a>
                        <b>
                            <c/>
                        </b>
                    </a>'''
        root = libxml2.parseDoc(document)
        self.assertTrue(hasChildren(root))
        self.assertTrue(hasChildren(root.xpathEval('/a/b')[0]))
        self.assertFalse(hasChildren(root.xpathEval('/a/b/c')[0]))
        
    def test_getFirstChild(self):
        document = '''<?xml version="1.0"?>
                    <a>
                        <b/>
                        <c/>
                        <d/>
                    </a>'''
        root = libxml2.parseDoc(document)
        a = root.xpathEval('/a')[0]
        b = getFirstChild(a)
        self.assertEqual(root.xpathEval('/a/b')[0], b)

