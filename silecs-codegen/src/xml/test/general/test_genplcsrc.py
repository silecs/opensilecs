#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import libxml2
import unittest

import genplcsrc
from model.Deploy.Deploy import Deploy
from model.Param.Param import Param

import libxml2


testFolder = "test"
generationFolder = testFolder + "/generated_temp"
comparisonFolder = testFolder + "/generated_correct"
generationFoldeController = generationFolder + "/controller"
comparisonFolderController = comparisonFolder + "/controller"


class TestGenPLCSrc(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestGenPLCSrc, cls).setUpClass()

        # Generate PLC sources once before the tests are run.
        silecsDeploy = Deploy.getDeployFromFile(testFolder + "/AllTypesDU.silecsdeploy")
        for controller in silecsDeploy.controllers:
            paramsFile = generationFolder + "/client/" + controller.hostName + ".silecsparam"
            paramDOM = libxml2.parseFile(paramsFile)
            param = Param()
            param.initFromXMLNode(paramDOM)
            param.deployName = silecsDeploy.name
            param.deployVersion = silecsDeploy.version
            genplcsrc.generateControllerCode(controller, param, generationFoldeController ,{'errorlog': True})

    def assertPLCCodeEqual(self, filePath1, filePath2):
        with open(filePath1) as a: a_content = a.readlines()
        with open(filePath2) as b: b_content = b.readlines()
        # Ignore lines with date
        a_content = [line for line in a_content if not (("DT#" in line) or ("SILECS_set_dt" in line))]
        b_content = [line for line in b_content if not (("DT#" in line) or ("SILECS_set_dt" in line))]
        self.assertListEqual(a_content, b_content)

    def compareGeneratedFiles(self, hostName, fileExtension):
        fileGeneratedPath = generationFoldeController + "/" + hostName + fileExtension
        fileCorrectPath = comparisonFolderController + "/" + hostName + fileExtension
        self.assertPLCCodeEqual(fileGeneratedPath, fileCorrectPath)

    def test_siemensSourcesTest(self):
        self.compareGeneratedFiles("Siemens_TiaDevice", ".scl")
        self.compareGeneratedFiles("Siemens_TiaDevice", ".sdf")
        self.compareGeneratedFiles("Siemens_TiaBlock", ".scl")
        self.compareGeneratedFiles("Siemens_TiaBlock", ".sdf")
        self.compareGeneratedFiles("Siemens_Step7Device", ".scl")
        self.compareGeneratedFiles("Siemens_Step7Device", ".sdf")
        self.compareGeneratedFiles("Siemens_Step7Block", ".scl")
        self.compareGeneratedFiles("Siemens_Step7Block", ".sdf")
        
    def test_siemensVirtualSourcesTest(self):
        self.compareGeneratedFiles("SilecsHeader_1.0.0", ".h")
        self.compareGeneratedFiles("Virtual_SiemensBlock_0.1.0", ".cpp")
        self.compareGeneratedFiles("Virtual_SiemensDevice_0.1.0", ".cpp")

    def test_beckhoffSourcesTest(self):
        self.compareGeneratedFiles("Beckhoff_BC9020", ".exp")
        self.compareGeneratedFiles("Beckhoff_CX9020_TC2", ".exp")
        self.compareGeneratedFiles("Beckhoff_CX9020_TC3", ".exp")

    def test_schneiderSourcesTest(self):
        self.compareGeneratedFiles("Schneider_M340", ".xsy")
        self.compareGeneratedFiles("Schneider_PremiumQuantum", ".xsy")

    def test_rabbitSourcesTest(self):
        self.compareGeneratedFiles("Rabbit_BlockMode", ".h")
        self.compareGeneratedFiles("Rabbit_DeviceMode", ".h")
