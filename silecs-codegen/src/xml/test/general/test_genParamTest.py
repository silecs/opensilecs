#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from genparam import genParamBase

testFolder = "test"
generationFolder = testFolder + "/generated_temp/client"
comparisonFolder = testFolder + "/generated_correct/client"

def fakeGetSilecsDesignFilePath(workspacePath, designName):
    return testFolder + "/AllTypes.silecsdesign"

def fakeGetParameterFile(workspacePath, deployName, controllerName):
    return generationFolder + "/" + controllerName + ".silecsparam" 

def fakeGetSilecsDeployFilePath(workspacePath, deployName):
    return testFolder + "/AllTypesDU.silecsdeploy"

def fakeGetParameterFileDirectory(workspacePath, deployName):
    return generationFolder

class TestGenParam(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestGenParam, cls).setUpClass()

        genParamBase(fakeGetSilecsDesignFilePath, fakeGetParameterFile,
            fakeGetSilecsDeployFilePath, fakeGetParameterFileDirectory,
            "fake", "AllTypesDU", "fake", "DEV")

    def assertParameterFileEqual(self, filePath1, filePath2):
        with open(filePath1) as a: a_content = a.readlines()
        with open(filePath2) as b: b_content = b.readlines()
        # Ignore lines with date
        a_content = [line for line in a_content if not ("<Generation date=" in line)]
        b_content = [line for line in b_content if not ("<Generation date=" in line)]
        self.assertListEqual(a_content, b_content)

    def compareGeneratedFiles(self, hostName, fileExtension):
        fileGeneratedPath = generationFolder + "/" + hostName + fileExtension
        fileCorrectPath = comparisonFolder + "/" + hostName + fileExtension
        self.assertParameterFileEqual(fileGeneratedPath, fileCorrectPath)

    def test_siemensSourcesTest(self):
        self.compareGeneratedFiles("Siemens_TiaDevice", ".silecsparam")
        self.compareGeneratedFiles("Siemens_TiaBlock", ".silecsparam")
        self.compareGeneratedFiles("Siemens_Step7Device", ".silecsparam")
        self.compareGeneratedFiles("Siemens_Step7Block", ".silecsparam")
        
    def test_siemensVirtualSourcesTest(self):
        self.compareGeneratedFiles("Virtual_SiemensBlock", ".silecsparam")
        self.compareGeneratedFiles("Virtual_SiemensDevice", ".silecsparam")

    def test_beckhoffSourcesTest(self):
        self.compareGeneratedFiles("Beckhoff_BC9020", ".silecsparam")
        self.compareGeneratedFiles("Beckhoff_CX9020_TC2", ".silecsparam")
        self.compareGeneratedFiles("Beckhoff_CX9020_TC3", ".silecsparam")

    def test_schneiderSourcesTest(self):
        self.compareGeneratedFiles("Schneider_M340", ".silecsparam")
        self.compareGeneratedFiles("Schneider_PremiumQuantum", ".silecsparam")

    def test_rabbitSourcesTest(self):
        self.compareGeneratedFiles("Rabbit_BlockMode", ".silecsparam")
        self.compareGeneratedFiles("Rabbit_DeviceMode", ".silecsparam")
    