#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import libxml2
from collections import namedtuple

import iecommon
import iefiles
import genduwrappertemplate

from iecommon import *
from model.Class.Register import DesignRegister
from model.Class.Block import DesignBlock
from model.Class.Class import DesignClass

from model.Deploy.Deploy import Deploy
            
def genClassHeader(workspacePath, deploy, design, funcGetSilecsDesignFilePath, funcGetDuDesignWrapperFile, logTopics):
    designDOM = iefiles.loadSilecsDesignDOM(workspacePath, design, deploy.silecsVersion, funcGetSilecsDesignFilePath)
    designClass = DesignClass.getDesignClassFromRootNode(designDOM)
    
    classDeclarations = ""
    for block in designClass.getDesignBlocks():
        registerInitializerList = genduwrappertemplate.getBlockInitializerList(block)
        registerGetterSetter = genduwrappertemplate.getRegisterGetterSetter(block)
        registersDimentionsDeclaration = genduwrappertemplate.getRegistersDimetionsDeclaration(block)
        registersDeclaration = genduwrappertemplate.getRegistersDeclaration(block)
        classDeclarations += genduwrappertemplate.getBlockClass(block,registerInitializerList,registerGetterSetter,registersDimentionsDeclaration,registersDeclaration)
    
    blockGetters = ""
    sendRecvBlocks = genduwrappertemplate.getDeviceSendRecvBlocks(designClass.getBlockNodes(), designClass.customTypesXmlNode)
    for block in designClass.getDesignBlocks():
        blockGetters += genduwrappertemplate.getDeviceBlockGetterSetter(block)

    classDeclarations += genduwrappertemplate.getDeviceClass(blockGetters,sendRecvBlocks)

    sendRecvBlocks = genduwrappertemplate.getControllerSendRecvBlocks(designClass.getBlockNodes(), designClass.customTypesXmlNode)
    classDeclarations = genduwrappertemplate.getDesignClass(design.name, design.version)
    designWrapper = genduwrappertemplate.designFileTemplate.substitute({'designNameCapitalized' :  iecommon.capitalizeString(design.name),'designNameUpper' :  design.name.upper(),'classDeclarations' : classDeclarations})

    designWrapperFile = funcGetDuDesignWrapperFile(workspacePath, deploy.name, design.name)
    fdesc = open(designWrapperFile, "w")
    fdesc.write(designWrapper)
    iecommon.logInfo('Generated Wrapper for Design %s'%(design.name), logTopics)
    fdesc.close()

def genDuWrapperBase(deployFile,funcGetDuWrapperFile,workspacePath,funcGetSilecsDesignFilePath, funcGetDuDesignWrapperFile,logTopics={'errorlog': True}):
    deploy = Deploy.getDeployFromFile(deployFile)
    
    for controller in deploy.controllers:
        deployCustomInclude = constructorBody = destructorBody = designGetters = designMemberDeclarations = ''
        for design in controller.getUsedDesigns():
            genClassHeader(workspacePath, deploy, design, funcGetSilecsDesignFilePath, funcGetDuDesignWrapperFile, logTopics)
            deployCustomInclude += genduwrappertemplate.deployIncludeTemplate.substitute({'designNameCapitalized' : design.getNameCapitalized()})
            constructorBody += genduwrappertemplate.designAllocation.substitute({'designName' : design.name,'designNameCapitalized' : design.getNameCapitalized()})
            destructorBody += genduwrappertemplate.designDeallocation.substitute({'designName' : design.name})
            designGetters += genduwrappertemplate.designGetterTemplate.substitute({'designName' : design.name,'designNameCapitalized' : design.getNameCapitalized()})
            designMemberDeclarations += genduwrappertemplate.deployUnitMembersDeclaration.substitute({'designName' : design.name,'designNameCapitalized' : design.getNameCapitalized()})
    
        controllerCtor = deviceGetter = ''
        for device in controller.devices:
            controllerCtor +=  genduwrappertemplate.controllerDeviceInit.substitute({'deviceName' : device.silecsDeviceLabel})
            deviceGetter += genduwrappertemplate.deviceGetterTemplate.substitute({'deviceName' : device.silecsDeviceLabel, 'deviceNameCapitalizedNoUndercore' : device.getSilecsDeviceLabelCapitalizedNoUnderscore()})
        controllerClass = genduwrappertemplate.getControllerClass(controllerCtor,deviceGetter,controller.hostName, controller.domain)
        code = genduwrappertemplate.generateControllerFile(deploy.name,deploy.version,deployCustomInclude,constructorBody,destructorBody,designGetters,designMemberDeclarations,controllerClass)
    
        fdesc = open(funcGetDuWrapperFile(workspacePath, deploy.name, controller.hostName) , "w")
        fdesc.write(code)
        iecommon.logInfo('Generated Wrapper for Controller %s'%(controller.hostName), logTopics)
        fdesc.close()

def genDuWrapper(workspacePath,deployName,deployVersion,logTopics={'errorlog': True}):
    deployFile = iefiles.getSilecsDeployFilePath(workspacePath, deployName)
    funcGetDuWrapperFile = iefiles.getDuWrapperFile
    
    # Create output directory if necessary
    sourcePath = iefiles.getDuWrapperSourceDirectory(workspacePath, deployName)
    if not os.path.exists(sourcePath):
        os.makedirs(sourcePath)
        
    genDuWrapperBase(deployFile,funcGetDuWrapperFile,workspacePath,iefiles.getSilecsDesignFilePath, iefiles.getDuDesignWrapperFile,logTopics)
    
    
