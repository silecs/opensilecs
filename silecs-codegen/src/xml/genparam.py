#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import time
import zlib
import glob
import shutil
import libxml2

import iecommon
import xmltemplate
import iefiles

from iecommon import *
from Mapping import *
from model.Class.Register import *
from model.Class.Block import *
from model.Class.Class import *
from model.Deploy.Deploy import *
from model.Deploy.Controller import *
import model.Deploy.Device
from model.Param.Param import *

# maximum number of boolean bits in the same register address
MAX_BITS_IN_ADDRESS = 8

#-------------------------------------------------------------------------
# Trimming: remove new-line, tabulation and spaces of the string
# Used to compute CRC32 on significant data only
def trim (str):
    str = str.replace(' ', '')
    str = str.replace('\s', '')
    str = str.replace('\t', '')
    str = str.replace('\r', '')
    str = str.replace('\n', '')
    return str

def computeChecksumController( workspacePath, deploy, controller, silecsVersion, funcGetSilecsDesignFilePath,  logTopics={'errorlog': True}):
    majorSilecsVersion = iecommon.getMajorSilecsVersion(silecsVersion)
    CRC32 = zlib.crc32(trim(str(majorSilecsVersion)).encode(),0)& 0xffffffff
    CRC32 = zlib.crc32(trim(str(controller.address[MEM_TYPE])).encode(),CRC32)& 0xffffffff
    for silecsDesign in deploy.silecsDesigns:
        devices = controller.getDevicesOfDesign(silecsDesign.name)
        if len(devices) > 0:
            CRC32       = zlib.crc32(trim(silecsDesign.name).encode(),CRC32)& 0xffffffff
            CRC32       = zlib.crc32(trim(silecsDesign.version).encode(),CRC32)& 0xffffffff
            designDOM = iefiles.loadSilecsDesignDOM(workspacePath, silecsDesign, silecsVersion, funcGetSilecsDesignFilePath)
            CRC32 = computeChecksumClass(designDOM,CRC32,logTopics)
            for device in devices:
                CRC32     = zlib.crc32(trim(device.silecsDeviceLabel).encode(),CRC32)& 0xffffffff
    iecommon.logInfo("CRC32: %s" % str(CRC32),logTopics)
    return CRC32
    
def computeChecksumClass(designDOM, CRC32, logTopics={'errorlog': True}):
    designClass = DesignClass.getDesignClassFromRootNode(designDOM)
    for block in designClass.getDesignMemoryBlocks():
        CRC32     = zlib.crc32(trim(block.name).encode(),CRC32)& 0xffffffff
        for register in block.getDesignRegisters():
            CRC32     = zlib.crc32(trim(register.name).encode(),CRC32)& 0xffffffff
            CRC32     = zlib.crc32(trim(register.format).encode(),CRC32)& 0xffffffff
            CRC32     = zlib.crc32(trim(str(register.dim1)).encode(),CRC32)& 0xffffffff
            CRC32     = zlib.crc32(trim(str(register.dim2)).encode(),CRC32)& 0xffffffff
            CRC32     = zlib.crc32(trim(str(register.stringLength)).encode(),CRC32)& 0xffffffff
    return CRC32
    
# Needed to encapsulate "genParam" in order to allow unit-testing (fake all file interactions)
def genParamBase( funcGetSilecsDesignFilePath, funcGetParameterFile, funcGetSilecsDeployFilePath,  funcGetParameterFileDirectory, workspacePath, deployName, deployVersion, silecsVersion, logTopics={'debuglog': True}):
    # Check the Deployment document exist for that PLC
    deployPath = funcGetSilecsDeployFilePath(workspacePath, deployName)
    if(not os.path.isfile(deployPath)):
        iecommon.logError(deployPath + ": '" + deployName + "' cannot be found in provided workspace '" + workspacePath + "'." ,True,logTopics)
    
    deploy = Deploy.getDeployFromFile(deployPath)

    iecommon.logDebug("------Processing for output file generation------",logTopics)
    paramPath = funcGetParameterFileDirectory(workspacePath, deployName)

    #create output directory if necessary
    if not os.path.exists(paramPath):
        os.makedirs(paramPath)
        iecommon.logDebug("create directory %s" %paramPath,logTopics)
    
    for controller in deploy.controllers:
        mappings = { MEM_TYPE: None, DI_TYPE: None, DO_TYPE: None, AI_TYPE: None, AO_TYPE: None }
        for mappingType, mapping in  mappings.items():
            mappings[mappingType] = Mapping(controller.address[mappingType], mappingType)
        
        # Messagges for debugging purpose
        iecommon.logDebug("------ XML extracted informations ------",logTopics)
        iecommon.logDebug("owner = " + deploy.owner,logTopics)
        iecommon.logDebug("plcSystem = " + controller.system,logTopics)
        iecommon.logDebug("plcModel = " + controller.model,logTopics)
        iecommon.logDebug("plcBrand = " + controller.brand,logTopics)
        iecommon.logDebug("plcProtocol = " + controller.protocol,logTopics)
        for mappingType, mapping in  mappings.items():
            iecommon.logDebug("address" + mappingType2String[mappingType] + " = " + str(controller.address[mappingType]),logTopics)

        paramFile = funcGetParameterFile(workspacePath, deployName, controller.hostName )
        iecommon.logInfo("Generate xml for parameters file: " + paramFile,logTopics)
        checksum = computeChecksumController(workspacePath, deploy, controller, silecsVersion, funcGetSilecsDesignFilePath, logTopics)
        param = Param.createParam(silecsVersion, deploy.owner, checksum)
        param.addController(controller)

        for deployDesign in deploy.silecsDesigns:
            devices = controller.getDevicesOfDesign(deployDesign.name)
            nbDevice = len(devices)
            iecommon.logDebug("Class %s has %s devices" %(deployDesign.name,nbDevice),logTopics)
            if nbDevice == 0:
                continue #skip unused design
            
            iecommon.logDebug("-----------------------------------------",logTopics)
            iecommon.logDebug("------  Analysing Class " + deployDesign.name + "   ------",logTopics)
            iecommon.logDebug("-----------------------------------------",logTopics)

            if iecommon.supportsDateTimeLong(controller):
                datetime_format = xmltemplate.DATE_TIME_LONG
            else:
                datetime_format = xmltemplate.DATE_AND_TIME

            iecommon.logDebug("Datetime format is:", datetime_format)

            designDOM = iefiles.loadSilecsDesignDOM(workspacePath, deployDesign, silecsVersion, funcGetSilecsDesignFilePath, datetime_format)
            designClass = DesignClass.getDesignClassFromRootNode(designDOM)
            paramClass = ParamClass()
            paramClass.initWithDesignClass(designClass)
            param.controller.addParamClass(paramClass)
            controller.blockCounter = 0
            for mappingType, mapping in mappings.items():
                mapping.blockAddress = 0
                mapping.nbBlock = 0
                mapping.deviceDataSize = 0
                mapping.clearInstanceAddressIfDefined()

            # INNER LOOP TO ACCESS AT BLOCK LEVEL
            for designBlock in designClass.getDesignBlocks():
                # Siemens does not support to read back IO-Setting registers TODO: What about other PLC types ?
                if controller.brand == "SIEMENS" and designBlock.isIOBlock() and designBlock.isSetting():
                    raise Exception("Error: Siemens PLC's do not support to read back IO-Settings. Please use 'Command-IO-Block' instead of 'Setting-IO-Block' in the silecsdesign") 
                    
                paramBlock = ParamBlock()
                paramBlock.initWithDesignBlock(designBlock)
    
                iecommon.logDebug("----- Processing Block "+ designBlock.name + " from design -----",logTopics)
                # Initialization for inner loop local variables 
                regAddress = 0      # memory address of the register (using byte addressing)
                blockSize = 0           # block size (sum of the register size)
                blockMemSize = 0    # block size (sum of the regist)
                controller.regCounter = 0      # used for NI register address generation

                # additional data for managing booleans
                isBooleanRegister = False
                bitNumber = 0
                nextRegister = None

                # INNER LOOP TO ACCESS AT REGISTER LEVEL
                designRegisters = designBlock.getDesignRegisters()
                for i, designRegister in enumerate(designRegisters):
                    if i + 1 != len(designRegisters):
                        nextRegister = designRegisters[i+1]
                    else: 
                        nextRegister = None

                    iecommon.logDebug("------ Processing Register " + designRegister.name + " ------",logTopics)

                    # Allow bool register only for Siemens controller
                    if designRegister.format == 'bool':
                        isBooleanRegister = True

                        if not designRegister.isScalar():
                            iecommon.logError("Invalid format in register: %s Bool format available only for scalars" %designRegister.name, False, logTopics)
                            sys.exit(2)

                        if controller.brand != 'SIEMENS':
                            iecommon.logError("Invalid format in register: %s. Bool format available only on SIEMENS controllers" %designRegister.name, False,logTopics)
                            sys.exit(2)
                    else:
                        isBooleanRegister = False
                        bitNumber = 0

                    # Set length attribute only for string registers
                    if designRegister.format == 'string': # TODO: Port this constraint to java
                        if controller.brand == 'DIGI':
                            # RABBIT has 8bit memory alignment but uses 16bit word communication protocol (MODBUS).
                            # String buffer (8bit elements) must have even number of bytes.
                            if ((int(designRegister.dim1)*int(designRegister.dim2)*int(designRegister.stringLength)) % 2 == 1): #XML CliX cannot check that constraint!   
                                iecommon.logError("String length of %s designRegister must be an even value." %designRegister.name, False,logTopics)
                                sys.exit(2)
                    
                    regAddress = controller.alignRegAddress(designBlock, designRegister, regAddress)
                    
                    controller.regCounter += 1 # used for NI register address generation
                    # Set register mem-size
                    paramRegister = ParamRegister()

                    if isBooleanRegister:
                        paramRegister.initWithDesignRegister(designRegister,regAddress,controller.msize, bitNumber)
                    else:
                        paramRegister.initWithDesignRegister(designRegister,regAddress,controller.msize)

                    if isBooleanRegister and nextRegister and nextRegister.format == "bool":
                        bitNumber = (bitNumber + 1) % MAX_BITS_IN_ADDRESS
                        if bitNumber == 0:
                            # Compute address for the next register if max bits in address achieved
                            regAddress = controller.computeNextRegAddress(designBlock, designRegister, regAddress)
                    else:
                        # Compute address for the next register
                        regAddress = controller.computeNextRegAddress(designBlock, designRegister, regAddress)
                    
                    paramBlock.xmlNode.addChild(paramRegister.xmlNode)
                    #paramRegister.xmlNode.shellPrintNode()
                    #iterativelly compute the block size (accumulator initialized outside the loop)
                    blockSize = blockSize + (paramRegister.size * paramRegister.dim1 * paramRegister.dim2)
                # END OF INNER LOOP TO ACCESS AT REGISTER LEVEL

                paramBlock.setSize(blockSize)
                
                blockSpecificMapping = mappings[paramBlock.type]    
                paramBlock.setAddress(controller.computeBlkAddress(paramBlock,blockSpecificMapping, regAddress,nbDevice))
                paramBlock.setMemSize(blockSpecificMapping.blockDataSize)
                paramClass.xmlNode.addChild(paramBlock.xmlNode)
            # END OF INNER LOOP TO ACCESS AT BLOCK LEVEL (LOOP-2)
            # Set block Address
            for mappingType, mapping in mappings.items():
                paramClass.setAddress(mapping.classBaseAddress,mappingType)
            
            for device in devices:
                instance = libxml2.newNode("Instance")
                instance.setProp("label", device.silecsDeviceLabel)
                instance.setProp("fesa-label", device.fesaDeviceName)
                instance.setProp("address",    str(int(controller.computeInstAddress(mappings[MEM_TYPE]))))
                instance.setProp("DI-address", str(int(controller.computeInstAddress(mappings[DI_TYPE]))))
                instance.setProp("DO-address", str(int(controller.computeInstAddress(mappings[DO_TYPE]))))
                instance.setProp("AI-address", str(int(controller.computeInstAddress(mappings[AI_TYPE]))))
                instance.setProp("AO-address", str(int(controller.computeInstAddress(mappings[AO_TYPE]))))
                paramClass.xmlNode.addChild(instance)

            # Compute the memory address for the next class
            for mappingType, mapping in mappings.items():
                controller.computeNextBaseAddress(mapping, nbDevice);
            
            for mappingType, mapping in mappings.items():
                paramClass.setUsedData(mapping.usedData,mappingType)
                iecommon.logInfo("Used-"+ mappingType2String[mappingType] +" for Class " + deployDesign.name + ": " + mapping.usedData ,logTopics)

        #Display details about the PLC memory using for the global configuration
        if (controller.brand == 'SIEMENS'):
            # SIEMENS works with data-block addressing
            plcUsedMem = "DB"+str(controller.address[MEM_TYPE])+"..DB"+str(controller.memLast)+" / "+str(controller.memSize)+" bytes"
        else:
            # SCHNEIDER works with absolute addressing
            startAddr   = int(controller.address[MEM_TYPE] /2)         #Memory info uses word-addressing
            plcUsedMem  = "MW"+str(startAddr)+"..MW"+str(controller.memLast)+" / "+str(controller.memSize)+" words"
        
        # print plc used memory
        iecommon.logInfo("Used-memory for PLC " + deploy.name + ": "+str(plcUsedMem),logTopics)
        #------------------------------------------------------
        # Generate output XML
        #------------------------------------------------------
        iefiles.saveXMLToFile(paramFile,param.xmlNode.get_doc())
        
        iecommon.logDebug("---------------------------------------------------------",logTopics)
        iecommon.logDebug("------GENERATED FILE: %s "%paramFile,logTopics)
        iecommon.logDebug("---------------------------------------------------------",logTopics)

#=========================================================================
# Entry point
#=========================================================================
def genParam(workspacePath, deployName, deployVersion, silecsVersion, logTopics={'errorlog': True}):
    genParamBase( iefiles.getSilecsDesignFilePath, iefiles.getParameterFile, iefiles.getSilecsDeployFilePath,  iefiles.getParameterFileDirectory, workspacePath, deployName, deployVersion, silecsVersion, logTopics)


