#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import iecommon
import xmltemplate
import socket
import libxml2
import os
import fnmatch
import shutil

designFormat    = '.silecsdesign'
deployFormat    = '.silecsdeploy'
paramFormat     = '.silecsparam'

def loadSilecsDesignDOM(workspacePath, silecsDesign, silecsVersion, funcGetSilecsDesignFilePath, datetime_format=xmltemplate.DATE_AND_TIME):
    if silecsDesign.name == "SilecsHeader":
        silecsHeader = xmltemplate.getSilecsHeader(silecsVersion, datetime_format)
        return libxml2.parseDoc(silecsHeader)
    else:
        designPath = funcGetSilecsDesignFilePath(workspacePath, silecsDesign.name)
        return libxml2.parseFile(designPath)
    
#==================================================================
# FILE ACCESS 
#==================================================================

def saveXMLToFile(filePath,xmlDoc):
    with open(filePath, 'w') as fd:
        xmlDoc.saveTo(fd,format = True)
        #xmlDoc.saveTo(fd,"UTF-8",libxml2.XML_SAVE_NO_EMPTY) # it seems like there is a bug in libxml2  XML_SAVE_NO_EMPTY does the job of XML_SAVE_FORMAT
            
    
#--- DESIGN ---

def getSilecsDesignFileDir(workspacePath, projectName):
	path = workspacePath + "/" + projectName + "/src"
	return  os.path.normpath(path)

def getSilecsDesignFilePath(workspacePath, projectName):
	path = getSilecsDesignFileDir(workspacePath, projectName) + "/" + projectName + designFormat;
	return  os.path.normpath(path)

def newDesignProject(workspacePath, designName, schemaPath, silecsVersion ):
    designDir = getSilecsDesignFileDir(workspacePath,designName)
    designFile = getSilecsDesignFilePath(workspacePath,designName)
    if(os.path.isfile(designFile)):
        raise Exception("Design %s already exists in current workspace" % designFile)
    
    # create src directory
    os.makedirs(designDir)
    # create default design file
    designText = xmltemplate.getDesignTemplate(designName, schemaPath, silecsVersion)
    fdesc = open(designFile, "w")
    fdesc.write(designText)
    fdesc.close()
  
#--- DEPLOY ---

def getSilecsDeployFileDir(workspacePath, projectName):
	path = workspacePath + "/" + projectName + "/src"
	return  os.path.normpath(path)

def getSilecsDeployFilePath(workspacePath, projectName):
	path = getSilecsDeployFileDir(workspacePath, projectName) + "/" + projectName + deployFormat;
	return  os.path.normpath(path)

def newDeployProject(workspacePath, deployName, schemaPath, silecsVersion):
    deployDir = getSilecsDeployFileDir(workspacePath, deployName)
    deployFile = getSilecsDeployFilePath(workspacePath, deployName)
    if(os.path.isfile(deployFile)):
        raise Exception("Deploy %s already exists in current workspace" % deployFile)
    
    # create src directory
    os.makedirs(deployDir)
    # create default deploy file
    deployText = xmltemplate.getDeployTemplate(deployName, schemaPath, silecsVersion)
    fdesc = open(deployFile, "w")
    fdesc.write(deployText)
    fdesc.close()

def getGeneratedSilecsDirectory(workspacePath, deployName):
    return os.path.join( workspacePath, deployName, 'generated-silecs')
    
#--- PARAMETER ---
def getParameterFileName(controllerName):
    return controllerName + paramFormat
    
def getParameterFileDirectory(workspacePath, deployName):
    paramPath =  os.path.join( getGeneratedSilecsDirectory(workspacePath, deployName), 'client' )
    return os.path.normpath(paramPath)

def getParameterFile(workspacePath, deployName, controllerName):
    paramPath = os.path.join(getParameterFileDirectory(workspacePath, deployName), getParameterFileName(controllerName))
    return os.path.normpath(paramPath)
    
#--- CONTROLLER SOURCES ---
def getControllerSourcesDirectory(workspacePath, deployName):
    controllerPath =  os.path.join( getGeneratedSilecsDirectory(workspacePath, deployName), 'controller' )
    return os.path.normpath(controllerPath)

#--- DU WRAPPER ---
def getDuWrapperFileName( controllerName ):
    return iecommon.capitalizeString(controllerName) + ".h"

def getDuWrapperSourceDirectory(workspacePath, deployName):
    wrapperPath =  os.path.join( getGeneratedSilecsDirectory(workspacePath, deployName), 'wrapper' )
    return os.path.normpath(wrapperPath)
    
def getDuWrapperFile(workspacePath, deployName, controllerName ):
    return os.path.join( getDuWrapperSourceDirectory(workspacePath, deployName), getDuWrapperFileName(controllerName))
        
def getDuDesignWrapperFile(workspacePath, deployName, designName):
    return os.path.join(getDuWrapperSourceDirectory(workspacePath, deployName), getDuWrapperFileName(designName))
        

#--- DU VIRTUAL-S7 ---
def getDuVirtualS7FileName(deployName, deployVersion):
    str = iecommon.capitalizeString(deployName) + "_" + deployVersion
    return str.replace(".", "_") + ".h"

def getDuVirtualS7SourceDirectory(workspacePath, deployName, deployVersion):
    controllerPath = workspacePath + '/' + getDeployDirectoryName(deployName, deployVersion) + '/' \
    + ieglobal.generatedDir + '/' + ieglobal.controllerDir
    controllerNorm = os.path.normpath(controllerPath)
    return controllerNorm
    
def getDuVirtualS7File(workspacePath, deployName, deployVersion):
    return getDuVirtualS7SourceDirectory(workspacePath, deployName, deployVersion) + "/" \
        + getDuVirtualS7FileName(deployName, deployVersion)
        
def getDuDesignVirtualS7File(workspacePath, deployName, deployVersion, designName, designVersion):
    return getDuVirtualS7SourceDirectory(workspacePath, deployName, deployVersion) + "/" \
        + getDuVirtualS7FileName(designName, designVersion)
        
#--- General ---
def getFesa3CommonDirectory(workspacePath, designName):
    fesa3ClassPath = workspacePath + '/' + designName + '/src/' + designName + '/Common'
    fesa3ClassPathNorm = os.path.normpath(fesa3ClassPath)
    return fesa3ClassPathNorm

def getProjectDirectory(workspacePath, designName):
    path = workspacePath + '/' + designName
    pathNorm = os.path.normpath(path)
    return pathNorm

def getFesaDeployFilePath(workspacePath, deployName):
    path = workspacePath + '/' + deployName + '/src/' + deployName + '.deploy'
    pathNorm = os.path.normpath(path)
    return pathNorm
