#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Project: SILECS
# Author: F.Locci/ BE-CO
# Date: August 2012
# Last modification: Jannuary 2015
#
# Description:
# This module implement the methods to build each components of the sources.
# A component source consists of one 'parameterizable' text that can
# be specialized on call to append the string to be stored in the file.
#

import string
import struct
import iecommon

#-------------------------------------------------------------------------
# Hash-Table

whichRegisterFormat = {
    'uint8'   : 'uint8_t',
    'int8'    : 'int8_t',
    'uint16'  : 'uint16_t',
    'int16'   : 'int16_t',
    'uint32'  : 'uint32_t',
    'int32'   : 'int32_t',
    'float32' : 'float',
    'uint64'  : 'uint64_t',
    'int64'   : 'int64_t',
    'string'  : 'string',    
    'float64' : 'double',
    'date'    : 'double',
    'char'    :'int8_t',
    'byte'    :'uint8_t',
    'word'    :'uint16_t',
    'dword'    :'uint32_t',
    'int'    :'int16_t',
    'dint'    :'int32_t',
    'real'    :'float',
    'dt'    :'double',
    'dtl'	:'double'
}

#=========================================================================
# Virtual S7 DEPLOY-UNIT template
#=========================================================================

duDesignInclude = string.Template("""#include "${designName}_${designVersion}.h"
""")

duConstructor = string.Template("""DeployUnit() : SilecsServer::DeployUnit("${deployName}", "${deployVersion}", SilecsServer::S7Protocol, SilecsServer::${mode}, ${baseAddr})
""")

duDesignAlloc = string.Template("""    mapDesigns_["${designName}"] = new ${designName}_${designVersion}::Design();
""")

duDesignDelete = string.Template("""    delete mapDesigns_["${designName}"];
""")

duDesignGet = string.Template("""    ${designName}_${designVersion}::Design* get${designName}()
    {
        return dynamic_cast<${designName}_${designVersion}::Design*>(mapDesigns_["${designName}"]);
    }
    
""")

duHeader = string.Template("""
/* Copyright CERN 2015
 *
 * WARNING: This code is automatically generated from your SILECS deploy unit document.
 * You should never modify the content of this file as it would break consistency.
 * Furthermore, any changes will be overwritten in the next code generation.
 * Any modification shall be done using the SILECS development environment
 * and regenerating this source code.
 */

#ifndef ${deployNameUpper}_H_
#define ${deployNameUpper}_H_

#include <silecs-virtual-controller/interface/DeployUnit.h>
#include <silecs-virtual-controller/interface/Design.h>
${designIncludes}

namespace ${deployName}_${deployVersion}
{

class DeployUnit : public SilecsServer::DeployUnit
{
public:

    ${duConstructor}    {
    ${designAllocs}    }

    ~DeployUnit()
    {
    ${designDeletes}    }

${designGetters}};

}

#endif
""")

mainCode = string.Template("""
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>

#include <silecs-virtual-controller/core/SilecsSnap7Server.h>
#include "${deployName}_${deployVersion}.h"

class UserSnap7Server : public SilecsServer::SilecsSnap7Server
{
public:
    UserSnap7Server(${fullDeployName}::DeployUnit* du) : SilecsSnap7Server(du, true) {}
    virtual ~UserSnap7Server() {}

    virtual void userFunction(PSrvEvent PEvent)
    {
      // Implement the specific process control here!
      // Look at SILECS Wikis: 'Create a virtual controller' chapter 
    }
};

int main(int argc, char*argv[])
{
    ${fullDeployName}::DeployUnit du;
    UserSnap7Server server(&du);
    if (server.startServer() < 0)
    {
        std::cout << "Failed to start the VC server: " << du.getName() << std::endl;
        return -1;
    }
    return 0;
}
""")


#=========================================================================
# Virtual S7 CLASS template
#=========================================================================
classScalarGetSet = string.Template("""    
    /*!
     * \\brief Get ${regName} register.
     * \\return value.
     */
    ${regFormat} get${RegName}() const
    {
        return structData_.${regName};
    }

    /*!
     * \\brief Set ${regName} register.
     * \\param value to be set.
     */
    void set${RegName}(${regFormat} value)
    {
        structData_.${regName} = value;
    }
""")

classArrayGetSet = string.Template("""    
    /*!
     * \\brief Get array ${regName} register.
     * \\return value.
     */
    void get${RegName}(${regFormat}* value) const
    {
        memcpy(value, &structData_.${regName}, ${regName}Dim1_ * ${regName}Dim2_ * sizeof(${regFormat}));
    }

    /*!
     * \\brief Set array ${regName} register.
     * \\param value to be set.
     */
    void set${RegName}(${regFormat}* value)
    {
        memcpy(&structData_.${regName}, value, ${regName}Dim1_ * ${regName}Dim2_ * sizeof(${regFormat}));
    }
""")

classScalarStringGetSet = string.Template("""    
    /*!
     * \\brief Get ${regName} register.
     * \\return value.
     */
    std::string get${RegName}() const
    {
        size_t len = (size_t)structData_.${regName}[1];
        return std::string((char*)&(structData_.${regName}[2]), len);
    }

    /*!
     * \\brief Set ${regName} register.
     * \\param value to be set.
     */
    void set${RegName}(const std::string &value)
    {
        size_t len = (value.length() < ${regName}Len_) ? value.length() : ${regName}Len_;
        memcpy((char*)&(structData_.${regName}[2]), value.c_str(), len);
        structData_.${regName}[0] = char(0);
        structData_.${regName}[1] = char(len);
    }
""")

classArrayStringGetSet = string.Template("""    
    /*!
     * \\brief Get std::string ${regName} register.
     * \\param value buffer where the value will be stored.
     */
    void get${RegName}(std::string* value) const
    {
        for (std::size_t i = 0; i < ${regName}Dim1_; i++)
        {
            size_t len = (size_t)structData_.${regName}[i][1];
            value[i].assign(&(structData_.${regName}[i][2]), len);
        }
    }

    /*!
     * \\brief Set std::string ${regName} register.
     * \\param value to be set.
     */
    void set${RegName}(std::string* value)
    {
        for (std::size_t i = 0; i < ${regName}Dim1_; i++)
        {
            size_t len = (value[i].length() < ${regName}Len_) ? value[i].length() : ${regName}Len_;
            memcpy(&(structData_.${regName}[i][2]), value[i].c_str(), len);
            structData_.${regName}[i][0] = char(0);
            structData_.${regName}[i][1] = char(len);
        }
    }
""")

classHeaderInit = string.Template("""    set_version("${framework}");
        set_checksum(${checksum});
        set_user("${owner}");
        set_date(0.0);""")

classBlock = string.Template("""
class ${BlockName} : public SilecsServer::Block
{
public:
    /*!
     * \\brief ${blockName} constructor. It creates an empty block.
     */
    ${BlockName}() : SilecsServer::Block("${className}:${blockName}")
    {
    ${initCodeString}
    }

    ~${BlockName}()
    {
    }
    ${regsCode}
    virtual inline size_t getSize() const
    {
        return sizeof(structData_);
    }

    virtual void getData(unsigned char * data) const
    {
        memcpy(data, &structData_, this->getSize());
    }

    virtual void setData(unsigned char * data)
    {
        memcpy(&structData_, data, this->getSize());
    }

    virtual inline size_t getOffset() const { return ${blockOffset}; }

${regsDim}
private:

#pragma pack(push, 1)
    struct
    {
${regsDef}
    } structData_;
#pragma pack(pop)
    
};
""")

classArrayDim = string.Template("""    static const std::size_t ${regName}Dim1_ = ${dim1};
    static const std::size_t ${regName}Dim2_ = ${dim2};
""")

classStringLen = string.Template("""    static const std::size_t ${regName}Len_ = ${len};
""")

classCreateBlock = string.Template("""
        blockMap_["${className}:${blockName}"] = new ${BlockName}();""")

classDeleteBlock = string.Template("""
        delete (blockMap_["${className}:${blockName}"]);""")

classDevice = string.Template("""
class Device : public SilecsServer::Device
{
public:
    Device(const std::string& label, size_t number):SilecsServer::Device(label, number)
    {    ${createBlocks}    
    }

    ~Device()
    {    ${deleteBlocks}    
    }
};""")

classCreateDevice = string.Template("""
        deviceMap_["${className}:${deviceLabel}"] = new Device("${className}:${deviceLabel}", ${deviceIndex});""")

classDeleteDevice = string.Template("""
        delete(deviceMap_["${className}:${deviceLabel}"]);""")

classDesign = string.Template("""
class Design : public SilecsServer::Design
{
public:

    Design():SilecsServer::Design("${className}", "${classVersion}")
    {    ${createDevices}    
    }

    ~Design()
    {    ${deleteDevices}    
    }

    /*!
     * \\brief Return pointer to the requested device.
     * \\param label Device label.
     */
    ${className}_${class_version}::Device* getDevice(const std::string& label)
    {
        if (deviceMap_.find(label) != deviceMap_.end())
        {
            return dynamic_cast<${className}_${class_version}::Device*>(deviceMap_[label]);
        }
        return NULL;
    }
};""")

classHeader = string.Template("""
/* Copyright CERN 2015
 *
 * WARNING: This code is automatically generated from your SILECS deploy unit document.
 * You should never modify the content of this file as it would break consistency.
 * Furthermore, any changes will be overwritten in the next code generation.
 * Any modification shall be done using the SILECS development environment
 * and regenerating this source code.
 */

#ifndef ${designNameUpper}_${designVersion}_H_
#define ${designNameUpper}_${designVersion}_H_

#include <silecs-virtual-controller/interface/Block.h>
#include <silecs-virtual-controller/interface/DeployUnit.h>
#include <silecs-virtual-controller/interface/Design.h>
#include <silecs-virtual-controller/interface/Device.h>

namespace ${designName}_${designVersion}
{

class Design;
${classBlocks}

${classDevice}

${classDesign}

} /* namespace */
#endif
""")


#=========================================================================
# Virtual S7 generation Sub-function
#=========================================================================

# SERVER MAIN code generation ---------------------------------------------
def vs7MainCode(deployName, deployVersion):
    fullDeployName = deployName + '_' + deployVersion.replace(".", "_")
    return mainCode.substitute(deployName=deployName, deployVersion=deployVersion, fullDeployName=fullDeployName)
     
# DEPLOY-UNIT generation -------------------------------------------------
def vs7DuHeader(deployName, deployVersion, duConstructor, designIncludes, designAllocs, designDeletes, designGetters):
    deployVersion = deployVersion.replace(".", "_")
    return duHeader.substitute(deployNameUpper=deployName.upper(), designIncludes=designIncludes, deployName=deployName, deployVersion=deployVersion, duConstructor=duConstructor, designAllocs=designAllocs, designDeletes=designDeletes, designGetters=designGetters)  
   
def vs7DuDesignInclude(designName, designVersion):
    return duDesignInclude.substitute(designName=designName, designVersion=designVersion)

def vs7DuConstructor(deployName, deployVersion, mode, baseAddr):
    return duConstructor.substitute(deployName=deployName, deployVersion=deployVersion, mode=mode, baseAddr=baseAddr)
   
def vs7DuDesignAlloc(designName, designVersion):
    designVersion = designVersion.replace(".", "_")
    return duDesignAlloc.substitute(designName=designName, designVersion=designVersion)
   
def vs7DuDesignDelete(designName):
    return duDesignDelete.substitute(designName=designName)

def vs7DuDesignGet(designName, designVersion):
    designVersion = designVersion.replace(".", "_")
    return duDesignGet.substitute(designName=designName, designVersion=designVersion)

# CLASSES generation -----------------------------------------------------
def hexSwap32(input):
    input = format(input, '#02X')
    hexswap = input[0:2]+input[8:10]+input[6:8]+input[4:6]+input[2:4]
    return hexswap  
   
def vs7ClassHeader(designName, designVersion, classBlocks, classDevice, classDesign):
    designVersion = designVersion.replace(".", "_")
    return classHeader.substitute(designNameUpper=designName.upper(), designVersion=designVersion, designName=designName, classBlocks=classBlocks, classDevice=classDevice, classDesign=classDesign)  

def vs7ClassBlock(framework, owner, checksum, className, blockName, regsCode, blockOffset, regsDim, regsDef):
    BlockName = iecommon.capitalizeString(blockName);
    initCodeString = ""
    if className == "SilecsHeader":
        initCodeString = classHeaderInit.substitute(framework=framework, checksum=hexSwap32(checksum), owner=owner)   
    return classBlock.substitute(BlockName=BlockName, blockName=blockName, className=className, initCodeString=initCodeString, regsCode=regsCode, blockOffset=blockOffset, regsDim=regsDim, regsDef=regsDef)

def vs7ClassGetSet(regName, regFormat, regDim1, regDim2, regLen):
    RegName = iecommon.capitalizeString(regName)
    regFormat = whichRegisterFormat[regFormat]
    if regDim1 == 1 and regDim2 == 1:    # scalar
        if regFormat == 'string':
            return classScalarStringGetSet.substitute(regName=regName, RegName=RegName)
        else:
            return classScalarGetSet.substitute(regName=regName, regFormat=regFormat, RegName=RegName)
    else:
        if regFormat == 'string':
            return classArrayStringGetSet.substitute(regName=regName, RegName=RegName)
        else:
            return classArrayGetSet.substitute(regName=regName, RegName=RegName, regFormat=regFormat)

def vs7ClassDimension(regName, regFormat, regDim1, regDim2, regLen):
    dimCodeString = ''
    if regDim1 > 1 or regDim2 > 1:    # not a scalar ==> define dimension constants
        dimCodeString += classArrayDim.substitute(regName=regName, dim1=regDim1, dim2=regDim2)
    if regFormat == 'string':
        dimCodeString += classStringLen.substitute(regName=regName, len=regLen)
    return dimCodeString

def vs7ClassDummyRegister(curRegAddress, prevRegMemSize, regMemSize, dummyIndex):
    dummyCodeString = ''
    if curRegAddress % 2 != 0: #is current register address an odd address?
        if prevRegMemSize > 1 or regMemSize > 1:   #only 8bit scalar (not following array) uses 8bit alignment! 
            dummyCodeString = "        uint8_t dummy%s;\n" %(dummyIndex)
    return dummyCodeString

def vs7ClassDataRegister(regName, regFormat, regDim1, regDim2, regLen):
    length = ''
    regFormat = whichRegisterFormat[regFormat]
    if regFormat == 'string':
        length = "[%sLen_+2]" %(regName)
        regFormat = 'char'
    dataCodeString = "        %s %s" %(regFormat, regName)
    if regDim1 > 1 or regDim2 > 1:    # not scalar
        dataCodeString += "[%sDim1_]" %(regName)    
        if regDim2 > 1:    # not scalar
            dataCodeString += "[%sDim2_]" %(regName)
    dataCodeString += "%s;\n" %(length)
    return dataCodeString

def vs7ClassCreateBlock(className, blockName):
    BlockName = iecommon.capitalizeString(blockName);
    return classCreateBlock.substitute(className=className, blockName=blockName, BlockName=BlockName)

def vs7ClassDeleteBlock(className, blockName):
    return classDeleteBlock.substitute(className=className, blockName=blockName)

def vs7ClassCreateDevice(className, deviceLabel, deviceIndex):
    return classCreateDevice.substitute(className=className, deviceLabel=deviceLabel, deviceIndex=deviceIndex)

def vs7ClassDeleteDevice(className, deviceLabel):
    return classDeleteDevice.substitute(className=className, deviceLabel=deviceLabel)

def vs7ClassDevice(createBlocks, deleteBlocks):
    return classDevice.substitute(createBlocks=createBlocks, deleteBlocks=deleteBlocks)

def vs7ClassDesign(className, classVersion, createDevices, deleteDevices):
    class_version = classVersion.replace(".", "_")
    return classDesign.substitute(className=className, classVersion=classVersion, createDevices=createDevices, deleteDevices=deleteDevices, class_version=class_version)
