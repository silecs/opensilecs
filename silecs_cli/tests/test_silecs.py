import os
import sys
import unittest
import filecmp
import shutil
from pathlib import Path

RELEASE_PARAM_DIR_PATH = os.path.join("tests", "releaseParamDir")
os.environ["RELEASE_PARAM_DIR"] = RELEASE_PARAM_DIR_PATH

sys.path.append('../')

from silecs_cli import silecs, silecs_utils

DESIGN_PATH = os.path.join("tests", "testClasses", "SilecsTestClass", "src", "SilecsTestClass.design")
DEPLOY_PATH = os.path.join("tests", "testClasses", "SilecsTestClass_DU", "src", "SilecsTestClass_DU.deploy")
BACKUP = DESIGN_PATH + ".backup"

SILECS_DESIGN_PATH = DESIGN_PATH.split(".")[0] + ".silecsdesign"
SILECS_DEPLOY_PATH = DEPLOY_PATH.split(".")[0] + ".silecsdeploy"

RELEASE_PARAM_FILEPATH = os.path.join(RELEASE_PARAM_DIR_PATH, "SilecsTestClass", "SilecsTestClass_DU", "tsts7001.silecsparam")
RELEASE_PARAM_FILEPATH_DEV = os.path.join(RELEASE_PARAM_DIR_PATH, "SilecsTestClass", "SilecsTestClass_DU-d", "tsts7001.silecsparam")

design_schema_path_mock = "../silecs-model/src/xml/DesignSchema.xsd"
deploy_schema_path_mock = "../silecs-model/src/xml/DeploySchema.xsd"

silecs_cli_location = os.path.realpath(__file__)


class SilecsTest(unittest.TestCase):

    """Class to test silecs.py script"""
    @classmethod
    def tearDownClass(cls):
        """Remove created files during tests"""
        if os.path.isfile(BACKUP):
            os.remove(BACKUP)

        if os.path.isfile(SILECS_DESIGN_PATH):
            os.remove(SILECS_DESIGN_PATH)

        if os.path.isfile(RELEASE_PARAM_FILEPATH):
            shutil.rmtree(RELEASE_PARAM_DIR_PATH)

    def test_get_extension(self):
        """Test getting extension from file"""
        filenames_extensions = [("test1.design", "design"), ("test2.deploy", "deploy")]
        for filename, expected in filenames_extensions:
            with self.subTest():
                self.assertEqual(silecs_utils.get_extension(filename), expected)

    def test_get_project_name(self):
        """Test getting extension from filepath"""
        filenames_project_names = [("test1.design", "test1"), ("test2.deploy", "test2")]
        for filename, project_name in filenames_project_names:
            with self.subTest():
                self.assertEqual(silecs_utils.get_project_name(filename), project_name)

    def test_get_project_path(self):
        """Test getting project path from filepath"""
        filenames_project_paths = [("workspace/test1/src/test1.design", "workspace/test1"),
                    ("workspace/test2/src/test2.deploy", "workspace/test2")]
        for filename, project_path in filenames_project_paths:
            with self.subTest():
                self.assertEqual(silecs_utils.get_project_path(filename), project_path)

    def test_get_project_dir(self):
        """Test getting project directory from filepath"""
        filenames_dirs = [("workspace/test1/src/test1.design", "workspace/test1/src"),
                    ("workspace/test2/src/test2.deploy", "workspace/test2/src")]
        for filename, directory in filenames_dirs:
            with self.subTest():
                self.assertEqual(silecs_utils.get_project_dir(filename), directory)

    def test_get_schema_path(self):
        """Test getting xml schema path for silecs design deploy files"""
        filenames_schema_paths = [("/home/test/example/test1.silecsdesign",
                            os.path.join(Path(silecs_cli_location).parents[2], "silecs-model/src/xml/DesignSchema.xsd")),
                            ("/home/test/example/test2.silecsdeploy",
                             os.path.join(Path(silecs_cli_location).parents[2], "silecs-model/src/xml/DeploySchema.xsd"))]
        silecs_cli = silecs.SilecsCli(run_from_source=True)
        for filename, schema_path in filenames_schema_paths:
            with self.subTest():
                self.assertEqual(silecs_utils.get_schema_path(silecs_cli.silecs_env, filename), schema_path)

    def test_get_schema_path_invalid(self):
        """Test getting xml schema path for invalid path (passed file path
            must have the extension '.silecsdesign' or '.silecsdeploy')"""
        try:
            silecs_cli = silecs.SilecsCli(run_from_source=True)
            silecs_utils.get_schema_path(silecs_cli.silecs_env, "/home/test/example/test1.design")
        except Exception as e:
            self.assertEqual("Error: Passed file for validation needs to have the extension '.silecsdesign' or '.silecsdeploy'", str(e))

    def test_get_silecs_file_path_from_fesa(self):
        """Test getting xml schema path for silecs design deploy files"""
        self.assertEqual(silecs_utils.get_silecs_file_path_from_fesa("../test1.design"), os.path.join("..", "test1.silecsdesign"))

    def test_get_silecs_file_path_from_fesa_invalid(self):
        """Test getting xml schema path for invalid file (passed file path
            must have the extension '.design' or '.deploy')
        """
        try:
            silecs_utils.get_silecs_file_path_from_fesa("../test1.silecsdesign")
        except Exception as e:
            self.assertEqual("Error: Passed FESA file needs to have the extension '.design' or '.deploy'", str(e))

    def test_create_backup_file(self):
        """Test creating backup file"""
        silecs_utils.create_backup_file(DESIGN_PATH)
        self.assertNotEqual(os.stat(BACKUP).st_size,  0)

    def test_silecs_validate(self):
        """Test validating silecs design/deploy files"""
        silecs_cli = silecs.SilecsCli(run_from_source=True)
        silecs_design_deploy_paths_result = [
                                              (os.path.join("tests", "examples", "SchneiderM340TestDU_invalid.silecsdeploy"), False), \
                                              (os.path.join("tests", "examples", "SilecsTestClassDU_invalid.silecsdeploy"), False)
                                            ]
        for path, expected_result in silecs_design_deploy_paths_result:
            xsd_path = silecs_utils.get_schema_path(silecs_cli.silecs_env, path)
            self.assertEqual(silecs_cli.validate(path, xsd_path), expected_result)

    def test_create_silecs_design(self):
        """Test creating silecs design files from design file"""
        path = os.path.abspath(DESIGN_PATH)

        silecs_cli = silecs.SilecsCli(run_from_source=True)
        new_file_path = silecs_utils.get_silecs_file_path_from_fesa(path)
        silecs_cli.create(new_file_path)
        print(new_file_path)
        self.assertEqual(os.path.exists(new_file_path), True)
        self.assertNotEqual(os.stat(new_file_path).st_size,  0)

        try:
            silecs_cli.create(new_file_path)
        except Exception as e:
            self.assertIn("There is already a .silecsdesign file available:", str(e))

    def test_create_silecs_deploy(self):
        """Test creating silecs deploy files from deploy file"""
        path = os.path.abspath(DEPLOY_PATH)
        silecs_cli = silecs.SilecsCli(run_from_source=True)

        new_file_path = silecs_utils.get_silecs_file_path_from_fesa(path)

        try:
            silecs_cli.create(new_file_path)
        except Exception as e:
            print(str(e))
            self.assertIn("There is already a .silecsdeploy file available:", str(e))

    def test_release_param(self):
        """Test release param file"""
        silecs_cli = silecs.SilecsCli(run_from_source=True)
        result = silecs_cli.release(SILECS_DEPLOY_PATH, "dev", "yocto")

        self.assertEqual(result, True)
        expected_files = [RELEASE_PARAM_FILEPATH, RELEASE_PARAM_FILEPATH_DEV]
        for file in expected_files:
            self.assertEqual(os.path.isfile(file), True)
            self.assertEqual(filecmp.cmp(file, os.path.join("tests", "testClasses",
                "SilecsTestClass_DU", "generated-silecs", "client", "tsts7001.silecsparam")), True)

if __name__ == "_main__":
    unittest.main()
