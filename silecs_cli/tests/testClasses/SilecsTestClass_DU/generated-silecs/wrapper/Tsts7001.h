/* Copyright CERN 2015
 *
 * WARNING: This code is automatically generated from your SILECS deploy unit document.
 * You should never modify the content of this file as it would break consistency.
 * Furthermore, any changes will be overwritten in the next code generation.
 * Any modification shall be done using the SILECS development environment
 * and regenerating this source code.
 */

#ifndef SILECSTESTCLASS_DU_H_
#define SILECSTESTCLASS_DU_H_

#include <silecs-communication/interface/equipment/SilecsCluster.h>
#include <silecs-communication/wrapper/DeployUnit.h>
#include <silecs-communication/wrapper/Design.h>

#include "SilecsHeader.h"
#include "SilecsTestClass.h"

namespace SilecsTestClass_DU
{

typedef SilecsWrapper::DeployConfig DeployConfig;
typedef SilecsWrapper::DesignConfig DesignConfig;

class DeployUnit : public SilecsWrapper::DeployUnit
{
public:

    /*!
     * \brief Use this method to create/get the unique instance of the Deploy Unit.
     *
     * \param logTopics This parameter can be used to enable/disable log topics
     * valid topics are ERROR[,INFO,DEBUG,SETUP,ALLOC,RECV,SEND,COMM,DATA,LOCK].
     *
     * \param globalConfig This parameter can be used to pass different parameters to
     * the library. I.e. enabling automatic connection.
     */
    static DeployUnit* getInstance(const std::string& logTopics = "",
            const SilecsWrapper::DeployConfig& globalConfig = SilecsWrapper::DeployConfig())
    {
        if (_instance == NULL)
        {
            _instance = new DeployUnit(logTopics, globalConfig);
        }
        else
        {
            if (logTopics.empty() == false)
            {
                _instance->getService()->setLogTopics(logTopics);
            }
        }
        return dynamic_cast<DeployUnit*>(_instance);
    }
    
    /*!
     * \brief Use this method to create/get the unique instance of the Deploy Unit.
     *
     * \param globalConfig This parameter can be used to pass different parameters to
     * the library. I.e. enabling automatic connection.
     */
    static DeployUnit* getInstance(const SilecsWrapper::DeployConfig& globalConfig)
    {
        return getInstance("", globalConfig);
    }
    
    /*!
     * \brief Return pointer to the deployed design SilecsHeader.
     */
    SilecsHeader::Design* getSilecsHeader()
    {
        return _SilecsHeader;
    }

    /*!
     * \brief Return pointer to the deployed design SilecsTestClass.
     */
    SilecsTestClass::Design* getSilecsTestClass()
    {
        return _SilecsTestClass;
    }

private:

    SilecsHeader::Design* _SilecsHeader;
    SilecsTestClass::Design* _SilecsTestClass;

    DeployUnit(const std::string& logTopics, const SilecsWrapper::DeployConfig& globalConfig) :
                    SilecsWrapper::DeployUnit("SilecsTestClass_DU", "0.1.0", logTopics, globalConfig)
    {
        // Construct Design SilecsHeader
        _SilecsHeader = new SilecsHeader::Design((SilecsWrapper::DeployUnit*) this);
        
        // Construct Design SilecsTestClass
        _SilecsTestClass = new SilecsTestClass::Design((SilecsWrapper::DeployUnit*) this);
        
    }
    
    ~DeployUnit()
    {
        delete _SilecsHeader;
        delete _SilecsTestClass;
    }
};


class Controller : public SilecsWrapper::Controller
{
public:
    Controller(SilecsWrapper::Design *design, const std::string parameterFile) :
                    SilecsWrapper::Controller("tsts7001", "", design, parameterFile)
    {
            _deviceMap.insert(std::pair<std::string, SilecsWrapper::Device*>("SilecsHeader", new SilecsWrapper::Device("SilecsHeader", this)));
            _deviceMap.insert(std::pair<std::string, SilecsWrapper::Device*>("dev0", new SilecsWrapper::Device("dev0", this)));
    }

    ~Controller()
    {
        map<std::string, SilecsWrapper::Device*>::iterator it;
        for (it = _deviceMap.begin(); it != _deviceMap.end(); it++)
        {
            delete it->second;
        }
    }

    /*!
     * \brief Return pointer to the requested device.
     * \param label Device label.
     */
    SilecsWrapper::Device* getDevice(const std::string& label)
    {
        if (_deviceMap.find(label) != _deviceMap.end())
        {
            return _deviceMap[label];
        }
        throw Silecs::SilecsException(__FILE__, __LINE__, Silecs::PARAM_UNKNOWN_DEVICE_NAME, label);
    }

    std::map<std::string, SilecsWrapper::Device*>& getDeviceMap()
    {
        return _deviceMap;
    }
    
    /*!
     * \brief Get pointer to device SilecsHeader.
     */
    SilecsWrapper::Device* getSilecsHeader()
    {
        return _deviceMap["SilecsHeader"];
    }
    
    /*!
     * \brief Get pointer to device dev0.
     */
    SilecsWrapper::Device* getDev0()
    {
        return _deviceMap["dev0"];
    }
    
private:
    std::map<std::string, SilecsWrapper::Device*> _deviceMap;
};


} /* namespace SilecsTestClass_DU */

#endif /* SILECSTESTCLASS_DU_H_ */
