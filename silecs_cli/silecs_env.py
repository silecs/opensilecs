import os
import sys
import re
from packaging import version

import silecs_const
import silecs_utils

class SilecsEnvironment:
    """Class containing paths to silecs modules which are used in silecs cli tool"""
    DESIGN_SHEMA_FILE="DesignSchema.xsd"
    DEPLOY_SHEMA_FILE="DeploySchema.xsd"

    def __init__(self, silecs_base, silecs_version, run_from_source):
        self.SILECS_BASE = silecs_base
        self.update_silecs_variables(silecs_version, run_from_source)

    def update_silecs_variables(self, silecs_version, run_from_source):
        if not silecs_version:
            self._SILECS_VERSION = silecs_const.DEFAULT_SILECS_VERSION
        else:
            self._SILECS_VERSION = silecs_version
        
        if not run_from_source:
            self.DESIGN_SHEMA_PATH = f"{self.SILECS_BASE}/silecs-model/xml/{SilecsEnvironment.DESIGN_SHEMA_FILE}"
            self.DEPLOY_SHEMA_PATH = f"{self.SILECS_BASE}/silecs-model/xml/{SilecsEnvironment.DEPLOY_SHEMA_FILE}"
            self.SILECS_CODEGEN_BASE=f"{self.SILECS_BASE}/silecs-codegen/xml/"
        else:
            self.DESIGN_SHEMA_PATH = f"{self.SILECS_BASE}/silecs-model/src/xml/{SilecsEnvironment.DESIGN_SHEMA_FILE}"
            self.DEPLOY_SHEMA_PATH = f"{self.SILECS_BASE}/silecs-model/src/xml/{SilecsEnvironment.DEPLOY_SHEMA_FILE}"
            self.SILECS_CODEGEN_BASE=f"{self.SILECS_BASE}/silecs-codegen/src/xml/"

        self.SILECS_CODEGEN_MIGRATION=f"{self.SILECS_CODEGEN_BASE}/migration"

    def update_fesa_variables(self, fesa_version):
        print(f"Using fesa version: {fesa_version}")

        self.FESA_VERSION = fesa_version
        self.FESA_BASE=f"/opt/fesa/fesa-fwk/{self.FESA_VERSION}"
        self.FESA_XSD=f"{self.FESA_BASE}/fesa-model-gsi/xml/deployment/deployment-gsi.xsd"
        self.FESA_GSI_TEMPLATE=f"{self.FESA_BASE}/fesa-model-gsi/xml/design/templates/GSIClassTemplate.xml"
        
        self.SILECS_CODEGEN_FESA = silecs_utils.get_silecs_codegen_fesa(f"{self.SILECS_CODEGEN_BASE}/fesa", self.FESA_VERSION)

    def get_available_versions(self):
        """Find available versions of silecs in SILECS_DIR"""
        script_name_format = "[0-9]+_[0-9]+_([0-9]+|x)to[0-9]+_[0-9]+_([0-9]+|x).py"
        is_migration_script = lambda f: re.search(script_name_format, f) is not None

        migration_files = [f.name.split(".")[0] for f in os.scandir(self.SILECS_CODEGEN_MIGRATION) if os.path.isfile(f) and is_migration_script(f.name)]
        get_target_version_from_file = lambda f : f.split("to")[-1].replace("_x", "").replace("_", ".")
        available_versions = [get_target_version_from_file(f) for f in migration_files]

        for v in available_versions:
            try:
                version.Version(v)
            except version.InvalidVersion:
                target_versions.remove(v)

        return sorted(available_versions, key= lambda x: version.Version(x))

    def get_version_underscored_tiny_as_x(self, version):
        """Get underscored version string with tiny part as x
        eg. 2.3.4 -> 2_3_x"""
        version_splitted = version.split(".")
        major = version_splitted[0]
        minor = version_splitted[1]
        return major + "_" + minor + "_x" 

    def get_matching_versions(self, available_versions, old_version, new_version):
        """Get versions not older than old_version and not newer than new_version"""
        is_between_old_new_version = lambda v: version.parse(v) >= version.parse(old_version) and version.parse(v) <= version.parse(new_version)
        is_in_major_minor_format = lambda v: len(v.split(".")) >= 2
        return [v for v in available_versions if is_between_old_new_version(v) and is_in_major_minor_format(v)]

    def get_migration_modules(self, matching_versions, old_version):
        """Get migration modules that need to be run and from which to wchich 
        version """
        migration_modules = []

        matching_versions_underscored = [self.get_version_underscored_tiny_as_x(v) for v in matching_versions]
        prev_version = old_version
        migration_modules = []

        # check if major minor are the same -no need to perform migration then
        are_major_minor_same = lambda v1, v2: v1.split(".")[0] == v2.split(".")[0] and v1.split(".")[1] == v2.split(".")[1]
        
        for version in matching_versions:
            if are_major_minor_same(prev_version, version):
                continue
            
            prev_version_underscored = self.get_version_underscored_tiny_as_x(prev_version)
            version_underscored = self.get_version_underscored_tiny_as_x(version)
 
            migration_module = f"{prev_version_underscored}to{version_underscored}"
            if not prev_version_underscored == version_underscored:
                migration_modules += [(migration_module, prev_version, version)]
            
            prev_version = version

        return migration_modules
