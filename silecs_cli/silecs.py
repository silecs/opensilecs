import os
import sys
import argparse
import shutil
import subprocess
from packaging import version
from pathlib import Path

from xml.dom import minidom
import xmlschema

import silecs_env
import silecs_utils
import silecs_const

class SilecsArgumentParser(argparse.ArgumentParser):
    """Own argument parser"""
    def __init__(self, *args, **kwargs):
        argparse.ArgumentParser.__init__(self, *args, **kwargs)

    def show_examples(self):
        examples = "\nExamples: \n\n" \
                    "\t To create a new file 'MyClass.silecsdesign' for the FESA class. \n" \
                    "\t\t silecs -c ~/workspace/MyClass \n\n" \
                    "\t To validate 'MyClass.silecsdesign'. \n" \
                    "\t\t silecs -v ~/workspace/MyClass \n\n" \
                    "\t To add missing properties and value-items specified in 'MyClass.silecsdesign' into the FESA document 'MyClass.design'.\n" \
                    "\t This will as well generate C++ code.\n" \
                    "\t\t silecs -g ~/workspace/MyClass\n\n" \
                    "\t To release silecs deploy param to FEC specific FESA release directory.\n" \
                    "\t Please set RELEASE_PARAM_DIR to use custom FESA release directory. If not, default location will be used\n" \
                    "\t\t silecs -r ~/workspace/MyClass_DU\n\n" \
                    "\t For all options you can specify either the direct (silecs) file or the root project folder.\n" \
                    "\nCheck the Silecs Wiki for more info: https://www-acc.gsi.de/wiki/Frontend/SILECS\n" \

        print(examples)

    def print_help(self, file=None):
        super().print_help(file)
        self.show_examples()

##### Silecs CLI ########
class SilecsCli:
    def __init__(self, run_from_source=False):
        silecs_cli_location = os.path.realpath(__file__)

        if not run_from_source: 
            VERSION_DIR = os.path.basename(Path(silecs_cli_location).parents[1])
            try:
                SILECS_DIR = Path(silecs_cli_location).parents[2]
                version.Version(VERSION_DIR)
                SILECS_BASE = os.path.join(SILECS_DIR, VERSION_DIR)
            except version.InvalidVersion as e:
                print(f"Invalid script localisation of script - invalid version directory {VERSION_DIR}" )
            except Exception as e:
                print(e)

        else:
            SILECS_DIR = Path(silecs_cli_location).parents[1]
            VERSION_DIR = None
            SILECS_BASE = SILECS_DIR

        self.silecs_env = silecs_env.SilecsEnvironment(SILECS_BASE, VERSION_DIR, run_from_source)
        print(f"using sileces base path: {self.silecs_env.SILECS_BASE}")

    ###### Import silecs modules accding to silecs and fesa version #####
    def import_modules(self):   
        for module in silecs_const.SILECS_MODULES:
            try:
                del sys.modules[module]
            except:
                pass

        global genparam
        global genplcsrc
        global genduwrapper
        global fillFESADeployUnit
        global generateFesaDesign
        global generateSourceCode

        try:
            import genparam
            import genplcsrc
            import genduwrapper
            import fillFESADeployUnit
            import generateFesaDesign
            import generateSourceCode
        except ModuleNotFoundError as e:
            print(f"Error during importing module: {e}")

    def prepare_and_import_silecs_modules(self):
        sys.path.append(self.silecs_env.SILECS_CODEGEN_BASE)
        sys.path.append(self.silecs_env.SILECS_CODEGEN_MIGRATION)
        sys.path.append(self.silecs_env.SILECS_CODEGEN_FESA)
        self.import_modules()

    def prepare_codegen_modules(self, fesa_version):
        """Set fesa version and import modules for code generation regarding to that version"""
        self.silecs_env.update_fesa_variables(fesa_version)
        self.prepare_and_import_silecs_modules()

    ##### Validate silecs design deploy ######

    def validate(self, path, xsd_path=None, check_version_mismatch=True):
        """Validate silecs design deploy"""
        silecs_filepath, _, _ = silecs_utils.get_paths(os.path.abspath(path))
        if silecs_filepath is None:
            print(f"Could not find SILECS file in '{path}'")
            return False
        validation_result = True
        try:
            silecs_version = silecs_utils.get_silecs_version_from_file(silecs_filepath)

            if check_version_mismatch:
                silecs_utils.check_silecs_version_mismatch(script_version=self.silecs_env._SILECS_VERSION, file_version=silecs_version)

            if not xsd_path:
                xsd_path = silecs_utils.get_schema_path(self.silecs_env, silecs_filepath)

            print(f"validating {silecs_filepath}")
 
            schema = xmlschema.XMLSchema11(xsd_path)
            try:
                schema.validate(silecs_filepath)
            except xmlschema.validators.exceptions.XMLSchemaValidationError as e:
                print(e)
                validation_result = False
            return validation_result

        except Exception as e:
            print(e)
            return False


    def silecs_validate(self, file_paths):
        for path in file_paths:
            try:
                val_result = self.validate(path=path)
                print(val_result)
                if val_result:
                    print("File is valid")
                else:
                    print("File is not valid. Check errors above.")
            except Exception as e:
                print(e)

    ##### Create silecs design/deploy file #####

    def create_silecs_design(self, filepath, project_name):
        """"Create silecs design file"""
        design_template = silecs_utils.get_silecs_design_template(project_name, self.silecs_env._SILECS_VERSION, self.silecs_env.DESIGN_SHEMA_PATH)

        if os.path.isfile(filepath):
            raise Exception(f"Error: There is already a .{silecs_const.SILECSDESIGN} file available: {os.path.abspath(filepath)}")

        with open(filepath, "w") as fp:
            fp.write(design_template)

        print(f"\ncreated new silecsdesign: {filepath}")


    def create_silecs_deploy(self, filepath, project_name):
        """"Create silecs deploy file"""
        deploy_template = silecs_utils.get_silecs_deploy_template(project_name, self.silecs_env._SILECS_VERSION, self.silecs_env.DEPLOY_SHEMA_PATH)

        if os.path.isfile(filepath):
            raise Exception(f"Error: There is already a .{silecs_const.SILECSDEPLOY} file available: {os.path.abspath(filepath)}")

        with open(filepath, "w") as fp:
            fp.write(deploy_template)

        print(f"\ncreated new silecs_deploy: {filepath}")


    def create(self, filepath):
        """Create silecs design/deploy file"""
        project_name = silecs_utils.get_project_name(filepath)
        extension = silecs_utils.get_extension(filepath)
        if extension == silecs_const.SILECSDESIGN:
            self.create_silecs_design(filepath, project_name)
        elif extension == silecs_const.SILECSDEPLOY:
            self.create_silecs_deploy(filepath, project_name)


    def silecs_create(self, file_paths):
        for path in file_paths:
            path = os.path.abspath(path)
            _, fesa_filepath, _ = silecs_utils.get_paths(os.path.abspath(path))
            if fesa_filepath is None:
                print(f"Could not find FESA file in '{path}'")
                return
            try:
                new_file_path = silecs_utils.get_silecs_file_path_from_fesa(fesa_filepath)
                self.create(new_file_path)
                print("Created")
            except Exception as e:
                print(e)

    ##### Generation source code for silecs deploy path ######

    def update_makefile(self, project_path):
        makeSpecific = os.path.join(project_path, "Makefile.specific")
        if os.path.isfile(makeSpecific):
            silecs_utils.create_backup_file(makeSpecific)

            included = False
            with open(makeSpecific, 'r') as file:
                filedata = file.read()
                if "include Makefile.silecs" in filedata:
                    # Correct include already in file, we have nothing to do.
                    included = True

            if not included:
                filedata = "# Include SILECS makefile\n" + "include Makefile.silecs\n" + filedata

                with open(makeSpecific, 'w') as file:
                    file.write(filedata)
        else:
            # In case the Makefile.specific does not exist. Print an info message.
            info = "Please note that you need to include the Makefile.silecs in your " \
                "Makefile.specific by adding the line 'include Makefile.silecs'.\n"
            print(info)

    def generate_silecsdeploy(self, silecs_deploy_path, project_path):
        """Generate source files from silecs deploy file"""
        project_name = silecs_utils.get_project_name(silecs_deploy_path)
        workspace_path = os.path.abspath(os.path.join(project_path, os.pardir))
        fesa_deploy_file = os.path.abspath(os.path.join(project_path, "src", project_name + ".deploy"))
        SILECS_DEPLOY_VERSION="unused_parameter"

        try:
            fesa_version = silecs_utils.get_fesa_version_from_design_deploy(fesa_deploy_file)
        except:
            print(f"Cannot get fesa version from {fesa_deploy_file}")
            return

        try:
            self.prepare_codegen_modules(fesa_version)
        except Exception as e:
            print(f"Failed to generate code for file: {fesa_deploy_file}. Error: {e}")
            return

        print(f"generating code for {project_name} ...\n")
        if os.path.isfile(fesa_deploy_file):
            info = f"Please note that old, obsolete xml-elements are not deleted automatically! Sometimes this can lead to an invalid FESA document! \n" \
                    f"If this is the case, please remove the obsolete elements by hand. \n" \
                    f"Creating a backup of the existing FESA file at: {fesa_deploy_file}.backup \n"

            print(info)
            silecs_utils.create_backup_file(fesa_deploy_file)

        genparam.genParam(workspace_path, project_name, SILECS_DEPLOY_VERSION, self.silecs_env._SILECS_VERSION)
        genplcsrc.genPlcSrc(workspace_path, project_name, self.silecs_env._SILECS_VERSION)
        genduwrapper.genDuWrapper(workspace_path, project_name, SILECS_DEPLOY_VERSION)
        fillFESADeployUnit.fillDeployUnit(workspace_path, project_name, self.silecs_env.FESA_XSD, self.silecs_env.FESA_VERSION)

        self.update_makefile(project_path)

        print("Code generation finished")


    def generate_silecsdesign(self, silecs_design_path, project_path):
        """Generate source files from silecs design file"""
        project_name = silecs_utils.get_project_name(silecs_design_path)
        workspace_path = os.path.abspath(os.path.join(project_path, os.pardir))
        fesa_design_file = os.path.abspath(os.path.join(project_path, "src", project_name + ".design"))

        try:
            fesa_version = silecs_utils.get_fesa_version_from_design_deploy(fesa_design_file)
        except:
            print(f"Cannot get fesa version from {fesa_design_file}")
            return

        try:
            self.prepare_codegen_modules(fesa_version)
        except Exception as e:
            print(f"Failed to generate code for file: {fesa_design_file}. Error: {e}")
            return

        print(f"generating code for {project_name} ...\n")
        if os.path.isfile(fesa_design_file):
            info = f"Please note that old, obsolete xml-elements are not deleted automatically! Sometimes this can lead to an invalid FESA document! \n" \
                    f"If this is the case, please remove the obsolete elements by hand. \n" \
                    f"Creating a backup of the existing FESA file at: {fesa_design_file}.backup \n"
            print(info)
            silecs_utils.create_backup_file(fesa_design_file)

        self.update_makefile(project_path)

        generateFesaDesign.fillDesignFile(self.silecs_env.FESA_VERSION, project_name, workspace_path, self.silecs_env.FESA_XSD)
        generateSourceCode.genCppFiles(project_name, workspace_path, silecs_design_path)

        print("Code generation finished")

    def generate(self, path):
        """Generate source files"""
        silecs_filepath, _, project_path = silecs_utils.get_paths(os.path.abspath(path))
        if silecs_filepath is None:
            print(f"Could not find SILECS file in '{path}'")
            return
        extension = silecs_utils.get_extension(silecs_filepath)
        makefile_path = os.path.join(project_path, silecs_const.SILECS_MAKEFILE)

        silecs_version = silecs_utils.get_silecs_version_from_file(silecs_filepath)
        silecs_utils.check_silecs_version_mismatch(script_version=self.silecs_env._SILECS_VERSION, file_version=silecs_version)

        if extension == silecs_const.SILECSDESIGN:
            self.generate_silecsdesign(silecs_filepath, project_path)
            makefile = silecs_utils.get_silecs_design_makefile_template(self.silecs_env.SILECS_BASE)
        elif extension == silecs_const.SILECSDEPLOY:
            self.generate_silecsdeploy(silecs_filepath, project_path)
            # Snap7 path is one folder up from the silecs release path.
            makefile = silecs_utils.get_silecs_deploy_makefile_template(self.silecs_env.SILECS_BASE)
    
        with open(makefile_path, 'w') as fp:
            fp.write(makefile)

    def silecs_generate(self, file_paths):
        for path in file_paths:
            try:
                val_result = self.validate(path=path, check_version_mismatch=False)
                if val_result:
                    print("File is valid")
                else:
                    print("File is not valid. Check errors above.")
                    continue
                self.generate(path)
            except Exception as e:
                print(e)


    ##### Open diagnostic ######
    def silecs_diagnostic_tool(self, path):
        silecs_filepath, _, _ = silecs_utils.get_paths(os.path.abspath(path))
        if silecs_filepath is None:
            print(f"Could not find SILECS file in '{path}'")
            return
        extension = silecs_utils.get_extension(silecs_filepath)
        if extension != silecs_const.SILECSDEPLOY:
            print("Error: diag tool only can be opened for a -silecsdeploy file")
            sys.exit()

        silecs_diag_tool=f"{self.silecs_env.SILECS_BASE}/silecs-diagnostic-cpp/bin/x86_64/silecs-diagnostic"
        subprocess.call([silecs_diag_tool, "-d", silecs_filepath])

    def prepare_nfsinit(self, fesa_fec_name, env):
        base_release_dir = f"/common/export/nfsinit/{fesa_fec_name}"
        # Check if folder exists. Tests would fail otherwise since the location is global.
        if not os.path.exists(base_release_dir):
            print("Folder for nfsinit release does not exist. Skipping configuration of nfsinit scripts.")
            return

        fesa_release_script = f"{base_release_dir}/50_fesa"
        silecs_release_script = f"{base_release_dir}/50_silecs"
        if os.path.islink(fesa_release_script):
            os.remove(fesa_release_script)
        if os.path.islink(silecs_release_script):
            os.remove(silecs_release_script)

        os.symlink(f"../global/silecs_yocto_{env}", silecs_release_script)

    def copy_snap7(self, fesa_fec_name, project_name, target):
        """Copy snap7 library to the default, FEC specific FESA release directory (RELEASE_PARAM_DIR/fesa_fec_name/project_name)
        and development directory (RELEASE_PARAM_DIR/fesa_fec_name/project_name-d) .
        """
        fesa_fec_dst_folder = os.path.join(silecs_const.RELEASE_PARAM_DIR, fesa_fec_name, f"{project_name}")
        fesa_fec_dst_folder_dev = fesa_fec_dst_folder + "-d"
        fesa_fec_dst_folders = [fesa_fec_dst_folder_dev, fesa_fec_dst_folder]

        snap7_path = f"{self.silecs_env.SILECS_BASE}/../snap7/latest/{target}-linux/lib/libsnap7.so"

        if not os.path.isfile(snap7_path):
            print(f"Snap 7 library not found at {snap7_path}. Will not copy snap7.")
            return

        for fesa_fec_dst in fesa_fec_dst_folders:
            if not os.path.isdir(fesa_fec_dst):
                print(f"Generating folder-structures for FEC: {os.path.abspath(fesa_fec_dst)}")
                os.makedirs(fesa_fec_dst)
                print("Folder generated")

            snap7_file_name = silecs_utils.get_filename(snap7_path)
            fesa_fec_dst_file = os.path.join(fesa_fec_dst, snap7_file_name)
            print(f"Installing snap7 library {snap7_file_name} to {fesa_fec_dst}.")
            with open(fesa_fec_dst_file, 'w+'):
                shutil.copyfile(src=snap7_path, dst=fesa_fec_dst_file)

    def copy_param_file(self, fesa_fec_name, param_file_src, project_name):
        """Copy file from param_file_src to the default, FEC specific FESA release directory (RELEASE_PARAM_DIR/fesa_fec_name/project_name)
        and development directory (RELEASE_PARAM_DIR/fesa_fec_name/project_name-d) .
        """
        fesa_fec_dst_folder = os.path.join(silecs_const.RELEASE_PARAM_DIR, fesa_fec_name, f"{project_name}")
        fesa_fec_dst_folder_dev = fesa_fec_dst_folder + "-d"
        fesa_fec_dst_folders = [fesa_fec_dst_folder_dev, fesa_fec_dst_folder]

        for fesa_fec_dst in fesa_fec_dst_folders:
            if not os.path.isdir(fesa_fec_dst):     
                print(f"Generating folder-structures for FEC: {os.path.abspath(fesa_fec_dst)}")     
                os.makedirs(fesa_fec_dst)
                print("Folder generated")

            param_file_name = silecs_utils.get_filename(param_file_src)
            fesa_fec_dst_file = os.path.join(fesa_fec_dst, param_file_name)
            print(f"Installing parameter-file {param_file_name} to {fesa_fec_dst}.")
            with open(fesa_fec_dst_file, 'w+'):
                shutil.copyfile(src=param_file_src, dst=fesa_fec_dst_file)


    ##### Release parameter file, snap7 and nfsinit scripts ######
    def release(self, path, env, target):
        """Copy generated silecs deploy parameter files to directory"""
        silecs_filepath, _, project_path = silecs_utils.get_paths(os.path.abspath(path))
        if silecs_filepath is None:
            print(f"Could not find SILECS file in '{path}'")
            return False
        project_name = silecs_utils.get_project_name(silecs_filepath)
        param_dir = os.path.join(project_path, "generated-silecs", "client")
        extension = silecs_utils.get_extension(silecs_filepath)
        
        if extension != silecs_const.SILECSDEPLOY:
            print(f"Error: you need to provide {silecs_const.SILECSDEPLOY} file")
            return False
        
        try: 
            silecs_deploy_document = minidom.parse(silecs_filepath)
            controller_nodes = silecs_deploy_document.getElementsByTagName("Controller")
            for controller in controller_nodes:
                host_name = controller.getAttribute("host-name")
                plc_nodes = controller.childNodes

                plc_element = None
                for plc in plc_nodes:
                    if plc.nodeType == minidom.Node.ELEMENT_NODE:
                        plc_element = plc
                        break
                if not plc_element:
                    raise Exception(f"No concrete plc defined for controller: {host_name}")

                device_nodes = plc_element.getElementsByTagName("Device")
                fesa_fec_name = None
                for device in device_nodes:
                    fesa_fec_name = device.getAttribute("fesa-fec-name")
                    device_label = device.getAttribute("silecs-device-label")

                    if not fesa_fec_name:
                        print(f"Skipping controller: {host_name} Reason:")
                        print(f"For silecs-device {device_label} the attribute @fesa-fec-name is not define")
                        continue

                    param_file_name = f"{host_name}.silecsparam"              
                    param_file_path = os.path.abspath(os.path.join(param_dir, param_file_name))
                    if not os.path.isfile(param_file_path):
                        print(f"Skipping controller: {host_name} Reason:")
                        print(f"No parameter file found for this controller. Did you run the code-generation ?")
                        continue
                
                    self.copy_param_file(fesa_fec_name, param_file_path, project_name)
                    self.copy_snap7(fesa_fec_name, project_name, target)
                    self.prepare_nfsinit(fesa_fec_name, env)
            return True

        except Exception as e:
            print(e)
            return False


    def silecs_release(self, file_paths, env, target):
        print(f"Using release directory: {silecs_const.RELEASE_PARAM_DIR}")
        for filepath in file_paths:
            release_result = self.release(filepath, env, target)
            if release_result:
                print("Release successfull")
            else:
                print("Error during release. Check errors above.")

    ##### Migrate file to new silecs version ######

    def silecs_migrate(self, path, new_version):
        try:
            silecs_filepath, _, project_path = silecs_utils.get_paths(os.path.abspath(path))
            project_type = silecs_utils.get_project_type(project_path)

            #Import codegen modules needed for migration
            project_name = silecs_utils.get_project_name(silecs_filepath)
            project_path = silecs_utils.get_project_path(silecs_filepath)
            if project_type == silecs_utils.ProjectType.DESIGN:
                fesa_file = os.path.abspath(os.path.join(project_path, "src", project_name + ".design"))
            elif project_type == silecs_utils.ProjectType.DEPLOY:
                fesa_file = os.path.abspath(os.path.join(project_path, "src", project_name + ".deploy"))
            else:
                raise Exception("Invalid project type")

            try:
                fesa_version = silecs_utils.get_fesa_version_from_design_deploy(fesa_file)
            except:
                print(f"Cannot get fesa version from {fesa_file}")
                return

            try:
                self.prepare_codegen_modules(fesa_version)
            except Exception as e:
                print(f"Failed to generate code for file: {fesa_file}. Error: {e}")
                return

            #Get silecs version from project
            old_version = silecs_utils.get_silecs_version_from_project(project_path)
            if version.parse(new_version) <= version.parse(old_version):
                raise Exception(f"Migration from {old_version} to {new_version} not possible. The target-version always need to be newer than the current version")

            available_versions = self.silecs_env.get_available_versions()
            matching_versions = self.silecs_env.get_matching_versions(available_versions, old_version, new_version)
            migration_modules = self.silecs_env.get_migration_modules(matching_versions, old_version)

            if project_type == silecs_utils.ProjectType.DESIGN:
                new_xml_schema = self.silecs_env.DESIGN_SHEMA_PATH
            elif project_type == silecs_utils.ProjectType.DEPLOY:
                new_xml_schema = self.silecs_env.DEPLOY_SHEMA_PATH
            else:
                raise Exception(f"Migration {old_version} to {new_version} not possible. The project {project_path} is not a silecs project")

            current_version = old_version

            for migration_module in migration_modules:
                module, version_from, version_to = migration_module

                # Create backup only during first migration
                if current_version == old_version:
                    create_backup_file = True
                else:
                    create_backup_file = False
                try:
                    module = __import__(module)
                    module.run_migrate(silecs_filepath, new_xml_schema, version_from, version_to, create_backup_file)
                    current_version = version_to
                except Exception as e:
                    print(e)
                    migration_modules.remove(migration_module)

            # Update to specific version version
            if not current_version == new_version:
                print("INFO: Running final migration. Only basic xml-version strings will be changed")
                module = __import__("migrationBase")
                module.run_migrate(silecs_filepath,  new_xml_schema, current_version, new_version, False)
            elif not migration_modules:
                print(f"WARNING: There is no Migration Script for an update from: {old_version} to {new_version}\n" \
                        f"Using generic migration-script. Only basic xml-version strings will be changed.\n")
                module = __import__("migrationBase")
                module.run_migrate(silecs_filepath,  new_xml_schema, old_version, new_version, False)

        except Exception as e:
            print(e)

##### Parse arguments ######

def _parse_arguments():
    parser = SilecsArgumentParser(
        description="Script in order to model PLC devices in FESA by using the Silecs framework",
        add_help=False
    )

    parser.add_argument(
        '-h', 
        '--help',
        action='help',
        help='Show this help message and exit.'
    )

    parser.add_argument(
        "-c",
        "--create",
        nargs="+",
        metavar="project or file",
        help="create new silecs design / deploy file for the specified FESA design / deploy file"
    )

    parser.add_argument(
        "-v",
        "--validate",
        nargs="+",
        metavar="project or file",
        help="validate design / deploy file of a silecs project"
    )

    parser.add_argument(
        "-g",
        "--generate",
        nargs="+",
        metavar="project or file",
        help="generate source code from the design / deploy file of a silecs project"
    )

    parser.add_argument(
        "-d",
        "--diagnostic",
        nargs=1,
        metavar="project or file",
        help="start diagnostic tool for silecs deploy project"
    )

    parser.add_argument(
        "-r",
        "--release",
        nargs="+",
        metavar="project or file",
        help="release parameter files of a silecs deploy project"
    )

    parser.add_argument(
        "-e",
        "--env",
        nargs=1,
        metavar=("release env"),
        default="dev",
        choices=['dev', 'int', 'pro'],
        help="release enviornment in case --release is specified [defaults to dev]"
    )

    parser.add_argument(
        "-t",
        "--target",
        nargs=1,
        metavar=("target for snap7 library"),
        default="yocto",
        choices=['yocto', 'x86_64'],
        help="release target in case --release is specified [defaults to yocto]"
    )

    parser.add_argument(
        "-m",
        "--migrate",
        nargs=2,
        metavar=("project or file", "new_version"),
        help="migrate silecs design / deploy project to a new silecs version"
    )

    return parser


##### Entry point ######

def run(run_from_source=False):
    silecs_cli = SilecsCli(run_from_source)

    parser = _parse_arguments()
    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)

    try:
        options = parser.parse_args()
    except Exception:
        sys.exit(0)

    if options.create:
        silecs_cli.silecs_create(options.create)
    if options.validate:
        silecs_cli.silecs_validate(options.validate)
    if options.generate:
        silecs_cli.silecs_generate(options.generate)
    if options.diagnostic:
        filepath = options.diagnostic[0]
        silecs_cli.silecs_diagnostic_tool(filepath)
    if options.release:
        # When the default is used (user did not provide the option), the value type is not a list.
        # When user provides the value, it's in a list.
        env = options.env
        target = options.target
        if type(options.env) is list:
            env = options.env[0]
        if type(options.target) is list:
            target = options.target[0]
        silecs_cli.silecs_release(options.release, env, target)
    if options.migrate:
        path=options.migrate[0]
        new_version=options.migrate[1]
        silecs_cli.silecs_migrate(path, new_version)

if __name__ == "__main__":
    run(run_from_source=True)
