import os
import getpass

# Default silecs version - used when silecs cli is used without installation to directory by build_and_install script
DEFAULT_SILECS_VERSION = ""

USER = getpass.getuser()
SILECSDESIGN = "silecsdesign"
SILECSDEPLOY = "silecsdeploy"
DESIGN = "design"
DEPLOY = "deploy"
SILECS_MAKEFILE = "Makefile.silecs"

# Silecs modules needed to import to generate code
SILECS_MODULES = ["genparam", "genplcsrc", "genduwrapper", "fillFESADeployUnit", "generateFesaDesign", "generateSourceCode"]

# Release param directory for releasing silecs parameter file
try:
    RELEASE_PARAM_DIR=(os.environ['RELEASE_PARAM_DIR'])
except KeyError:
    RELEASE_PARAM_DIR="/common/export/fesa/local"