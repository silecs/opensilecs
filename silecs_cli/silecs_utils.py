import os
import sys
import shutil
import datetime
from enum import Enum
from packaging import version
from xml.dom import minidom

import silecs_const
import silecs_env

class ProjectType(Enum):
    """Class used to describe type of project"""
    DESIGN = 1
    DEPLOY = 2
    OTHER = 3

def get_version_underscored(version):
    """Get underscored version string eg. 2.3.4 -> 2_3_4"""
    return version.replace(".", "_")

def get_silecs_codegen_fesa(silecs_codegen_base_fesa, fesa_version):
    get_version = lambda f: f.split("fesa_")[-1].replace("_", ".")

    try:
        available_fesa_versions = [get_version(f) for f in os.listdir(silecs_codegen_base_fesa) if f.startswith("fesa")]
        available_fesa_versions = sorted(available_fesa_versions, key= lambda x: version.Version(x), reverse=True)
    except:
        raise Exception("Cannot get available fesa codegen versions")

    is_older_version = lambda v: version.parse(v) < version.parse(fesa_version)

    if fesa_version not in available_fesa_versions:
        print(f"No available fesa codegen version for version {fesa_version}. Searching for different version")
        versions_older = [v for v in available_fesa_versions if is_older_version(v)]
        versions_newer = [v for v in available_fesa_versions if v not in versions_older]
        
        if versions_older:
            fesa_version = versions_older[0] 
        else:
            raise Exception("Cannot find suitable fesa codegen version")

        print(f"Using codegen version {fesa_version}")
    
    version_underscored = get_version_underscored(fesa_version)
    return f"{silecs_codegen_base_fesa}/fesa_{version_underscored}/"

def get_extension(filepath):
    return filepath.split('.')[-1]

def is_silecs_design_file(filepath):
    return get_extension(filepath) == silecs_const.SILECSDESIGN

def is_silecs_deploy_file(filepath):
    return get_extension(filepath) == silecs_const.SILECSDEPLOY

def is_silecs_file(path):
    return is_silecs_design_file(path) or is_silecs_deploy_file(path)

def is_fesa_design_file(path):
    return get_extension(path) == silecs_const.DESIGN

def is_fesa_deploy_file(path):
    return get_extension(path) == silecs_const.DEPLOY

def is_fesa_file(path):
    return is_fesa_design_file(path) or is_fesa_deploy_file(path)

def get_paths(path):
    if is_silecs_file(path):
        project_path = get_project_path(path)
        try:
            fesa_filepath = get_fesa_design_deploy_file(project_path)
        except Exception as exc:
            fesa_filepath = None
        silecs_filepath = path
    elif is_fesa_file(path):
        project_path = get_project_path(path)
        fesa_filepath = path
        try:
            silecs_filepath = get_silecs_design_deploy_file(project_path)
        except Exception as exc:
            silecs_filepath = None
    else:
        project_path = path
        try:
            fesa_filepath = get_fesa_design_deploy_file(project_path)
        except Exception as exc:
            fesa_filepath = None
        try:
            silecs_filepath = get_silecs_design_deploy_file(project_path)
        except Exception as exc:
            silecs_filepath = None

    return silecs_filepath, fesa_filepath, project_path

def get_project_name(filepath):
    """Get project name
    eg. workspace/TestClass/src/TestClass.silecsdesign -> TestClass
    """
    return os.path.splitext(os.path.basename(filepath))[0]

def get_project_dir(filepath):
    """Get project dir
    eg. workspace/TestClass/src/TestClass.silecsdesign -> workspace/TestClass/src"""
    return os.path.dirname(filepath)

def get_project_path(filepath):
    """Get project path
    eg. workspace/TestClass/src/TestClass.silecsdesign -> workspace/TestClass"""
    return os.path.dirname(os.path.dirname(filepath))

def get_filename(filepath):
    """Get filename from path
     eg. workspace/TestClass/src/TestClass.silecsdesign -> TestClass.silecsdesign
     """
    _, filename = os.path.split(filepath)
    return filename

def get_project_type(project_path):
    """Get project type based on presence of silecs design or deploy file in project_path"""
    project_path_src = os.path.join(project_path, "src")

    silecs_design_files = [f for f in os.listdir(project_path_src) if os.path.isfile(os.path.join(project_path_src, f)) and is_silecs_design_file(f)]
    silecs_deploy_files = [f for f in os.listdir(project_path_src) if os.path.isfile(os.path.join(project_path_src, f)) and is_silecs_deploy_file(f)]
    project_type = ProjectType.OTHER

    if silecs_design_files:
        project_type = ProjectType.DESIGN
    elif silecs_deploy_files:
        project_type = ProjectType.DEPLOY
    return project_type

def get_schema_path(silecs_env, silecs_design_deploy_path):
    """Get schema for silecs design/deploy XML validation"""
    extension = get_extension(silecs_design_deploy_path)
    if extension == silecs_const.SILECSDESIGN:
        return silecs_env.DESIGN_SHEMA_PATH
    if extension == silecs_const.SILECSDEPLOY:
        return silecs_env.DEPLOY_SHEMA_PATH
    raise Exception(f"Error: Passed file for validation needs to have the extension '.{silecs_const.SILECSDESIGN}' or '.{silecs_const.SILECSDEPLOY}'")


def get_silecs_file_path_from_fesa(fesa_design_deploy_path):
    """Get schema for silecs design/deploy XML validation"""
    extension = get_extension(fesa_design_deploy_path)
    project_name = get_project_name(fesa_design_deploy_path)
    project_dir = get_project_dir(fesa_design_deploy_path)
    if extension == silecs_const.DESIGN:
        extension = silecs_const.SILECSDESIGN
    elif extension == silecs_const.DEPLOY:
        extension = silecs_const.SILECSDEPLOY
    else:
        raise Exception(f"Error: Passed FESA file needs to have the extension '.{silecs_const.DESIGN}' or '.{silecs_const.DEPLOY}'")
    return os.path.join(project_dir, project_name + "." + extension)

def create_backup_file(filepath):
    backup_file = filepath + ".backup"
    with open(backup_file, 'w+'):
        shutil.copyfile(src=filepath, dst=backup_file)

def get_silecs_design_makefile_template(silecs_base):
    return f"""# This Makefile is auto-generated with SILECS generation tools.
# It should be included in the project's Makefile.specific by the user by adding the line 'include Makefile.silecs'.

SILECS_BASE ?= {silecs_base}
COMPILER_FLAGS += -I$(SILECS_BASE)/silecs-communication-cpp/include
SNAP7_BASE ?= /common/usr/cscofe/silecs
MODBUS_BASE ?= /common/usr/cscofe/silecs
LINKER_FLAGS += -L$(SILECS_BASE)/silecs-communication-cpp/lib/$(CPU) -lsilecs-comm
LINKER_FLAGS += -L$(SNAP7_BASE)/snap7/latest/$(CPU)-linux/lib -lsnap7
LINKER_FLAGS += -L$(MODBUS_BASE)/modbus/latest/$(CPU)-linux/lib -l:libmodbus.a

#add default search path for dynamic snap7 library
LINKER_FLAGS += -Wl,-rpath,$(SNAP7_BASE)/snap7/latest/$(CPU)-linux/lib,-rpath,/usr/lib
"""

def get_silecs_deploy_makefile_template(silecs_base):
    return f"""# This Makefile is auto-generated with SILECS generation tools.
# It should be included in the project's Makefile.specific by the user by adding the line 'include Makefile.silecs'.

SILECS_BASE ?= {silecs_base}
SNAP7_BASE ?= /common/usr/cscofe/silecs
MODBUS_BASE ?= /common/usr/cscofe/silecs
LINKER_FLAGS += -L$(SILECS_BASE)/silecs-communication-cpp/lib/$(CPU) -lsilecs-comm
LINKER_FLAGS += -L$(SNAP7_BASE)/snap7/latest/$(CPU)-linux/lib -lsnap7
LINKER_FLAGS += -L$(MODBUS_BASE)/modbus/latest/$(CPU)-linux/lib -l:libmodbus.a

#add default search path for dynamic snap7 library
LINKER_FLAGS += -Wl,-rpath,$(SNAP7_BASE)/snap7/latest/$(CPU)-linux/lib,-rpath,/usr/lib
"""

def get_silecs_design_template(project_name, silecs_version, design_schema_path):
    date = datetime.datetime.today().strftime('%m/%d/%Y')
    return f"""<?xml version="1.0" encoding="UTF-8"?>
<SILECS-Design silecs-version="{silecs_version}" created="{date}" updated="{date}"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:noNamespaceSchemaLocation="{design_schema_path}">
    <Information>
        <Owner user-login="{silecs_const.USER}"/>
        <Editor user-login="{silecs_const.USER}"/>
    </Information>
    <SILECS-Class name="{project_name}" version="0.1.0" domain="OPERATIONAL" >
        <Acquisition-Block name="MyBlock" generateFesaProperty="true">
            <Acquisition-Register name="myRegister" generateFesaValueItem="true">
                <scalar format="int32"/>
            </Acquisition-Register>
        </Acquisition-Block>
    </SILECS-Class>
</SILECS-Design>"""

def get_silecs_deploy_template(project_name, silecs_version, deploy_schema_path):
    date = datetime.datetime.today().strftime('%m/%d/%Y')
    return f"""<?xml version="1.0" encoding="UTF-8"?>
<SILECS-Deploy silecs-version="{silecs_version}" created="{date}" updated="{date}" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:noNamespaceSchemaLocation="{deploy_schema_path}">
    <Information>
        <Owner user-login="{silecs_const.USER}"/>
        <Editor user-login="{silecs_const.USER}"/>
    </Information>
    <Deploy-Unit name="{project_name}" version="0.1.0"/>
    <SilecsDesign silecs-design-name="" silecs-design-version=""/>    
    <Controller host-name="">
        <Siemens-PLC system="TIA-PORTAL" model="SIMATIC_S7-300" protocol="DEVICE_MODE" base-DB-number="1">
            <Device silecs-device-label="dev0" silecs-design-ref="" fesa-device-name="" fesa-fec-name=""/>
        </Siemens-PLC>
    </Controller>
</SILECS-Deploy>"""

def get_silecs_version_from_silecsdesign_file(silecsdesign_file):
    silecs_design_document = minidom.parse(silecsdesign_file)
    silecs_design_element = silecs_design_document.getElementsByTagName("SILECS-Design")[0]
    version = silecs_design_element.getAttribute("silecs-version")
    return version

def get_silecs_version_from_silecsdeploy_file(silecsdeploy_file):
    fesa_document_parsed = minidom.parse(silecsdeploy_file)
    silecs_deploy_element = fesa_document_parsed.getElementsByTagName("SILECS-Deploy")[0]
    version = silecs_deploy_element.getAttribute("silecs-version")
    return version

def get_silecs_version_from_file(filepath):
    if is_silecs_design_file(filepath):
        return get_silecs_version_from_silecsdesign_file(filepath)
    else:
        return get_silecs_version_from_silecsdeploy_file(filepath)

def get_design_deploy_file(project_path, design_extension, deploy_extension, error_string):
    project_path_src = os.path.join(project_path, "src")
    is_deploy_design = lambda f: f.split(".")[-1] == design_extension or f.split(".")[-1] == deploy_extension
    design_deploy_files = [f for f in os.listdir(project_path_src) if os.path.isfile(os.path.join(project_path_src, f)) and is_deploy_design(f)]
    if not design_deploy_files:
        raise Exception(f"No {error_string} design/deploy files found in project '{project_path}'")
    elif len(design_deploy_files) > 1 :
        raise Exception(f"There should be one {error_string} design/deploy file in the project '{project_path}'")
    return os.path.abspath(os.path.join(project_path_src, design_deploy_files[0]))

def get_fesa_design_deploy_file(project_path):
    return get_design_deploy_file(project_path, silecs_const.DESIGN, silecs_const.DEPLOY, "FESA")

def get_silecs_design_deploy_file(project_path):
    return get_design_deploy_file(project_path, silecs_const.SILECSDESIGN, silecs_const.SILECSDEPLOY, "SILECS")

def get_silecs_version_from_project(project_path):
    try:
        silecs_design_deploy_file = get_silecs_design_deploy_file(project_path)
        return get_silecs_version_from_file(silecs_design_deploy_file)
    except Exception as e:
        print(e)

def ask_if_continue(question):
    i = 0
    result = False
    while i < 2:
        answer = input(question)
        if any(answer.lower() == f for f in ["yes", 'y', '1', 'ye']):
            print("Yes")
            result = True
            break
        elif any(answer.lower() == f for f in ['no', 'n', '0']):
            print("No")
            result = False
            break
        else:
            i += 1
            if i < 2:
                print('Please enter yes or no')
            else:
                print("Nothing done")

    return result

def check_silecs_version_mismatch(script_version, file_version):
    """Ask if continue when silecs version of script does not match silecs version in file.
    If scipt version is older than file version exit
    """
    if isinstance(version.parse(script_version), version.LegacyVersion):
        print(f"WARNING: You are using legacy silecs version \"{script_version}\"")
    elif version.parse(script_version) > version.parse(file_version):
        print(f"WARNING: Silecs version mismatch. File version: {file_version}. Script version: {script_version}")
        question = f"Do you want to use current silecs version ({script_version}) (yes/no)?"
        will_continue = ask_if_continue(question)
        if not will_continue:
            print(f"Use silecs script that matches the silecs version {file_version}. Exiting.")
            sys.exit()
    elif version.parse(script_version) < version.parse(file_version):
        print(f"You are using outdated silecs script. Use silecs script that matches the silecs version {file_version}. Exiting.")
        sys.exit()
    return

def get_fesa_version_from_design_deploy(fesa_design_deploy_file):
    fesa_document_parsed = minidom.parse(fesa_design_deploy_file)
    information_node = fesa_document_parsed.getElementsByTagName("information")[0]
    fesa_version = information_node.getElementsByTagName("fesa-version")[0].firstChild.nodeValue
    return fesa_version
