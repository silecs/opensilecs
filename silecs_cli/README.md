# silecs-cli

Command line tool for silecs project creation, xml validation, code generation, etc.

Component of the opensilecs PLC-framework.


## Tests

In [tests](tests) directory there is a file [tests_silecs.py](tests/tests_silecs.py) containg unit tests written using `unittest` framework.  
Directory [testClasses](tests/testClasses) contain example classes uses for tests and some extra example files used for tests are provided in directory [examples](tests/examples).

Tests can be run using following command:
```
python3 -m unittest
```
