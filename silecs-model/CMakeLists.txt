cmake_minimum_required(VERSION 3.20.2)

project(silecs-model)

install (
    DIRECTORY src/xml
    DESTINATION ${CMAKE_PROJECT_VERSION}/${PROJECT_NAME}
    FILES_MATCHING PATTERN "*.xsd*")
