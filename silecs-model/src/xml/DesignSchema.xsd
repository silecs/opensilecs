<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2016 CERN and GSI This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/. -->

<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified" attributeFormDefault="unqualified">
    <xs:include schemaLocation="shared.xsd" />
    <xs:include schemaLocation="DesignSchema-data-types.xsd" />
    <xs:include schemaLocation="DesignSchema-custom-types.xsd" />
    
    <xs:element name="SILECS-Design">
        <xs:annotation>
            <xs:documentation>A SILECS configuration contains the class designs, the memory mapping and the hardware distribution for a given set of PLCs.</xs:documentation>
            <xs:appinfo>
                <doc>
                    The SILECS configuration defines the data structure and the communication parameters required for the interconnection of the PLCs and the Front-End clients. It consists of 3 parts:
                    &lt;ol&gt;
                    &lt;li&gt; Design: to define the structure of the exchanged data
                    &lt;li&gt; Mapping: to define the PLC memory mapping and the protocol parameters
                    &lt;li&gt; Generation: to define a the hardware on which we will deployed the different Mapping
                    &lt;/ol&gt;
                    Generally under the responsability of a unique expert entity, an SILECS configuration is done for a set of classes and PLCs in the scope of a particular project or set of equipments.
                </doc>
            </xs:appinfo>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="Information">
                    <xs:complexType>
                        <xs:sequence>
                            <xs:element name="Owner">
                                <xs:annotation>
                                    <xs:documentation>Owner is the one who created the class. He has write-access on the Design document and can deploy the corresponding class course. Owner can add Editors and Users on his class.</xs:documentation>
                                    <xs:appinfo>
                                        <doc>Owner is the one who created the class. He has write-access on the Design document and can deploy the corresponding class course. Owner can add Editors and Users on his class.</doc>
                                    </xs:appinfo>
                                </xs:annotation>
                                <xs:complexType>
                                    <xs:attribute name="user-login" type="xs:string" use="required" />
                                </xs:complexType>
                            </xs:element>
                            <xs:choice maxOccurs="unbounded">
                                <xs:element name="Editor" minOccurs="0" maxOccurs="unbounded">
                                    <xs:annotation>
                                        <xs:documentation>Editor has write-access on the Design document and can deploy the corresponding class. He cannot add Editors or Users on the class.</xs:documentation>
                                        <xs:appinfo>
                                            <doc>Editor has write-access on the Design document and can deploy the corresponding class. He cannot add Editors or Users on the class.</doc>
                                        </xs:appinfo>
                                    </xs:annotation>
                                    <xs:complexType>
                                        <xs:attribute name="user-login" type="xs:string" use="required" />
                                    </xs:complexType>
                                </xs:element>
                                <xs:element name="User" minOccurs="0" maxOccurs="unbounded">
                                    <xs:annotation>
                                        <xs:documentation>User has read-only access on the Design document but can deploy the corresponding class. He cannot add Editors or Users on the class.</xs:documentation>
                                        <xs:appinfo>
                                            <doc>User has read-only access on the Design document but can deploy the corresponding class. He cannot add Editors or Users on the class.</doc>
                                        </xs:appinfo>
                                    </xs:annotation>
                                    <xs:complexType>
                                        <xs:attribute name="user-login" type="xs:string" use="required" />
                                    </xs:complexType>
                                </xs:element>
                            </xs:choice>
                        </xs:sequence>
                    </xs:complexType>
                </xs:element>
                <xs:element name="SILECS-Class">
                    <xs:annotation>
                        <xs:documentation>The Class node contains the definition of a particular class</xs:documentation>
                        <xs:appinfo>
                            <doc>
                                The Class node contains the definition of a particular class. Designing a class means:
                                &lt;ol&gt;
                                &lt;li&gt; Defining a unique class name in the scope of the all configuration
                                &lt;li&gt; Providing list of variables which will be exchanged with the target PLC
                                &lt;li&gt; Identifying coherent groups of variables according to their nature and access mode
                                &lt;/ol&gt;
                                The SILECS tool assists the expert in specifying the data blocks from this abstract point of view.
                            </doc>
                        </xs:appinfo>
                    </xs:annotation>
                    <xs:complexType>
                        <xs:sequence>
                            <xs:choice minOccurs="1" maxOccurs="unbounded">
                                <xs:element name="Setting-Block" type="Setting-BlockType" />
                                <xs:element name="Acquisition-Block" type="Acquisition-BlockType" />
                                <xs:element name="Configuration-Block" type="Configuration-BlockType" />
                                <xs:element name="Command-Block" type="Command-BlockType" />
                                <xs:element name="Setting-IO-Block" type="Setting-IO-BlockType" />
                                <xs:element name="Acquisition-IO-Block" type="Acquisition-IO-BlockType" />
                                <xs:element name="Command-IO-Block" type="Setting-IO-BlockType" />
                            </xs:choice>
                            <xs:element name="Description" type="xs:string" minOccurs="0" />
                            <xs:element name="custom-types" type="custom-type-collection" minOccurs="0" />
                        </xs:sequence>
                        <xs:attribute name="name" type="SilecsClassNameType" use="required" />
                        <xs:attribute name="version" type="VersionType" use="required" />
                        <xs:attribute name="domain" use="required">
                            <xs:annotation>
                                <xs:documentation>Defines the domain of use of the class: Operation or Test</xs:documentation>
                            </xs:annotation>
                            <xs:simpleType>
                                <xs:restriction base="xs:string">
                                    <xs:enumeration value="TEST" />
                                    <xs:enumeration value="OPERATIONAL" />
                                </xs:restriction>
                            </xs:simpleType>
                        </xs:attribute>
                    </xs:complexType>
                    <xs:key name="custom-key">
                        <xs:selector xpath="./custom-types/*" />
                        <xs:field xpath="@name" />
                    </xs:key>
                    <xs:keyref name="custom-scalar-keyref" refer="custom-key">
                        <xs:selector xpath=".//custom-type-scalar | .//custom-type-array | .//custom-type-array2D"/>
                        <xs:field xpath="@custom-type-name-ref"/>
                    </xs:keyref>
                    <xs:unique name="Register-name-has-to-be-unique">
                        <xs:selector xpath="*/*" />
                        <xs:field xpath="@name" />
                    </xs:unique>
                    <xs:unique name="Block-name-has-to-be-unique">
                        <xs:selector xpath="*" />
                        <xs:field xpath="@name" />
                    </xs:unique>
                    <xs:unique name="Property-name-has-to-be-unique">
                        <xs:selector xpath="*" />
                        <xs:field xpath="@fesaPropertyName" />
                    </xs:unique>
                    <xs:unique name="Field-name-has-to-be-unique">
                        <xs:selector xpath="*/*" />
                        <xs:field xpath="@fesaFieldName" />
                    </xs:unique>
                </xs:element>
            </xs:sequence>
            <xs:attribute name="silecs-version" type="VersionType" use="required" />
            <xs:attribute name="created" type="xs:string" use="required" />
            <xs:attribute name="updated" type="xs:string" use="required" />
        </xs:complexType>
    </xs:element>


    <xs:complexType name="FesaCodegenType-ServerOnly">
        <xs:sequence>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="FesaBlockCodegenConfigType">
        <xs:sequence>
            <xs:element name="Server" minOccurs="0">
                <xs:complexType>
                    <xs:attribute name="fesaPropertyName" type="BlockNameType" use="required">
                        <xs:annotation>
                            <xs:appinfo>Defines the name of the generated Fesa-Property.</xs:appinfo>
                        </xs:annotation>
                    </xs:attribute>
                </xs:complexType>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="BlockType">
        <xs:sequence>
            <xs:element name="Description" type="xs:string" minOccurs="0" />
        </xs:sequence>
        <xs:attribute name="name" type="BlockNameType" use="required">
            <xs:annotation>
                <xs:appinfo>
                    Defines the name of the block that must be unique in all the class.
                    Must not exceed 12 characters.
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="generateFesaProperty" type="xs:boolean" use="required">
            <xs:annotation>
                <xs:appinfo>
                    Defines if the code-generation will generate a FESA-Property and the related Actions/Events/etc for this block
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="fesaPropertyName" type="BlockNameType" use="optional">
            <xs:annotation>
                <xs:appinfo>Defines the name of the generated Fesa-Property. If not set, the Blockname will be used.</xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="fesaGetServerActionName" type="BlockNameType" use="optional">
            <xs:annotation>
                <xs:appinfo>Defines the name of the generated Fesa-Server-Action. If not set, the Blockname will just be prefixed with get/set.</xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="fesaSetServerActionName" type="BlockNameType" use="optional">
            <xs:annotation>
                <xs:appinfo>Defines the name of the generated Fesa-Server-Action. If not set, the Blockname will just be prefixed with get/set.</xs:appinfo>
            </xs:annotation>
        </xs:attribute>
    </xs:complexType>

    <xs:complexType name="IO-BlockType">
        <xs:complexContent>
            <xs:extension base="BlockType">
                <xs:attribute name="ioType" use="required">
                    <xs:annotation>
                        <xs:appinfo>
                            Registers addressing area.
                            &lt;ul&gt;
                            &lt;li&gt;DIGITAL: for digital I/O register depending on the mode attribute&lt;/li&gt;
                            &lt;li&gt;ANALOG : for analog I/O register depending on the mode attribute&lt;/li&gt;
                            &lt;/ul&gt; </xs:appinfo>
                    </xs:annotation>
                    <xs:simpleType>
                        <xs:restriction base="xs:string">
                            <xs:enumeration value="DIGITAL-IO" />
                            <xs:enumeration value="ANALOG-IO" />
                        </xs:restriction>
                    </xs:simpleType>
                </xs:attribute>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <xs:complexType name="Setting-BlockType">
        <xs:complexContent>
            <xs:extension base="BlockType">
                <xs:sequence>
                    <xs:choice minOccurs="1" maxOccurs="unbounded">
                        <xs:element name="Setting-Register" type="Memory-RegisterType">
                            <xs:annotation>
                                <xs:appinfo>
                                    <doc>Controller register which can be setted and read back by Silecs. The controller should not modify it's value.</doc>
                                </xs:appinfo>
                            </xs:annotation>
                        </xs:element>
                    </xs:choice>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    
    <xs:complexType name="Configuration-BlockType">
        <xs:complexContent>
            <xs:extension base="BlockType">
                <xs:sequence>
                    <xs:choice minOccurs="1" maxOccurs="unbounded">
                        <xs:element name="Configuration-Register" type="Memory-RegisterType">
                            <xs:annotation>
                                <xs:appinfo>
                                    <doc>Controller register which is set to the controller at Silecs startup. Later only readout is possible.</doc>
                                </xs:appinfo>
                            </xs:annotation>
                        </xs:element>
                    </xs:choice>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <xs:complexType name="Acquisition-BlockType">
        <xs:complexContent>
            <xs:extension base="BlockType">
                <xs:sequence>
                    <xs:choice minOccurs="1" maxOccurs="unbounded">
                        <xs:element name="Acquisition-Register" type="Memory-RegisterType">
                            <xs:annotation>
                                <xs:appinfo>
                                    <doc>Controller register which is only read out by Silecs</doc>
                                </xs:appinfo>
                            </xs:annotation>
                        </xs:element>
                    </xs:choice>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <xs:complexType name="Setting-IO-BlockType">
        <xs:complexContent>
            <xs:extension base="IO-BlockType">
                <xs:sequence>
                    <xs:choice minOccurs="1" maxOccurs="unbounded">
                        <xs:element name="Setting-IO-Register" type="IO-RegisterType">
                            <xs:annotation>
                                <xs:appinfo>
                                    <doc>Controller register which is only set by Silecs and cannot be read. It connects directly to Hardware</doc>
                                </xs:appinfo>
                            </xs:annotation>
                        </xs:element>
                    </xs:choice>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <xs:complexType name="Acquisition-IO-BlockType">
        <xs:complexContent>
            <xs:extension base="IO-BlockType">
                <xs:sequence>
                    <xs:choice minOccurs="1" maxOccurs="unbounded">
                        <xs:element name="Acquisition-IO-Register" type="IO-RegisterType">
                            <xs:annotation>
                                <xs:appinfo>
                                    <doc>Controller register which is only read out by Silecs and cannot be set. It connects directly to Hardware</doc>
                                </xs:appinfo>
                            </xs:annotation>
                        </xs:element>
                    </xs:choice>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <xs:complexType name="Command-BlockType">
        <xs:complexContent>
            <xs:extension base="BlockType">
                <xs:sequence>
                    <xs:choice minOccurs="1" maxOccurs="unbounded">
                        <xs:element name="Setting-Register" type="Memory-RegisterType">
                            <xs:annotation>
                                <xs:appinfo>
                                    <doc>Controller register which can be setted by Silecs. The controller should not modify it's value.</doc>
                                </xs:appinfo>
                            </xs:annotation>
                        </xs:element>
                    </xs:choice>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <xs:complexType name="BaseRegisterType">
        <xs:sequence>
            <xs:element name="Description" type="xs:string" minOccurs="0" />
        </xs:sequence>
        <xs:attribute name="name" type="RegisterNameType" use="required">
            <xs:annotation>
                <xs:appinfo>
                    <doc>Name must be unique and must respect the following standard: [_A-Za-z]+[_A-Za-z0-9]*</doc>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="generateFesaValueItem" type="xs:boolean" use="required">
            <xs:annotation>
                <xs:appinfo>
                    Defines if the code-generation will generate a FESA-Value-Item for this Register
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="fesaFieldName" type="RegisterNameType" use="optional">
            <xs:annotation>
                <xs:appinfo>Defines the name of the generated Fesa-Field. If not set, the Registername will be used.</xs:appinfo>
            </xs:annotation>
        </xs:attribute>
    </xs:complexType>

    <xs:complexType name="Memory-RegisterType">
        <xs:complexContent>
            <xs:extension base="BaseRegisterType">
                <xs:sequence>
                    <xs:choice>
                        <xs:element name="scalar" type="ScalarValueType" />
                        <xs:element name="array" type="ArrayValueType" />
                        <xs:element name="array2D" type="Array2DValueType" />
                        <xs:element name="string" type="StringValueType" />
                        <xs:element name="stringArray" type="StringArrayValueType" />
                        <xs:element name="stringArray2D" type="StringArray2DValueType" />
                        <xs:element name="custom-type-scalar" type="CustomScalarValueType" />
                        <xs:element name="custom-type-array" type="CustomArrayValueType" />
                        <xs:element name="custom-type-array2D" type="CustomArray2DValueType" />
                    </xs:choice>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <xs:complexType name="IO-RegisterType">
        <xs:complexContent>
            <xs:extension base="BaseRegisterType">
                <xs:sequence>
                    <xs:element name="scalar" type="IO-ScalarValueType" />
                    <xs:element name="custom-type-scalar" type="CustomScalarValueType" />
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <xs:simpleType name="BlockNameType">
        <xs:restriction base="xs:string">
            <xs:minLength value="1" />
            <xs:maxLength value="30" />
            <!-- Siemens Support bezüglich der maximalen Zeichenlänge für PLC- Tags : S7-300/1200/1500er bei 128 Zeichen -->
            <!-- FESA PropertyNames have max. 30 characters. Since For each Block a prop is generated, we have to use this limit -->
            <xs:pattern value="[_A-Za-z]+[_A-Za-z0-9]*" />
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="RegisterNameType">
        <xs:restriction base="xs:string">
            <xs:minLength value="1" />
            <xs:maxLength value="60" />
            <!-- FESA Fields have max. 60 characters. Since For each Register a field is generated, we have to use this limit -->
            <xs:pattern value="[_A-Za-z]+[_A-Za-z0-9]*" />
        </xs:restriction>
    </xs:simpleType>

</xs:schema>
