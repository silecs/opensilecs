#!/bin/sh
set -e

PARAM_FILE=/common/home/bel/schwinn/lnx/workspace-silecs-neon/SiemensTestDU/generated/client/tsts7001.silecsparam
REGISTER=DQ_Anlog00
DEVICE=MyDevice1
DELAY_MICRO_SEC=100000

# config for SiemensTestDU
ARGUMENTS="-f $PARAM_FILE -p $DELAY_MICRO_SEC -d $DEVICE -r $REGISTER -m GET_REGISTER"
COMMAND="$BINARY $ARGUMENTS"
$COMMAND

