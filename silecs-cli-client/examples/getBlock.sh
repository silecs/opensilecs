#!/bin/sh
set -e

PARAM_FILE=/common/home/bel/schwinn/lnx/workspace-silecs-neon/SiemensTestDU/generated/client/tsts7001.silecsparam
BLOCK=MyAcquisitionBlock
DEVICE=MyDevice1

# config for SiemensTestDU
ARGUMENTS="-f $PARAM_FILE -b $BLOCK -d $DEVICE -m GET_BLOCK -c"
COMMAND="$BINARY $ARGUMENTS"
$COMMAND

