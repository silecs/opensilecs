// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <getopt.h>
#include <termios.h>// getch
#include <iostream>
#include <iomanip> // std::right, etc
#include <unistd.h>//usleep

#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/equipment/SilecsRegister.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>

const std::string startbold = "\e[1m";
const std::string endbold = "\e[0m";

enum ModeType
{
    UNDEFINED,
    GET_DEVICE,
    GET_BLOCK,
    GET_REGISTER,
    SET_REGISTER
};

std::string arg_logTopics; //comma separated list ? space-separated ?
std::string arg_parameterFile = "";
std::string arg_plcName = "";
std::string arg_deviceName = "";
std::string arg_blockName = "";
std::string arg_registerName = "";
bool arg_silent = false;
bool arg_verbose = false;
bool arg_checkChecksum = true;
ModeType arg_mode = UNDEFINED;
uint32_t periodicInterval = 0;

char *log_arg_base[2] = {(char*)"-plcLog", (char*)"ERROR"};
char *log_arg_verbose[2] = {(char*)"-plcLog", (char*)"ERROR,DEBUG,SETUP,ALLOC,LOCK,COMM,SEND,RECV,DATA,DIAG"};

void printHelp()
{
    std::cout << std::endl;
    std::cout << startbold << "NAME" << endbold << std::endl;
    std::cout << "\tsilecs-cli-client - Command Line Client to connect to a silecs-configured PLC" << std::endl;
    std::cout << std::endl;

    std::cout << startbold << "SYNOPSIS" << endbold << std::endl;
    std::cout << "\tsilecs-cli-client - [OPTIONS]" << std::endl;
    std::cout << std::endl;

    std::cout << startbold << "DESCRIPTION" << endbold << std::endl;
    std::cout << "\t Connect to a PLC and get/set values by using the Silecs-framework." << std::endl;
    std::cout << startbold << "\t-b " << endbold << "silecs-block to get" << std::endl;
    std::cout << startbold << "\t-c " << endbold << "ignore silecs-checksum, force connect to PLC" << std::endl;
    std::cout << startbold << "\t-d " << endbold << "silecs-device to request" << std::endl;
    std::cout << startbold << "\t-f " << endbold << "path to silecs-parameter-file of the plc" << std::endl;
    std::cout << startbold << "\t-h " << endbold << "print this help" << std::endl;
    std::cout << startbold << "\t-i " << endbold << "start interactive session" << std::endl; // TODO: Implement
    std::cout << startbold << "\t-v " << endbold << "verbose, all LOGTOPICS are enabled" << std::endl; // TODO: Implement
    //std::cout << startbold << "\t-m " << endbold << "mode, can be 'GET_DEVICE', 'GET_BLOCK', 'GET_REGISTER' or 'SET_REGISTER'" << std::endl;
    std::cout << startbold << "\t-m " << endbold << "mode, can be 'GET_DEVICE', 'GET_BLOCK' or 'GET_REGISTER'" << std::endl;
    std::cout << startbold << "\t-p " << endbold << "periodic Interval(ms) us ? to measure" << std::endl;
    std::cout << startbold << "\t-r " << endbold << "optional, register to get/set" << std::endl;
    std::cout << startbold << "\t-s " << endbold << "silent, minimal output (only the register-values, no meta information)" << std::endl;

    std::cout << std::endl;

    std::cout << startbold << "AUTHOR" << endbold << std::endl;
    std::cout << "\t Written by Alexander Schwinn" << std::endl;
    std::cout << std::endl;

    std::cout << startbold << "REPORTING BUGS" << endbold << std::endl;
    std::cout << "\t Please contact the author vie the gsi mail-address" << std::endl;
    std::cout << std::endl;

    std::cout << startbold << "COPYRIGHT" << endbold << std::endl;
    std::cout << "\tCopyright  ©  2017  Free Software Foundation, Inc.  License GPLv3+: GNU" << std::endl;
    std::cout << "\tGPL version 3 or later <http://gnu.org/licenses/gpl.html>." << std::endl;
    std::cout << "\tThis is free software: you are free  to  change  and  redistribute  it." << std::endl;
    std::cout << "\tThere is NO WARRANTY, to the extent permitted by law." << std::endl;
    std::cout << std::endl;
}

/* reads from keypress, doesn't echo */
int getch(void)
{
    struct termios oldattr, newattr;
    int ch;
    tcgetattr(STDIN_FILENO, &oldattr);
    newattr = oldattr;
    newattr.c_lflag &= ~ (ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newattr);
    ch = getchar();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldattr);
    return ch;
}

//returns -1 for "return to previous menu" or array-index of list
int querryUserPickFromList(std::vector<std::string> &list)
{
    int selection = -1;
    std::vector<std::string>::iterator item;
    while (selection < 0 || (unsigned int)selection > list.size())
    {
        int itemIndex = 1;
        std::cout << std::endl;
        std::cout << "The following options are available:" << std::endl;
        std::cout << std::endl;
        for (item = list.begin(); item != list.end(); item++)
        {
            std::cout << std::setw(5) << std::right << itemIndex << ": " << std::setw(30) << std::left << *item << std::endl;
            itemIndex++;
        }

        std::cout << std::endl;
        std::cout << std::setw(5) << std::right << "0" << ": " << std::setw(30) << std::left << "return to previous menu" << std::endl;
        std::cout << std::endl;
        std::cout << "Waiting for your selection ";
        int input = getch();
        std::cout << std::endl;
        selection = input - 48; //ascii magic
        if (selection < 0 || (unsigned int)selection > list.size())
        {
            std::cout << "Invalid input: '" << input << "'. press any key to take another try!" << std::endl;
            getch();
        }
    }
    return selection - 1; // -1 = return ... everything else is array-index
}

std::string getPLCName(Silecs::XMLParser &paramParser)
{
    Silecs::ElementXML mappingNode = paramParser.getFirstElementFromXPath("/SILECS-Param/SILECS-Mapping");
    return mappingNode.getAttribute("plc-name");
}

Silecs::PLC& getSilecsPLC(const std::string& plcName, Silecs::Service *silecsService)
{
    return silecsService->getPLCHandler().getPLC(plcName);
}

Silecs::SilecsDesign& getSilecsDesignbyDevice(std::string deviceName, Silecs::XMLParser &paramParser, Silecs::Service *silecsService)
{
    Silecs::ElementXML classNode = paramParser.getFirstElementFromXPath("/SILECS-Param/SILECS-Mapping/SILECS-Class[Instance/@label='" + deviceName + "']");
    std::string className = classNode.getAttribute("name");
    std::string classVersion = classNode.getAttribute("version");
    auto& plc = getSilecsPLC(getPLCName(paramParser), silecsService);
    return plc.getDeploy().getDesign(className);
}

std::string getSilecsBlockNamebyRegisterName(std::string registerName, Silecs::XMLParser &paramParser)
{
    Silecs::ElementXML blockNode = paramParser.getFirstElementFromXPath("/SILECS-Param/SILECS-Mapping/SILECS-Class/*[*/@name='" + registerName + "']");
    return blockNode.getAttribute("name");
}

bool isRegisterInBlock(std::string registerName, std::string blockName, Silecs::XMLParser &paramParser)
{
    Silecs::ElementXML blockNode = paramParser.getFirstElementFromXPath("/SILECS-Param/SILECS-Mapping/SILECS-Class/*[*/@name='" + registerName + "']");
    if (blockName == blockNode.getAttribute("name"))
        return true;
    return false;
}

std::vector<std::string> getDeviceNames(Silecs::XMLParser &paramParser)
{
    std::vector < std::string > deviceNames;
    auto deviceNodes = paramParser.getElementsFromXPath("/SILECS-Param/SILECS-Mapping/SILECS-Class/Instance");
    for (auto deviceNode = deviceNodes.begin(); deviceNode != deviceNodes.end(); deviceNode++)
    {
        deviceNames.push_back(deviceNode->getAttribute("label"));
    }
    return deviceNames;
}

std::vector<std::string> getBlockNamesFromDeviceName(std::string deviceName, Silecs::XMLParser &paramParser)
{
    std::vector < std::string > blockNames;
    Silecs::ElementXML classNode = paramParser.getFirstElementFromXPath("/SILECS-Param/SILECS-Mapping/SILECS-Class[Instance/@label='" + deviceName + "']");
    std::string className = classNode.getAttribute("name");
    auto blocks = paramParser.getElementsFromXPath("/SILECS-Param/SILECS-Mapping/SILECS-Class[@name='" + className + "']/*[ name()='Acquisition-Block' or name()='Setting-Block' or name()='Command-Block']");
    for (auto block = blocks.begin(); block != blocks.end(); block++)
    {
        //std::cout<< block->getAttribute("name") << std::endl;
        blockNames.push_back(block->getAttribute("name"));
    }
    return blockNames;
}

std::vector<std::string> getRegisterNamesFromDeviceBlockName(std::string deviceName, std::string blockName, Silecs::XMLParser &paramParser)
{
    std::vector < std::string > registerNames;
    Silecs::ElementXML classNode = paramParser.getFirstElementFromXPath("/SILECS-Param/SILECS-Mapping/SILECS-Class[Instance/@label='" + deviceName + "']");
    std::string className = classNode.getAttribute("name");
    auto registerNodes = paramParser.getElementsFromXPath("/SILECS-Param/SILECS-Mapping/SILECS-Class[@name='" + className + "']/*[@name='" + blockName + "']/*[ name()='Acquisition-Register' or name()='Setting-Register' or name()='Volatile-Register']");
    for (auto registerNode = registerNodes.begin(); registerNode != registerNodes.end(); registerNode++)
    {
        //std::cout<< block->getAttribute("name") << std::endl;
        registerNames.push_back(registerNode->getAttribute("name"));
    }
    return registerNames;
}

std::string getRegisterValueAsString(Silecs::Register *reg)
{
    std::ostringstream os;
    uint32_t dim1 = reg->getDimension1();
    uint32_t dim2 = reg->getDimension2();
    for (unsigned int i = 0; i < dim1; i++)
        for (unsigned int j = 0; j < dim2; j++)
            os << reg->getInputValAsString(i, j) << " ";
    return os.str();
}

void printTableHead()
{
    std::cout << "------------------------------------------------------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "| " << std::setw(20) << std::left << "Device" << "| " << std::setw(30) << std::left << "Block" << "| " << std::setw(50) << std::left << "Register" << "| " << std::setw(30) << std::left << "Value" << "|" << std::endl;
    std::cout << "------------------------------------------------------------------------------------------------------------------------------------------------" << std::endl;
}

void printRunState(Silecs::PLC& plc)
{
    std::cout << "------------------------------------------------------------------------------------------------------------------------------------------------" << std::endl;
    if (plc.isRunning())
        std::cout << "plc run-state is: RUNNING" << std::endl;
    else
        std::cout << "plc run-state is: STOPPED" << std::endl;
    std::cout << "------------------------------------------------------------------------------------------------------------------------------------------------" << std::endl;
}

void printRegister(Silecs::Device *device, Silecs::Register *reg)
{
    if (arg_silent)
    {
        std::cout << getRegisterValueAsString(reg) << std::endl;
    }
    else
    {
        std::cout << "| " << std::setw(20) << std::left << device->getLabel() << "| " << std::setw(30) << std::left << reg->getBlockName() << "| " << std::setw(50) << std::left << reg->getName() << "| " << std::setw(30) << std::left << getRegisterValueAsString(reg) << "|" << std::endl;
    }
}

void printBlock(Silecs::Device *device, std::string blockName, Silecs::XMLParser &paramParser)
{
    device->recv(blockName);
    auto& regCol = device->getRegisterCollection(blockName);
    for (auto reg = regCol.begin(); reg != regCol.end(); reg++)
    {
        if (isRegisterInBlock( (*reg)->getName(), blockName, paramParser))
        {
            printRegister(device, (*reg).get());
        }
    }
}

void printDevice(Silecs::Device *device, Silecs::XMLParser &paramParser)
{
    std::vector < std::string > blockNames = getBlockNamesFromDeviceName(device->getLabel(), paramParser);
    std::vector<std::string>::iterator blockName;
    for (blockName = blockNames.begin(); blockName != blockNames.end(); blockName++)
    {
        device->recv(*blockName);
        auto& regCol = device->getRegisterCollection(*blockName);
        for (auto reg = regCol.begin(); reg != regCol.end(); reg++)
        {
            printRegister(device, (*reg).get());
        }
    }
}

void setRegister(Silecs::Register *reg, std::string value)
{
    std::cout << "Error: Setting values not yet supported." << std::endl;
    exit (EXIT_FAILURE);
}

int connectInteractive(Silecs::Service *service, Silecs::XMLParser &paramParser)
{
    std::vector <std::string> mainMenu = {"connect to plc-device", "select block",
        "select register", "print whole device", "print block", "print register",
        "query plc run state", "cold-restart plc"};

    while (true)
    {
        std::cout << std::setw(20) << std::right << "Device:" << std::setw(20) << std::left << startbold << arg_deviceName << endbold << std::endl;
        std::cout << std::setw(20) << std::right << "Block:" << std::setw(20) << std::left << startbold << arg_blockName << endbold << std::endl;
        std::cout << std::setw(20) << std::right << "Register:" << std::setw(20) << std::left << startbold << arg_registerName << endbold << std::endl;
        int item = querryUserPickFromList(mainMenu);
        switch (item)
        {
            case -1:
                return EXIT_SUCCESS;
            case 0:
            {
                std::vector < std::string > devices = getDeviceNames(paramParser);
                int index = querryUserPickFromList(devices);
                if (index == -1)
                    break;
                arg_deviceName = devices[index];
                arg_blockName = "";
                arg_registerName = "";
                auto& plc = getSilecsPLC(getPLCName(paramParser), service);
                plc.connect(true, arg_checkChecksum);
                break;
            }
            case 1:
            {
                if (arg_deviceName.empty())
                {
                    std::cout << "Please select a device first! - Press any key to continue." << std::endl;
                    getch();
                    break;
                }
                std::vector < std::string > blocks = getBlockNamesFromDeviceName(arg_deviceName, paramParser);
                int index = querryUserPickFromList(blocks);
                if (index == -1)
                    break;
                arg_blockName = blocks[index];
                arg_registerName = "";
                break;
            }
            case 2:
            {
                if (arg_deviceName.empty() || arg_blockName.empty())
                {
                    std::cout << "Please connect to a device first! - Press any key to continue." << std::endl;
                    getch();
                    break;
                }
                std::vector < std::string > registers = getRegisterNamesFromDeviceBlockName(arg_deviceName, arg_blockName, paramParser);
                int index = querryUserPickFromList(registers);
                if (index == -1)
                    break;
                arg_registerName = registers[index];
                break;
            }
            case 3:
            {
                if (arg_deviceName.empty())
                {
                    std::cout << "Please connect to a device first! - Press any key to continue." << std::endl;
                    getch();
                    break;
                }
                printTableHead();
                auto& design = getSilecsDesignbyDevice(arg_deviceName, paramParser, service);
                auto device = design.getDevice(arg_deviceName);
                printDevice(device, paramParser);
                break;
            }
            case 4:
            {
                if (arg_deviceName.empty() || arg_blockName.empty())
                {
                    std::cout << "Please first connect to a device and select a block! - Press any key to continue." << std::endl;
                    getch();
                    break;
                }
                printTableHead();
                auto& design = getSilecsDesignbyDevice(arg_deviceName, paramParser, service);
                auto device = design.getDevice(arg_deviceName);
                printBlock(device, arg_blockName, paramParser);
                break;
            }
            case 5:
            {
                if (arg_deviceName.empty() || arg_blockName.empty() || arg_registerName.empty())
                {
                    std::cout << "Please first connect to device and pick a block and a register! - Press any key to continue." << std::endl;
                    getch();
                    break;
                }
                printTableHead();
                auto& design = getSilecsDesignbyDevice(arg_deviceName, paramParser, service);
                auto device = design.getDevice(arg_deviceName);
                auto& reg = device->getRegister(arg_registerName);
                printRegister(device, reg.get());
                break;
            }
            case 6:
            {
                if (arg_deviceName.empty())
                {
                    std::cout << "Please connect to a device first! - Press any key to continue." << std::endl;
                    getch();
                    break;
                }
                auto& plc = getSilecsPLC(getPLCName(paramParser), service);
                printRunState(plc);
                break;
            }
            case 7:
            {
                if (arg_deviceName.empty())
                {
                    std::cout << "Please connect to a device first! - Press any key to continue." << std::endl;
                    getch();
                    break;
                }
                auto& plc = getSilecsPLC(getPLCName(paramParser), service);
                plc.sendColdRestart();
                break;
            }
            default:
                std::cout << "Invalid option:" << item << std::endl;
                return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

int connectNonInteractive(Silecs::Service *service, Silecs::XMLParser &paramParser, bool periodicOptionSet)
{
    auto& plc = getSilecsPLC(getPLCName(paramParser), service);
    plc.connect(true, arg_checkChecksum);
    if (!plc.isConnected())
    {
        std::cout << "Error: Failed to connect to PLC." << std::endl;
        return EXIT_FAILURE;
    }

    if (!arg_silent)
    {
        printRunState(plc);
    }
    auto& design = getSilecsDesignbyDevice(arg_deviceName, paramParser, service);
    auto device = design.getDevice(arg_deviceName);

    Silecs::Register *reg = NULL;
    if (!arg_registerName.empty())
    {
        reg = device->getRegister(arg_registerName).get();
        arg_blockName = getSilecsBlockNamebyRegisterName(arg_registerName, paramParser);
    }

    if (!arg_silent)
    {
        printTableHead();
    }

    do
    {
        switch (arg_mode)
        {
            case GET_DEVICE:
                printDevice(device, paramParser);
                break;
            case GET_BLOCK:
                printBlock(device, arg_blockName, paramParser);
                break;
            case GET_REGISTER:
                device->recv(arg_blockName);
                printRegister(device, reg);
                break;
            case SET_REGISTER:
                setRegister(reg, "");
                break;
            default:
                std::cout << "Unknown mode defined by -m" << std::endl;
                return EXIT_FAILURE;
        }
        usleep(periodicInterval);
    }
    while (periodicOptionSet);

    return EXIT_SUCCESS;
}

int main(int argc, char **argv)
{
    try
    {
        int opt;
        bool interactiveOptionSet = false;
        bool periodicOptionSet = false;

        while ( (opt = getopt(argc, argv, ":vhcp:r:b:d:f:m:is")) != -1)
        {

            switch (opt)
            {
                case 'c':
                    arg_checkChecksum = false;
                    break;
                case 'h':
                    printHelp();
                    return EXIT_SUCCESS;
                case 'v':
                    arg_verbose = true;
                    break;
                case 'p':
                {
                    if (interactiveOptionSet)
                    {
                        std::cout << "Error: options '-i' and '-p' are mutually exclusive " << std::endl;
                        printHelp();
                        return EXIT_FAILURE;
                    }
                    periodicOptionSet = true;
                    std::istringstream ss(optarg);
                    ss >> periodicInterval;
                    break;
                }
                case 'r':
                    arg_registerName.assign(optarg);
                    break;
                case 'd':
                    arg_deviceName.assign(optarg);
                    break;
                case 'f':
                    arg_parameterFile.assign(optarg);
                    break;
                case 'b':
                    arg_blockName.assign(optarg);
                    break;
                case 'm':
                {
                    std::string mode;
                    mode.assign(optarg);

                    if (mode == "GET_DEVICE")
                        arg_mode = GET_DEVICE;
                    else if (mode == "GET_BLOCK")
                        arg_mode = GET_BLOCK;
                    else if (mode == "GET_REGISTER")
                        arg_mode = GET_REGISTER;
                    else if (mode == "SET_REGISTER")
                        arg_mode = SET_REGISTER;
                    else
                    {
                        std::cout << "Error: invalid mode passed. Please check the help:" << std::endl;
                        printHelp();
                        return EXIT_FAILURE;
                    }
                    break;
                }
                case 'i':
                    if (interactiveOptionSet)
                    {
                        std::cout << "Error: options '-i' and '-p' are mutually exclusive " << std::endl;
                        printHelp();
                        return EXIT_FAILURE;
                    }
                    interactiveOptionSet = true;
                    break;
                case 's':
                    arg_silent = true;
                    break;
                case '?':
                    std::cout << "Unrecognized option: " << optopt << std::endl;

            }
        }

        if (arg_mode == GET_BLOCK && arg_blockName.empty())
        {
            std::cout << "Error: No block-name specified, cannot execute GET_BLOCK" << std::endl;
            printHelp();
            return EXIT_FAILURE;
        }

        if ( (arg_mode == GET_REGISTER || arg_mode == SET_REGISTER) && arg_registerName.empty())
        {
            std::cout << "Error: No block-name specified, cannot execute GET_REGISTER or SET_REGISTER" << std::endl;
            printHelp();
            return EXIT_FAILURE;
        }

        if (arg_parameterFile.empty())
        {
            std::cout << "Error: no parameter-file was passed. You need to at least pass -f [filename]" << std::endl;
            printHelp();
            return EXIT_FAILURE;
        }

        Silecs::XMLParser::init();
        Silecs::XMLParser paramParser(arg_parameterFile, true);
        Silecs::Service *silecsService = NULL;
        if (arg_verbose)
            silecsService = Silecs::Service::getInstance(2, log_arg_verbose);
        else
            silecsService = Silecs::Service::getInstance(2, log_arg_base);

        if (interactiveOptionSet)
        {
            return connectInteractive(silecsService, paramParser);
        }
        else
        {
            if (arg_deviceName.empty())
            {
                std::cout << "Error: no deviceName was passed. You need to pass -d [devicename]" << std::endl;
                printHelp();
                return EXIT_FAILURE;
            }
            return connectNonInteractive(silecsService, paramParser, periodicOptionSet);
        }

        return EXIT_SUCCESS;
    }
    catch(std::string *str)
    {
        std::cout << str << std::endl;
    }
    catch(std::exception &ex)
    {
        std::cout << ex.what() << std::endl;
    }
    catch(...)
    {
        std::cout << "Unexpected error caught in Main. Please notify SILECS support" << std::endl;
    }
    return -1;
}
