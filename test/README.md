# Testing AllTypesFESA class on the test PLC

- In order to run the block mode tests, the generated *-block.scl file needs to be uploaded to the PLC.
- In order to run the device mode tests, the generated *-device.scl file needs to be uploaded to the PLC. For device-mode it seems to be required to modifiy the DB Numbers by hand, according to the [generated sdf file](./AllTypesFESA_DU/generated-silecs//controller/tsts7001-device.sdf).

More info on how to upload *.scl files and to change the DB numbers can be found [here](https://www-acc.gsi.de/wiki/Frontend/Upload_Controller_Siemens).

## How tests work

You can run the [silecsTypesTester.sh](./silecsTypesTester.sh) script which will synchronize, build and release the class. Then it will ask to startup and configure the class and PLC in device mode. Upon confirmation it will use `cmwTester.py` to test over the FESA interface. Finally the last steps are repeated for block mode.
