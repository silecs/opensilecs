#/bin/sh

set -e

SCRIPT_PATH=$(dirname $(readlink -f "$0"))
TESTING_FEC=vmla016
RELEASE_PATH=/common/export/fesa/local/${TESTING_FEC}/AllTypesFESA_DU/

cd $SCRIPT_PATH/../

./configure.sh -r $SCRIPT_PATH/build-test -t x86_64
./build.sh -t x86_64
./install.sh -t x86_64

./configure.sh -r $SCRIPT_PATH/build-test -t yocto
./build.sh -t yocto
./install.sh -t yocto

cd $SCRIPT_PATH

. $SCRIPT_PATH/build-test/latest/silecs_environment

RELATIVE_PATH=$(dirname "$0")

silecs -g $RELATIVE_PATH/AllTypesFESA $RELATIVE_PATH/AllTypesFESA_DU

echo -e "\n"
echo -e "Make sure that the <path> element inside AllTypesFESA_DU.deploy contains the correct absolute path to the Fesa class.\n"
read -p "Press any key to start fesa-codegen and build the Fesa binary"
echo -e "\n"

yocto-fesa3 -c $RELATIVE_PATH/AllTypesFESA -d $RELATIVE_PATH/AllTypesFESA_DU gen
yocto-fesa3 -c $RELATIVE_PATH/AllTypesFESA -d $RELATIVE_PATH/AllTypesFESA_DU build

echo -e "\n"
echo -e "Load the device-mode scl on the PLC (Make sure that the correct DB-Numbers are used (DB1000 and DB1001).\n"
echo -e "Modify DB-Numbers if required .. moe info here: https://www-acc.gsi.de/wiki/Frontend/Upload_Controller_Siemens"
echo -e "Connect to thePLC and update/run the software changes"
read -p "Press any key to continue"
echo -e "\n"

echo -e "\n"
echo -e "Make sure that the instance file only contains the one device-mode device.\n"
read -p "Press any key to continue to release the fesa files to the testing FEC"
echo -e "\n"

yocto-fesa3 -d $RELATIVE_PATH/AllTypesFESA_DU release -f ${TESTING_FEC} -e int
silecs -r $SCRIPT_PATH/AllTypesFESA_DU -e int

#Make sure to link the device-mode param file
rm -rf ${RELEASE_PATH}/tsts7001.silecsparam
ln -s ${RELEASE_PATH}/tsts7001-device.silecsparam ${RELEASE_PATH}/tsts7001.silecsparam

echo -e "\n"
echo -e "Now start the binary using the systemd fesa tools/the init script:\n"
echo -e "\tStop the class, if it is still running\n"
echo -e "\t\tsystemctl stop fesa@AllTypesFESA_DU\n"
echo -e "\tExecute the silecs init script to update the files to run:\n"
echo -e "\t\t/opt/nfsinit/vmla016/50_silecs\n"
echo -e "\tCheck for errors:\n"
echo -e "\t\tsystemctl status fesa@AllTypesFESA_DU\n"
echo -e "\t\tjournalctl -u fesa@AllTypesFESA_DU\n"
echo -e "(If you wish to avoid testing with systemd tools, you can synchronize the class and copy over startManually_AllTypesFESA_DU_M.sh. Since this is not done automatically by the fesa yocto release scripts)\n"
read -p "Press any key to continue to run the cmw-tester in device mode"
echo -e "\n"

$SCRIPT_PATH/cmwTester.py --int

echo -e "\n"
echo -e "Congratulation, the integration test for device-mode finished without errors\n"
read -p "Press any key to exit this script"
echo -e "\n"

echo -e "\n"
echo -e "Now Load the block-mode scl on the PLC (Make sure that the correct DB-Numbers are used (DB2000 ... DB2004).\n"
echo -e "Modify DB-Numbers if required .. moe info here: https://www-acc.gsi.de/wiki/Frontend/Upload_Controller_Siemens"
echo -e "Connect to thePLC and update/run the software changes"
read -p "Press any key to continue"
echo -e "\n"

echo -e "\n"
echo -e "Make sure that the instance file only contains the one block-mode device.\n"
read -p "Press any key to continue to release the fesa files to the testing FEC"
echo -e "\n"

yocto-fesa3 -d $RELATIVE_PATH/AllTypesFESA_DU release -f ${TESTING_FEC} -e int

#Make sure to link the block-mode param file
rm -rf ${RELEASE_PATH}/tsts7001.silecsparam
ln -s ${RELEASE_PATH}/tsts7001-block.silecsparam ${RELEASE_PATH}/tsts7001.silecsparam

echo -e "\n"
echo -e "Now start the binary using the systemd fesa tools/the init script:\n"
echo -e "\tStop the class, if it is still running\n"
echo -e "\t\tsystemctl stop fesa@AllTypesFESA_DU\n"
echo -e "\tExecute the silecs init script to update the files to run:\n"
echo -e "\t\t/opt/nfsinit/vmla016/50_silecs\n"
echo -e "\tCheck for errors:\n"
echo -e "\t\tsystemctl status fesa@AllTypesFESA_DU\n"
echo -e "\t\tjournalctl -u fesa@AllTypesFESA_DU\n"
echo -e "(If you wish to avoid testing with systemd tools, you can synchronize the class and copy over startManually_AllTypesFESA_DU_M.sh. Since this is not done automatically by the fesa yocto release scripts)\n"
read -p "Press any key to continue to run the cmw-tester in block mode"
echo -e "\n"

$SCRIPT_PATH/cmwTester.py --int --block-mode

echo -e "\n"
echo -e "Congratulation, the integration test for block-mode finished without errors\n"
read -p "Press any key to exit this script"
echo -e "\n"
