#!/common/usr/cscofe/bin/rdapython

import cmwrda
import random
import time
import argparse

parser = argparse.ArgumentParser(description="Test AllTypesFESA class")
parser.add_argument("--block-mode", action="store_false", dest="block_mode", help="Select block mode for testing. Device mode otherwise.")

args = parser.parse_args()


fecName = "AllTypesFESA_DU.vmla016"
devModeDevice = "allTypesDevModeFesaDevice"
blockModeDevice = "allTypesBlkModeFesaDevice"

if args.block_mode:
    device = devModeDevice
else:
    device = blockModeDevice
    
print('Connecting to devicename:',device,'on FEC', fecName)

rwBlockProp = cmwrda.Rda(device, "MyRWBlockProp", fecName)

setData = cmwrda.Data()
setData.add(cmwrda.ValueScalar('RW_enum_fesa', cmwrda.RdaType['INT'], random.randint(0, 3)))
setData.add(cmwrda.ValueScalar('RW_dt_fesa', cmwrda.RdaType['DOUBLE'], random.randrange(1893457980, 1893457999)))
setData.add(cmwrda.ValueScalar('RW_real_fesa', cmwrda.RdaType['FLOAT'], random.randrange(0, 10000)))
setData.add(cmwrda.ValueScalar('RW_dint_fesa', cmwrda.RdaType['INT'], random.randrange(0, 10000)))
setData.add(cmwrda.ValueScalar('RW_int_fesa', cmwrda.RdaType['SHORT'], random.randrange(0, 10000)))
setData.add(cmwrda.ValueScalar('RW_dword_fesa', cmwrda.RdaType['LONG'], random.randrange(0, 10000)))
setData.add(cmwrda.ValueScalar('RW_word_fesa', cmwrda.RdaType['INT'], random.randrange(0, 10000)))
setData.add(cmwrda.ValueScalar('RW_byte_fesa', cmwrda.RdaType['SHORT'], random.randrange(0, 10000)))
setData.add(cmwrda.ValueScalar('RW_char_fesa', cmwrda.RdaType['BYTE'], random.randrange(0, 100)))
setData.add(cmwrda.ValueScalar('RW_date_fesa', cmwrda.RdaType['DOUBLE'], random.randrange(0, 10000)))
setData.add(cmwrda.ValueScalar('RW_string_fesa', cmwrda.RdaType['STRING'], "TestString"))
setData.add(cmwrda.ValueScalar('RW_float32_fesa', cmwrda.RdaType['FLOAT'], random.randrange(0, 10000)))
setData.add(cmwrda.ValueScalar('RW_uint32_fesa', cmwrda.RdaType['LONG'], random.randrange(0, 10000)))
setData.add(cmwrda.ValueScalar('RW_int32_fesa', cmwrda.RdaType['INT'], random.randrange(0, 10000)))
setData.add(cmwrda.ValueScalar('RW_uint16_fesa', cmwrda.RdaType['INT'], random.randrange(0, 10000)))
setData.add(cmwrda.ValueScalar('RW_int16_fesa', cmwrda.RdaType['SHORT'], random.randrange(0, 10000)))
setData.add(cmwrda.ValueScalar('RW_uint8_fesa', cmwrda.RdaType['SHORT'], random.randrange(0, 10000)))
setData.add(cmwrda.ValueScalar('RW_int8', cmwrda.RdaType['BYTE'], random.randrange(0, 100)))
setData.add(cmwrda.ValueArray2D('RW2_dt_fesa', cmwrda.RdaType['DOUBLE_ARRAY_2D'], random.sample(range(1893457980, 1893457999), 4), 2, 2))
setData.add(cmwrda.ValueArray2D('RW2_real_fesa', cmwrda.RdaType['FLOAT_ARRAY_2D'], random.sample(range(0, 10000), 4), 2, 2))
setData.add(cmwrda.ValueArray2D('RW2_dint_fesa', cmwrda.RdaType['INT_ARRAY_2D'], random.sample(range(0, 10000), 4), 2, 2))
setData.add(cmwrda.ValueArray2D('RW2_int_fesa', cmwrda.RdaType['SHORT_ARRAY_2D'], random.sample(range(0, 10000), 4), 2, 2))
setData.add(cmwrda.ValueArray2D('RW2_dword_fesa', cmwrda.RdaType['LONG_ARRAY_2D'], random.sample(range(0, 10000), 4), 2, 2))
setData.add(cmwrda.ValueArray2D('RW2_word_fesa', cmwrda.RdaType['INT_ARRAY_2D'], random.sample(range(0, 10000), 4), 2, 2))
setData.add(cmwrda.ValueArray2D('RW2_byte_fesa', cmwrda.RdaType['SHORT_ARRAY_2D'], random.sample(range(0, 10000), 4), 2, 2))
setData.add(cmwrda.ValueArray2D('RW2_char_fesa', cmwrda.RdaType['BYTE_ARRAY_2D'], random.sample(range(0, 100), 4), 2, 2))
setData.add(cmwrda.ValueArray2D('RW2_date_fesa', cmwrda.RdaType['DOUBLE_ARRAY_2D'], random.sample(range(1893457980, 1893457999), 4), 2, 2))
setData.add(cmwrda.ValueArray2D('RW2_float32_fesa', cmwrda.RdaType['FLOAT_ARRAY_2D'], random.sample(range(0, 10000), 4), 2, 2))
setData.add(cmwrda.ValueArray2D('RW2_uint32_fesa', cmwrda.RdaType['LONG_ARRAY_2D'], random.sample(range(0, 10000), 4), 2, 2))
setData.add(cmwrda.ValueArray2D('RW2_int32_fesa', cmwrda.RdaType['INT_ARRAY_2D'], random.sample(range(0, 10000), 4), 2, 2))
setData.add(cmwrda.ValueArray2D('RW2_uint16_fesa', cmwrda.RdaType['INT_ARRAY_2D'], random.sample(range(0, 10000), 4), 2, 2))
setData.add(cmwrda.ValueArray2D('RW2_int16_fesa', cmwrda.RdaType['SHORT_ARRAY_2D'], random.sample(range(0, 10000), 4), 2, 2))
setData.add(cmwrda.ValueArray2D('RW2_uint8_fesa', cmwrda.RdaType['SHORT_ARRAY_2D'], random.sample(range(0, 10000), 4), 2, 2))
setData.add(cmwrda.ValueArray2D('RW2_int8', cmwrda.RdaType['BYTE_ARRAY_2D'], random.sample(range(0, 100), 4), 2, 2))

print('Writing to property MyRWBlockProp')
rwBlockProp.write(setData)
print('Writing to property MyRWBlockProp done')

time.sleep(0.5)

print('Reading from property MyRWBlockProp')
readData = rwBlockProp.read()
print('Reading from property MyRWBlockProp done')

readValuesDict = readData.getValues()
setValuesDict = setData.getValues()

allEqual = True

print('Comparing received data with sent data')
for key in readValuesDict:
    readValue = readValuesDict[key]
    setValue = setValuesDict[key]
    if readValue.data() != setValue.data():
        print("Mismatch between set value\n", setValue, "\nand read value\n", readValue)
        allEqual = False

if not allEqual:
    exit(1)
print('Comparing received data with sent data done')
print('-- Received data matches sent data. Read/Write Test for device',device,'finished sucesfully --')

