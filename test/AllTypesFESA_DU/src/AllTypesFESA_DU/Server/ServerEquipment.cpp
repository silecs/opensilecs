#include <AllTypesFESA_DU/Server/ServerEquipment.h>
#include <AllTypesFESA/GeneratedCode/ServiceLocator.h>
namespace AllTypesFESA_DU
{

ServerEquipment::ServerEquipment (const std::map<std::string, fesa::AbstractServiceLocator*>& serviceLocatorCol) : ServerEquipmentGen(serviceLocatorCol)
{
}

ServerEquipment::~ServerEquipment ()
{
}

void ServerEquipment:: specificInit () 
{
}

}
