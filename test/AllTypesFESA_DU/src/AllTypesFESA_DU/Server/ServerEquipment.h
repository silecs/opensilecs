#ifndef AllTypesFESA_DU_SERVER_EQUIPMENT_H_
#define AllTypesFESA_DU_SERVER_EQUIPMENT_H_

#include <fesa-core/Server/AbstractServerEquipment.h>

#include <fesa-core/Exception/FesaException.h>

#include <AllTypesFESA_DU/GeneratedCode/ServerEquipmentGen.h>

namespace AllTypesFESA_DU
{

class ServerEquipment : public ServerEquipmentGen
{
	public:

		ServerEquipment (const std::map<std::string, fesa::AbstractServiceLocator*>& serviceLocatorCol);

		virtual  ~ServerEquipment();

		void specificInit();

};
}
#endif
