#include <AllTypesFESA_DU/RealTime/RTEquipment.h>
#include <fesa-core/Core/AbstractServiceLocator.h>
#include <AllTypesFESA/GeneratedCode/ServiceLocator.h>
namespace AllTypesFESA_DU
{

RTEquipment::RTEquipment(const std::map<std::string, fesa::AbstractServiceLocator*>& serviceLocatorCol) :
	RTEquipmentGen(serviceLocatorCol)
{
}

RTEquipment::~RTEquipment()
{
}

void RTEquipment::specificInit()
{
}

void RTEquipment::handleEventSourceError (fesa::AbstractEventSource* eventSource,fesa::FesaException& exception)
{
}
void RTEquipment::handleSchedulerError (fesa::RTScheduler* scheduler,fesa::FesaException& exception)
{
}
}
