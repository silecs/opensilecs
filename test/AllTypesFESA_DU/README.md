# Usage

This is a deploy unit for testing the Silecs AllTypes testing class. It configures a single PLC as two PLCs with one device in device mode and one in block mode. This way both modes can be tested with a single deployment.

However this requires a custom user hosts file before starting the class. The file is provided in the DU named `hosts`. Before starting the class you have to set an environment variable.

```
export HOSTALIASES=<DU_path>/hosts
```
