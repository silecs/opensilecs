
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _AllTypesFESA_StatusUpdateAction_H_
#define _AllTypesFESA_StatusUpdateAction_H_

#include <AllTypesFESA/GeneratedCode/Device.h>
#include <AllTypesFESA/GeneratedCode/GenRTActions.h>

namespace AllTypesFESA
{

class StatusUpdateAction : public StatusUpdateActionBase
{
public:
    StatusUpdateAction (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~StatusUpdateAction();
    void execute(fesa::RTEvent* pEvt);
};

} // AllTypesFESA

#endif // _AllTypesFESA_StatusUpdateAction_H_
