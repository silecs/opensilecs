
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _AllTypesFESA_RT_DEVICE_CLASS_H_
#define _AllTypesFESA_RT_DEVICE_CLASS_H_

#include <AllTypesFESA/GeneratedCode/RTDeviceClassGen.h>

namespace AllTypesFESA
{

class RTDeviceClass: public RTDeviceClassGen
{
public:
    static RTDeviceClass* getInstance();
    static void releaseInstance();
    void specificInit();
    void specificShutDown();
private:
    RTDeviceClass();
    virtual ~RTDeviceClass();
    static RTDeviceClass* instance_;
};

} // AllTypesFESA

#endif // _AllTypesFESA_RT_DEVICE_CLASS_H_
