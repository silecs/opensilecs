
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <AllTypesFESA/RealTime/RTDeviceClass.h>
#include <AllTypesFESA/GeneratedCode/ServiceLocator.h>
#include <fesa-core/Core/AbstractEvent.h>

#include <AllTypesFESA/Common/AllTypesFESA.h>

#include <cmw-log/Logger.h>

#include <string>
#include <vector>
#include <unordered_set>

namespace 
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.AllTypesFESA.RealTime.RTDeviceClass");

} // namespace

namespace AllTypesFESA
{

RTDeviceClass* RTDeviceClass::instance_ = NULL;

RTDeviceClass::RTDeviceClass () :
    RTDeviceClassGen()
{
}

RTDeviceClass::~RTDeviceClass()
{
    AllTypesFESA::cleanup();
}

RTDeviceClass* RTDeviceClass::getInstance()
{
    if (instance_ == NULL)
    {
        instance_ = new RTDeviceClass();
    }
    return instance_;
}

void RTDeviceClass::releaseInstance()
{
    if (instance_ != NULL) 
    {
        delete instance_;
        instance_ = NULL;
    }
}

// This method is called when the FESA class starts up.
// You can write code that initializes devices in the loop below.
void RTDeviceClass::specificInit()
{
    try 
    {
        if (!AllTypesFESA::isInitialized())
        {
            try
            {
                AllTypesFESA::setup(this->AllTypesFESAServiceLocator_);
            }
            catch(std::exception& e) 
            { 
                std::string message = "Exception while initializing SILECS " + std::string(e.what());
                logger.warning(message);

                const Devices& devices = AllTypesFESAServiceLocator_->getDeviceCollection();
                std::unordered_set<std::string> uniqueHostnames;
                for (Device* device : devices)
                {
                    uniqueHostnames.insert(device->plcHostName.get());
                }

                // Since SILECS generated code stops connecting to PLCs if only one of them is not available,
                // we try and connect to all of them ourselves to make sure that all the available PLCs are connected.
                for (const std::string& plc : uniqueHostnames)
                {
                    try
                    {
                        Silecs::PLC& pPLC = Silecs::Service::getInstance()->getPLCHandler().getPLC(plc);

                        if (!pPLC.isEnabled())
                        {
                            pPLC.connect(true);
                        }
                    }
                    catch(const Silecs::SilecsException& e)
                    {
                        logger.debug(e.getMessage());
                    }
                }
            }
        }
    } 
    catch(std::exception& ex) 
    { 
        // required to trace failures on logstash.acc.gsi.de 
        LOG_ERROR_IF(logger, ex.what()); 
        throw; 
    } 
    catch(...) 
    { 
        LOG_ERROR_IF(logger, "Unexpected error. Please notify support"); 
        throw;     
    }
}

void RTDeviceClass::specificShutDown()
{
    // This method is executed just before a normal shut down of the process.
}

} // AllTypesFESA
