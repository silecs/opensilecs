
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <AllTypesFESA/GeneratedCode/ServiceLocator.h>
#include <AllTypesFESA/RealTime/StatusUpdateAction.h>

#include <AllTypesFESA/Common/AllTypesFESA.h>

#include <cmw-log/Logger.h>

namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.AllTypesFESA.RealTime.StatusUpdateAction");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "AllTypesFESA"; \
    diagMsg.name = "StatusUpdateAction"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    AllTypesFESAServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace AllTypesFESA
{

StatusUpdateAction::StatusUpdateAction(fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
     StatusUpdateActionBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses) 
{
}

StatusUpdateAction::~StatusUpdateAction()
{
}

// This method is called whenever an event triggers this action; this is where custom code must be written.
// A real time action operates on a device collection, this is why a loop which iterates over the device collection
// of this real time action is there.
void StatusUpdateAction::execute(fesa::RTEvent* pEvt)
{
    // We don't need the status action, however it's left here for compliance with the FESA GSI template.

    // We reuse this action to retrieve the RO block.
    AllTypesFESA::MyROBlock.getAllDevices(AllTypesFESAServiceLocator_, true, pEvt->getMultiplexingContext());
}

} // AllTypesFESA
