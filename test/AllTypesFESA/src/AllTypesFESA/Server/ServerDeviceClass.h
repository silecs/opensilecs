
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _AllTypesFESA_SERVER_DEVICE_CLASS_H_
#define _AllTypesFESA_SERVER_DEVICE_CLASS_H_

#include <AllTypesFESA/GeneratedCode/ServerDeviceClassGen.h>

namespace AllTypesFESA
{

class ServerDeviceClass: public ServerDeviceClassGen
{
    public:
        static ServerDeviceClass* getInstance();
        static void releaseInstance();
        void specificInit();
        void specificShutDown();
    private:
        ServerDeviceClass();
        virtual  ~ServerDeviceClass();
        static ServerDeviceClass* instance_;
};

} // AllTypesFESA

#endif // _AllTypesFESA_SERVER_DEVICE_CLASS_H_
