
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <AllTypesFESA/Server/GetMyROBlockProp.h>
#include <AllTypesFESA/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

namespace 
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.AllTypesFESA.Server.GetMyROBlockProp");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "AllTypesFESA"; \
    diagMsg.name = "GetMyROBlockProp"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    AllTypesFESAServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace AllTypesFESA
{

GetMyROBlockProp::GetMyROBlockProp(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
        GetMyROBlockPropBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetMyROBlockProp::~GetMyROBlockProp()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that aggregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter input parameter meant to fine tune the treatment.
 */
void GetMyROBlockProp::execute(fesa::RequestEvent* pEvt, Device* pDev, MyROBlockPropPropertyData& data, const MyROBlockPropFilterData& filter)
{
    // Read only blocks don't (yet) support server side getOneDevice functions. The data is retrieved in a Real-time action.
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetMyROBlockProp::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const MyROBlockPropFilterData& filter) const
{
    //Device& device = static_cast<Device&>(abstractDevice);
    return true;
}

// This method is called when a new subscription is requested. Returning false will abort the subscription.
bool GetMyROBlockProp::isFilterValid(const fesa::AbstractDevice& abstractDevice, const MyROBlockPropFilterData& filter) const
{
    //Device& device = static_cast<Device&>(abstractDevice);
    return true;
}

} // AllTypesFESA
