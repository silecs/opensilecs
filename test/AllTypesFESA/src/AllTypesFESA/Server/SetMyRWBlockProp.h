
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _AllTypesFESA_SetMyRWBlockProp_H_
#define _AllTypesFESA_SetMyRWBlockProp_H_

#include <AllTypesFESA/GeneratedCode/GetSetDefaultServerAction.h>
#include <AllTypesFESA/GeneratedCode/PropertyData.h>
#include <AllTypesFESA/GeneratedCode/TypeDefinition.h>
#include <AllTypesFESA/GeneratedCode/Device.h>

namespace AllTypesFESA
{

class SetMyRWBlockProp : public SetMyRWBlockPropBase
{
public:
    SetMyRWBlockProp(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~SetMyRWBlockProp();
    void execute(fesa::RequestEvent* pEvt, Device* pDev, const MyRWBlockPropPropertyData& data, const MyRWBlockPropFilterData& filter);
};

} // AllTypesFESA

#endif // _AllTypesFESA_SetMyRWBlockProp_H_
