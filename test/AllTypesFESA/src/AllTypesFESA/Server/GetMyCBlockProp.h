
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _AllTypesFESA_GetMyCBlockProp_H_
#define _AllTypesFESA_GetMyCBlockProp_H_

#include <AllTypesFESA/GeneratedCode/GetSetDefaultServerAction.h>
#include <AllTypesFESA/GeneratedCode/PropertyData.h>
#include <AllTypesFESA/GeneratedCode/TypeDefinition.h>
#include <AllTypesFESA/GeneratedCode/Device.h>

namespace AllTypesFESA
{

class GetMyCBlockProp : public GetMyCBlockPropBase
{
public:
    GetMyCBlockProp(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~GetMyCBlockProp();
    void execute(fesa::RequestEvent* pEvt, Device* pDev, MyCBlockPropPropertyData& data, const MyCBlockPropFilterData& filter);
    bool hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const MyCBlockPropFilterData& filter) const;
    bool isFilterValid(const fesa::AbstractDevice& abstractDevice, const MyCBlockPropFilterData& filter) const;
};

} // AllTypesFESA

#endif // _AllTypesFESA_GetMyCBlockProp_H_
