
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _AllTypesFESA_GetMyRWBlockProp_H_
#define _AllTypesFESA_GetMyRWBlockProp_H_

#include <AllTypesFESA/GeneratedCode/GetSetDefaultServerAction.h>
#include <AllTypesFESA/GeneratedCode/PropertyData.h>
#include <AllTypesFESA/GeneratedCode/TypeDefinition.h>
#include <AllTypesFESA/GeneratedCode/Device.h>

namespace AllTypesFESA
{

class GetMyRWBlockProp : public GetMyRWBlockPropBase
{
public:
    GetMyRWBlockProp(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~GetMyRWBlockProp();
    void execute(fesa::RequestEvent* pEvt, Device* pDev, MyRWBlockPropPropertyData& data, const MyRWBlockPropFilterData& filter);
    bool hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const MyRWBlockPropFilterData& filter) const;
    bool isFilterValid(const fesa::AbstractDevice& abstractDevice, const MyRWBlockPropFilterData& filter) const;
};

} // AllTypesFESA

#endif // _AllTypesFESA_GetMyRWBlockProp_H_
