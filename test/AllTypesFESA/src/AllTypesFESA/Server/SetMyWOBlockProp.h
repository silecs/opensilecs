
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _AllTypesFESA_SetMyWOBlockProp_H_
#define _AllTypesFESA_SetMyWOBlockProp_H_

#include <AllTypesFESA/GeneratedCode/GetSetDefaultServerAction.h>
#include <AllTypesFESA/GeneratedCode/PropertyData.h>
#include <AllTypesFESA/GeneratedCode/TypeDefinition.h>
#include <AllTypesFESA/GeneratedCode/Device.h>

namespace AllTypesFESA
{

class SetMyWOBlockProp : public SetMyWOBlockPropBase
{
public:
    SetMyWOBlockProp(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~SetMyWOBlockProp();
    void execute(fesa::RequestEvent* pEvt, Device* pDev, const MyWOBlockPropPropertyData& data, const MyWOBlockPropFilterData& filter);
};

} // AllTypesFESA

#endif // _AllTypesFESA_SetMyWOBlockProp_H_
