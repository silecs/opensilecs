**Opensilecs** is a framwork which can be used to configure and access PLC systems via FESA.

- Opensilecs can generate a FESA design file and related c++ code for easy FESA integration
- Opensilecs can generate a PLC data model which can be uploaded to the PLC
- Opensilecs Provides a GUI to access PLC data in a easy way

Supported PLC vendors: Siemens, Beckhoff(untested), Schneider(untested)

## Getting Started

If you plan to just use the silecs framework, please check the [Opensilecs Wiki](https://www-acc.gsi.de/wiki/Frontend/SILECS) for hands-on courses and all required documentation.

## Build & Install

If you want to touch the code of silecs itself, you can build it using CMake.
There are three scripts available `configure.sh`, `build.sh` and `install.sh`. To setup the CMake build environment use `configure.sh`. The script accepts a few options (use `configure.sh -h` to see available options)
with which the release path, target and the build type can be configured. Once the build is configured
use `build.sh` to compile all the projects and `install.sh` to install them to the release path you
provided to `configure.sh`. In case no release path is provided the project will be released/installed
to the build directory. You can optionally specify the build target to `build.sh` and `install.sh`
scripts which can be used to build with yocto SDK.

Note: when using the option `--target yocto` a different build directory is used `build-yocto`.
This means that for a full yocto build you need to provide the `--target yocto` option to all three scripts (`configure.sh`, `build.sh` and `install.sh`).

Please open issues / merge requests in order to upstream your changes. ([GSI account required](https://www-oracle.gsi.de/pls/gsi/a_admin.acc_antrag))

## History

The GSI Version of Silecs is a fork of the CERN Silecs version. It was forked 2018 and has evolved into a slightly different framework. The CERN Version of Silecs is difficult to access. It only can be viewed from within a CERN machine(proxy) while having a valid CERN-account. Here a link to the [CERN Silecs Wiki](https://wikis.cern.ch/display/SIL/SILECs+Home).

## License

Licensed under the GNU GENERAL PUBLIC LICENSE Version 3. See the [LICENSE file][license] for details.

[license]: LICENSE
[GSI_Wiki]: https://www-acc.gsi.de/wiki/Frontend/SILECS
