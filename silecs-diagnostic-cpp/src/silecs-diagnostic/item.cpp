/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-diagnostic/item.h>


Item::Item(void* linkedObject)
{
    this->linkedObject    = linkedObject;
}

void Item::setLinkedObject(void* linkedObject)
{
    this->linkedObject    = linkedObject;
}

void* Item::getLinkedObject()
{
    return this->linkedObject;
}

