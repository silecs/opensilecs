/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/

#include <QtCore/QDebug>

#include <silecs-diagnostic/silecsmodule.h>
#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/utility/StringUtilities.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsDesign.h>

extern std::string UserName;


silecsModule::silecsModule(const std::string& logTopics, QTextBrowser *messageConsole):
		messageConsole_(messageConsole), debugLoggingEnabled_(false)
{
    silecsService = NULL;
    counterConnectedPLC = 0;
    metadata.design = NULL;
    metadata.plcHandler = NULL;

    if(logTopics.length() == 0)
    {
    	Utils::logInfo(messageConsole_,"no log topics defined");
        silecsService  = Silecs::Service::getInstance(0, NULL);
    }
    else
    {
    	Utils::logInfo(messageConsole_,"the following log topics will be used: " + logTopics);
        char *a = new char[logTopics.size()+1];
        a[logTopics.size()]=0;
        memcpy(a,logTopics.c_str(),logTopics.size());
        char *argv[2] = { (char *)"-plcLog", a };
        silecsService  = Silecs::Service::getInstance(2,argv);
    }
}

silecsModule::~silecsModule()
{
    silecsService->deleteInstance();
    silecsService = NULL;
    Utils::logInfo(messageConsole_,"silecsModule destructor executed");
}

void silecsModule::setLogTopics(const std::string& logTopics)
{
    if(silecsService != NULL)
    {
        if(!silecsService->setLogTopics(logTopics))
        	Utils::logError(messageConsole_,"Error while setting log topics ");
    }
}

bool silecsModule::isDebugLogEnabled()
{
	return debugLoggingEnabled_;
}

void silecsModule::enableDebugLog()
{
	debugLoggingEnabled_ = true;
}

void silecsModule::updatePLCRunState(Item *plcItem)
{
	int index = 1;
	try
	{
		Silecs::PLC* plc = (Silecs::PLC*)(plcItem->getLinkedObject());
		if( !plc->isConnected() )
		{
			plcItem->setText(index,"");
			plcItem->setBackground(index, Qt::white);
		}
		if( plc->isRunning() )
		{
			plcItem->setText(index,"running");
			plcItem->setBackground(index, Qt::green);
		}
		else
		{
			plcItem->setText(index,"stopped");
			plcItem->setBackground(index, Qt::yellow);
		}
	}
	catch(const Silecs::SilecsException& ex2)
	{
		plcItem->setText(index,"state unknown");
		plcItem->setBackground(index, Qt::red);
		std::string message = "Failed to obtain plc run-state: ";
		message +=  ex2.what();
		Utils::logError(messageConsole_,message);
	}

}

Item *silecsModule::generateTree(const std::string& className, const std::string& deployFile)
{
    Utils::logInfo(messageConsole_,"Loading deploy file: " + deployFile);
    XMLParser parserDeploy(deployFile,true);

    Item *root =  new Item(&metadata);
    auto& plcHandler = silecsService->getPLCHandler();
    metadata.plcHandler = &plcHandler;
    if( className == HEADER_NAME )
    {
        root->setText(0,QString::fromStdString(HEADER_NAME+" v"+HEADER_VERSION));
    }
    else
    {
        ElementXML silecsDesign = parserDeploy.getFirstElementFromXPath("/SILECS-Deploy/SilecsDesign[@silecs-design-name='" + className + "']");
        std::string classVersion = silecsDesign.getAttribute("silecs-design-version");
        root->setText(0,QString::fromStdString(className+" v"+classVersion));
    }
    root->setWhatsThis(0,QString::fromStdString(CLUSTER_TYPE));

    ElementXML deployUnitNode = parserDeploy.getFirstElementFromXPath("/SILECS-Deploy/Deploy-Unit");
    std::string deployName = deployUnitNode.getAttribute("name");
    std::size_t deployFolderPos = deployFile.find(deployName);
    std::string deployProjectPath = deployFile.substr(0,deployFolderPos) + deployName;
    std::vector<ElementXML> controllerNodes = parserDeploy.getElementsFromXPath("/SILECS-Deploy/Controller");
    for(auto controllerIter = controllerNodes.begin(); controllerIter != controllerNodes.end(); controllerIter++)
    {
        std::string plcName = controllerIter->getAttribute("host-name");
        Silecs::PLC *plc;
        std::string parameterFile = "";
        try
        {
            parameterFile = deployProjectPath + "/generated-silecs/client/" + plcName + ".silecsparam";
            Utils::logInfo(messageConsole_,"Loading parameter file: " + parameterFile);
            plc = &plcHandler.getPLC(plcName, parameterFile);
        }
        catch(const Silecs::SilecsException& ex2)
        {
            Utils::logError(messageConsole_,"Error while loading "+ plcName+". "+ ex2.what());
            continue;
        }

        auto& designs = plc->getDeploy().getDesigns();
        for (auto& design : designs)
        {
            if (design.getName() == className)
            {
                metadata.design = &plc->getDeploy().getDesign(className);
            }
        }

        // add plc on the tree
        Item *plcItem = Utils::addTreeItem(root,QString::fromStdString(plcName),"",QString::fromStdString(PLC_TYPE),plc,":/Images/PLC.png");

        std::vector<ElementXML> instances;
        XMLParser parseParam(parameterFile,true);
        try
        {
            instances = parseParam.getElementsFromXPath("/SILECS-Param/SILECS-Mapping/SILECS-Class[@name='" + className + "']/Instance");
        }
        catch(const Silecs::SilecsException& ex2)
        {
            Utils::logError(messageConsole_,"Failed to fetch instances for class '" + className + "' from parameter-file '" + parameterFile + "':" + ex2.what());
            continue;;
        }

        for(auto pInstanceIter = instances.begin(); pInstanceIter != instances.end(); ++pInstanceIter)
        {
            std::string deviceName = pInstanceIter->getAttribute("label");
            Utils::logInfo(messageConsole_,"found device: '" + deviceName + "' in parameter-file");
            Silecs::Device *device = plc->getDeploy().getDesign(className).getDevice(deviceName);

            // add device on the tree
            Item *deviceItem = Utils::addTreeItem(plcItem,QString::fromStdString(deviceName),"",QString::fromStdString(DEVICE_TYPE),device,":/Images/DEV.png");

            // get register List for the current device
            std::string registerList = device->getRegisterList();
            std::istringstream registerListSplitted(registerList);

            do
            {
                std::string registerName;
                registerListSplitted >> registerName;

                //avoid last empty register
                if(registerName.compare("")==0) break;

                Utils::logInfo(messageConsole_,"found Register: '" + registerName + "' in parameter file");
                auto& reg = device->getRegister(registerName);

                Item *registerItem = Utils::addTreeItem(deviceItem,QString::fromStdString(registerName),"",QString::fromStdString(REGISTER_TYPE),reg.get(),":/Images/REG.png" );

                // Set the block name
                registerItem->setText(1 , QString::fromStdString(reg->getBlockName()));

                // Color background of input and output buffer
                registerItem->setBackground(2, QColor(255,255,204));//light yellow
                registerItem->setBackground(3, QColor(204,255,255));//light blue

            }while(registerListSplitted);
        }
    } // END OF PLC LOOP

    root->setLinkedObject(&metadata);

    // Reset the number of connected PLC
    this->counterConnectedPLC =0;
    return root;
}


void silecsModule::setScalarDataInDeviceFromItem(Item *currentItem, const std::string& blockName)
{
    Silecs::Device* device = (Silecs::Device*)(currentItem->getLinkedObject());

    // PLC and device name just for error messages
    std::string plcName = ((Silecs::PLC*)dynamic_cast<Item*>(currentItem->parent())->getLinkedObject())->getName();
    std::string deviceName = device->getLabel();

    std::string registerList = device->getRegisterList();
    std::istringstream registerListSplitted(registerList);

    int registerIndex = -1;
    do
    {
        registerIndex++;
        std::string registerName;
        registerListSplitted >> registerName;

        qDebug() << "get Register"<<QString::fromStdString(registerName);
        //avoid last empty register

        if(registerName.compare("")==0) break;

        auto& reg = device->getRegister(registerName);

        // analyse just the register within the selected block
        if(reg->getBlockName().compare(blockName)!=0)
        {
            qDebug() << "Continue";
            continue;
        }

        std::string stringValue = currentItem->child(registerIndex)->text(3).toStdString();
        if(reg->isScalar()) //TODO: What happens for non-scalar values ?
            reg->setScalarFromString(stringValue);

    }while(registerListSplitted);
}

void silecsModule::setArrayRegValue(Item *currentItem, const std::vector<QString>& dataVector)
{
    Silecs::Register* reg = (Silecs::Register*)currentItem->getLinkedObject();

    std::vector<std::string> data;
    for (auto& s : dataVector)
    {
        data.push_back(s.toStdString());
    }
    
    reg->setArrayFromString(data);
}

void silecsModule::updateClusterItem(Item *Cluster,bool updateInputBufferOnly)
{
    int numberOfPLC = Cluster->childCount();
    for(int PLCIndex=0; PLCIndex < numberOfPLC;PLCIndex++)
    {
        this->updatePLCItem(dynamic_cast<Item*>(Cluster->child(PLCIndex)),updateInputBufferOnly);
    }
}


void silecsModule::updatePLCItem(Item *PLCItem,bool updateInputBufferOnly)
{
	updatePLCRunState(PLCItem);
	Silecs::PLC* plc = (Silecs::PLC*)(PLCItem->getLinkedObject());
	Utils::logInfo(messageConsole_,std::string("updating controller: '") + plc->getName() + "'");
    int numberOfdevice = PLCItem->childCount();
    for(int deviceIndex=0; deviceIndex<numberOfdevice;deviceIndex++)
    {
        this->updateDeviceItem(dynamic_cast<Item*>(PLCItem->child(deviceIndex)),updateInputBufferOnly);
    }
}


void silecsModule::updateDeviceItem(Item *deviceItem,bool updateInputBufferOnly)
{
    Silecs::Device *device = (Silecs::Device*)deviceItem->getLinkedObject();
    Utils::logInfo(messageConsole_,std::string("updating device: '") + device-> getLabel() + "'");

    int numberOfRegisters = deviceItem->childCount();
    for(int regIndex=0;regIndex<numberOfRegisters;regIndex++)
    {
        // Get the register Item in the tree view
        Item *regItem = dynamic_cast<Item*>(deviceItem->child(regIndex));
        auto& reg = device->getRegister(regItem->text(0).toStdString());
        Utils::logInfo(messageConsole_,"updating register: '" + reg->getName() + "'of device: '" + device-> getLabel() + "'" );

        // PLC values
        try
        {
            if(reg->isReadable())
            {
                if(reg->isScalar())
                {
                    // Display the value for scalar
                    regItem->setText(2 , QString::fromStdString(reg->getInputValAsString()));
                }
                else
                {
                    // Display [...] for arrays
                    regItem->setText(2 , "[...]");
                }
            }
            else // is WRITE only for silecs
            {
                regItem->setText(2 , "WRITE-ONLY block");
            }
        }
        catch (std::exception& ex)
        {
            regItem->setText(2 , "?");
            std::ostringstream message;
            message << "Impossible to read the input buffer value of - '" << regItem->text(0).toStdString() << "'";
            Utils::logError(messageConsole_,message.str());
        }

        if(!updateInputBufferOnly)
        {
            // local values
            try{
                if(reg->isWritable()) // = is WRITE or READ+WRITE for silecs
                {
                    if(reg->isScalar())
                    {
                        // Display the value for scalar
                        regItem->setText(3 , QString::fromStdString(reg->getOutputValAsString()));
                    }
                    else
                    {
                        // Display [...] for arrays
                        regItem->setText(3 , "[...]");
                    }
                    // Output for type date not supported
                    if(reg->getFormat()==Date)
                    {
                        regItem->setText(3 , "Date format not supported.");
                    }
                }
                else // = is READ only for silecs
                {
                    regItem->setText(3 , "READ-ONLY block");
                }

            }
            catch (std::exception& ex)
            {
                regItem->setText(3 , "?");
                std::ostringstream message;
                message << "Impossible to read the output buffer value of - '" << regItem->text(0).toStdString() << "'";
                Utils::logError(messageConsole_, message.str());
            }
        }
    }
}

