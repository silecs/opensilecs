/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <QtCore/QDebug>
#include <QtCore/QElapsedTimer>
#include <QtCore/QTimer>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMenu>

#include <silecs-diagnostic/diagnostictoolmainview.h>
#include <resources/gui/ui_diagnostictoolmainview.h>
#include <silecs-diagnostic/utils.h>

#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsDesign.h>

#include <exception>
extern silecsModule *mysilecs;
extern std::string UserName;

diagnosticToolMainView::diagnosticToolMainView(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::diagnosticToolMainView)
{
    ui->setupUi(this);

    this->redirector = NULL;
    mysilecs = NULL;

    // Set tree widget coloumn and headers
    ui->treeWidget->setColumnCount(3);
    QStringList headers;
    headers << "Resource name"<< "Block name" << "PLC Value" << "Local Value";
    ui->treeWidget->setHeaderLabels(headers);

    // Create context menu to let the user the posibility of cleaning the console
    ui->console->setReadOnly(true);
    ui->InformationMessage->setReadOnly(true);
    connect(ui->console, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(ctxMenu(const QPoint &)));

    //set vertical/horizontal splitter
    ui->verticalSplitter->setStretchFactor(1,1);
    ui->verticalSplitter->setStretchFactor(0,0);
}

diagnosticToolMainView::~diagnosticToolMainView()
{
    // If closing application without closing cluster
    if(redirector!=NULL) delete redirector;

    delete ui;
}

std::string diagnosticToolMainView::generateLogTopics()
{
    // generate string of log parameters
    std::ostringstream logTopics;
    if(ui->checkERROR->isChecked()) logTopics << "ERROR";
    if(ui->checkDEBUG->isChecked()) logTopics << ",DEBUG";
    if(ui->checkSETUP->isChecked()) logTopics << ",SETUP";
    if(ui->checkALLOC->isChecked()) logTopics << ",ALLOC";
    if(ui->checkLOCK->isChecked())  logTopics << ",LOCK";
    if(ui->checkCOMM->isChecked())  logTopics << ",COMM";
    if(ui->checkSEND->isChecked())  logTopics << ",SEND";
    if(ui->checkRECV->isChecked())  logTopics << ",RECV";
    if(ui->checkDATA->isChecked())  logTopics << ",DATA";
    if(ui->checkDIAG->isChecked())  logTopics << ",DIAG";
    return logTopics.str();
}

void diagnosticToolMainView::resetGUI()
{
    try
    {
        //Stop periodic receiving if any
        if(ui->periodicReceiveButton->isChecked()==true)
        {
            ui->periodicReceiveButton->setChecked(false);
            Utils::logInfo(ui->console,"Monitoring was stopped due to cluster closure.");
        }
        ui->sendButton->setEnabled(false);
        ui->receiveButton->setEnabled(false);
        ui->periodicReceiveButton->setEnabled(false);
        ui->copyButton->setEnabled(false);
        //Clear the tree
        ui->treeWidget->clear();

        ui->sendComboBox->clear();
        ui->receiveComboBox->clear();
        ui->copyComboBox->clear();
        // disable connect and disconnect
        ui->connectButton->setEnabled(false);
        ui->disconnectButton->setEnabled(false);

        if(mysilecs!=NULL)
        {
            delete mysilecs;
            mysilecs = NULL;
        }

        if(redirector!=NULL)
        {
            delete redirector;
            redirector = NULL;
        }
    }
    catch(std::string *str)
    {
        Utils::logError(ui->console,*str);
    }
    catch(std::exception& ex)
    {
        Utils::logError(ui->console,ex.what());
    }
    catch(...)
    {
        Utils::logError(ui->console,"Unexpected error while reseting GUI. Please notify SILECS support");
    }
}


void diagnosticToolMainView::loadFiles()
{
    resetGUI();
    try
    {
        this->redirector = new StdErrRedirect(ui->console);
        mysilecs = new silecsModule(generateLogTopics(), ui->console);

        ui->treeWidget->clear();
        Item * root = mysilecs->generateTree(designName,deployFile);

        if(root == NULL)
        {
            Utils::logError(ui->console,"Error while opening the design/deploy");
        }
        else
        {
            ui->treeWidget->addTopLevelItem(root);
            /*
             * Create Block List for combo boxes
             */
            Item* topLevelItem = dynamic_cast<Item*>(ui->treeWidget->topLevelItem(0));
            ModuleMetadata* meta = (ModuleMetadata*)topLevelItem->getLinkedObject();
            if (meta->design == nullptr)
            {
                Utils::logError(ui->console,"Design pointer is null.");
                return;
            }
            
            std::string writeBlockList = meta->design->getBlockList(AccessType::Command)+" "+ meta->design->getBlockList(AccessType::Setting);

            std::istringstream blockListSplitted(writeBlockList);
            Utils::logInfo(ui->console, writeBlockList);
            do
            {
                std::string blockName;
                blockListSplitted >> blockName;
                // avoid last empty block
                if(!blockName.compare("")) break;

                ui->sendComboBox->addItem(QString::fromStdString(blockName));
            }while(blockListSplitted);

            // Receive combo box
            std::string readBlockList = meta->design->getBlockList(AccessType::Acquisition)+" "+ meta->design->getBlockList(AccessType::Setting);
            std::istringstream blockListSplitted2(readBlockList);
            Utils::logInfo(ui->console,readBlockList);
            do
            {
                std::string blockName;
                blockListSplitted2 >> blockName;
                // avoid last empty block
                if(!blockName.compare("")) break;

                ui->receiveComboBox->addItem(QString::fromStdString(blockName));
            }while(blockListSplitted2);

            // Send combo box
            std::string copyBlockList = meta->design->getBlockList(AccessType::Setting);
            std::istringstream blockListSplitted3(copyBlockList);
            Utils::logInfo(ui->console, copyBlockList);
            do
            {
                std::string blockName;
                blockListSplitted3 >> blockName;
                // avoid last empty block
                if(!blockName.compare("")) break;

                ui->copyComboBox->addItem(QString::fromStdString(blockName));
            }while(blockListSplitted3);
        }
    }
    catch (const Silecs::SilecsException& ex)
    {
        Utils::logError(ui->console,  ex.what());
    }
    catch(std::string *str)
    {
        Utils::logError(ui->console,*str);
    }
    catch(std::exception& ex)
    {
        Utils::logError(ui->console,ex.what());
    }
    catch (...) //catch any exception
    {
    	Utils::logError(ui->console,"Error while opening the specified Class. Please contact SILECS support");
    }
}

void diagnosticToolMainView::on_connectButton_clicked()
{
    ui->console->clear();

    // Connect button

    // Get current Item
    Item *currentItem = dynamic_cast<Item*>(ui->treeWidget->currentItem());

    // Cast the linked object PLC type
    Silecs::PLC* plc = (Silecs::PLC*)(currentItem->getLinkedObject());

    try
    {
        plc->connect(true,ui->compareChecksums->isChecked());
        if(!plc->isConnected())
        {
            Utils::logError(ui->console,"Connection to PLC failed.");
            return;
        }
        mysilecs->updatePLCItem(currentItem,false);
        if(!plc->isConnected())
        {
            Utils::logError(ui->console,"Updating PLC-items failed.");
            return;
        }

        // disable connect button
        ui->connectButton->setEnabled(false);
        ui->disconnectButton->setEnabled(true);

        // enable send and receive
        ui->sendBox->setEnabled(true);
        ui->receiveBox->setEnabled(true);
        ui->copyBox->setEnabled(true);

        //refresh console
        Utils::displayPLCInformation(plc,ui->InformationMessage);

        //highlight PLC background in green
        currentItem->setBackground(0, Qt::green);

        //show the run-state of the PLC
        mysilecs->updatePLCRunState(currentItem);

        // Increment number of connected PLC
        mysilecs->counterConnectedPLC++;
    }
    catch(std::string *str)
    {
        Utils::logError(ui->console,*str);
    }
    catch(std::exception& ex)
    {
        Utils::logError(ui->console,ex.what());
    }
	catch(...)
	{
		Utils::logError(ui->console,"Unknown error while connecting to the selected PLC.");
	}

}

void diagnosticToolMainView::on_disconnectButton_clicked()
{
	ui->console->clear();

    // Disconnect button
    try
    {
        // Get current Item
        Item *currentItem = dynamic_cast<Item*>(ui->treeWidget->currentItem());

        // Cast the linked object PLC type
        Silecs::PLC* plc = (Silecs::PLC*)(currentItem->getLinkedObject());

        //update the runstate
		mysilecs->updatePLCRunState(currentItem);

        // Disconnect
        plc->disconnect();

        // disable disconnect button
        ui->disconnectButton->setEnabled(false);
        ui->connectButton->setEnabled(true);

        // disable send and receive
        ui->sendBox->setEnabled(false);
        ui->receiveBox->setEnabled(false);
        ui->copyBox->setEnabled(false);

        // refresh console
        Utils::displayPLCInformation(plc,ui->InformationMessage);

        // remove green background
        ui->treeWidget->currentItem()->setBackground(0, Qt::white);

        // Decrement number of connected PLC
        mysilecs->counterConnectedPLC--;
        if(mysilecs->counterConnectedPLC==0)
        {
            // if there are no more plc connected stop monitoring
            if(ui->periodicReceiveButton->isChecked()==true)
            {
                ui->periodicReceiveButton->setChecked(false);
                Utils::logInfo(ui->console,"Monitoring was stopped because no more PLC are connected.");
            }
        }
    }
    catch(std::string *str)
    {
        Utils::logError(ui->console,*str);
    }
    catch(std::exception& ex)
    {
        Utils::logError(ui->console,ex.what());
    }
    catch(...)
    {
        Utils::logError(ui->console,"Unknown Error while disconnecting to the selected PLC.");
    }
}

void diagnosticToolMainView::on_treeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
    ui->console->clear();
    (void)current; // to suppress unused warning
    (void)previous; // to suppress unused warning
    try
    {
        if(current == NULL)
        {
            qDebug()<< "Current item does not exist. probably the tree has bean cleared";
            ui->InformationMessage->clear();
            ui->treeWidget->resizeColumnToContents(0);
            return;
        }

        Item *currentItem = dynamic_cast<Item*>(current);

        /*
            * If the selected item is a Cluster
            */
        if(!currentItem->whatsThis(0).compare(QString::fromStdString(CLUSTER_TYPE)))
        {
            qDebug() << "Cluster selected";
            auto meta = ((ModuleMetadata*)(currentItem->getLinkedObject()));
            Utils::displayDesignInformation(meta->design->getName(),ui->InformationMessage);
        }

        /*
            * If the selected item is a PLC
            */
        if(!currentItem->whatsThis(0).compare(QString::fromStdString(PLC_TYPE)))
        {
            qDebug() << "Plc selected";
            Utils::displayPLCInformation(((Silecs::PLC*)(currentItem->getLinkedObject())),ui->InformationMessage);

            bool connected = ((Silecs::PLC*)(currentItem->getLinkedObject()))->isConnected();
            ui->connectButton->setEnabled(!connected);
            ui->disconnectButton->setEnabled(connected);
        }
        else
        {
            ui->connectButton->setEnabled(false);
            ui->disconnectButton->setEnabled(false);
        }

        /*
            * If the selected item is a device
            */
        if(!currentItem->whatsThis(0).compare(QString::fromStdString(DEVICE_TYPE)))
        {
            qDebug() << "Device selected";
            Silecs::Device* device = (Silecs::Device*)(currentItem->getLinkedObject());
            Utils::displayDeviceInformation(device,ui->InformationMessage);

            bool connected = ((Silecs::PLC*)dynamic_cast<Item*>(currentItem->parent())->getLinkedObject())->isConnected();

            ui->sendBox->setEnabled(connected);
            ui->receiveBox->setEnabled(connected);
            ui->copyBox->setEnabled(connected);
            ui->sendButton->setEnabled(connected);
            ui->receiveButton->setEnabled(connected);
            ui->copyButton->setEnabled(connected);
            ui->periodicReceiveButton->setEnabled(connected);
        }
        else
        {
            ui->sendBox->setEnabled(false);
            ui->receiveBox->setEnabled(false);
            ui->copyBox->setEnabled(false);
            ui->sendButton->setEnabled(false);
            ui->receiveButton->setEnabled(false);
            ui->copyButton->setEnabled(false);
            ui->periodicReceiveButton->setEnabled(false);
        }

        /*
            * If the selected item is a register
            */
        if(!currentItem->whatsThis(0).compare(QString::fromStdString(REGISTER_TYPE)))
        {
            qDebug() << "Register selected";
            Utils::displayRegisterInformation(((Silecs::Register*)(currentItem->getLinkedObject())),ui->InformationMessage);

            //bool connected = ((Silecs::PLC*)dynamic_cast<Item*>(currentItem->parent()->parent())->getLinkedObject())->isConnected();
            Utils::displayRegisterValue((Silecs::Register*)(currentItem->getLinkedObject()),
                                        ui->binValueLabel,
                                        ui->HexValueLabel,
                                        ui->DecValueLabel,
                                        ui->asciiValueLabel);
        }
        else
        {
            ui->binValueLabel->setText("--Not relevant--");
            ui->HexValueLabel->setText("--Not relevant--");
            ui->DecValueLabel->setText("--Not relevant--");
            ui->asciiValueLabel->setText("--Not relevant--");
        }
        // Automaticaly resize the coloumn with the item name
        ui->treeWidget->resizeColumnToContents(0);
    }
    catch(std::string *str)
    {
        Utils::logError(ui->console,*str);
    }
    catch(std::exception& ex)
    {
        Utils::logError(ui->console,ex.what());
    }
    catch(...)
    {
        Utils::logError(ui->console,"Unknown error in currentItemChanged. Please notify SILECS support");
    }
}

std::vector<QString> diagnosticToolMainView::openArrayDialogBase(Silecs::Register* reg, const std::vector<QString>& dataVector, bool localData)
{
    DisplayArrayDialog arrayDialog;
    arrayDialog.setDataVector(dataVector,localData,reg->getDimension2());
    arrayDialog.setWindowTitle("Array View");
    arrayDialog.setModal(true);
    if(arrayDialog.exec()==1) // ok pressed
    {
        return arrayDialog.getDataVector();
    }
    return dataVector;
}

std::vector<QString> diagnosticToolMainView::open1DArrayDialog(Silecs::Register* reg, bool localData)
{
    std::vector<QString> dataVector;
    for(unsigned long i=0;i<reg->getDimension1();i++)
    {
        if( localData )
            dataVector.push_back(QString::fromStdString(reg->getOutputValAsString(i)));
        else
            dataVector.push_back(QString::fromStdString(reg->getInputValAsString(i)));
    }

    return openArrayDialogBase(reg, dataVector, localData);
}

std::vector<QString> diagnosticToolMainView::open2DArrayDialog(Silecs::Register* reg, bool localData)
{
    std::vector<QString> dataVector;
    for(unsigned long i=0; i<reg->getDimension1(); i++)
    {
        for(unsigned long j=0; j<reg->getDimension2(); j++)
        {
            if( localData )
                dataVector.push_back(QString::fromStdString(reg->getOutputValAsString(i,j)));
            else
                dataVector.push_back(QString::fromStdString(reg->getInputValAsString(i,j)));
        }
    }
    return openArrayDialogBase(reg, dataVector, localData);
}

void diagnosticToolMainView::markItemNotEdiable(QTreeWidgetItem *item)
{
	item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
    qDebug()<< "item marked as not editable";
}

void diagnosticToolMainView::on_treeWidget_doubleClicked(const QModelIndex &index)
{
    ui->console->clear();
    (void)index; // to suppress unused warning
    try
    {
        QTreeWidgetItem *itm = ui->treeWidget->currentItem();
        if( itm->whatsThis(0).toStdString().compare(REGISTER_TYPE) != 0 )
        {
            markItemNotEdiable(itm);
            return;
        }
        Item *registerItem = dynamic_cast<Item*>(itm);
        Silecs::Register* reg = (Silecs::Register*)registerItem->getLinkedObject();

        switch(ui->treeWidget->currentColumn())
        {
            case 2:// double click performed on 2th column (PLC value) --> open non-editable ArrayDialog for arrays
                if(reg->getDimension2()<=1)  // scalar or 1d array (noting happens for scalar)
                {
                    if(reg->getDimension1()<=1) //scalar
                    {
                        return;
                    }
                    open1DArrayDialog(reg,false);
                }
                else    // 2d array
                {
                    open2DArrayDialog(reg,false);
                }
                markItemNotEdiable(itm);
                return;
            case 3:// double click performed on 3th column (local value)
                if(!reg->isWritable() )// is READ only for silecs or is wrong column
                {
                    markItemNotEdiable(itm);
                    return;
                }
                if(reg->getDimension2()<=1)  // scalar or 1d array
                {
                    if(reg->getDimension1()<=1) //scalar
                    {
                        itm->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsEditable);
                        return;
                    }
                    std::vector<QString> dataVector = open1DArrayDialog(reg,true);
                    mysilecs->setArrayRegValue(registerItem, dataVector);
                }
                else    // 2d array
                {
                    std::vector<QString> dataVector= open2DArrayDialog(reg,true);
                    mysilecs->setArrayRegValue(registerItem, dataVector);
                }
                return;
            default: // other columns are not editable
                markItemNotEdiable(itm);
                return;
        }
    }
    catch(std::string *str)
    {
        Utils::logError(ui->console,*str);
    }
    catch(std::exception& ex)
    {
        Utils::logError(ui->console,ex.what());
    }
    catch(...)
    {
        Utils::logError(ui->console,"Unknown error in treeWidget_doubleClicked. Please notify SILECS support");
    }
}

void diagnosticToolMainView::on_sendButton_clicked()
{
	ui->console->clear();
    try{
        Item *currentItem = dynamic_cast<Item*>(ui->treeWidget->currentItem());

        /*
         * If the selected item is a cluster
         */
        if(!currentItem->whatsThis(0).compare(QString::fromStdString(CLUSTER_TYPE)))
        {
            // Cast the linked object to cluster type
            ModuleMetadata* meta = (ModuleMetadata*)(currentItem->getLinkedObject());
            int numberOfPLC = currentItem->childCount();

            // Set all the PLC whithin the cluster
            for(int i=0;i<numberOfPLC;i++)
            {
                int numberOfdevice = currentItem->childCount();
                Item* plcItem = (Item*)currentItem->child(i);
                // Set all the device whithin the PLC
                for(int j=0;j<numberOfdevice;j++)
                {
                    Silecs::PLC* plc = (Silecs::PLC*)(plcItem->getLinkedObject());
                    if (plc->isConnected())
                        mysilecs->setScalarDataInDeviceFromItem((Item*)plcItem->child(i),ui->sendComboBox->currentText().toStdString());
                }
			}
			try{
				//send and mesure sending time
				QElapsedTimer mytimer;
				mytimer.start();
                meta->plcHandler->send(ui->sendComboBox->currentText().toStdString());
				logCommunicationSuccess(mytimer.elapsed());
			}
			catch(...)
			{
				qDebug()<<"Error while calling cluster.send()";
				Utils::logError(ui->console,"Error while sending the block to the selected cluster.");
			}
        }

        /*
         * If the selected item is a plc
         */
        if(!currentItem->whatsThis(0).compare(QString::fromStdString(PLC_TYPE)))
        {
            // Cast the linked object to PLC type
            Silecs::PLC* plc = (Silecs::PLC*)(currentItem->getLinkedObject());
            int numberOfdevice = currentItem->childCount();

            // Set all the device whithin the PLC
            for(int i=0;i<numberOfdevice;i++)
                mysilecs->setScalarDataInDeviceFromItem((Item*)currentItem->child(i),ui->sendComboBox->currentText().toStdString());
            try{
                //send and mesure sending time
                QElapsedTimer mytimer;
                mytimer.start();
                try
                {
                    plc->send(ui->sendComboBox->currentText().toStdString());
                }
                catch(const std::exception& e)
                {
                    Utils::logError(ui->console,"Error while receiving the block to the selected device. " + std::string(e.what()));
                	return;
                }
                logCommunicationSuccess(mytimer.elapsed());
            }
            catch(...)
            {
                qDebug()<<"Error while calling plc.send()";
                Utils::logError(ui->console,"Error while sending the block to the selected plc.");
            }
        }

        /*
         * If the selected item is a device
         */
        if(!currentItem->whatsThis(0).compare(QString::fromStdString(DEVICE_TYPE)))
        {
            try{
                mysilecs->setScalarDataInDeviceFromItem(currentItem,ui->sendComboBox->currentText().toStdString());
                Silecs::Device* device = (Silecs::Device*)(currentItem->getLinkedObject());

                //send and mesure sending time
                QElapsedTimer mytimer;
                mytimer.start();
                Utils::logInfo(ui->console,"Sending data to device: '" + device->getLabel() + "', block: '" + ui->sendComboBox->currentText().toStdString() + "'");
                try
                {
                    device->send(ui->sendComboBox->currentText().toStdString());
                }
                catch(const std::exception& e)
                {
                    Utils::logError(ui->console,"Error while receiving the block to the selected device. " + std::string(e.what()));
                	return;
                }
                logCommunicationSuccess( mytimer.elapsed());
            }
            catch(...)
            {
                qDebug()<<"Error while calling plc.send()";
                Utils::logError(ui->console,"Error while sending the block to the selected device.");
            }
        }
    }
    catch(std::string *str)
    {
        Utils::logError(ui->console,*str);
    }
    catch(std::exception& ex)
    {
        Utils::logError(ui->console,ex.what());
    }
    catch(...)
    {
        Utils::logError(ui->console,"Unexpected error while sending. Please notify SILECS support");
    }
}

void diagnosticToolMainView::on_receiveButton_clicked()
{
	ui->console->clear();
    // Receive Button
    try{
        Item *currentItem = dynamic_cast<Item*>(ui->treeWidget->currentItem());

        // block to be received
        std::string blockName = ui->receiveComboBox->currentText().toStdString();

    	if( currentItem->whatsThis(0).toStdString().compare(DEVICE_TYPE) != 0 ) // actually this should not be possible at all
    	{
    		Utils::logError(ui->console,"Please select a device");
    		return;
    	}
        this->receiveDevice(blockName,currentItem);
    }
    catch(std::string *str)
    {
        Utils::logError(ui->console,*str);
    }
    catch(std::exception& ex)
    {
        Utils::logError(ui->console,ex.what());
    }
    catch(...)
    {
        Utils::logError(ui->console,"Unknown error while receiving. Please notify SILECS support");
    }
}


void diagnosticToolMainView::on_classNameComboBox_currentIndexChanged(const QString &arg1)
{
    Q_UNUSED(arg1);
    ui->console->clear();
    if( !deployFile.empty() )
    {
        ui->DesignName->setText("");
        designName = ui->classNameComboBox->currentText().toStdString();
        loadFiles();
    }
    return;
}

void diagnosticToolMainView::on_loadDeployButton_clicked()
{
    ui->console->clear();
    deployFile = QFileDialog::getOpenFileName(this, tr("Deploy to Open"), "" ,tr("Silecs Deploy Files (*.silecsdeploy)")).toStdString();
    if( !deployFile.empty() )
    {
        ui->DeployName->setText(QString::fromStdString(deployFile));
        ui->DesignName->setText("pick a design");
        fillClassNameComboBox();
    }
    return;
}

void diagnosticToolMainView::setDeployFile(const std::string& file)
{
    deployFile = file;
    ui->DeployName->setText(QString::fromStdString(deployFile));
    ui->DesignName->setText("pick a design");
    fillClassNameComboBox();
}

void diagnosticToolMainView::fillClassNameComboBox()
{
    Utils::logInfo(ui->console,"Loading deploy file: " + deployFile);
    XMLParser parserDeploy(deployFile,true);
    std::vector<ElementXML> classNodes;
    try
    {
        classNodes = parserDeploy.getElementsFromXPath("/SILECS-Deploy/SilecsDesign");
    }
    catch(const Silecs::SilecsException& ex2)
    {
        Utils::logError(ui->console,"No classes found for this Deploy: " + deployFile + ". Exception: " + ex2.what());
        return;
    }

    QStringList list;
    for(auto classIter = classNodes.begin(); classIter != classNodes.end(); classIter++)
    {
        QString className(classIter->getAttribute("silecs-design-name").c_str());
        if(!list.contains(className))
            list += className;
    }
    QString headerName(HEADER_NAME.c_str());
    list += headerName;
    ui->classNameComboBox->clear();
    ui->classNameComboBox->addItems(list);
}

void diagnosticToolMainView::ctxMenu(const QPoint &pos)
{
    QMenu *menu = new QMenu;
    menu->addAction(tr("Clear information console"), this, SLOT(clearConsole_slot()));
    menu->exec(ui->console->mapToGlobal(pos));
}

void diagnosticToolMainView::clearConsole_slot()
{
    ui->console->clear();
}

void diagnosticToolMainView::on_treeWidget_itemExpanded(QTreeWidgetItem *item)
{
    (void)item;// to supress unused warning
    // Automaticaly resize the coloumn with the item name
    ui->treeWidget->resizeColumnToContents(0);
}

void diagnosticToolMainView::logBlockReceived(const std::string& blockName, qint64 elapsedTime)
{
    std::ostringstream message;
    message << "Block '" << blockName << "' received successfully from the device in " <<  elapsedTime << " milliseconds";
    Utils::logInfo(ui->console,message.str().c_str());
}

void diagnosticToolMainView::logCommunicationSuccess(qint64 elapsedTime)
{
    std::ostringstream message;
    message << "PLC communication successful. Elapsed time: " <<  elapsedTime << " milliseconds";
    Utils::logInfo(ui->console,message.str().c_str());
}


void diagnosticToolMainView::receiveDevice(const std::string& blockName, Item* deviceItem)
{
    Silecs::Device *device = (Silecs::Device*)deviceItem->getLinkedObject();
    qDebug() << "try to receive " << QString::fromStdString(blockName);
    try{
        //receive device and mesure sending time
        QElapsedTimer mytimer;
        mytimer.start();
        Utils::logInfo(ui->console,"Receiving data from device: '" + device->getLabel() + "', block: '" + blockName + "'");
        device->recv(blockName);
        qint64 elapsedTime = mytimer.elapsed();
        logBlockReceived(blockName, elapsedTime);
    }
    catch(std::string *str)
    {
        Utils::logError(ui->console,*str);
    }
    catch(std::exception& ex)
    {
        Utils::logError(ui->console,ex.what());
    }
    catch (...)
    {
    	Utils::logError(ui->console,"unknown error in diagnosticToolMainView::receiveDevice");
    }
    mysilecs->updateDeviceItem(deviceItem,true);
}

void diagnosticToolMainView::periodicReceiveDevice()
{
    Silecs::Device *device = (Silecs::Device*)currentItemForPeriodicReceive->getLinkedObject();
    qDebug()<< "try to receive "<< QString::fromStdString(blockNameForPeriodicReceive);
    try{
        //receive device and mesure sending time
        QElapsedTimer mytimer;
        mytimer.start();
        device->recv(blockNameForPeriodicReceive);
        qint64 elapsedTime = mytimer.elapsed();
        logBlockReceived(blockNameForPeriodicReceive, elapsedTime);
    }
    catch(std::string *str)
    {
    	Utils::logError(ui->console,"error while receiving block: " + blockNameForPeriodicReceive);
        Utils::logError(ui->console,*str);
    }
    catch(std::exception& ex)
    {
    	Utils::logError(ui->console,"error while receiving block: " + blockNameForPeriodicReceive);
        Utils::logError(ui->console,ex.what());
    }
    catch (...)
    {
    	Utils::logError(ui->console,"error while receiving block: " + blockNameForPeriodicReceive);
    	Utils::logError(ui->console,"unknown error in diagnosticToolMainView::periodicReceiveDevice");
    }
    mysilecs->updateDeviceItem(currentItemForPeriodicReceive,true);
}

void diagnosticToolMainView::on_periodicReceiveButton_toggled(bool checked)
{
    ui->console->clear();
    if(checked)
    {
        // receive periodic Button
        try{
            this->currentItemForPeriodicReceive = dynamic_cast<Item*>(ui->treeWidget->currentItem());

            // block to be received
            this->blockNameForPeriodicReceive = ui->receiveComboBox->currentText().toStdString();

            periodicReceiveTimer = new QTimer(this);
            /*
                 * If the selected item is a cluster
                 */
            if(!currentItemForPeriodicReceive->whatsThis(0).compare(QString::fromStdString(CLUSTER_TYPE)))
            {
                connect(periodicReceiveTimer,SIGNAL(timeout()),SLOT(periodicReceiveCluster()));
                ui->monitoringLabel->setText("<font color='blue'>"+ui->treeWidget->currentItem()->text(0)+"</font>");
            }

            /*
                 * If the selected item is a plc
                 */
            if(!currentItemForPeriodicReceive->whatsThis(0).compare(QString::fromStdString(PLC_TYPE)))
            {
                connect(periodicReceiveTimer,SIGNAL(timeout()),SLOT(periodicReceivePLC()));
                ui->monitoringLabel->setText("<font color='blue'>"+currentItemForPeriodicReceive->parent()->text(0)+"/"
                                             +ui->treeWidget->currentItem()->text(0)+"</font>");
            }

            /*
                 * If the selected item is a device
                 */
            if(!currentItemForPeriodicReceive->whatsThis(0).compare(QString::fromStdString(DEVICE_TYPE)))
            {
                connect(periodicReceiveTimer,SIGNAL(timeout()),SLOT(periodicReceiveDevice()));
                ui->monitoringLabel->setText("<font color='blue'>"+currentItemForPeriodicReceive->parent()->parent()->text(0)+"/"
                                             +currentItemForPeriodicReceive->parent()->text(0)+"/"
                                             +ui->treeWidget->currentItem()->text(0)+"</font>");
            }
            periodicReceiveTimer->start((int)(ui->timeSpinBox->value()*1000));
        }
        catch(...)
        {
            Utils::logError(ui->console,"Unexpected error while setting up the timed operation.");
            ui->periodicReceiveButton->setChecked(false);
        }
        ui->timeSpinBox->setEnabled(false);
    }
    else
    {
        // better checking if button was enabled because code is called as well
        // when a cluster is closed
        if(periodicReceiveTimer->isActive())
            periodicReceiveTimer->stop();
        delete periodicReceiveTimer;

        //delete monitoring label text
        ui->monitoringLabel->setText("");

        ui->timeSpinBox->setEnabled(true);
    }

}

void diagnosticToolMainView::on_copyButton_clicked()
{
	ui->console->clear();
    // copy input value to output Button

    try{
        std::string blockName = ui->copyComboBox->currentText().toStdString();

        Item *currentItem = dynamic_cast<Item*>(ui->treeWidget->currentItem());

        /*
         * If the selected item is a cluster
         */
        if(!currentItem->whatsThis(0).compare(QString::fromStdString(CLUSTER_TYPE)))
        {
            ModuleMetadata* meta = (ModuleMetadata*)(currentItem->getLinkedObject());
            if(QMessageBox::question(this,"Copy input value to output ",
                                     "You are trying to copy all the input value to the output values.\nThe copy will effect all the register of the block "+ui->copyComboBox->currentText()+" of all the connected PLC.\nDo you confirm this operation?",
                                     QMessageBox::Yes,QMessageBox::No)==QMessageBox::Yes)
            {
                meta->plcHandler->copyInToOut(blockName);
                mysilecs->updateClusterItem(currentItem,false);
            }
        }

        /*
         * If the selected item is a plc
         */
        if(!currentItem->whatsThis(0).compare(QString::fromStdString(PLC_TYPE)))
        {
            Silecs::PLC* plc = (Silecs::PLC*)(currentItem->getLinkedObject());
            if(QMessageBox::question(this,"Copy input value to output ",
                                     "You are trying to copy all the input value to the output values.\nThe copy will effect all the register of the block "+ui->copyComboBox->currentText()+" in the PLC "+QString::fromStdString(plc->getName())+".\nDo you confirm this operation?",
                                     QMessageBox::Yes,QMessageBox::No)==QMessageBox::Yes)
            {
                plc->copyInToOut(blockName);
                mysilecs->updatePLCItem(currentItem,false);
            }
        }

        /*
         * If the selected item is a device
         */
        if(!currentItem->whatsThis(0).compare(QString::fromStdString(DEVICE_TYPE)))
        {
            Silecs::Device* device = (Silecs::Device*)(currentItem->getLinkedObject());
            if(QMessageBox::question(this,"Copy input value to output ",
                                     "You are trying to copy all the input value to the output values.\nThe copy will effect all the register of the block "+ui->copyComboBox->currentText()+" in the device "+QString::fromStdString(device->getLabel())+".\nDo you confirm this operation?",
                                     QMessageBox::Yes,QMessageBox::No)==QMessageBox::Yes)
            {
                device->getBlock(blockName)->copyInToOut();
                mysilecs->updateDeviceItem(currentItem,false);
            }
        }

    }
    catch(...)
    {
        Utils::logError(ui->console,"Unexpected error while trying to copy input value to output value.");
    }
}

void diagnosticToolMainView::on_checkERROR_stateChanged(int arg1)
{
    (void) arg1;
    qDebug()<<"checkERROR changed";
    if(mysilecs != NULL)
    {
        mysilecs->setLogTopics(this->generateLogTopics());
    }
}
void diagnosticToolMainView::on_checkDEBUG_stateChanged(int arg1)
{
    (void) arg1;
    qDebug()<<"checkDEBUG changed";
    if(mysilecs != NULL)
    {
        mysilecs->setLogTopics(this->generateLogTopics());
        mysilecs->enableDebugLog();
    }
}

void diagnosticToolMainView::on_checkSETUP_stateChanged(int arg1)
{
    (void) arg1;
    qDebug()<<"checkSETUP changed";
    if(mysilecs != NULL)
    {
        mysilecs->setLogTopics(this->generateLogTopics());
    }
}

void diagnosticToolMainView::on_checkALLOC_stateChanged(int arg1)
{
    (void) arg1;
    qDebug()<<"checkALLOC changed";
    if(mysilecs != NULL)
    {
        mysilecs->setLogTopics(this->generateLogTopics());
    }
}

void diagnosticToolMainView::on_checkLOCK_stateChanged(int arg1)
{
    (void) arg1;
    qDebug()<<"checkLOCK changed";
    if(mysilecs != NULL)
    {
        mysilecs->setLogTopics(this->generateLogTopics());
    }
}

void diagnosticToolMainView::on_checkCOMM_stateChanged(int arg1)
{
    (void) arg1;
    qDebug()<<"checkCOMM changed";
    if(mysilecs != NULL)
    {
        mysilecs->setLogTopics(this->generateLogTopics());
    }
}

void diagnosticToolMainView::on_checkSEND_stateChanged(int arg1)
{
    (void) arg1;
    qDebug()<<"checkSEND changed";
    if(mysilecs != NULL)
    {
        mysilecs->setLogTopics(this->generateLogTopics());
    }
}

void diagnosticToolMainView::on_checkRECV_stateChanged(int arg1)
{
    (void) arg1;
    qDebug()<<"checkRECV changed";
    if(mysilecs != NULL)
    {
        mysilecs->setLogTopics(this->generateLogTopics());
    }
}

void diagnosticToolMainView::on_checkDATA_stateChanged(int arg1)
{
    (void) arg1;
    qDebug()<<"checkDATA changed";
    if(mysilecs != NULL)
    {
        mysilecs->setLogTopics(this->generateLogTopics());
    }
}

void diagnosticToolMainView::on_checkDIAG_stateChanged(int arg1)
{
    (void) arg1;
    qDebug()<<"checkDIAG changed";
    if(mysilecs != NULL)
    {
        mysilecs->setLogTopics(this->generateLogTopics());
    }
}

void diagnosticToolMainView::on_treeWidget_clicked()
{

}
