/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef SILECS_DIAG_SILECSMODULE_H
#define SILECS_DIAG_SILECSMODULE_H

#include <QtWidgets/QTextBrowser>

#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/equipment/SilecsDesign.h>

#include <silecs-diagnostic/constants.h>
#include <silecs-diagnostic/diagnostictoolmainview.h>
#include <silecs-diagnostic/item.h>
#include <silecs-diagnostic/utils.h>

#include <string>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <algorithm>

using namespace Silecs;

enum LogTopic{ERROR, DEBUG, SETUP, ALLOC, LOCK, COMM, SEND, RECV, DATA, DIAG };

struct ModuleMetadata
{
    Silecs::SilecsDesign* design;
    Silecs::PLCHandler* plcHandler;
};

class silecsModule
{
private:
    Silecs::Service* silecsService;
    ModuleMetadata metadata;

    /**
      * Add an element to the tree
      */
    QTreeWidgetItem* addTreeItem(QTreeWidgetItem *parent,QString name ,QString description);

public:

    /**
      * Constructor. Get the SILECS service from the library
      */
    silecsModule(const std::string& logParam, QTextBrowser *messageConsole);

    /**
      * setLogTopics from the service
      */
    void setLogTopics(const std::string& logParam);

    bool isDebugLogEnabled();
    void enableDebugLog();

    /**
      * Denstructor. Remove the SILECS service from the library
      */
    ~silecsModule();

    void updatePLCRunState(Item *plcItem);

    /**
      *Generate the empty entire tree
      */
    Item *generateTree(const std::string& className, const std::string& deployFile);

    /**
      * Set the device with the proper scalar data converting
      * from the string present in the GUI
      * in case of error returns a string exception with the message
      */
    void setScalarDataInDeviceFromItem(Item *currentItem, const std::string& blockName);

    /**
      * Set the register with the proper data converting
      * from the string present in the GUI
      * in case of error returns a string exception with the message
      */
    void setArrayRegValue(Item *currentItem, const std::vector<QString>& dataVector);

    /**
     * Updates the input and output values of all the register
     * for the given device Item without any other impact on the GUI
     */
    void updateDeviceItem(Item *deviceItem,bool updateInputBufferOnly);

    /**
     * Updates the input and output values of all the register
     * for the given PLC Item without any other impact on the GUI
     */
    void updatePLCItem(Item *PLCItem,bool updateInputBufferOnly);

    /**
     * Updates the input and output values of all the register
     * for the given cluster Item without any other impact on the GUI
     */
    void updateClusterItem(Item *Cluster,bool updateInputBufferOnly);

    /**
      * Count the number of PLC connected
      */
    int counterConnectedPLC;

    QTextBrowser *messageConsole_;

private:

    bool debugLoggingEnabled_;

};

#endif // SILECS_DIAG_SILECSMODULE_H
