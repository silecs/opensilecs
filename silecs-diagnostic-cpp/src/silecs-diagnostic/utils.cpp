/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/

#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QScrollBar>

#include <silecs-diagnostic/utils.h>
#include <silecs-diagnostic/silecsmodule.h>
#include <iomanip>

#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>


extern silecsModule *mysilecs;

void Utils::logError(QTextBrowser* errorConsole, const std::string& message)
{
    errorConsole->insertHtml(QString::fromStdString("<font color='red'>DIAG-TOOL [ERROR] " + message + "</font><br>"));
    errorConsole->verticalScrollBar()->setValue(errorConsole->verticalScrollBar()->maximum());
}

void Utils::logInfo(QTextBrowser* errorConsole, const std::string& message)
{
    errorConsole->insertHtml(QString::fromStdString("<font color='black'>DIAG-TOOL [INFO] " + message + "</font><br>"));
    errorConsole->verticalScrollBar()->setValue(errorConsole->verticalScrollBar()->maximum());
}

void Utils::logDebugIf(QTextBrowser* errorConsole, const std::string& message)
{
    if (mysilecs->isDebugLogEnabled())
    {
        errorConsole->insertHtml(QString::fromStdString("<font color='black'>DIAG-TOOL [DEBUG] " + message + "</font><br>"));
        errorConsole->verticalScrollBar()->setValue(errorConsole->verticalScrollBar()->maximum());
    }
}

//convert int to string
std::string Utils::toString(int number)
{
    char tmp[32];
    sprintf(tmp, "%d", number);
    return tmp;
}

//convert unsigned int to string
std::string Utils::toString(unsigned int number)
{
    char tmp[32];
    sprintf(tmp, "%ud", number);
    return tmp;
}

//convert long to string
std::string Utils::toString(long number)
{
    char tmp[32];
    sprintf(tmp, "%ld", number);
    return tmp;
}

//convert unsigned long to string
std::string Utils::toString(unsigned long number)
{
    char tmp[32];
    sprintf(tmp, "%lud", number);
    return tmp;
}

//convert long to a given base
std::string Utils::toString(long number, int base)
{
    std::stringstream stream;
    stream << std::setbase(base) << number;
    return stream.str();
}

Item* Utils::addTreeItem(Item *parent, const QString& name, const QString& description, const QString& type, void* linkedObject, const QString& icon)
{
    Item *itm = new Item(linkedObject);
    itm->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
    itm->setText(0, name);
    itm->setText(2, description);
    itm->setWhatsThis(0, type);

    if (icon.compare("") != 0)
        itm->setIcon(0, QIcon(QPixmap(icon)));

    parent->addChild(itm);
    return itm;
}

char* Utils::chop(char *string)
{
    size_t i, len;
    len = strlen(string);
    char *newstring;
    newstring = (char *)malloc(len);
    for (i = 0; i < strlen(string) - 1; i++)
    {
        newstring[i] = string[i];
    }
    newstring[len] = '\0';

    return newstring;
}

void Utils::displayDesignInformation(const std::string& className, QTextEdit *console)
{
    std::string text = "";

    text.append("<h3>Cluster general information</h3>");
    text.append("<ul>");
    text.append("<li>Class name:    " + className + "</li>");
    text.append("</ul>");

    console->setText(QString::fromStdString(text));

}

void Utils::displayPLCInformation(Silecs::PLC *plc, QTextEdit *console)
{
    std::string text = "";

    auto& paramConfig = plc->getParamConfig();

    if (plc->isConnected(false))
    {
        text.append("<h3><font color ='green'>This PLC is currently connected</font></h3><hr/>");

        if (paramConfig.typeID == BusCoupler)
        {
            text.append("<h3><font color ='green'>PLC Runtime information</font></h3>");
            text.append("<ul>");
            text.append("<li>No runtime information available: Bus controller (no memory)</li>");
            text.append("</ul><hr/>");

        }
        else
        {
            auto& device = plc->getDeploy().getHeader();
            text.append("<h3><font color ='green'>PLC Runtime information</font></h3>");
            text.append("<ul>");
            text.append("<li>PLC HEADER DATA uploaded from: " + plc->getName() + "</li>");
            text.append("<li>Software Release: " + device.getRegister(HEADER_RELEASE_REG)->getVal<std::string>() + "</li>");
            text.append("<li>Mapping Owner:    " + device.getRegister(HEADER_OWNER_REG)->getVal<std::string>() + "</li>");
            text.append("<li>Mapping Checksum: " + device.getRegister(HEADER_CHECKSUM_REG)->getInputValAsString() + "</li>");
            text.append("<li>Generation Date:  " + device.getRegister(HEADER_DATE_REG)->getInputValAsString() + "</li>");
            text.append("</ul><hr/>");
        }
    }
    else
        text.append("<h3>This PLC is currently disconnected</h3><hr/>");

    text.append("<h3>PLC general information</h3>");
    text.append("<ul>");
    text.append("<li>Name:    " + plc->getName() + "</li>");
    text.append("<li>Address: " + plc->getIPAddress() + "</li>");
    text.append("<li>Brand:   " + paramConfig.brand + "</li>");
    text.append("<li>Model:   " + paramConfig.model + "</li>");
    text.append("<li>Owner:   " + paramConfig.localOwner + "</li>");
    text.append("</ul><hr/>");

    text.append("<h3>PLC configuration information</h3>");
    text.append("<ul>");
    text.append("<li>Protocol type: " + paramConfig.protocolType + "</li>");
    text.append("<li>Protocol mode: " + paramConfig.protocolMode + "</li>");

    if (paramConfig.baseMemAddr > -1)
        text.append("<li>Memory Base address:  " + toString(paramConfig.baseMemAddr) + " (0x" + toString(paramConfig.baseMemAddr, 16) + ")" + "</li>");
    else
        text.append("<li>Memory Base address:  Not applicable</li>");

    if (paramConfig.baseDIAddr > -1)
        text.append("<li>DI Base address:  " + toString(paramConfig.baseDIAddr) + " (0x" + toString(paramConfig.baseDIAddr, 16) + ")" + "</li>");
    else
        text.append("<li>DI Base address:  Not applicable</li>");

    if (paramConfig.baseDOAddr > -1)
        text.append("<li>DO Base address:  " + toString(paramConfig.baseDOAddr) + " (0x" + toString(paramConfig.baseDOAddr, 16) + ")" + "</li>");
    else
        text.append("<li>DO Base address:  Not applicable</li>");

    if (paramConfig.baseAIAddr > -1)
        text.append("<li>AI Base address:  " + toString(paramConfig.baseAIAddr) + " (0x" + toString(paramConfig.baseAIAddr, 16) + ")" + "</li>");
    else
        text.append("<li>AI Base address:  Not applicable</li>");

    if (paramConfig.baseAOAddr > -1)
        text.append("<li>AO Base address:  " + toString(paramConfig.baseAOAddr) + " (0x" + toString(paramConfig.baseAOAddr, 16) + ")" + "</li>");
    else
        text.append("<li>AO Base address:  Not applicable</li>");

    text.append("</ul><hr/>");

    text.append("<h3>PLC generation information</h3>");
    text.append("<ul>");
    text.append("<li>Release:  " + paramConfig.localRelease + "</li>");
    text.append("<li>Date:     " + paramConfig.localDate + "</li>");
    text.append("<li>Checksum: " + toString(paramConfig.localChecksum) + "</li>");
    text.append("</ul><hr/>");

    console->setText(QString::fromStdString(text));
}

void Utils::displayDeviceInformation(Silecs::Device *device, QTextEdit *console)
{
    std::string text = "";

    text.append("<h3>Device general information</h3>");
    text.append("<ul>");
    text.append("<li>Name:    " + device->getLabel() + "</li>");
    text.append("</ul>");

    console->setText(QString::fromStdString(text));
}

void Utils::displayRegisterInformation(Silecs::Register *reg, QTextEdit *console)
{
    std::string text = "";

    text.append("<h3>Register general information</h3>");
    text.append("<ul>");
    text.append("<li>Name:        " + reg->getName() + "</li>");
    text.append("<li>Format:      " + reg->getFormatAsString() + "</li>");
    if (reg->getFormat() == String)
        text.append("<li>String length: " + toString(reg->getLength()) + "</li>");
    text.append("<li>Dimension1:   " + toString(reg->getDimension1()) + "</li>");
    text.append("<li>Dimension2:  " + toString(reg->getDimension2()) + "</li>");
    text.append("<li>Block name:  " + reg->getBlockName() + "</li>");
    text.append("<li>Last update: " + reg->getTimeStampAsString() + "</li>");

    // Access method
    switch(reg->getAccessType())
    {
    case AccessType::Setting:
        text.append("<li>Access method: Read / Write </li>");
        break;
    case AccessType::Command:
        text.append("<li>Access method: Write only </li>");    
        break;
    case AccessType::Acquisition:
        text.append("<li>Access method: Read only </li>");
        break;
    default:
        throw std::logic_error{"Unknown AccessType."};
    }

    text.append("<li>Access area: " + reg->whichAccessArea(reg->getAccessArea()) + "</li>");
    text.append("</ul>");

    console->setText(QString::fromStdString(text));
}

void Utils::displayRegisterValue(Silecs::Register *reg, QLabel *binValueLabel, QLabel *hexValueLabel, QLabel *decValueLabel, QLabel *asciiValueLabel)
{

    if (!reg->isReadable())
    {
        binValueLabel->setText("--Write only register--");
        hexValueLabel->setText("--Write only register--");
        decValueLabel->setText("--Write only register--");
        asciiValueLabel->setText("--Write only register--");
    }
    else
    {
        // The register has input access
        if (reg->getDimension1() > 1 || reg->getDimension2() > 1)
        {
            binValueLabel->setText("--Vector--");
            hexValueLabel->setText("--Vector--");
            decValueLabel->setText("--Vector--");
            asciiValueLabel->setText("--Vector--");
        }
        else
        {
            //Scalar with input access
            switch (reg->getFormat())
            {
                case uInt8:
                {

                    binValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::uint8_t>(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::uint8_t>(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::uint8_t>(), 0, 10));
                    //QString temp;
                    //asciiValueLabel->setText(temp.sprintf("%c",reg->getVal<uint8_t>()));
                    QChar c = reg->getVal<std::uint8_t>();
                    //if(!c.isPrint())
                    //    c='?';
                    //asciiValueLabel->setText(QObject::tr("%1").arg(c));
                    asciiValueLabel->setText(c.isPrint() ? QObject::tr("%1").arg(c) : "--Not printable--");

                    break;
                }
                case Int8:
                {
                    binValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::int8_t>(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::int8_t>(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::int8_t>(), 0, 10));
                    QChar c = reg->getVal<std::int8_t>();
                    asciiValueLabel->setText(c.isPrint() ? QObject::tr("%1").arg(c) : "--Not printable--");
                    break;
                }
                case uInt16:
                {
                    binValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::uint16_t>(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::uint16_t>(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::uint16_t>(), 0, 10));
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }

                case Int16:
                {
                    binValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::int16_t>(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::int16_t>(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::int16_t>(), 0, 10));
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }

                case uInt32:
                {
                    binValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::uint32_t>(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::uint32_t>(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::uint32_t>(), 0, 10));
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }

                case Int32:
                {
                    binValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::int32_t>(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::int32_t>(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::int32_t>(), 0, 10));
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }

                case uInt64:
                {
                    binValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::uint64_t>(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::uint64_t>(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::uint64_t>(), 0, 10));
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }

                case Int64:
                {
                    binValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::int64_t>(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::int64_t>(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getVal<std::int64_t>(), 0, 10));
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }

                case String:
                {
                    binValueLabel->setText("--Not relevant--");
                    hexValueLabel->setText("--Not relevant--");
                    decValueLabel->setText("--Not relevant--");
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }

                case Bool:
                {
                    bool value = reg->getVal<bool>();
                    std::string textValue;
                    std::string numericValue;

                    if (value == true)
                    {
                        numericValue = "1";
                        textValue = "TRUE";

                    }
                    else
                    {
                        numericValue = "0";
                        textValue = "FALSE";
                    }

                    binValueLabel->setText(QString::fromStdString(numericValue));
                    hexValueLabel->setText(QString::fromStdString(numericValue));
                    decValueLabel->setText(QString::fromStdString(numericValue));
                    asciiValueLabel->setText(QString::fromStdString(textValue));
                    break;
                }


                default: // Float32,Float64,Date
                {
                    binValueLabel->setText("--Not relevant--");
                    hexValueLabel->setText("--Not relevant--");
                    decValueLabel->setText("--Not relevant--");
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }
            }
        }
    }
}
