/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-diagnostic/logindialog.h>
#include <resources/gui/ui_logindialog.h>


LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog)
{
    ui->setupUi(this);
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

//set and get username
//void LoginDialog::setUsername(std::string username)
//{
//    this->username = username;
//}

std::string LoginDialog::getUsername() const
{
    return this->username;
}

//set and get password
//void LoginDialog::setPassword(std::string password)
//{
//    this->password = password;
//}

std::string LoginDialog::getPassword() const
{
    return this->password;
}

void LoginDialog::on_buttonBox_accepted()
{
    this->username = ui->usernameLineEdit->text().toStdString();
    this->password = ui->passwordLineEdit->text().toStdString();
}

