/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef SILECS_DIAG_STDERRREDIRECT_H
#define SILECS_DIAG_STDERRREDIRECT_H

#include <QtCore/QObject>
#include <QtCore/QFile>
#include <QtCore/QFileSystemWatcher>
#include <QtWidgets/QTextEdit>

class StdErrRedirect : public QObject
{
    Q_OBJECT

public:
    // Constructor
    StdErrRedirect();
    StdErrRedirect(QTextEdit *errorLog, QObject   *parent = NULL);

    // Destructor
    ~StdErrRedirect();

private slots:
    void fileChanged(const QString &filename);

private:
    QFile              tmp;
    QFileSystemWatcher watcher;
    QString            tmpFileNameQtFormat;
    QString            tmpFileNameNativeFormat;

    QTextEdit *m_errorLog;
    QString   oldContent;
};

#endif // SILECS_DIAG_STDERRREDIRECT_H
