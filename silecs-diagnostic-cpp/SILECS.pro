#-------------------------------------------------
#
# Project created by QtCreator 2011-09-27T14:25:35
#
#-------------------------------------------------

QT       += core gui
TEMPLATE = app
SOURCES += src/silecs-diagnostic/*.cpp
HEADERS += src/silecs-diagnostic/*.h
FORMS   += src/silecs-diagnostic/resources/gui/diagnostictoolmainview.ui \
           src/silecs-diagnostic/resources/gui/displayarraydialog.ui \
           src/silecs-diagnostic/resources/gui/logindialog.ui

INCLUDEPATH += src/         								# Source directory to be included
RESOURCES   += src/silecs-diagnostic/resources/images.qrc   # Resource file
DESTDIR     =  bin          								# Target file directory
OBJECTS_DIR =  build        								# Intermediate object files directory
MOC_DIR     =  build        								# Intermediate moc files directory

UI_DIR      =  build        								# Store ui generated source file with other headers
INCLUDEPATH += build/       								# Build includes the header for the gui

# Disable debugger
DEFINES     += QT_NO_DEBUG_OUTPUT QT_NO_WARNING_OUTPUT

# Enable console
CONFIG += console

# Define RBAC support
#QMAKE_CXXFLAGS += -DWITH_RBAC

#Settings
CPU = L866
SILECS_VERSION = 1.m.p
CONFIG += SILECSLOCAL


#======================================================
#------------------TARGET------------------------------
#======================================================
equals( CPU, L865 ){
    TARGET = silecs-diagnostic-32
} else {
    TARGET = silecs-diagnostic-64
}

#======================================================
#------------------LIBRARIES---------------------------
#======================================================
#RBAC Library
#BOOST_VERSION = 1.54.0
#RBAC_VERSION = 6.1.0
#RBAC_HOME = /acc/local/$${CPU}/cmw/cmw-rbac/$${RBAC_VERSION}
#BOOST_HOME = /acc/local/$${CPU}/3rdparty/boost/$${BOOST_VERSION}

#DEPENDENT_COMPILER_OPTIONS = $${RBAC_HOME}/include \
#        $${BOOST_HOME}/include

#DEPENDENT_LINKER_OPTIONS = $${RBAC_HOME}/lib/libcmw-rbac.a \
#        $${RBAC_HOME}/lib/libcmw-serializer.a \
#        $${RBAC_HOME}/lib/libcmw-log.a \
#        $${RBAC_HOME}/lib/libcmw-util.a \
#        $${BOOST_HOME}/lib/libboost_1_54_0_thread.a \
#        $${BOOST_HOME}/lib/libboost_1_54_0_system.a \
#        $${BOOST_HOME}/lib/libboost_1_54_0_filesystem.a

LIBS            += -L/usr/lib64 $${DEPENDENT_LINKER_OPTIONS} -lcurl
INCLUDEPATH     += $${DEPENDENT_COMPILER_OPTIONS}
DEPENDPATH      += $${DEPENDENT_LINKER_OPTIONS}

#-------SILECS Operational Library-------
SILECSLOCAL{
    SILECS_HOME = ../silecs-communication-cpp/build
    message("linking with local silecs library")
} else {
    SILECS_HOME = /acc/local/$${CPU}/silecs/$${SILECS_VERSION}/library/
    message("linking with operational silecs library")
}

SILECS_LIBS      = $${SILECS_HOME}/lib/$${CPU} -lsilecs-comm

LIBS            += -L$${SILECS_LIBS}
INCLUDEPATH     += $${SILECS_HOME}/include/
DEPENDPATH      += $${SILECS_HOME}/include/

#-------Lib XML 2-------
XML2_HOME       = /usr/lib/
XML2_LIBS       = $${XML2_HOME} -lxml2
XML2_INCLUDES   = /usr/include/libxml2
LIBS            += -L$${XML2_LIBS}
INCLUDEPATH     += $${XML2_INCLUDES}
DEPENDPATH      += $${XML2_INCLUDES}

#-------CNV-------
#CNV_HOME        = /acc/sys/$${CPU}/usr/local/natinst/ninetv
CNV_PATH		= /home/sd/rhaseitl/lnx/workspace.luna/CNV/ninetv
CNV_INC         = $${CNV_HOME}/include
equals( CPU, L865 ){
    CNV_LIBS    = $${CNV_HOME}/lib -lstdc++ -lninetv -llkdynam -llkrealt -llksec -llksock
} else {
    CNV_LIBS    = $${CNV_HOME}/lib64 -lstdc++ -lninetv -llkdynam -llkrealt -llksec -llksock -llkbrow
}
LIBS            += -L$${CNV_LIBS}
INCLUDEPATH     += $${CNV_INC}
DEPENDPATH      += $${CNV_INC}
