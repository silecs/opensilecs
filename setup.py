import os
from setuptools import setup, find_packages

__version__ = "0.1.0"
__description__ = "Script in order to model PLC devices in FESA by using the Silecs framework"

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name="opensilecs",
    version=__version__,
    description=__description__,
    url="https://git.gsi.de/silecs/opensilecs",
    author="GSI",
    license="GPLv3",
    packages=find_packages(),
    install_requires=required
)